﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace YulgangBotFinal.EventCore.Test
{
    [TestClass]
    public class PathFinder
    {
        [TestMethod]
        public void ViewPathFinder()
        {
            using (var read=File.OpenRead("D:\\MySpace\\BotFinal\\PathPoint_101.pth"))
            {
                var len = read.Seek(0, SeekOrigin.End);
                read.Seek(0, SeekOrigin.Begin);
                var data = new byte[len];
                read.Read(data, 0, (int) len);
                for (int i = 0; i < data.Length; i++)
                {
                    data[i] ^= 0x86;
                }
                var decrypt = new string(data.Select(Convert.ToChar).ToArray());
               // Debug.Write(decrypt);
                foreach (var s in decrypt.Split('#'))
                {
                    Debug.WriteLine(s);
                }
            }
        }

        void Step0(string ck=null)
        {
            var web =
                HttpWebRequest.Create(
                    "http://www.mgame.com/ulnauth/login/yulgang_main_login_v3.mgame");
            web.Proxy = new WebProxy("211.110.165.238:3128");
            web.Method = "GET";
            web.Headers.Add("Cookie", "fxBannerDefault_2017003201100_01=V; _ga=GA1.2.1063302123.1490315814");

            //using (var s = web.GetRequestStream())
            //{
            //    using (TextWriter w = new StreamWriter(s))
            //    {
            //        w.WriteLine(
            //            $"mgamelogindata1={user}" +
            //            $"&mgamelogindata2={pass}&lt=4&tu=&ru=&x=47&y=35");

            //    }
            //}
            using (var r = web.GetResponse())
            {
                using (var s = r.GetResponseStream())
                {
                    using (TextReader re = new StreamReader(s))
                    {
                        Debug.WriteLine(r.Headers.ToString());
                        //Debug.WriteLine(re.ReadToEnd());
                    }
                }
            }
            Debug.WriteLine("------------0-------------");
        }

        void Step1(string user, string pass)
        {
            HttpWebRequest web =(HttpWebRequest)
                HttpWebRequest.Create(
                    "https://sign.mgame.com/login/login_action_pub_type_b.mgame?tu=http://yulgang.mgame.com");
            web.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            web.Proxy=new WebProxy("211.110.165.238:3128");
            web.Method = "POST";
            web.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            web.Host = "sign.mgame.com";
            web.Referer = "http://www.mgame.com/ulnauth/login/yulgang_main_login_v3.mgame";
            
            web.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            web.Headers.Add("Accept-Language", "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2");
            web.Headers.Add("Cookie", "MCV=0; ECF04KO=0; _ga=GA1.2.1063302123.1490315814");
            web.Headers.Add("Origin", "http://www.mgame.com");
            web.Headers.Add("Upgrade-Insecure-Requests", "1");
            web.UserAgent= "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";
           
            web.ContentType = "application/x-www-form-urlencoded";

            using (var s = web.GetRequestStream())
            {
                using (TextWriter w = new StreamWriter(s))
                {
                    var tmp = $"mgamelogindata1={user}" +
                        $"&mgamelogindata2={pass}&lt=4&tu=&ru=&x=47&y=35";
                    w.WriteLine(
                        tmp
                       );
                }
            }
            using (var r = web.GetResponse())
            {
                using (var s = r.GetResponseStream())
                {
                    using (TextReader re = new StreamReader(s))
                    {
                       // Debug.WriteLine(r.Headers.ToString());
                       // Debug.WriteLine(re.ReadToEnd());
                    }
                }
            }
            Debug.WriteLine("------------1-------------");
        }

        string Step2(string user, string pass)
        {
            var web =(HttpWebRequest)
                HttpWebRequest.Create(
                    "https://sign.mgame.com/login/login_action.mgame");
            web.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            web.Proxy = new WebProxy("211.110.165.238:3128");
            web.Method = "POST";
            web.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            web.Host = "sign.mgame.com";
            web.Referer = "https://sign.mgame.com/login/login_action_pub_type_b.mgame?tu=http://yulgang.mgame.com";

            web.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            web.Headers.Add("Accept-Language", "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2");
            web.Headers.Add("Cookie", "MCV=0; ECF04KO=0; _ga=GA1.2.1063302123.1490315814");
            web.Headers.Add("Origin", "https://sign.mgame.com");
            web.Headers.Add("Upgrade-Insecure-Requests", "1");
            web.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";

            web.ContentType = "application/x-www-form-urlencoded";


            using (var s = web.GetRequestStream())
            {
                using (TextWriter w = new StreamWriter(s))
                {
                    w.WriteLine(
                        $"mgamelogindata1={user}&mgamelogindata2={pass}&lt=4&tu=&ru=&x=47&y=35&tu=http%3A%2F%2Fyulgang.mgame.com&loginparamgood=43f64974f7eb40ab71558daed8f5a88108d8345bf76511d479a2285d10bf166a&mac_addr=&pc_name=");

                }
            }
            var result = "";
            using (var r = web.GetResponse())
            {
                using (var s = r.GetResponseStream())
                {
                    using (TextReader re = new StreamReader(s))
                    {
                        result = r.Headers["Set-Cookie"];
                       // Debug.WriteLine(r.Headers.ToString());
                       // Debug.WriteLine(re.ReadToEnd());
                    }
                }
            }
            Debug.WriteLine("-----------2--------------");
            return result;
        }
        void Step3(string ck)
        {
            var web = (HttpWebRequest)
                HttpWebRequest.Create(
                    "https://sign.mgame.com/login/login_action_result.mgame?lt=4&rp=&tu=http%3A%2F%2Fyulgang.mgame.com");
            web.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            web.Proxy = new WebProxy("211.110.165.238:3128");
            web.Method = "GET";
            web.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            web.Host = "sign.mgame.com";
            web.Referer = "https://sign.mgame.com/login/login_action.mgame";

            web.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            web.Headers.Add("Accept-Language", "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2");
            web.Headers.Add("Cookie", "_ga=GA1.2.1063302123.1490315814;"+ck);
            web.Headers.Add("Origin", "https://sign.mgame.com");
            web.Headers.Add("Upgrade-Insecure-Requests", "1");
            web.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";

           
            using (var r = web.GetResponse())
            {
                using (var s = r.GetResponseStream())
                {
                    using (TextReader re = new StreamReader(s))
                    {
                       // Debug.WriteLine(r.Headers.ToString());
                        //Debug.WriteLine(re.ReadToEnd());
                    }
                }
            }
            Debug.WriteLine("------------3-------------");
        }

        void Step4(string ck)
        {
            var web = (HttpWebRequest)
               HttpWebRequest.Create(
                   "http://www.mgame.com/ulnauth/login/yulgang_main_login_v3.mgame?returl=http://yulgang.mgame.com/");
            web.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            web.Proxy = new WebProxy("211.110.165.238:3128");
            web.Method = "GET";
            web.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            web.Host = "www.mgame.com";
            web.Referer = "http://yulgang.mgame.com/";

            web.Headers.Add("Accept-Encoding", "gzip, deflate, sdch");
            web.Headers.Add("Accept-Language", "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2");
            web.Headers.Add("Cookie", "_ga=GA1.2.1063302123.1490315814;" + ck);
            //web.Headers.Add("Origin", "https://sign.mgame.com");
            web.Headers.Add("Upgrade-Insecure-Requests", "1");
            web.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";


            using (var r = web.GetResponse())
            {
                using (var s = r.GetResponseStream())
                {
                    using (TextReader re = new StreamReader(s, Encoding.UTF8))
                    {
                        // Debug.WriteLine(r.Headers.ToString());
                        var tmp = re.ReadToEnd();
                        //Debug.WriteLine(tmp);
                    }
                }
            }
            Debug.WriteLine("------------4-------------");
        }
        void Step5(string ck)
        {
            var web = (HttpWebRequest)
                HttpWebRequest.Create(
                    "http://gstart.mgame.com/launch/launch_yulgang.mgame?goUrl=yulgang");
            web.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            web.Proxy = new WebProxy("211.110.165.238:3128");
            web.Method = "GET";
            web.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            web.Host = "gstart.mgame.com";
            web.Referer = "http://yulgang.mgame.com/";

            web.Headers.Add("Accept-Encoding", "gzip, deflate, sdch");
            web.Headers.Add("Accept-Language", "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2");
            web.Headers.Add("Cookie", ck + ";_ga=GA1.2.1063302123.1490315814");
            //web.Headers.Add("Origin", "https://sign.mgame.com");
            web.Headers.Add("Upgrade-Insecure-Requests", "1");
            web.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";


            using (var r = web.GetResponse())
            {
                using (var s = r.GetResponseStream())
                {
                    using (TextReader re = new StreamReader(s,Encoding.UTF8))
                    {
                        // Debug.WriteLine(r.Headers.ToString());
                        var tmp =
                            re.ReadToEnd().Split('\n').Where(m => m.Contains("INET")).FirstOrDefault()?.Split(',')[9]
                                .Split(')')[0].Split(' ');

                        Debug.WriteLine($"username : {tmp[3]}");
                        Debug.WriteLine($"pass : {tmp[4]}");

                    }
                }
            }
            Debug.WriteLine("-----------5--------------");
        }

        [TestMethod]
        public void test1()
        {

            var engine = new Jurassic.ScriptEngine();
            engine.ExecuteFile(@"D:\js.js");
            //var user = HttpUtility.UrlEncode(engine.CallGlobalFunction("getEncrypt", "21312").ToString());
            //var pass = HttpUtility.UrlEncode(engine.CallGlobalFunction("getEncrypt", "@1231231").ToString());
            var user = HttpUtility.UrlEncode(engine.CallGlobalFunction("getEncrypt", "liemhd").ToString());
            var pass = HttpUtility.UrlEncode(engine.CallGlobalFunction("getEncrypt", "@lananh123").ToString());

            Step0();
            Step1(user,pass);
            var ck=Step2(user, pass);
            var sp = ck.Split(',').ToList();
            sp.RemoveAt(7);
            sp.RemoveAt(7);
            sp.RemoveAt(1);
            sp.RemoveAt(1);
            sp.RemoveAt(1);
            sp.RemoveAt(1);
            var valid = "";
            foreach (var s in sp)
            {
                valid += s.Split(';')[0] + "; ";
            }
            Debug.WriteLine(valid);
            Step3(valid);
            Step4(valid);
            Step5(valid);

        }

        [TestMethod]
        public void Packet1()
        {
            var username = "liemhd";
            using (var st = File.OpenRead("D:\\GAME\\YULGANG\\MGAME\\datas\\Texture\\dhme_esea.d2s"))
            {
                using (var bRead = new BinaryReader(st))
                {
                    var uk1 = bRead.ReadInt32();
                    var data = bRead.ReadBytes(32);
                    for (int i = 0; i < data.Length; i++)
                    {
                        data[i] ^= 0x5B;
                    }

                    // lấy user bỏ vào 1 mảng 80byte
                    var userData = new byte[80];
                    var userbyte = Encoding.ASCII.GetBytes(username.ToUpper());
                    Buffer.BlockCopy(userbyte, 0, userData, 0, userbyte.Length);
                    var index = 0;
                    var result = 0;
                    do
                    {
                        var v3 = userData[index];
                        if (v3 == 0)
                            break;
                        result += v3;
                        var v4 = userData[index + 1];
                        if (v4 == 0)
                            break;
                        result += v4;
                        var v5 = userData[index + 2];
                        if (v5 == 0)
                            break;
                        result += v5;
                        var v6 = userData[index + 3];
                        if (v6 == 0)
                            break;
                        result += v6;
                        var v7 = userData[index + 4];
                        if (v7 == 0)
                            break;
                        index += 5;
                        result += v7;
                    }
                    while (index < 80);
                    for (int i = 0; i < data.Length; i++)
                    {
                        data[i] ^= (byte)result;
                    }
                    foreach (var b in data)
                    {
                        Debug.Write($"{b:X} ");
                    }
                }
            }
        }

       
    }
}
