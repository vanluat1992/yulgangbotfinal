﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.EventCore.Test
// FileName : PacketTest.cs
// Time Create : 2:43 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion

using System;
using System.CodeDom;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YulgangBotFinal.Network.Packet.Auth.Send;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.EventCore.Test
{
    [TestClass]
    public class PacketTest
    {
        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
        [TestMethod]
        public void TestOpcode()
        {
            var data =
                StringToByteArray(
                    "aa556c00095cee423c7c8a427926de720d4cde720f45de7a3d7cee423d7cee423d7cee423d7cee423d7cee70094cee423d7cee423d7cee423d7cee423d7cee423d7cee423d7cee423d7cee42cd7cec420c45dc6c0c4ad66c0c52d7423d7cee420b4caf76093fdc07054dac7a3d7cee4255aa");
            var stream = new MemoryStream(data);
            var bread=new BinaryReader(stream);
            bread.ReadInt16();
            var len = bread.ReadInt16();
            var key = bread.ReadUInt16();
            var pdata = bread.ReadBytes(len - 2);
            bread.ReadInt16();
            var keyEcrpt = DzoEncrypt.CreateKey(key);
            DzoEncrypt.Descrypt(pdata, keyEcrpt);
            Console.WriteLine(BitConverter.ToString(pdata));
        }

        [TestMethod]
        public void Test()
        {
            var data =
                StringToByteArray(
                    "aa5508001246e34bacafe34b55aa");
            var stream = new MemoryStream(data);
            var bread = new BinaryReader(stream);
            while (bread.BaseStream.Position<data.Length)
            {
                bread.ReadInt16();
                var len = bread.ReadInt16();
                var key = bread.ReadUInt16();
                var pdata = bread.ReadBytes(len - 2);
                bread.ReadInt16();
                var keyEcrpt = DzoEncrypt.CreateKey(key);
                DzoEncrypt.Descrypt(pdata, keyEcrpt);
                Console.WriteLine(BitConverter.ToString(pdata).Replace("-", ""));

            }

        }

        [TestMethod]
        public void Test240Opcode()
        {
            var data =
                StringToByteArray(
                    "AA550311060040020F010B012C0F0D6F008B100C00B90100006F00000020062007028D00142005030000716220042000401D801903640088018009050000616263374000200AE013000206000A403160030D0166261D43000070419AB9DF4465E012350201350C601D0341F6A7356008E01A0000102062E0035FC03200FFC008E002000064E0020BE0070000FFA000204E6000A00E01FF02600D0000E0000F200AE01400000120010004E01421E031004093006F20C5E1928D20DCE1878DE04F00E1FF8DE3251BE04F00818D037600500AE00961C3D3E04700E0955707302B902CAA043501C54DE03FAD063D0C2683AC06F5E500ADE03F57E00900E04F59E0FF57E1FF07E1FF07E1FF07E1FF07E1FF07E1FF07E1EB0701000055AA");

            
            DecryptPacket.ExtractPacket(data, m =>
            {
                Console.WriteLine(m.Opcode.ToString("X4"));
            });
        }

        [TestMethod]
        public void TestB0()
        {
            var t = 0x02CAC6D9;
            var result = t & 0xF000000;
            var _time = (int)(((result | ((0xF00000 & t | (t >> 4) & 0xF000000) >> 8)) >>
                            12) | ((t & 0xFF0F | 16 * (t & 0xFFFF0000 | ((t & 0xF0) << 8))) << 8));
            Console.WriteLine(_time.ToString("X4"));

        }
    }

}