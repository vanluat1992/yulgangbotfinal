﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace YulgangBotFinal.EventCore.Test
{
    [TestClass]
    public class KoreaLoginTest
    {
        [TestMethod]
        public void Encrypt()
        {
            //Rfc2898DeriveBytes rfcdb = new System.Security.Cryptography.Rfc2898DeriveBytes("73ko82475f8f83fn6q95b46j471a0287", Encoding.UTF8.GetBytes(salt), 1);

            Encoding byteEncoder = Encoding.UTF8;
            byte[] rijnKey = byteEncoder.GetBytes("73ko82475f8f83fn6q95b46j471a0287");
            byte[] keyBytes = new byte[16];
            Array.Copy(rijnKey, keyBytes, Math.Min(keyBytes.Length,
    rijnKey.Length));

            byte[] keyIv = new byte[16];
            byte[] rijnIV = byteEncoder.GetBytes("5b9n439135k0203jks62pqe479005s2");

            Array.Copy(rijnIV, keyIv, Math.Min(keyBytes.Length,
    rijnKey.Length));
            String message = HttpUtility.UrlEncode("123123123123");
            String encryption = EncryptIt(message, rijnKey, rijnIV);
            //String decryption = DecryptIt(encryption, rijnKey, rijnIV);
            Console.WriteLine("Message: {0}", message);
            Console.WriteLine("Encryption: {0}", encryption);
            //Console.WriteLine("Decryption: {0}", decryption);
        }

        private static String EncryptIt(String s, byte[] key, byte[] IV)
        {
            String result;
            var rijn = new RijndaelManaged();
            rijn.Mode = CipherMode.CBC;
            rijn.Padding = PaddingMode.PKCS7;
            rijn.BlockSize = IV.Length*8;
            rijn.KeySize = key.Length * 8;
            rijn.FeedbackSize = 128;
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (ICryptoTransform encryptor = rijn.CreateEncryptor(key, IV))
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(s);
                        }
                    }
                }
                result = Convert.ToBase64String(msEncrypt.ToArray());
            }
            rijn.Clear();
            return result;
        }
        private static String DecryptIt(String s, byte[] key, byte[] IV)
        {
            String result;
            RijndaelManaged rijn = new RijndaelManaged();
            rijn.Mode = CipherMode.ECB;
            rijn.Padding = PaddingMode.Zeros;
            using (MemoryStream msDecrypt = new MemoryStream(Convert.FromBase64String(s)))
            {
                using (ICryptoTransform decryptor = rijn.CreateDecryptor(key, IV))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader swDecrypt = new StreamReader(csDecrypt))
                        {
                            result = swDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            rijn.Clear();
            return result;
        }
    }

    //public class ClsCrypto
    //{
    //    private RijndaelManaged myRijndael = new RijndaelManaged();
    //    private int iterations;
    //    private byte[] salt;

    //    public ClsCrypto(string strPassword)
    //    {
    //        myRijndael.BlockSize = 128;
    //        myRijndael.KeySize = 128;
    //        myRijndael.IV = HexStringToByteArray("a5s8d2e9c1721ae0e84ad660c472y1f3");

    //        myRijndael.Padding = PaddingMode.PKCS7;
    //        myRijndael.Mode = CipherMode.CBC;
    //        iterations = 1000;
    //       // salt = System.Text.Encoding.UTF8.GetBytes("cryptography123example           myRijndael.Key = GenerateKey(strPassword);
    //    }

    //    public string Encrypt(string strPlainText)
    //    {
    //        byte[] strText = new System.Text.UTF8Encoding().GetBytes(strPlainText);
    //        ICryptoTransform transform = myRijndael.CreateEncryptor();
    //        byte[] cipherText = transform.TransformFinalBlock(strText, 0, strText.Length);
    //        return Convert.ToBase64String(cipherText);
    //    }

    //    public string Decrypt(string encryptedText)
    //    {
    //        var encryptedBytes = Convert.FromBase64String(encryptedText);
    //        ICryptoTransform transform = myRijndael.CreateDecryptor();
    //        byte[] cipherText = transform.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);
    //        return System.Text.Encoding.UTF8.GetString(cipherText);
    //    }

    //    public static byte[] HexStringToByteArray(string strHex)
    //    {
    //        dynamic r = new byte[strHex.Length / 2];
    //        for (int i = 0; i <= strHex.Length - 1; i += 2)
    //        {
    //            r[i / 2] = Convert.ToByte(Convert.ToInt32(strHex.Substring(i, 2), 16));
    //        }
    //        return r;
    //    }

    //    private byte[] GenerateKey(string strPassword)
    //    {
    //        Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(System.Text.Encoding.UTF8.GetBytes(strPassword), salt, iterations);
    //        return rfc2898.GetBytes(128 / 8);
    //    }
    //}

}
