﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.PathFinder;
using YulgangBotFinal.Resource;
using YulgangBotFinal.UserControl.Maps;

namespace YulgangBotFinal.Tool.FindPath
{
    public partial class Form1 : Form, IDrawMap
    {
        private readonly MapCaculator _map;
        private readonly IYulgangPathFinder _finder;
        private IList<IYulgangLocation> _allPoint = new List<IYulgangLocation>();
        public Form1()
        {
            InitializeComponent();
            var ybi =
                new YBiHandle
                    <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill,
                        YulgangYbiGameSpell>();
            ybi.Load("Resource\\Ybi.cfg");
            _map = new MapCaculator(ybi);
            _map.SetBackground(101, mapGui1.ClientRectangle);
           
            _finder = new YulgangPathFinder(_map);
            mapGui1.AddDrawer(this);
            mapGui1.SetMapManager(_map);

            using (var read = File.OpenRead("C:\\Users\\rxjh\\Documents\\yulgangbotfinal\\PathPoint_101.pth"))
            {
                var len = read.Seek(0, SeekOrigin.End);
                read.Seek(0, SeekOrigin.Begin);
                var data = new byte[len];
                read.Read(data, 0, (int)len);
                for (int i = 0; i < data.Length; i++)
                {
                    data[i] ^= 0x86;
                }
                var decrypt = new string(data.Select(Convert.ToChar).ToArray());
                // Debug.Write(decrypt);
                var spoint = decrypt.Split('#');
                for (int i = 1; i < spoint.Length; i++)
                {
                    var p = spoint[i].Replace("\u008c\u008c","").Replace("$","").Split('\t');
                    _allPoint.Add(new YulgangLocation(float.Parse(p[1]), float.Parse(p[3]), 101) {Z = float.Parse(p[2])});

                }
            }
        }
        private PointF GetControlPointFormPathLocation(int i, int j, float cellw, float cellh)
        {
            var rec = _map.GetMapRect();
            return new PointF(i * cellw + rec.X1, j * cellh + rec.Y1);
        }
        public void Draw(bool val, Graphics g)
        {
            var map = _map.GetMapRect();
            var cellWidth = map.Width / _finder.GetMaxSize();
            var cellHeight = map.Height / _finder.GetMaxSize();
            if (val)
            {
                /*draw grid*/


                for (var i = 0; i < _finder.GetMaxSize(); i++) // draw line vertical
                {
                    g.DrawLine(new Pen(Color.Black),
                        _map.PointMapToPointControl(new PointF(map.X1, map.Y1 + i * cellHeight)),
                        _map.PointMapToPointControl(new PointF(map.X2, cellHeight * i + map.Y1)));

                    g.DrawLine(new Pen(Color.Black),
                        _map.PointMapToPointControl(new PointF(map.X1 + i * cellWidth, map.Y1)),
                        _map.PointMapToPointControl(new PointF(map.X1 + i * cellWidth, map.Y2)));
                }
            }
            /*draw path*/

            for (var i = 0; i < _finder.GetMaxSize(); i++)
            {
                for (var j = 0; j < _finder.GetMaxSize(); j++)
                {
                    if (_finder.GetGrid()[i, j] == 2)
                    {
                        var poin1 =
                            _map.PointMapToPointControl(GetControlPointFormPathLocation(i, j, cellWidth,
                                cellHeight));
                        var point2 =
                            _map.PointMapToPointControl(GetControlPointFormPathLocation(i + 1, j + 1,
                                cellWidth, cellHeight));
                        g.FillRectangle(new SolidBrush(Color.FromArgb(150, 0, 255, 139)), poin1.X, poin1.Y,
                            Math.Abs(point2.X - poin1.X), Math.Abs(point2.Y - poin1.Y));
                    }
                }
            }
            foreach (var p in _allPoint)
            {
                var point =
                    _map.PointMapToPointControl(new PointF(p.X, p.Y));
                g.FillRectangle(new SolidBrush(Color.FromArgb(255, 255, 0, 0)), point.X, point.Y,
                    5, 5);
            }

            //if (_pathReady)
            //    for (int i = 0; i < _path.Count; i++)
            //    {
            //        var point = _backgroundManager.PointMapToPointControl(_path[i]);
            //        g.FillRectangle(new SolidBrush(Color.White), point.X - 4, point.Y - 4, 8, 8);
            //        if (i < _path.Count - 1)
            //        {
            //            var point1 = _backgroundManager.PointMapToPointControl(_path[i + 1]);
            //            g.DrawLine(new Pen(Color.White), point, point1);
            //        }
            //    }
        }


        public void Draw(Graphics g)
        {
            g.DrawImage(_map.GetBackgroundImage(), _map.GetClientRect(),
                _map.GetImageRect(), GraphicsUnit.Pixel);
            Draw(false, g);
        }
    }
}