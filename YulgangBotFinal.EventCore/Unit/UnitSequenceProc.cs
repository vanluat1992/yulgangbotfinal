﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: UnitSequenceProc.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Collections.Generic;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Define;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;
using YulgangBotFinal.Resource.Ybq;

#endregion

namespace YulgangBotFinal.EventCore.Unit
{
    
    public abstract class UnitSequenceProc<TInut, TConfig, TOutput> : ISquenceUnitProc where TInut : class, IUnitInput
        where TOutput : class, IUnitOutput
        where TConfig : class, IUnitConfig
    {
        public UnitSequenceProc(YbConfig
            cfg)
        {
            Ybi = cfg.Ybi;
            Log = cfg.Log;
            Quests = cfg.Quests;
            Main = new MainDefine();
            State = 0;
            Config = Activator.CreateInstance<TConfig>();
            Input = Activator.CreateInstance<TInut>();
            Output = Activator.CreateInstance<TOutput>();
            Wrap = cfg.Wrap;
        }

        protected Wrap Wrap { get;  }

        protected IDictionary<int,Quest> Quests { get; }
        protected
            YBiHandle
                <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill, YulgangYbiGameSpell>
            Ybi { get; }

        /// <summary>
        ///     log
        /// </summary>
        protected ILog Log { get; }

        public TInut Input { get; set; }
        protected virtual MainDefine Main { get; private set; }
        public int State { get; set; }
        public TOutput Output {  get; set; }
        public TConfig Config {  get; set; }

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public abstract void HandlePacket(IPacket p);

        public abstract event Action<string> Break;

        protected void Wait(int timeout = 60000)
        {
            State = 0;
            var evt = DateTime.Now;
            while (State == 0 && !Main.RequestLoginAgain)
            {
                if ((DateTime.Now - evt).TotalMilliseconds > timeout)
                    break;
                Thread.Sleep(10);
            }
        }

        //public virtual TInut GetInput()
        //{
        //    var inut = Input as TInut;
        //    if (inut != null)
        //        return inut;
        //    throw new InvalidCastException("");
        //}

        //public virtual TOutput GetOutput()
        //{
        //    var output = Output as TOutput;
        //    if (output != null)
        //        return output;
        //    throw new InvalidCastException("");
        //}

        //public virtual TConfig GetConfig()
        //{
        //    var cfg = Config as TConfig;
        //    if (cfg != null)
        //        return cfg;
        //    throw new InvalidCastException("");
        //}

        #region Implementation of ISquenceUnitProc

        public abstract int Run();

        public abstract Type GetSeekWhenFail();

        public void SetMainDefine(MainDefine def)
        {
            Main = def;
        }

        public abstract void Clean();
        protected IProcScope Scope { get; private set; }

        public void SetScopeProcess(IProcScope scope)
        {
            Scope = scope;
        }

        public T ParsePacket<T>(IPacket p) where T : RecvPacket
        {
            var t = (T) Activator.CreateInstance(typeof (T), p.Data, p.SessionId);
            t?.Parse();
            return (T) t;
        }

        #endregion

        protected void Moveto(IYulgangLocation toLocation)
        {
            // kiểm tra coi có cùng bản đồ hay ko

            var arrayPoint = Main.PathFinder.Route(Main.Location, toLocation);

            //todo: hack
            foreach (var yulgangLocation in arrayPoint)
            {
                Main.GsConnect.Send(new S0048SelecDieType(Main.PlayerSessionId).SendAdminMove(yulgangLocation));
                var last = DateTime.Now;
                var fail = false;
                while (Main.Location.DistanceTo(yulgangLocation) > 5)
                {
                    // todo nên đặt timeout đề phòng ko send dc
                    if ((DateTime.Now - last).TotalMilliseconds > 10000)
                    {
                        Log.Warning(Main.Username, $"Move Fail ");
                        fail = true;
                        break;
                    }
                    Thread.Sleep(10);
                }
                if (!fail)
                    Log.Debug(Main.Username, "Di chuyển thành công");
            }

            Main.RequestMove = null;
            return;
            //todo: cách cũ 
            var failCount = 0;
            foreach (var location in arrayPoint)
            {
                location.MapId = Main.Location.MapId;
                Main.GsConnect.Send(new S0007PlayerMove(Main.PlayerSessionId).Send(Main.Location, location));
                Log.Debug(Main.Username, $"Di chuyển từ {Main.Location} đến {location}");

                var t = DateTime.Now;
                while (Main.Location.DistanceTo(location) > 5)
                {
                    if ((DateTime.Now - t).TotalMilliseconds > 10000)
                    {
                        Log.Warning(Main.Username, $"Move Fail  {failCount++}");
                        break;
                    }
                    Thread.Sleep(10);
                }
                Thread.Sleep(2000);
            }
        }

        private readonly Random _random = new Random();
        private IYulgangLocation _backupLocation;
        protected void MoveRandomTo(IYulgangLocation tolocation)
        {
            if (_backupLocation == null)
            {
                var t = _random.Next(1, 15);
                tolocation.X += t;
                tolocation.Y += t;
                Main.GsConnect.Send(new S0007PlayerMove(Main.PlayerSessionId).Send(Main.Location, tolocation));

                _backupLocation = new YulgangLocation(Main.Location.X, Main.Location.Y, Main.Location.MapId);
            }
            else
            {
                Main.GsConnect.Send(new S0007PlayerMove(Main.PlayerSessionId).Send(Main.Location, _backupLocation));
                _backupLocation = null;
            }
           
            //Main.GsConnect.Send(new S0048SelecDieType(Main.PlayerSessionId).SendAdminMove(tolocation));
        }
        protected void EndTalk(int npcId)
        {
            // đã xong
            Main.GsConnect.Send(new S0090TalkWithNpc(Main.PlayerSessionId).Send(YulgangTalkNpcCommand.Close,
                npcId));
            Wait(10000);
        }

        protected bool TalkWithNpc(IYulgangLocation location, int npcId)
        {
            // kiểm tra khoảng cách với npc xem hợp lý ko
            if (Main.Location.DistanceTo(location) > 15)
                return false;

            // yêu cầu nói chuyện với npc
            Main.GsConnect.Send(new S0090TalkWithNpc(Main.PlayerSessionId).Send(YulgangTalkNpcCommand.OpenMenu,
                npcId));
            // đợi trả lời
            Wait(15000);
            if (State == 0) return false; // mạng lag hoặc bị cái vẹo ji đó ko có trả lời
            return true;
        }
    }

    public class YbConfig
    {
        public YbConfig(YBiHandle<YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill, YulgangYbiGameSpell> ybi, IDictionary<int, Quest> quests, ILog log, Wrap wr)
        {
            Ybi = ybi;
            Quests = quests;
            Log = log;
            Wrap = wr;
        }

        public YBiHandle
            <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill, YulgangYbiGameSpell> Ybi
        { get; set; }
        public IDictionary<int,Quest> Quests { get; set; }
        public ILog Log { get; set; }
        public Wrap Wrap { get; }
    }
}