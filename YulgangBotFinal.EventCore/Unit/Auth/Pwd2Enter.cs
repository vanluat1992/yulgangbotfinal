﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: Pwd2Enter.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.EventCore.Utils;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Network.Packet.Auth.Recv;
using YulgangBotFinal.Network.Packet.Auth.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Auth
{
    public class Pwd2Enter : UnitSequenceProc<Pwd2Input, Pwd2Config, Pwd2Output>
    {
        public Pwd2Enter(YbConfig cfg) : base(cfg)
        {
        }

        #region Overrides of UnitSequenceProc<Pwd2Input,Pwd2Config,Pwd2Output>

        public override event Action<string> Break;

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 0x8049:
                    var tmp = ParsePacket<R8049PassVerify>(p);
                    Log.Debug(Main.Username, $"Đăng nhập pass 2 {(tmp.Valid ? "Thành công " : "Không thành công")}");
                    State = tmp.Valid ? 1 : -1;
                    break;
                case 0x804D:
                    State = 1;
                    break;
            }
        }
        
        public override int Run()
        {
            if (Config.LoginType == LoginServerType.Korea)
            {
                Main.AuthConnect.Register(this, 0x804D,0x8049);

                Main.AuthConnect.Send(new S804C().Send(2));
                Wait();
                State = 0;

                Log.Debug(Main.Username, $"Đăng nhập với pass 2 : {Main.Pass2.GeneratePassword2()}");
                Main.AuthConnect.Send(new S8048PassVerify().Send(Main.Pass2.GeneratePassword2()));
            }
            if (Config.LoginType == LoginServerType.Dzo)
            {
                Main.AuthConnect.Register(this, 0x8049);
                Log.Debug(Main.Username, $"Đăng nhập với pass 2 : {Main.Pass}");
                Main.AuthConnect.Send(new S8048PassVerify().Send(Main.Pass2));
            }

            Wait();
            return State == 1 ? 1 : 0;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (AccountLogin);
        }

        public override void Clean()
        {
            Main.AuthConnect.Unregister(this);
        }

        #endregion
    }

    public enum LoginServerType
    {
        Dzo,
        Korea,
        Us
    }
    public class Pwd2Config : IUnitConfig
    {
        public LoginServerType LoginType { get; set; } = LoginServerType.Dzo;
    }

    public class Pwd2Input : IUnitInput
    {
    }

    public class Pwd2Output : IUnitOutput
    {
    }
}