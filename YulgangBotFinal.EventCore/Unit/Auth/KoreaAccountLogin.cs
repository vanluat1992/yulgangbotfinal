﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.EventCore
// FileName : KoreaAccountLogin.cs
// Time Create : 2:51 PM 24/03/2017
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Jurassic;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Define;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Resource;

namespace YulgangBotFinal.EventCore.Unit.Auth
{
    [UnitProcPriority(5, true)]
    public class KoreaAccountLogin : UnitSequenceProc<KoreaAccountLoginInput, KoreaAccountLoginConfig, KoreaAccountLoginOutput>
    {
        private ScriptEngine _engine;
        //private readonly string _proxy = "211.110.165.238:3128";
        public KoreaAccountLogin(YbConfig cfg) : base(cfg)
        {
            _engine = new ScriptEngine();
            _engine.ExecuteFile(@"KoreaLogin.js");
        }

        #region Overrides of UnitSequenceProc<AccountLoginInput,AccountLoginConfig,AccountLoginOutput>

        public override void HandlePacket(IPacket p)
        {
            throw new NotImplementedException();
        }

        public override event Action<string> Break;
        public override int Run()
        {
            //var user = HttpUtility.UrlEncode(_engine.CallGlobalFunction("getEncrypt", "liemhd").ToString());
            //var pass = HttpUtility.UrlEncode(_engine.CallGlobalFunction("getEncrypt", "@lananh123").ToString());
            var user = HttpUtility.UrlEncode(_engine.CallGlobalFunction("getEncrypt", Main.Username).ToString());
            var pass = HttpUtility.UrlEncode(_engine.CallGlobalFunction("getEncrypt", Main.Pass).ToString());

            Step0();
            Step1(user, pass);
            var ck = Step2(user, pass);
            var sp = ck.Split(',').ToList();
            sp.RemoveAt(7);
            sp.RemoveAt(7);
            sp.RemoveAt(1);
            sp.RemoveAt(1);
            sp.RemoveAt(1);
            sp.RemoveAt(1);
            var valid = "";
            foreach (var s in sp)
            {
                valid += s.Split(';')[0] + "; ";
            }
            Debug.WriteLine(valid);
            Step3(valid);
            Step4(valid);
            var tmp=Step5(valid);
            Main.DzoUsername = tmp.Item1;
            Main.DzoPass = tmp.Item2;
            Log.Debug(Main.Username, $"Đăng nhập thành công {Main.DzoUsername}----{Main.Pass}");
            Output.RunEventLogin(1, Main.DzoUsername, Main.DzoUsername);
            return 1;
        }

        public override Type GetSeekWhenFail()
        {
            return null;
        }

        public override void Clean()
        {
        }

        #endregion



        void Step0(string ck = null)
        {
            var web =
                HttpWebRequest.Create(
                    "http://www.mgame.com/ulnauth/login/yulgang_main_login_v3.mgame");
            if (!string.IsNullOrEmpty(Config.Proxy))
                web.Proxy = new WebProxy(Config.Proxy);
            web.Method = "GET";
            web.Headers.Add("Cookie", "fxBannerDefault_2017003201100_01=V; _ga=GA1.2.1063302123.1490315814");

            //using (var s = web.GetRequestStream())
            //{
            //    using (TextWriter w = new StreamWriter(s))
            //    {
            //        w.WriteLine(
            //            $"mgamelogindata1={user}" +
            //            $"&mgamelogindata2={pass}&lt=4&tu=&ru=&x=47&y=35");

            //    }
            //}
            //using (var r = web.GetResponse())
            //{
            //    using (var s = r.GetResponseStream())
            //    {
            //        using (TextReader re = new StreamReader(s))
            //        {
            //            Debug.WriteLine(r.Headers.ToString());
            //            //Debug.WriteLine(re.ReadToEnd());
            //        }
            //    }
            //}
            //Debug.WriteLine("------------0-------------");
        }

        void Step1(string user, string pass)
        {
            HttpWebRequest web = (HttpWebRequest)
                HttpWebRequest.Create(
                    "https://sign.mgame.com/login/login_action_pub_type_b.mgame?tu=http://yulgang.mgame.com");
            web.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            if (!string.IsNullOrEmpty(Config.Proxy))
                web.Proxy = new WebProxy(Config.Proxy);
            web.Method = "POST";
            web.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            web.Host = "sign.mgame.com";
            web.Referer = "http://www.mgame.com/ulnauth/login/yulgang_main_login_v3.mgame";

            web.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            web.Headers.Add("Accept-Language", "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2");
            web.Headers.Add("Cookie", "MCV=0; ECF04KO=0; _ga=GA1.2.1063302123.1490315814");
            web.Headers.Add("Origin", "http://www.mgame.com");
            web.Headers.Add("Upgrade-Insecure-Requests", "1");
            web.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";

            web.ContentType = "application/x-www-form-urlencoded";

            using (var s = web.GetRequestStream())
            {
                using (TextWriter w = new StreamWriter(s))
                {
                    var tmp = $"mgamelogindata1={user}" +
                        $"&mgamelogindata2={pass}&lt=4&tu=&ru=&x=47&y=35";
                    w.WriteLine(
                        tmp
                       );
                }
            }
            //using (var r = web.GetResponse())
            //{
            //    using (var s = r.GetResponseStream())
            //    {
            //        using (TextReader re = new StreamReader(s))
            //        {
            //            // Debug.WriteLine(r.Headers.ToString());
            //            // Debug.WriteLine(re.ReadToEnd());
            //        }
            //    }
            //}
            //Debug.WriteLine("------------1-------------");
        }

        string Step2(string user, string pass)
        {
            var web = (HttpWebRequest)
                HttpWebRequest.Create(
                    "https://sign.mgame.com/login/login_action.mgame");
            web.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            if (!string.IsNullOrEmpty(Config.Proxy))
                web.Proxy = new WebProxy(Config.Proxy);
            web.Method = "POST";
            web.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            web.Host = "sign.mgame.com";
            web.Referer = "https://sign.mgame.com/login/login_action_pub_type_b.mgame?tu=http://yulgang.mgame.com";

            web.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            web.Headers.Add("Accept-Language", "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2");
            web.Headers.Add("Cookie", "MCV=0; ECF04KO=0; _ga=GA1.2.1063302123.1490315814");
            web.Headers.Add("Origin", "https://sign.mgame.com");
            web.Headers.Add("Upgrade-Insecure-Requests", "1");
            web.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";

            web.ContentType = "application/x-www-form-urlencoded";


            using (var s = web.GetRequestStream())
            {
                using (TextWriter w = new StreamWriter(s))
                {
                    w.WriteLine(
                        $"mgamelogindata1={user}&mgamelogindata2={pass}&lt=4&tu=&ru=&x=47&y=35&tu=http%3A%2F%2Fyulgang.mgame.com&loginparamgood=43f64974f7eb40ab71558daed8f5a88108d8345bf76511d479a2285d10bf166a&mac_addr=&pc_name=");

                }
            }
            var result = "";
            using (var r = web.GetResponse())
            {
                using (var s = r.GetResponseStream())
                {
                    using (TextReader re = new StreamReader(s))
                    {
                        result = r.Headers["Set-Cookie"];
                        // Debug.WriteLine(r.Headers.ToString());
                        // Debug.WriteLine(re.ReadToEnd());
                    }
                }
            }
            //Debug.WriteLine("-----------2--------------");
            return result;
        }
        void Step3(string ck)
        {
            var web = (HttpWebRequest)
                HttpWebRequest.Create(
                    "https://sign.mgame.com/login/login_action_result.mgame?lt=4&rp=&tu=http%3A%2F%2Fyulgang.mgame.com");
            web.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            if (!string.IsNullOrEmpty(Config.Proxy))
                web.Proxy = new WebProxy(Config.Proxy);
            web.Method = "GET";
            web.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            web.Host = "sign.mgame.com";
            web.Referer = "https://sign.mgame.com/login/login_action.mgame";

            web.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            web.Headers.Add("Accept-Language", "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2");
            web.Headers.Add("Cookie", "_ga=GA1.2.1063302123.1490315814;" + ck);
            web.Headers.Add("Origin", "https://sign.mgame.com");
            web.Headers.Add("Upgrade-Insecure-Requests", "1");
            web.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";


            //using (var r = web.GetResponse())
            //{
            //    using (var s = r.GetResponseStream())
            //    {
            //        using (TextReader re = new StreamReader(s))
            //        {
            //            // Debug.WriteLine(r.Headers.ToString());
            //            //Debug.WriteLine(re.ReadToEnd());
            //        }
            //    }
            //}
            //Debug.WriteLine("------------3-------------");
        }

        void Step4(string ck)
        {
            var web = (HttpWebRequest)
               HttpWebRequest.Create(
                   "http://www.mgame.com/ulnauth/login/yulgang_main_login_v3.mgame?returl=http://yulgang.mgame.com/");
            web.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            if (!string.IsNullOrEmpty(Config.Proxy))
                web.Proxy = new WebProxy(Config.Proxy);
            web.Method = "GET";
            web.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            web.Host = "www.mgame.com";
            web.Referer = "http://yulgang.mgame.com/";

            web.Headers.Add("Accept-Encoding", "gzip, deflate, sdch");
            web.Headers.Add("Accept-Language", "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2");
            web.Headers.Add("Cookie", "_ga=GA1.2.1063302123.1490315814;" + ck);
            //web.Headers.Add("Origin", "https://sign.mgame.com");
            web.Headers.Add("Upgrade-Insecure-Requests", "1");
            web.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";


            //using (var r = web.GetResponse())
            //{
            //    using (var s = r.GetResponseStream())
            //    {
            //        using (TextReader re = new StreamReader(s, Encoding.UTF8))
            //        {
            //            // Debug.WriteLine(r.Headers.ToString());
            //            //var tmp = re.ReadToEnd();
            //            //Debug.WriteLine(tmp);
            //        }
            //    }
            //}
            //Debug.WriteLine("------------4-------------");
        }
        Tuple<string,string> Step5(string ck)
        {
            var web = (HttpWebRequest)
                HttpWebRequest.Create(
                    "http://gstart.mgame.com/launch/launch_yulgang.mgame?goUrl=yulgang");
            web.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            if (!string.IsNullOrEmpty(Config.Proxy))
                web.Proxy = new WebProxy(Config.Proxy);
            web.Method = "GET";
            web.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            web.Host = "gstart.mgame.com";
            web.Referer = "http://yulgang.mgame.com/";

            web.Headers.Add("Accept-Encoding", "gzip, deflate, sdch");
            web.Headers.Add("Accept-Language", "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2");
            web.Headers.Add("Cookie", ck + ";_ga=GA1.2.1063302123.1490315814");
            //web.Headers.Add("Origin", "https://sign.mgame.com");
            web.Headers.Add("Upgrade-Insecure-Requests", "1");
            web.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";


            using (var r = web.GetResponse())
            {
                using (var s = r.GetResponseStream())
                {
                    using (TextReader re = new StreamReader(s, Encoding.UTF8))
                    {
                        // Debug.WriteLine(r.Headers.ToString());
                        var tmp =
                            re.ReadToEnd().Split('\n').Where(m => m.Contains("INET")).FirstOrDefault()?.Split(',')[9]
                                .Split(')')[0].Split(' ');

                        //Debug.WriteLine($"username : {tmp[3]}");
                       // Debug.WriteLine($"pass : {tmp[4]}");
                        return new Tuple<string, string>(tmp[3], tmp[4]);
                    }
                }
            }
            //Debug.WriteLine("-----------5--------------");
        }

    }
    public class KoreaAccountLoginInput : IUnitInput
    {
    }

    public class KoreaAccountLoginConfig : IUnitConfig
    {
        public string Proxy { get; set; }
    }

    public class KoreaAccountLoginOutput : IUnitOutput
    {
        public event Action<int, string, string> LoginResponse;

        internal void RunEventLogin(int type, string user, string pass)
        {
            LoginResponse?.Invoke(type, user, pass);
        }
    }
}