﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: EnterChannel.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Network.Packet.Auth.Recv;
using YulgangBotFinal.Network.Packet.Auth.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Auth
{
    public class EnterChannel : UnitSequenceProc<EnterChannelInput, EnterChannelConfig, EnterChannelOutput>
    {
        public EnterChannel(YbConfig cfg) : base(cfg)
        {
        }

        public override event Action<string> Break;

        public override void HandlePacket(IPacket p)
        {
            //121.52.200.87:15001
            if (p.Opcode == 0x8064)
            {
                var tmp = ParsePacket<R8064EnterChannel>(p);
                Log.Debug(Main.Username, $"Ip GameServer : {tmp.Ip}:{tmp.Port}");
                Main.GsIp = tmp.Ip;
                Main.GsPort = tmp.Port;
                State = 1;
            }
        }

        public override int Run()
        {
            Main.AuthConnect.Register(this, 0x8064);

            Log.Debug(Main.Username, $"Đăng nhập vào server {Config.IdServer}, kênh {Config.IdChanel}");
            if(Config.LoginType==LoginServerType.Korea)
                Main.AuthConnect.Send(new S800CEnterChannel().SendKorea(Config.IdServer, Config.IdChanel));
            if (Config.LoginType == LoginServerType.Dzo)
                Main.AuthConnect.Send(new S800CEnterChannel().Send(Config.IdServer, Config.IdChanel));
            Wait();
            return State == 1 ? 1 : 0;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (AccountLogin);
        }

        public override void Clean()
        {
            Main.AuthConnect.Unregister(this);
        }
    }

    public class EnterChannelConfig : IUnitConfig
    {
        public int IdChanel { get; set; }
        public int IdServer { get; set; }
        public LoginServerType LoginType { get; set; }
    }

    public class EnterChannelInput : IUnitInput
    {
    }

    public class EnterChannelOutput : IUnitOutput
    {
    }
}