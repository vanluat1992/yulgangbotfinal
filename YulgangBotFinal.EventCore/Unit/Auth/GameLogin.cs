﻿// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.EventCore
// FILENAME : GameLogin.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 25/03/2017 12:55 PM
// **************************************************************************

#region

using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Network;
using YulgangBotFinal.Network.Packet.Auth.Recv;
using YulgangBotFinal.Network.Packet.Auth.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Auth
{
    public class GameLogin : UnitSequenceProc<GameLoginInput, GameLoginConfig, GameLoginOutPut>
    {
        private int _resultLogin = 0;

        public GameLogin(YbConfig cfg) : base(cfg)
        {
        }

        #region Overrides of UnitSequenceProc<GameLoginInput,GameLoginConfig,GameLoginOutPut>

        public override event Action<string> Break;

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 0x8001:
                    var parsePacket = ParsePacket<R8001FirstLogin>(p);
                    if (Config.LoginType == LoginServerType.Korea)
                    {
                        State = 1;
                        break;
                    }

                    State = parsePacket.Valid ? 1 : 0;
                    Log.Debug(Main.Username, $"Đăng nhập {(parsePacket.Valid ? "Thành công " : "Không thành công")}");
                    break;
            }
        }

        public override int Run()
        {
            // tạo 1 kết nối
            Main.AuthConnect = new AuthConnection(Log);
            Main.AuthConnect.Register(this, 0x8001);
            Main.AuthConnect.Connect(Main.AuthIp, Main.AuthPort);

            // send gói tin
            Log.Debug(Main.Username, $"Đăng nhập Dzo account :{Main.DzoUsername} - Pass: {Main.DzoPass}");
            if (Config.LoginType == LoginServerType.Korea)
                Main.AuthConnect.Send(new S8000FirstLogin().SendKorea(Main.DzoUsername, Main.DzoPass));
            else
                Main.AuthConnect.Send(new S8000FirstLogin().Send(Main.DzoUsername, Main.DzoPass));

            Wait(30000);
            return State == 1 ? 1 : 0;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof(AccountLogin);
        }

        public override void Clean()
        {
            Main.AuthConnect.Unregister(this);
        }

        #endregion
    }

    public class GameLoginOutPut : IUnitOutput
    {
    }

    public class GameLoginInput : IUnitInput
    {
    }

    public class GameLoginConfig : IUnitConfig
    {
        public LoginServerType LoginType { get; set; }
    }
}