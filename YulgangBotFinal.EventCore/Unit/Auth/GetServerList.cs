﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: GetServerList.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Network.Packet.Auth.Recv;
using YulgangBotFinal.Network.Packet.Auth.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Auth
{
    public class GetServerList : UnitSequenceProc<GetServerInput, GetServerConfig, GetServerOutput>
    {
        public GetServerList(YbConfig cfg) : base(cfg)
        {
        }

        #region Overrides of UnitSequenceProc<GetServerInput,GetServerConfig,GetServerOutput>

        public override event Action<string> Break;

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 0x8017:
                    if (Config.LoginType != LoginServerType.Korea)
                    {
                        var tmp = ParsePacket<R8017ServerList>(p);
                        Main.ServerList = tmp.Servers;
                        foreach (var channel in tmp.Servers)
                        {
                            Log.Debug(Main.Username, $"Server name:{channel.Value.Id} - {channel.Value.Name} ");
                            foreach (var channel1 in channel.Value.Chanels)
                            {
                                Log.Debug(Main.Username,
                                    $"   {channel1.Value.Id} - {channel1.Value.Name}:{channel1.Value.Percent}%");
                            }
                        }
                        State = 1;
                    }
                    else
                    {
                        var tmp = ParsePacket<R8017ServerListKorea>(p);
                        Main.ServerList = tmp.Servers;
                        foreach (var channel in tmp.Servers)
                        {
                            Log.Debug(Main.Username, $"Server name:{channel.Value.Id} - {channel.Value.Name} ");
                            foreach (var channel1 in channel.Value.Chanels)
                            {
                                Log.Debug(Main.Username,
                                    $"   {channel1.Value.Id} - {channel1.Value.Name}:{channel1.Value.Percent}%");
                            }
                        }
                        State = 1;
                    }
                    break;
            }
        }

        public override int Run()
        {
            Main.AuthConnect.Register(this, 0x8017);
            Log.Debug(Main.Username, "Yêu cầu lấy danh sách server");
            Main.AuthConnect.Send(new S8016GetServerList());
            while (State == 0)
            {
                Thread.Sleep(10);
            }
            return State == 1 ? 1 : 0;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (AccountLogin);
        }

        public override void Clean()
        {
            Main.AuthConnect.Unregister(this);
        }

        #endregion
    }

    public class GetServerConfig : IUnitConfig
    {
        public LoginServerType LoginType { get; set; }
    }

    public class GetServerInput : IUnitInput
    {
    }

    public class GetServerOutput : IUnitOutput
    {
    }
}