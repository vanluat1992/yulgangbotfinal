﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: AccountLogin.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Define;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Auth
{
    [UnitProcPriority(5, true)]
    public class AccountLogin : UnitSequenceProc<AccountLoginInput, AccountLoginConfig, AccountLoginOutput>
    {
        public AccountLogin(YbConfig cfg):base(cfg)
        {
        }

        public override event Action<string> Break;

        private int Login(string username, string pass)
        {
            switch (Config.RegionServer)
            {
                case 0:
                {
                    var tmp = ParseDzo(GetResponseFromDzoServer(username, pass));

                    if (string.IsNullOrEmpty(tmp.Item1) || string.IsNullOrEmpty(tmp.Item2))
                    {
                        Log.Warning(username, "Đăng nhập không thành công");
                        Output.RunEventLogin(0, "", "");
                        Break?.Invoke("Đăng Nhập không thành công");
                    }
                    else
                    {
                        var dzoUser = tmp.Item1;
                        var dzoPass = tmp.Item2;
                        Main.DzoUsername = dzoUser;
                        Main.DzoPass = dzoPass;
                        Log.Debug(username, $"Đăng nhập thành công {dzoUser}----{dzoPass}");
                        Output.RunEventLogin(1, dzoUser, dzoPass);
                        return 1;
                    }
                }
                    break;
            }
            return 0;
        }

        private Tuple<string, string> ParseDzo(string data)
        {
            var split = data.Split(',');
            var username = "";
            var pass = "";
            foreach (var para in split.Select(s => s.Replace("\"", ""))
                .Select(st => st.Split(':')))
            {
                if (para[0] == "user_id")
                {
                    username = para[1];
                }
                if (para[0] == "user_otp")
                {
                    pass = para[1];
                }
            }
            return new Tuple<string, string>(username, pass);
        }

        private string GetResponseFromDzoServer(string user, string pass)
        {
            const string url = "http://authen.dzogame.com:8080/LauncherLogin.aspx?gid=200";
            var myCookieContainer = new CookieContainer();

            // Convert the text into the url encoding string

            // Concat the string data which will be submit
            const string formatString =
                "ScriptManager1=UpdatePanel1%7CbtnLogin&__EVENTTARGET=btnLogin&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUJNTU3MDU3Mzk1D2QWAgIDD2QWAgIDD2QWAgIBD2QWAmYPZBYCAgsPZBYGAgEPDxYCHgtOYXZpZ2F0ZVVybAUdU05Mb2dpbi5hc3B4P2dpZD0yMDAmc3R5cGU9eWhkZAIDDw8WAh8ABR5TTkxvZ2luLmFzcHg%2FZ2lkPTIwMCZzdHlwZT1nbGVkZAIFDw8WAh8ABR1TTkxvZ2luLmFzcHg%2FZ2lkPTIwMCZzdHlwZT1mYmRkZA%3D%3D&__EVENTVALIDATION=%2FwEWBQLj4%2F3ECgLWn9eKCALq85jtCQKC3IeGDALPqpnxDQ%3D%3D&tbxUserName={0}&tbxPassword={1}&hdfGameID=200&__ASYNCPOST=true&";
            var postString = string.Format(formatString, user, pass);

            // Convert the submit string data into the byte array
            var postData = Encoding.ASCII.GetBytes(postString);

            // Set the request parameters
            var request = WebRequest.Create(url) as HttpWebRequest;
            if (request != null)
            {
                request.MaximumAutomaticRedirections = 4;
                request.MaximumResponseHeadersLength = 4;
                // Set credentials to use for this request.
                request.Credentials = CredentialCache.DefaultCredentials;

                request.Method = "POST";
                request.Referer = url;
                request.KeepAlive = true;
                request.UserAgent =
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.2; CIBA)";
                request.ContentType = "application/x-www-form-urlencoded";
                request.CookieContainer = myCookieContainer;
                request.ContentLength = postData.Length;

                // Submit the request data
                var outputStream = request.GetRequestStream();

                request.AllowAutoRedirect = true;
                outputStream.Write(postData, 0, postData.Length);
                outputStream.Close();

                // Get the return data
                var response = request.GetResponse() as HttpWebResponse;
                if (response != null)
                {
                    var responseStream = response.GetResponseStream();
                    //Console.WriteLine(srcString);
                    response.Close();


                    request = WebRequest.Create("http://authen.dzogame.com:8080/Notification.aspx") as HttpWebRequest;
                    if (request != null)
                    {
                        request.CookieContainer = myCookieContainer;
                        request.Method = "GET";
                        request.KeepAlive = true;
                        {
                            var response1 = request.GetResponse() as HttpWebResponse;
                            if (response1 != null)
                            {
                                new StreamReader(response1.GetResponseStream(), Encoding.UTF8)
                                    .ReadToEnd
                                    ();
                                response1.Close();
                            }
                        }
                    }

                    request = WebRequest.Create("http://authen.dzogame.com:8080/ReturnLogin.ashx") as HttpWebRequest;
                    if (request != null)
                    {
                        request.Method = "POST";
                        request.Referer = "http://authen.dzogame.com:8080/Notification.aspx";
                        request.KeepAlive = true;
                        request.UserAgent =
                            "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.2; CIBA)";
                        request.ContentType = "application/x-www-form-urlencoded";
                        request.CookieContainer = myCookieContainer;

                        request.ContentLength = 0;

                        // Get the return data
                        response = request.GetResponse() as HttpWebResponse;
                    }
                    if (response != null) responseStream = response.GetResponseStream();
                    if (responseStream != null)
                    {
                        var reader = new StreamReader(responseStream, Encoding.UTF8);
                        var srcString = reader.ReadToEnd();
                        reader.Dispose();
                        responseStream.Close();
                        response.Close();
                        request.Abort();

                        return srcString;
                    }
                }
            }
            return "";
        }

        #region Implementation of ISquenceUnitProc

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            throw new NotImplementedException();
        }

        public override int Run()
        {
            Log.Debug(Main.Username, $"Đăng nhập với WebAccount : {Main.Username}-{Main.Pass}");
            var result = Login(Main.Username, Main.Pass);
            if (result == 1)
                return 1;
            return -1;
        }

        public override Type GetSeekWhenFail()
        {
            return null;
        }

        public override void Clean()
        {
        }

        #endregion
    }

    public class AccountLoginInput : IUnitInput
    {
    }

    public class AccountLoginConfig : IUnitConfig
    {
        /// <summary>
        ///     vùng đăng nhập
        /// </summary>
        public int RegionServer { get; set; } = 0; // mặc định là dzo game
    }

    public class AccountLoginOutput : IUnitOutput
    {
        public event Action<int, string, string> LoginResponse;

        internal void RunEventLogin(int type, string user, string pass)
        {
            LoginResponse?.Invoke(type, user, pass);
        }
    }
}