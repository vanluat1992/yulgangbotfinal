﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: Welcome.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.EventCore.Unit.Auth;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Network;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs
{
    public class Welcome : UnitSequenceProc<WelcomeInput, WelcomeConfig, WelcomeOutput>
    {
        public Welcome(YbConfig cfg) : base(cfg)
        {
        }


        public override event Action<string> Break;

        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 2)
            {
                //Log.Dum(p.Data, "Packet 2");
                Main.PlayerSessionId = p.SessionId;
                State = 1;
            }
        }

        public override int Run()
        {
            Log.Debug(Main.Username, "Đóng kết nối với Auth Server");
            Main.AuthConnect.Release();
            Log.Debug(Main.Username, "Tạo Kết nối mới lên gameserver ");
            Main.GsConnect = new GsConnect(Log);
            Main.GsConnect.Disconnect += GsConnect_Disconnect;
            Main.GsConnect.Connect(Main.GsIp, Main.GsPort);
            Main.GsConnect.Register(this, 2);
            if (Config.LoginType == LoginServerType.Korea)
            {
                Main.GsConnect.Send(new S1606(Main.PlayerSessionId));
                Main.GsConnect.Send(new S0001FirstLogin(Main.PlayerSessionId).SendLoginKorea(Main.DzoUsername));
            }
            else
                Main.GsConnect.Send(new S0001FirstLogin(Main.PlayerSessionId).Send(Main.DzoUsername));
            Wait();
            return State == 1 ? 1 : 0;
        }

        private void GsConnect_Disconnect()
        {
            Main.RequestLoginAgain = true;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (AccountLogin);
        }

        public override void Clean()
        {
            Main.GsConnect.Unregister(this);
        }
    }

    public class WelcomeConfig : IUnitConfig
    {
        public LoginServerType LoginType { get; set; }
    }

    public class WelcomeInput : IUnitInput
    {
    }

    public class WelcomeOutput : IUnitOutput
    {
    }
}