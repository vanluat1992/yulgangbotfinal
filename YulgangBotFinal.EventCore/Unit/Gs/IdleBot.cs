﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 5:20 PM 17/09/2016
// FILENAME: IdleBot.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs
{
    /// <summary>
    ///     trạng thái nghỉ khi ko có yêu cầu chạy tự động
    /// </summary>
    public class IdleBot : UnitSequenceProc<IdleBotInput, IdleBotConfig, IdleBotOutput>
    {
        public IdleBot(YbConfig cfg) : base(cfg)
        {
        }

        public override event Action<string> Break;

        public override void HandlePacket(IPacket p)
        {
        }

        public override int Run()
        {
            Thread.Sleep(20);
            return Config.Auto ? 1 : 0;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (IdleBot);
        }

        public override void Clean()
        {
        }
    }

    public class IdleBotInput : IUnitInput
    {
    }

    public class IdleBotConfig : IUnitConfig
    {
        public bool Auto { get; set; } = true;
    }

    public class IdleBotOutput : IUnitOutput
    {
    }
}