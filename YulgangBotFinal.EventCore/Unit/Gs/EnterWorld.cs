﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: EnterWorld.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Drawing;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.EventCore.Unit.Auth;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.PathFinder;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs
{
    public class EnterWorld : UnitSequenceProc<EnterWorldInput, EnterWorldConfig, EnterWorldOutput>
    {
        private bool _isReload;

        public EnterWorld(YbConfig cfg) : base(cfg)
        {
        }

        #region Overrides of UnitSequenceProc<EnterWorldInput,EnterWorldConfig,EnterWorldOutput>

        public override event Action<string> Break;

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x20)
            {
                Log.Debug(Main.Username, $"Player Enter World");
                if (_isReload)
                {
                    State = 1;

                    // khởi tạo các giá trị quản lý bản đồ
                    /*Main.MapCaculartor = new MapCaculator(Ybi);
                    Main.MapCaculartor.SetBackground(Main.Player.Location.MapId, new Rectangle(0, 0, 512, 512));
                    Main.PathFinder = new YulgangPathFinder(Main.MapCaculartor);
                    */
                    Log.Debug(Main.Username, "Nhân vật đăng nhập thành công vào game");
                    Log.Debug(Main.Username, $"Tọa độ X,Y: {Main.Location.X},{Main.Location.Y} | {Main.Location.MapId}");
                    Output.InvokeOnValidMap();
                }
                Main.GsConnect.Send(new S0020EnterWorld(Main.PlayerSessionId));

                // todo: reload lại user 1 lần 
                // trở về trang login 

                if (!_isReload)
                {
                    Log.Debug(Main.Username, $"Reload lại phần chọn nhân vật ");
                    Main.GsConnect.Send(new S0056UndoToSelectCharacter(Main.PlayerSessionId));
                }
            }
            if (p.Opcode == 0x57)
            {
                State = 1;
                _isReload = true;
                // break task chính về trạng thái chọn nhân vật
                Scope.Break();
                Scope.Seek(typeof (GetListChar));
                Scope.Resume();
            }
        }

        public override int Run()
        {
            _isReload = true;
            Main.GsConnect.Register(this, 0x20, 0x57);
            Main.GsConnect.Send(new S0016GameOption(Main.PlayerSessionId).Send());
            Main.GsConnect.Send(new S0180(Main.PlayerSessionId));
            Main.GsConnect.Send(new S008F(Main.PlayerSessionId));
            Main.GsConnect.Send(new S0005LoadCharacter(Main.PlayerSessionId).Send(Main.Player.Index));
            Wait();
            if (State == 1)
                return 1;
            return 0;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (AccountLogin);
        }

        public override void Clean()
        {
            Main.GsConnect.Unregister(this);
        }

        #endregion
    }

    public class EnterWorldConfig : IUnitConfig
    {
    }

    public class EnterWorldInput : IUnitInput
    {
    }

    public class EnterWorldOutput : IUnitOutput
    {
        public event Action OnValidMap;

        internal void InvokeOnValidMap()
        {
            OnValidMap?.Invoke();
        }
    }
}