﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: GetListChar.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.EventCore.Unit.Auth;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs
{
    public class GetListChar : UnitSequenceProc<GetListCharInput, GetListCharConfig, GetListCharOutput>
    {
        public GetListChar(YbConfig cfg) : base(cfg)
        {
        }


        public override event Action<string> Break;

        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x11)
            {
                var tmp = ParsePacket<R0011GetListCharacter>(p);
                if (tmp.EmptyCharacter)
                {
                    State = 0;
                    return;
                }
                //abc77777
                Log.Debug(Main.Username, $"Lấy thông thành công thông tin nhân vật: {tmp.Player.Name} ");
                //if (tmp.Player.Name.Trim() == GetConfig().NamePlayerSelect.Trim())
                {
                    Log.Debug(Main.Username, $"Lựa chọn nhân vật {tmp.Player.Name} này để vào game ");
                    Main.Player = tmp.Player;
                    State = 1;
                }
            }
        }

        public override int Run()
        {
            Main.GsConnect.Register(this, 0x11);
            Main.GsConnect.Send(new S1638(Main.PlayerSessionId));
            Main.GsConnect.Send(new S1606(Main.PlayerSessionId));
            Main.GsConnect.Send(new S0010ListCharacter(Main.PlayerSessionId));
            Wait();
            if (State == 1)
            {
                Output.SetNamePlayerSelect(Main.Player.Name);
                return 1;
            }
            else
            {
                Break?.Invoke("Empty Character");
            }
            return 0;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (AccountLogin);
        }

        public override void Clean()
        {
            Main.GsConnect.Unregister(this);
        }
    }

    public class GetListCharConfig : IUnitConfig
    {
        public int IndexPlayet { get; set; } = 1;
    }

    public class GetListCharInput : IUnitInput
    {
    }

    public class GetListCharOutput : IUnitOutput
    {
        public event Action<string> SelectPlayer;

        internal void SetNamePlayerSelect(string name)
        {
            SelectPlayer?.Invoke(name);
        }
    }
}