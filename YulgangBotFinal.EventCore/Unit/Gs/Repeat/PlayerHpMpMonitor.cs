﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: PlayerHpMpMonitor.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Define;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Repeat
{
    public class PlayerHpMpMonitor : UnitRepeatProc<PlayerHpMpMonitorConfig, PlayerHpMpMonitorOutput>
    {
       
        #region Overrides of UnitRepeatProc<MonitorHpMpConfig,MonitorHpMpOutput>

        public PlayerHpMpMonitor(YbConfig cfg) : base(cfg)
        {
        }

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x69)
            {
                var tmp = ParsePacket<R0069UpgradeHpMpSp>(p);
                Main.Hp.Current = tmp.Info.CurrentHp;
                Main.Hp.Limit = tmp.Info.MaxHp;
                Main.Mp.Current = tmp.Info.CurrentMp;
                Main.Mp.Limit = tmp.Info.MaxMp;
                Main.Sp.Current = tmp.Info.CurrentSp;
                Main.Sp.Limit = tmp.Info.MaxSp;

                //Log.Info(Main.Username, $"Hp : {Main.Hp}");
                //Log.Info(Main.Username, $"Mp : {Main.Mp}");
                //Log.Info(Main.Username, $"Sp : {Main.Sp}");
                //Log.Info(Main.Username, $"Level : {Main.Player.Level}");
                Output.SetHpMp(Main.Hp, Main.Mp);
                if (Main.Hp.Current == 0)
                {
                    Log.Debug(Main.Username, "Player die");
                    Main.GsConnect.Send(new S0048SelecDieType(Main.PlayerSessionId).SendDie(Main.Location));
                }
            }
            if (p.Opcode == 0x3B)
            {
                State = 1;
            }
            if (p.Opcode == 0x6a)
            {
                var tmp = ParsePacket<R006AExpInfo>(p);
                Main.Player.CurrentExpNormal = tmp.CurrentExp;
                Main.Player.MaxExpNormal = tmp.MaxExp;
                Output.SetExp(new ScrollInfo() {Current = (int) tmp.CurrentExp, Limit = (int) tmp.MaxExp});
            }
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }

        /// <summary>
        ///     hàm chạy tiến trình
        /// </summary>
        /// <param name="time"></param>
        public override void Run(DateTime time)
        {
            if ((time - LastRun).TotalMilliseconds > 50)
            {
                LastRun = time;
                //todo: something

                // lấy danh sách item trong túi

                var items = new List<IYulgangGameItem>();
                var tmp = Main.Inventory.GetEnumerator();
                while (tmp.MoveNext())
                {
                    items.Add(tmp.Current.Value);
                }

                if (Main.Hp.Percent < Config.PercentHp)
                {
                    // sử dụng Item hồi phục Hp
                    var item = items.FirstOrDefault(m => Config.IdItemHp.Contains((int) m.BasicInfo.Id));
                    if (item != null)
                    {
                        Main.GsConnect.Send(new S003AUseItem(Main.PlayerSessionId).Send(1, (int) item.BasicInfo.Id,
                            (int) item.Count, item.Index));
                        State = 0;

                        Wait(5000);
                        Log.Debug(Main.Username, $"Sử dụng item hồi phục Hp {item.BasicInfo.Name}");
                    }
                    else
                    {
                        Log.Fatal(Main.Username, $"Hết item hồi phục Hp rồi {items.Count}");
                        Thread.Sleep(500);
                    }
                }

                if (Main.Mp.Percent < Config.PercentMp)
                {
                    // sử dụng Item hồi phục Mp
                    var item = items.FirstOrDefault(m => Config.IdItemMp.Contains((int)m.BasicInfo.Id));
                    if (item != null)
                    {
                        Main.GsConnect.Send(new S003AUseItem(Main.PlayerSessionId).Send(1, (int)item.BasicInfo.Id,
                            (int)item.Count, item.Index));
                        State = 0;

                        Wait(5000);
                        Log.Debug(Main.Username, $"Sử dụng item hồi phục Hp {item.BasicInfo.Name}");
                    }
                    else
                    {
                        Log.Fatal(Main.Username, $"Hết item hồi phục Hp rồi {items.Count}");
                        Thread.Sleep(500);
                    }
                }
                // Debug.WriteLine("Run");
            }

            Alive = false;
        }

        public override void Init()
        {
            Main.GsConnect.Register(this, 0x69, 0x3B, 0x6A);
        }

        public override Type RunAfter()
        {
            return typeof (EnterWorld);
        }

        #endregion
    }

    public class PlayerHpMpMonitorConfig : IUnitConfig
    {
        /// <summary>
        ///     % Hp còn lại sẽ sử dụng item
        /// </summary>
        public float PercentHp { get; set; } = 70.0f;

        /// <summary>
        ///     % Mp còn lại sẽ sử dụng item
        /// </summary>
        public float PercentMp { get; set; } = 20.0f;

        public List<int> IdItemHp { get; set; } = new List<int>() {1008000174, 1008000107, 1000000103};
        public List<int> IdItemMp { get; set; } = new List<int>() {1008000175};
    }

    public class PlayerHpMpMonitorOutput : IUnitOutput
    {
        public event Action<ScrollInfo, ScrollInfo> UpdateHpMp;
        public event Action<ScrollInfo> UpdateExp;

        internal void SetExp(ScrollInfo exp)
        {
            UpdateExp?.Invoke(exp);
        }

        internal void SetHpMp(ScrollInfo hp, ScrollInfo mp)
        {
            UpdateHpMp?.Invoke(hp, mp);
        }
    }
}