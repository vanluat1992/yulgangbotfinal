﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: MonsterMonitor.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Repeat
{
    public class MonsterMonitor : UnitRepeatProc<MonsterMonitorConfig, MonsterMonitorOutput>
    {
       
        #region Overrides of UnitRepeatProc<MonsterMonitorConfig,MonsterMonitorOutput>

        public MonsterMonitor(YbConfig cfg) : base(cfg)
        {
        }

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 0x67:
                {
                    var tmp = ParsePacket<R0067InitializerNpc>(p);
                    foreach (var npc in tmp.Npcs.Values)
                    {
                        IYulgangYbiGameMonster mt;
                        if (Ybi.DicMonsters.TryGetValue(npc.BasicInfo.Id, out mt))
                        {
                            npc.BasicInfo = mt;
                            if (!Main.Npcs.ContainsKey(npc.Id))
                            {
                                Main.Npcs.TryAdd(npc.Id, npc);
                                Log.Debug(Main.Username,
                                    $"Thêm mới thông tin mob {npc.BasicInfo.Name} ({npc.BasicInfo.Level} .lv)");
                                Output.AddNpc(npc);
                            }
                        }
                    }
                    break;
                }
                case 0x74:
                {
                    var tmp = ParsePacket<R0074NpcMove>(p);
                    IYulgangNpc npc;
                    Main.Npcs.TryGetValue(p.SessionId, out npc);
                    if (npc != null)
                    {
                        npc.CurrentHp = tmp.NpcMoves.Hp;
                        npc.Location = tmp.NpcMoves.Location;
                        if (Main.MobTarget?.Id == npc.Id)
                                Output.SetTargetHp(npc.CurrentHp);
                        Output.UpdateNpc(npc);
                    }
                    break;
                }
                case 0x68:
                {
                    var tmp = ParsePacket<R0068ReleaseNpc>(p);
                    foreach (var npcId in tmp.NpcIds)
                    {
                        IYulgangNpc npc;
                        Main.Npcs.TryRemove(npcId, out npc);
                        if (npc != null)
                        {
                            Log.Debug(Main.Username, $"Remove Mob {npc.BasicInfo.Name} -- {npcId}");
                            Output.RemoveNpc(npc);
                        }
                    }
                    break;
                }
                case 0x7B:
                {
                    var tmp = ParsePacket<R007BReviveMonster>(p);
                    foreach (var npc in tmp.Npcs.Values)
                    {
                        IYulgangYbiGameMonster mt;
                        if (Ybi.DicMonsters.TryGetValue(npc.BasicInfo.Id, out mt))
                        {
                            npc.BasicInfo = mt;
                            if (!Main.Npcs.ContainsKey(npc.Id))
                            {
                                Main.Npcs.TryAdd(npc.Id, npc);
                                Log.Debug(Main.Username,
                                    $"Thêm mới thông tin mob {npc.BasicInfo.Name} ({npc.BasicInfo.Level} .lv)");
                                Output.AddNpc(npc);
                            }
                        }
                    }
                    break;
                }
                case 0x88:
                {
                    IYulgangNpc tmp;
                    Main.Npcs.TryRemove(p.SessionId, out tmp);
                    if (tmp != null)
                    {
                        Log.Debug(Main.Username, $"Mob {tmp.BasicInfo.Name} đã chết");
                        if (Main.MobTarget != null && Main.MobTarget.Id == tmp.Id)
                        {
                            Main.MobTarget = null;
                        }
                        Output.RemoveNpc(tmp);
                    }
                    break;
                }
                case 0x0A:
                {
                    var tmp = ParsePacket<R000AAttackObject>(p);
                    IYulgangNpc npc;
                    Main.Npcs.TryGetValue(tmp.MobId, out npc);
                    if (npc != null)
                    {
                        npc.CurrentHp -= tmp.Dame;
                        if (npc.CurrentHp <= 0)
                            npc.CurrentHp = 0;

                        if (Main.MobTarget?.Id == npc.Id)
                                Output.SetTargetHp(npc.CurrentHp);
                    }
                    break;
                }
            }
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
            Main.Npcs.Clear();
        }

        /// <summary>
        ///     hàm chạy tiến trình
        /// </summary>
        /// <param name="time"></param>
        public override void Run(DateTime time)
        {
            Alive = true;
            Alive = false;
        }

        public override void Init()
        {
            Main.GsConnect.Register(this, 0x67, 0x68, 0x74, 0x7B, 0x88, 0x0A);
        }

        public override Type RunAfter()
        {
            return typeof (GetListChar);
        }

        #endregion
    }

    public class MonsterMonitorConfig : IUnitConfig
    {
    }

    public class MonsterMonitorOutput : IUnitOutput
    {
        public event Action<int> UpdateTargetHp;
        public event Action<IYulgangNpc> OnAddNpc;
        public event Action<IYulgangNpc> OnRemoveNpc;
        public event Action<IYulgangNpc> OnUpdateNpc;

        internal void AddNpc(IYulgangNpc npc) => OnAddNpc?.Invoke(npc);
        internal void RemoveNpc(IYulgangNpc npc) => OnRemoveNpc?.Invoke(npc);
        internal void UpdateNpc(IYulgangNpc npc) => OnUpdateNpc?.Invoke(npc);

        internal void SetTargetHp(int hp)
        {
            UpdateTargetHp?.Invoke(hp);
        }
    }
}