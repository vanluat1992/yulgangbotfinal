﻿// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.EventCore
// FILENAME : PlayerQuestMonitor.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 11:37 PM
// **************************************************************************

using System;
using System.Collections.Generic;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;

namespace YulgangBotFinal.EventCore.Unit.Gs.Repeat
{
    public class PlayerQuestMonitor : UnitRepeatProc<PlayerQuestMonitorConfig, PlayerQuestMonitorOutput>
    {
        private bool _loadQuestDoingOk;
        private bool _loadQuestDoneOk;

        public PlayerQuestMonitor(YbConfig cfg) : base(cfg)
        {
        }

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 0x85: // quest dang làm
                {
                    var packet = ParsePacket<R0085QuestDoing>(p);
                    Main.QuestDoing = new Dictionary<int, IYulgangPlayerQuest>();
                    foreach (var q in packet.Quest)
                        Main.QuestDoing.Add(q.Key, q.Value);
                    _loadQuestDoingOk = true;
                    if (_loadQuestDoneOk)
                        Main.LoadPlayerQuestOk = true;
                }
                    break;
                case 0x8B: // quest đã xong
                {
                    var packet = ParsePacket<R008BQuestDone>(p);
                    Main.QuestDone = new List<int>();
                    foreach (var i in packet.QuestDone)
                        Main.QuestDone.Add(i);
                    _loadQuestDoneOk = true;
                    if (_loadQuestDoingOk)
                            Main.LoadPlayerQuestOk = true;
                    }
                    break;
                case 0x84: // trình tự nhận quest
                    break;
            }
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }

        /// <summary>
        ///     hàm chạy tiến trình
        /// </summary>
        /// <param name="time"></param>
        public override void Run(DateTime time)
        {
            Alive = true;
            Alive = false;
        }

        public override void Init()
        {
            Main.GsConnect.Register(this, 0x85, 0x8b);
        }

        public override Type RunAfter()
        {
            return typeof(GetListChar);
        }
    }

    public class PlayerQuestMonitorOutput : IUnitOutput
    {
    }

    public class PlayerQuestMonitorConfig : IUnitConfig
    {
    }
}