﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 10:15 PM 19/09/2016
// FILENAME: PartyMonitor.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Collections.Generic;
using System.Linq;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Repeat
{
    /// <summary>
    ///     quản lý thông tin nhóm
    /// </summary>
    public class PartyMonitor : UnitRepeatProc<PartyMonitorConfig, PartyMonitorOutput>
    {
        public PartyMonitor(YbConfig cfg) : base(cfg)
        {
        }

        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x31)
            {
                var tmp = ParsePacket<R0031InviteParty>(p);
                IYulgangPlayer player;
                if (Main.AllPlayer.TryGetValue(tmp.SessionLead, out player))
                {
                    if (player.Name == Config.LeaderName)
                    {
                        Log.Debug(Main.Username, $"Có yêu cầu mời vào nhóm từ thành viên {player.Name}");
                        Main.GsConnect.Send(new S0034ConfirmInviteParty(Main.PlayerSessionId).Send(1, 1,
                            tmp.SessionLead, tmp.NameLeader));
                    }
                    //else
                    //{
                    //    Main.GsConnect.Send(new S0034ConfirmInviteParty(Main.PlayerSessionId).Send(1, 2,
                    //       tmp.SessionLead, tmp.NameLeader));
                    //}
                }
            }
            if (p.Opcode == 0x35)
            {
                var tmp = ParsePacket<R0035ConfirmInviteParty>(p);
                if (tmp.Result == 1)
                {
                    Log.Debug(Main.Username, "Mời thành công player");
                    State = 1;
                }
            }
            if (p.Opcode == 0x78 && p.SessionId == Main.PlayerSessionId)
            {
                var tmp = ParsePacket<R0078ListMemberParty>(p);
                if (!Main.Partys.ContainsKey((int) tmp.Leader.Id))
                    Main.Partys.TryAdd((int) tmp.Leader.Id, tmp.Leader);
                Main.Leader = tmp.Leader;
                Log.Info(Main.Username, $"Leader : {tmp.Leader.Name}  {tmp.Leader.CurrentHp}/{tmp.Leader.MaxHp}");
                foreach (var member in tmp.Members)
                {
                    Log.Info(Main.Username, $"Mem : {member.Name}  {member.CurrentHp}/{member.MaxHp}");

                    if (!Main.Partys.ContainsKey((int) member.Id))
                        Main.Partys.TryAdd((int) member.Id, member);
                }
            }
            if (p.Opcode == 0x37)
            {
                var tmp = ParsePacket<R0037LeaveParty>(p);

                YulgangPartyMember player;
                if (Main.Partys.TryRemove(tmp.Session, out player))
                    Log.Debug(Main.Username, $"Player {player.Name} rời nhóm");

                if (Main.AllPlayer.Count == 1)
                {
                    Main.Partys.Clear();
                    Log.Debug(Main.Username, $"Nhóm giải tán");
                }
            }
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }

        public override void Run(DateTime time)
        {
            if ((time - LastRun).TotalMilliseconds > 5000 && Config.LeaderName == Main.Player.Name)
            {
                LastRun = time;
                var allPlayer = new List<IYulgangPlayer>();
                var tmp = Main.AllPlayer.GetEnumerator();
                while (tmp.MoveNext())
                {
                    if (tmp.Current.Value != null)
                        allPlayer.Add(tmp.Current.Value);
                }
                foreach (var name in Config.OnlyAcceptWhenLeaderName.Where(m => m != Main.Player.Name))
                {
                    var player = allPlayer.FirstOrDefault(m => m.Name == name);
                    if (player != null && !Main.Partys.ContainsKey((int) player.Id))
                    {
                        Main.GsConnect.Send(new S0030InviteParty(Main.PlayerSessionId).Send((int) player.Id, 1,
                            player.Name));
                        Log.Debug(Main.Username, $"Gưởi yêu cầu mới player {player.Name} vào nhóm");
                        State = 0;
                        Wait(10000);
                    }
                }
            }
        }

        public override void Init()
        {
            Main.GsConnect.Register(this, 0x31, 0x78, 0x35, 0x37);
        }

        public override Type RunAfter()
        {
            return typeof (EnterWorld);
        }
    }

    public class PartyMonitorConfig : IUnitConfig
    {
        /// <summary>
        ///     Đồng ý mọi lời mời
        /// </summary>
        public bool AcceptAnyParty { get; set; }

        public IList<string> OnlyAcceptWhenLeaderName { get; set; } = new List<string>()
        {
            "hmt1",
            "hmt2",
            "hmt3",
            "hmt4",
            "hmt5",
            "hmt6",
            "htm7",
            "hmt8"
        };

        public string LeaderName { get; set; } = "hmt1";

        public bool RuntoLeader { get; set; } = true;
    }

    public class PartyMonitorOutput : IUnitOutput
    {
    }
}