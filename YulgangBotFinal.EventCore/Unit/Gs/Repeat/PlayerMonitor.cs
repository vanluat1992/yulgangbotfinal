﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: PlayerMonitor.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Repeat
{
    public class PlayerMonitor : UnitRepeatProc<PlayerHpMpMonitorConfig, PlayerMonitorOutput>
    {
        //private readonly IYulgangSyncDictionary<long, IYulgangPlayer> _allPlayer =
        //    new YulgangSyncDictionary<long, IYulgangPlayer>(); 
        

        #region Overrides of UnitRepeatProc<PlayerHpMpMonitorConfig,PlayerHpMpMonitorOutput>

        public PlayerMonitor(YbConfig cfg) : base(cfg)
        {
        }

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x64)
            {
                var tmp = ParsePacket<R0064InitCharacterOrPet>(p);
                foreach (var player in tmp.Players)
                {
                    if (player.Key == Main.PlayerSessionId)
                    {
                        Main.Player = player.Value;
                        Log.Debug(Main.Username, "Cập nhật nhân vật chính");
                        Main.Location = player.Value.Location;
                        Output.SetPlayerMain(Main.Player);
                    }
                    else
                    {
                        Log.Debug(Main.Username,
                            $"Thêm hoặc cập nhật mới nhân vật : {player.Value.Name} ({player.Value.Level} .lv) -- {player.Key}");
                        if (!Main.AllPlayer.ContainsKey(player.Key))
                        {
                            Main.AllPlayer.TryAdd(player.Key, player.Value);
                            Output.AddPlayer(player.Value);
                        }
                        else
                        {
                            Main.AllPlayer[player.Key] = player.Value;
                            Output.UpdatePlayer(player.Value);
                        }
                    }
                }
            }
            if (p.Opcode == 0x63)
            {
                var tmp = ParsePacket<R0063ReleasePlayer>(p);
                foreach (var playerId in tmp.PlayerIds)
                {
                    IYulgangPlayer tmpRemove;
                    if (Main.AllPlayer.TryRemove(playerId, out tmpRemove))
                    {
                        Log.Debug(Main.Username, $"Gỡ bỏ nhân vật id {playerId} - {tmpRemove.Name}");
                        Output.RemovePlayer(tmpRemove);
                    }
                }
            }
            if (p.Opcode == 0x65)
            {
                var tmp = ParsePacket<R0065PlayerMove>(p);
                if (p.SessionId == Main.PlayerSessionId)
                {
                    Log.Debug(Main.Username, $"Nhân vật di chuyển thành công từ {Main.Location} - > {tmp.ToLocation}");
                    Main.Location = tmp.ToLocation;
                }
                if (Main.AllPlayer.ContainsKey(p.SessionId))
                {
                    Main.AllPlayer[p.SessionId].Location = tmp.ToLocation;
                    Output.UpdatePlayer(Main.AllPlayer[p.SessionId]);
                }
            }

            if (p.Opcode == 0x79 && p.SessionId == Main.PlayerSessionId)
            {
                var tmp = ParsePacket<R0079SetPlayerLocation>(p);
                Log.Debug(Main.Username, $"Player được thiết lập tới vị trí {tmp.Location}");
                Main.Location.X = tmp.Location.X;
                Main.Location.Y = tmp.Location.Y;
                Main.Location.Z = 15f;
                Main.Location.MapId = tmp.Location.MapId;
                //Main.GsConnect.Send(new S008F(Main.PlayerSessionId));
                //Main.GsConnect.Send(new S001D(Main.PlayerSessionId).Send(Main.Location));
                //Main.GsConnect.Send(new S00E0(Main.PlayerSessionId));
                Output.SetPlayerMain(Main.Player);
            }
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }

        /// <summary>
        ///     hàm chạy tiến trình
        /// </summary>
        /// <param name="time"></param>
        public override void Run(DateTime time)
        {
            Alive = false;
        }

        public override void Init()
        {
            Main.GsConnect.Register(this, 0x64, 0x63, 0x65, 0x79);
        }

        public override Type RunAfter()
        {
            return typeof (GetListChar);
        }

        #endregion
    }

    public class PlayerMonitorConfig : IUnitConfig
    {
    }
    
    public class PlayerMonitorOutput : IUnitOutput
    {
        public event Action<IYulgangPlayer> UpdateMainPlayer;
        public event Action<IYulgangPlayer> OnAddPlayer;
        public event Action<IYulgangPlayer> OnRemovePlayer;
        public event Action<IYulgangPlayer> OnUpdatePlayer;
        internal void AddPlayer(IYulgangPlayer player) => OnAddPlayer?.Invoke(player);
        internal void RemovePlayer(IYulgangPlayer player) => OnRemovePlayer?.Invoke(player);
        internal void UpdatePlayer(IYulgangPlayer player) => OnUpdatePlayer?.Invoke(player);
        
        internal void SetPlayerMain(IYulgangPlayer p)
        {
            UpdateMainPlayer?.Invoke(p);
        }
    }
}