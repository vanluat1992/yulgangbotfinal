﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 9:54 PM 16/09/2016
// FILENAME: InventoryMonitor.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.EventCore.Unit.Gs.Train;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Repeat
{
    public class InventoryMonitor : UnitRepeatProc<InventoryMonitorConfig, InventoryMonitorOutput>
    {
        public InventoryMonitor(YbConfig cfg) : base(cfg)
        {
        }

        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x71 && p.SessionId == Main.PlayerSessionId)
            {
                var tmp = ParsePacket<R0071ItemInventory>(p);
                if (tmp.Type == 0x1)
                {
                    Log.Fatal(Main.Username, $"Cập nhật thông tin túi đồ {tmp.Items.Count} -- {tmp.Type.ToString("X4")}");
                    Main.Inventory.Clear();
                    foreach (var it in tmp.Items.Values)
                    {
                        IYulgangYbiGameItem item;
                        if (Ybi.DicItems.TryGetValue(it.BasicInfo.Id, out item))
                        {
                            it.BasicInfo = item;
                            it.Name = item.Name;
                            it.Forces = item.Force;
                            if (!Main.Inventory.ContainsKey(it.Index))
                            {
                                Main.Inventory.TryAdd(it.Index, it);
                                Log.Debug(Main.Username, $"Item Slot {it.Index}- {it.Name} -- {it.BasicInfo.Id}");
                            }
                            else
                            {
                                Log.Error(Main.Username, $"Trùng item túi đồ");
                            }
                        }
                    }
                }
                return;
            }
            if (p.Opcode == 0x7C)
            {
                var tmp = ParsePacket<R007CMoneyAndWeight>(p);
                Main.Money = tmp.Money;
                Main.Weight.Current = tmp.CurrentW;
                Main.Weight.Limit = tmp.MaxW;
                Output.SetMoney((int) Main.Money);
                return;
            }
            if (p.Opcode == 0x22)
            {
                var tmp = ParsePacket<R0022DeleteBagItem>(p);
                UpdateSlotItem(tmp.Index, tmp.Count, tmp.CountExist);
                return;
            }
            if (p.Opcode == 0x3B && p.SessionId == Main.PlayerSessionId)
            {
                var tmp = ParsePacket<R003BUseItem>(p);
                if (tmp.Command == 1)
                    UpdateSlotItem(tmp.Index, tmp.Count, tmp.CountExist);
                return;
            }
            if (p.Opcode == 0x93)
            {
                var tmp = ParsePacket<R0093BuyOrSellNpcItem>(p);
                var command = (ComfirmNpcBuySell) tmp.Command;
                switch (command)
                {
                    case ComfirmNpcBuySell.SellSuccess:
                    {
                        // bán đồ thành công
                        UpdateSlotItem(tmp.Item.Index, 0, 0);
                        break;
                    }
                    case ComfirmNpcBuySell.BuySuccess:
                        if (!UpdateSlotItem(tmp.Item.Index, (int) tmp.Item.Count))
                        {
                            IYulgangYbiGameItem item;
                            if (Ybi.DicItems.TryGetValue(tmp.Item.BasicInfo.Id, out item))
                            {
                                tmp.Item.BasicInfo = item;
                                tmp.Item.Name = item.Name;
                                tmp.Item.Forces = item.Force;
                                if (!Main.Inventory.ContainsKey(tmp.Item.Index))
                                {
                                    Main.Inventory.TryAdd(tmp.Item.Index, tmp.Item);
                                    Log.Debug(Main.Username,
                                        $"Item Slot {tmp.Item.Index}- {tmp.Item.Name} -- {tmp.Item.BasicInfo.Id}");
                                }
                                else
                                {
                                    Log.Error(Main.Username, $"Trùng item túi đồ");
                                }
                            }
                        }
                        break;
                    default: // lỗi đếu mua hay bán dc
                        break;
                }
            }
            if (p.Opcode == 0x82)
            {
                var packet = ParsePacket<R0082AddItemQuest>(p);
                Ybi.DicItems.TryGetValue(packet.ItemId, out IYulgangYbiGameItem baseItem);
                if (baseItem != null)
                {
                    Log.Success("QUEST", $" Nhận được item quest : {baseItem.Name} ({packet.Count})");
                }
            }
        }

        private bool UpdateSlotItem(int index, int add)
        {
            IYulgangGameItem it;
            if (Main.Inventory.TryGetValue(index, out it))
            {
                it.Count += add;
                Log.Debug(Main.Username,
                    $"Cập nhật lại item ở slot {index} , Số lượng thêm {add} , tổng cộng {it.Count}");
                return true;
            }
            return false;
        }

        private bool UpdateSlotItem(int index, int count, int exist)
        {
            Log.Debug(Main.Username, $"Cập nhật lại item ở slot {index} , Số lượng {count}, Còn lại {exist}");
            IYulgangGameItem it;
            if (Main.Inventory.TryGetValue(index, out it))
            {
                if (exist == 0)
                {
                    Main.Inventory.TryRemove(it.Index, out it);
                    return true;
                }
                it.Count = exist;
                return true;
            }
            else
            {
                return false;
            }
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }

        public override void Run(DateTime time)
        {
            Alive = true;
            Alive = false;
        }

        public override void Init()
        {
            Main.GsConnect.Register(this, 0x71, 0x7C, 0x22, 0x3B, 0x93,0x82,0x81);
        }

        public override Type RunAfter()
        {
            return typeof (GetListChar);
        }
    }

    public class InventoryMonitorConfig : IUnitConfig
    {
    }

    public class InventoryMonitorOutput : IUnitOutput
    {
        public event Action<int> UpdateMoney;

        internal void SetMoney(int money)
        {
            UpdateMoney?.Invoke(money);
        }
    }
}