﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 9:59 PM 19/09/2016
// FILENAME: ForceLogout.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Repeat
{
    /// <summary>
    ///     Server yêu cầu logout
    /// </summary>
    public class ForceLogout : UnitRepeatProc<ForceLogoutConfig, ForceLogoutOutput>
    {
        public ForceLogout(YbConfig cfg) : base(cfg)
        {
        }

        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x4)
            {
                State = 1;
            }
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }

        public override void Run(DateTime time)
        {
            if (State == 1)
            {
                // Yêu cầu hệ thống đăng nhập lại
                Log.Debug(Main.Username, $"Tài khoản đã bị hệ thống đá ra , yêu cầu đăng nhập lại ngay");
                Main.RequestLoginAgain = true;
                State = 0;
            }
        }

        public override void Init()
        {
            Main.GsConnect?.Register(this, 0x4);
        }

        public override Type RunAfter()
        {
            return typeof (GetListChar);
        }
    }

    public class ForceLogoutConfig : IUnitConfig
    {
    }

    public class ForceLogoutOutput : IUnitOutput
    {
    }
}