﻿// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.EventCore
// FILENAME : BuffMonitor.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 11/03/2017 3:55 PM
// **************************************************************************

using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Network.Packet.Gs.Recv;

namespace YulgangBotFinal.EventCore.Unit.Gs.Repeat
{
    public class BuffMonitor : UnitRepeatProc<BuffMonitorConfig, BuffMonitorOutput>
    {
        public BuffMonitor(YbConfig cfg) : base(cfg)
        {
        }

        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x87)
            {
                var tmp = ParsePacket<R0087ActiveBuff>(p);
                if (tmp.Active)
                {
                    if (Main.Buff.ContainsKey(tmp.Id))
                        Main.Buff[tmp.Id] = DateTime.Now;
                    else
                        Main.Buff.TryAdd(tmp.Id, DateTime.Now);
                }
                else
                {
                    DateTime dt;
                    Main.Buff.TryRemove(tmp.Id, out dt);
                }
            }
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }

        public override void Run(DateTime time)
        {
        }

        public override void Init()
        {
            Main.GsConnect?.Register(this, 0x87);
        }

        public override Type RunAfter()
        {
            return typeof(EnterWorld);
        }
    }

    public class BuffMonitorConfig : IUnitConfig
    {
    }

    public class BuffMonitorOutput : IUnitOutput
    {
    }
}