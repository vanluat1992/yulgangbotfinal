﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 9:21 PM 24/09/2016
// FILENAME: DropItemMonitor.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Repeat
{
    public class DropItemMonitor : UnitRepeatProc<DropItemMonitorConfig, DropItemMonitorOutput>
    {
        public DropItemMonitor(YbConfig cfg) : base(cfg)
        {
        }

        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x72)
            {
                var tmp = ParsePacket<R0072InitializerItemDrop>(p);
                if (tmp?.Items != null)
                {
                    foreach (var yulgangItemDrop in tmp.Items)
                    {
                        if (!Main.ItemDrop.ContainsKey(yulgangItemDrop.Id))
                        {
                            IYulgangYbiGameItem bItem;
                            Ybi.DicItems.TryGetValue(yulgangItemDrop.BasicInfo.Id, out bItem);
                            if (bItem != null)
                            {
                                Log.Debug(Main.Username, $"Add item drop : {bItem.Name}");
                                yulgangItemDrop.BasicInfo = bItem;
                                yulgangItemDrop.Location.MapId = Main.Location.MapId;
                                Main.ItemDrop.TryAdd(yulgangItemDrop.Id, yulgangItemDrop);
                            }
                        }
                    }
                }
            }
            if (p.Opcode == 0x73)
            {
                var tmp = ParsePacket<R0073ReleaseItemDrop>(p);
                if (tmp != null)
                {
                    IYulgangItemDrop dItem;
                    Main.ItemDrop.TryRemove(tmp.SessionItem, out dItem);
                    if (dItem != null)
                        Log.Debug(Main.Username, $"Remove item drop: {dItem.Name}");
                }
            }
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }

        public override void Run(DateTime time)
        {
        }

        public override void Init()
        {
            Main.GsConnect?.Register(this, 0x72, 0x73);
        }

        public override Type RunAfter()
        {
            return typeof (EnterWorld);
        }
    }

    public class DropItemMonitorConfig : IUnitConfig
    {
    }

    public class DropItemMonitorOutput : IUnitOutput
    {
    }
}