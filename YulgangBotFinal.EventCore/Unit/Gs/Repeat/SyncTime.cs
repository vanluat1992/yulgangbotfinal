﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: SyncTime.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Repeat
{
    public class SyncTime : UnitRepeatProc<SyncTimeConfig, SyncTimeOutput>
    {
        private int _time = 0;

      

        #region Overrides of UnitRepeatProc<SyncTimeConfig,SyncTimeOutput>

        public SyncTime(YbConfig cfg) : base(cfg)
        {
        }

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x80)
            {
                var tmp = ParsePacket<R0080SyncServerTime>(p);
                _time = tmp.TimeTick;
            }
            if (p.Opcode == 0x6)
            {
                // send 1606
                Main.GsConnect.Send(new S1606(Main.PlayerSessionId));
                Main.GsConnect.Send(new S0211(Main.PlayerSessionId));
                Main.GsConnect.Send(new S003CUsingSpellBuff(Main.PlayerSessionId).SendDefault(Main.Player.Location));
            }
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }

        /// <summary>
        ///     hàm chạy tiến trình
        /// </summary>
        /// <param name="time"></param>
        public override void Run(DateTime time)
        {
            Alive = true;
            if ((time - LastRun).TotalMilliseconds > 10000)
            {
                LastRun = time;
                try
                {
                    var t = Environment.TickCount;
                    var result = t & 0xF000000;
                    _time = (int) (((result | ((0xF00000 & t | (t >> 4) & 0xF000000) >> 8)) >>
                                    12) | ((t & 0xFF0F | 16*(t & 0xFFFF0000 | ((t & 0xF0) << 8))) << 8));

                    Log.Debug(Main.Username, $"SyncTime : {_time.ToString("X4")}");
                    Main.GsConnect.Send(new S00B0SyscServerTime(Main.PlayerSessionId).Send(_time));
                }
                catch (Exception ex)
                {
                    Log.Debug(Main.Username, "Send 0xB : Rớt kết nối , yêu cầu đăng nhập lại");
                    Main.RequestLoginAgain = true;
                }
            }
            Alive = false;
        }

        public override void Init()
        {
            Main.GsConnect.Register(this, 0x80,0x6);
        }

        public override Type RunAfter()
        {
            return typeof (GetListChar);
        }

        #endregion
    }

    public class SyncTimeConfig : IUnitConfig
    {
    }

    public class SyncTimeOutput : IUnitOutput
    {
    }
}