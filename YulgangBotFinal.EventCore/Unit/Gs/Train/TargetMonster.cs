﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 2:35 PM 18/09/2016
// FILENAME: TargetMonster.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Train
{
    public class TargetMonster : UnitSequenceProc<TargetMonsterInput, TargetMonsterConfig, TargetMonsterOutput>
    {
        private int _targetId;

        public TargetMonster(YbConfig cfg) : base(cfg)
        {
        }

        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x1089)
            {
                var tmp = ParsePacket<R1089TargetObject>(p);
                _targetId = tmp.TargetId;
            }
        }

        public override event Action<string> Break;

        public override int Run()
        {
            Main.GsConnect.Register(this, 0x1089);

            if (Main.MobTarget != null)
            {
                // kiểm tra vị trí đã oke chưa
                if (Main.MobTarget.Location.DistanceTo(Main.Location) > Config.Range)
                {
                    Output.UptargetNpc(Main.MobTarget);
                    Main.MobTarget = null; // tiến hành chọn lại
                }
                else
                    return 1; // nếu đang target rồi thì bỏ qua
            }

            Output.SetTargetNpc(null);
            // tiến hành chọn quái vật theo cấu hình đã load sẵn
            if (Main.Npcs.Count == 0) // hiện tại ko có mob nào quanh đây
                return 1;
            // lấy danh sách mob xung quanh
            var monsterArround = new List<IYulgangNpc>();
            using (var tmp = Main.Npcs.GetEnumerator())
            {
                while (tmp.MoveNext())
                {
                    if (tmp.Current.Value.BasicInfo.Id > 1000 && !tmp.Current.Value.BasicInfo.Name.Contains("Bunny"))
                        //ko lấy npc
                        monsterArround.Add(tmp.Current.Value);
                }
            }
            var validmob =
                monsterArround.Where(m => m.Level - Main.Player.Level < Config.LevelGreaterThan)
                    .Where(m => m.Location.DistanceTo(Main.Location) <= Config.Range).ToList();

            if (validmob.Count == 0)
                return 1; // không có con quái nào hợp lệ

            // chọn lấy và target 1 con 
            var mob = validmob.First();
            Log.Debug(Main.Username, $"Target vào quái vật {mob.BasicInfo.Name} ({mob.BasicInfo.Level} .lv) - {mob.Id}");
            Main.GsConnect.Send(new S1088TargetObject(Main.PlayerSessionId, mob.Id));
            _targetId = 0;
            var t = DateTime.Now;
            while (_targetId != mob.Id)
            {
                if ((DateTime.Now - t).TotalMilliseconds > 30000)
                    return 1;

                Thread.Sleep(100);
            }
            Main.MobTarget = mob;
            Output.SetTargetNpc(mob);
            Log.Debug(Main.Username, $"Target thành công quái vật {mob.BasicInfo.Name} ({mob.BasicInfo.Level} .lv)");
            return 1;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (IdleBot);
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }
    }

    public class TargetMonsterInput : IUnitInput
    {
    }

    public class TargetMonsterConfig : IUnitConfig
    {
        /// <summary>
        ///     đánh quái chênh lệch cấp độ
        /// </summary>
        public int LevelGreaterThan { get; set; } = 5;

        /// <summary>
        ///     Đánh quái chủ động
        /// </summary>
        public bool ActiveMonster { get; set; } = true;

        /// <summary>
        ///     bán kình tìm quái
        /// </summary>
        public int Range { get; set; } = 200;
    }

    public class TargetMonsterOutput : IUnitOutput
    {
        public event Action<IYulgangNpc> TargetNpc;
        public event Action<IYulgangNpc> OnUntargetNpc;
        internal void UptargetNpc(IYulgangNpc npc) => OnUntargetNpc?.Invoke(npc);
        internal void SetTargetNpc(IYulgangNpc npc)
        {
            TargetNpc?.Invoke(npc);
        }
    }
}