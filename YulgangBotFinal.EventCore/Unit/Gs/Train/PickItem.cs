﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 12:10 AM 25/09/2016
// FILENAME: PickItem.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Train
{
    public class PickItem : UnitSequenceProc<PickItemInput, PickItemConfig, PickItemOutput>
    {
        private IDictionary<long, Tuple<DateTime, IYulgangItemDrop>> _failItem =
            new Dictionary<long, Tuple<DateTime, IYulgangItemDrop>>();

        public PickItem(YbConfig cfg) : base(cfg)
        {
        }

        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x0D)
            {
                var tmp = ParsePacket<R000DPickItemDrop>(p);
                if (tmp.Item != null)
                    State = 1;
            }
        }

        public override event Action<string> Break;

        public override int Run()
        {
            Main.GsConnect.Register(this, 0x0D);
            if (Main.Inventory.Count == 36) return 1; // full item

            var items = new List<IYulgangItemDrop>();
            var tmp = Main.ItemDrop.GetEnumerator();
            while (tmp.MoveNext())
            {
                items.Add(tmp.Current.Value);
            }

            var validItem = items.Where(m => m.Location.DistanceTo(Main.Location) < Config.Range).ToArray();
            if (!validItem.Any()) return 1;

            foreach (var yulgangItemDrop in validItem.Where(m => !_failItem.ContainsKey(m.Id)))
            {
                Tuple<DateTime, IYulgangItemDrop> failItem;
                if (_failItem.TryGetValue(yulgangItemDrop.Id, out failItem))
                {
                }
                else
                {
                    //goto item drop
                    var distance = Main.Location.DistanceTo(yulgangItemDrop.Location);
                    if (distance > 10)
                    {
                        
                        Main.GsConnect.Send(
                            new S0048SelecDieType(Main.PlayerSessionId).SendAdminMove(yulgangItemDrop.Location));
                        var last = DateTime.Now;
                        while (Main.Location.DistanceTo(yulgangItemDrop.Location) > 5)
                        {
                            if ((DateTime.Now - last).TotalMilliseconds > 10000)
                            {
                                Log.Warning(Main.Username, $"Move to dropitem Fail ");
                                break;
                            }
                            Thread.Sleep(10);
                        }
                    }
                    Main.GsConnect.Send(new S000BPickItemDrop(Main.PlayerSessionId).Send(yulgangItemDrop.Id));
                    Log.Debug(Main.Username, "Nhặt item");
                    State = 0;
                    Wait(5000);
                    if (State == 0)
                    {
                        // pick fail , set timeout and repick 
                        IYulgangItemDrop itRm;
                        Main.ItemDrop.TryRemove(yulgangItemDrop.Id, out itRm);
                        //_failItem.Add(yulgangItemDrop.Id,
                        //    new Tuple<DateTime, IYulgangItemDrop>(DateTime.Now, yulgangItemDrop));
                    }
                }
            }
            return 1;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (EnterWorld);
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }
    }

    public class PickItemConfig : IUnitConfig
    {
        public int Range { get; set; } = 500;
    }

    public class PickItemOutput : IUnitOutput
    {
    }

    public class PickItemInput : IUnitInput
    {
    }
}