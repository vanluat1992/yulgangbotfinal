﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 3:19 PM 18/09/2016
// FILENAME: AttackMob.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Train
{
    public class AttackMob : UnitSequenceProc<AttackMobInput, AttackMobConfig, AttackMobOutput>
    {
        private int _fail;
        private bool _success;
        private bool _isAttack;

        public AttackMob(YbConfig cfg) : base(cfg)
        {
        }

        public override void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0xA && p.SessionId == Main.PlayerSessionId)
            {
                var tmp = ParsePacket<R000AAttackObject>(p);
                Log.Info(Main.Username, $"Tấn công : {Main.MobTarget?.BasicInfo.Name} - {tmp.Dame}");
                if (tmp.Dame > 0)
                    _success = true;
            }
        }

        public override event Action<string> Break;

        private long _oldTarget = 0;
        public override int Run()
        {
            Main.GsConnect.Register(this, 0xA);
            if (Main.MobTarget == null)
            {

                _isAttack = false;
                return 1;
            }

            if (_fail >= 10)
            {
                Main.Npcs.TryRemove(Main.MobTarget?.Id ?? 0, out IYulgangNpc tmp);
                Main.MobTarget = null;
                _fail = 0;
                _isAttack = false;
                return 1;
            }
            // lựa chọn spell cần đánh
            var spellId = 0;
            // kiểm tra khoảng cách với mod
            var distance = Main.Location.DistanceTo(Main.MobTarget.Location);
            // Thay đổi vị trí cho phù hợp
            if (distance >= 5)
            {
                Main.RequestMove = Main.MobTarget.Location;
                Main.RequestMove.MapId = Main.Location.MapId;
                _isAttack = false;
                // di chuyển tới gần mob
                return 1;
            }
            if (_oldTarget != Main.MobTarget.Id) _isAttack = false;
            if (_isAttack) return 1;

            _isAttack = true;
            _oldTarget = Main.MobTarget.Id;
            // tấn công nó
            _success = false;
            Main.GsConnect.Send(new S0009AttackObject(Main.PlayerSessionId).Send(Main.MobTarget.Id, spellId,
                Main.Location));
            Log.Debug(Main.Username, $"Tấn công {Main.MobTarget.BasicInfo.Name}");
            Thread.Sleep(1000);
            if (_success == false)
            {
                _isAttack = false;
                _fail++;
            }
            else
            {
                _fail = 0;
            }
            return 1;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (IdleBot);
        }

        public override void Clean()
        {
            Main.GsConnect.Unregister(this);
        }
    }

    public class AttackMobInput : IUnitInput
    {
    }

    public class AttackMobConfig : IUnitConfig
    {
    }

    public class AttackMobOutput : IUnitOutput
    {
    }
}