﻿#region header
// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 12:23 AM 25/09/2016
// FILENAME: UsingSupportSpell.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

namespace YulgangBotFinal.EventCore.Unit.Gs.Train
{
    /// <summary>
    /// sử dụng võ công hỗ trợ
    /// </summary>
    public class UsingSupportSpell:UnitSequenceProc<UsingSupportSpellInput,UsingSupportSpellConfig,UsingSupportSpellOutput>
    {
        public UsingSupportSpell(YbConfig cfg) : base(cfg)
        {
        }

        public override void HandlePacket(IPacket p)
        {
            throw new NotImplementedException();
        }

        public override event Action<string> Break;
        public override int Run()
        {
            //todo: tạm thời làm cho đại phu trước
            switch (Main.Player.Job)
            {
                 default:
                    return 1;
                    case YulgangCharaterJobs.Healer:
                    BuffHealer();
                    return 1;
            }
            // lấy thông tin danh sách spell đang sử dụng

            // kiểm tra coi có spell nào được cấu hình sử dụng

            // coi đã hết thời gian chưa

            // xem có yêu cầu buff nhóm ko

            // kiểm tra mana
            // hoàn thành tiến trình

            return 1;
        }

        private bool _rebuff = false;
        /// <summary>
        /// xử lý buff cho đại phu 
        /// </summary>
        private void BuffHealer()
        {
            // Kiểm tra danh sách buff cơ bản coi có cái nào chưa , cái nào chưa , cái nào có rồi

            // nhúc nhíc 1 tý cho hợp lệ ko dzo nó đá ra
            MoveRandomTo(new YulgangLocation(5648, 1122, 201));
            foreach (var buff in Config.Buffs.Where(m => !m.IsAlway && m.IsPublic))
            {
                if (!Main.Buff.ContainsKey(buff.Id))
                {
                    // send buff
                    MoveRandomTo(new YulgangLocation(5648, 1122, 201));
                    Main.GsConnect.Send(new S0009AttackObject(Main.PlayerSessionId).Send(Main.PlayerSessionId, buff.Id,
                        Main.Location));
                    Log.Debug(Main.Username, $"Tiến hành buff spell {buff.Id}");

                    Thread.Sleep(1000);
                    Thread.Sleep(1000);
                    foreach (var member in Main.Partys)
                    {
                        IYulgangPlayer player;
                        if (Main.AllPlayer.TryGetValue(member.Key, out player))
                        {
                            Main.GsConnect.Send(new S0009AttackObject(Main.PlayerSessionId).Send(member.Key,0, buff.Id,
                                player.Location));
                            _rebuff = false;
                            Log.Debug(Main.Username, $"Tiến hành buff spell {buff.Id}  cho party {player.Name}");
                            Thread.Sleep(2000);
                        }
                       

                    }
                }
            }
            
            foreach (var buff in Config.Buffs.Where(m=>m.IsAlway))
            {
                Main.GsConnect.Send(new S0009AttackObject(Main.PlayerSessionId).Send(Main.PlayerSessionId,0, buff.Id,
                    Main.Location));

                _rebuff = true;
                Log.Debug(Main.Username, $"Tiến hành buff spell {buff.Id}");
                Thread.Sleep(2000);
            }
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (IdleBot);
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }
    }

    public class UsingSupportSpellInput:IUnitInput
    {

    }

    public class UsingSupportSpellConfig:IUnitConfig
    {
        public IList<SpellBuffInfo> Buffs { get; set; } = new List<SpellBuffInfo>()
        {
            new SpellBuffInfo()
            {
                Id=501301,// thái cực ngũ hình (dame 10%)
                Interval = 300,
                IsPublic=true
            },
            new SpellBuffInfo()
            {
                Id = 501303,//thái cực canh khí (def 10%)
                Interval = 300,
                IsPublic=true
            },
            new SpellBuffInfo()
            {
                Id = 501203 ,// buff nhóm
                Interval = 2,
                IsAlway = true,
                IsPublic=true
            }
        };
    }

    public class UsingSupportSpellOutput:IUnitOutput
    {

    }

    public class SpellBuffInfo
    {
        /// <summary>
        /// Id buff
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// thời gian giữa mỗi lần buff
        /// </summary>
        public int Interval { get; set; }
        public bool IsAlway { get; set; }

        public bool IsPublic { get; set; }

    }
}