﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.EventCore
// FileName : PlayerQuestAction.cs
// Time Create : 7:59 AM 30/03/2017
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;
using System.Linq;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Model;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource.Ybq;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Train
{
    public class PlayerQuestAction :
        UnitSequenceProc<PlayerQuestActionInput, PlayerQuestActionConfig, PlayerQuestActionOutput>
    {
        private QuestEvent _qEvent;

        public PlayerQuestAction(YbConfig cfg) : base(cfg)
        {
        }

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 0x91:
                {
                    var tmp = ParsePacket<R0091TalkNpc>(p);
                    State = 1;
                }
                    break;
                case 0x84: // trình tự nhận quest
                {
                    var tmp = ParsePacket<R0084Quest>(p);
                    switch (_qEvent)
                    {
                        case QuestEvent.Accept:
                            if (tmp.EventQuest == 21) // nhận quest thành công
                            {
                                Main.Quest.Step = tmp.Step;
                                State = 1;
                            }
                            break;
                        case QuestEvent.Finish:
                            if (tmp.EventQuest == 51)
                            {
                                State = 1;
                            }
                            break;
                        case QuestEvent.Working:
                        {
                            if (tmp.EventQuest == 11)
                            {
                                Main.Quest.Step = tmp.Step;
                                State = 1;
                            }
                        }
                            break;
                    }
                }
                    break;
            }
        }

        public override event Action<string> Break;

        public override int Run()
        {
            Main.GsConnect.Register(this, 0x84, 0x91);
            if (Main.AttackQuest) return 1;
            // lấy 1 cái quest đang làm
            if (Main.Quest == null)
                Main.Quest = Main.QuestDoing.Values.FirstOrDefault();
            if (Main.Quest == null)
            {
                // coi các quest hợp lệ đang có 
                var validQuest =
                    Quests.Values.Where(m => m.Config.LevelQuest > 1
                                             && m.Config.LevelQuest <= Main.Player.Level
                                             && (m.Config.Forces == 0 || m.Config.Forces == (int) Main.Player.Forces)
                                             && m.Config.Upgrade <= Main.Player.Upgrade
                                             && (m.Config.Type == 4 || m.Config.Type == 3)
                                             && m.Config.Job == 0
                                             && m.Config.IsDelete == 0
                                             && m.Config.Type == 3
                    ).Where(m =>
                        m.Config.Type == 3 && m.Config.Unknown5 == 1
                        || m.Config.Type == 4).ToList();

                // so sanh với danh sách quest đã hoàn thành
                var tmp = validQuest.FirstOrDefault(m => !Main.QuestDone.Contains(m.IdQuest));

                // xác đinh 1 quest hợp lệ để đi làm 
                if (tmp != null)
                {
                    Main.Quest = new YulgangPlayerQuest() {Id = tmp.IdQuest,};
                }
                else
                    return 1;
            }
            // lấy thông tin quest
            Quest q = null;
            if (!Quests.TryGetValue(Main.Quest.Id, out q))
            {
                // không thực hiện dc quest này
                return 1;
            }

            // xem quest đang ở bước nào 
            var step = Main.Quest.Step;

            // xác định mục tiêu 
            var w = Wrap.WLocations.FirstOrDefault(m => m.Id == q.ConfigMainQuest.Npc.NpcId);
            if (w == null)
                return 1;
            var npcLocation = new YulgangLocation(w.X, w.Y, w.Zone);
            Moveto(npcLocation);
            Log.Debug("QUEST", $"Di chuển đến {Ybi.DicMonsters[q.ConfigMainQuest.Npc.NpcId]?.Name}");

            // bắt đầu nói chuyện với npc
            if (!OpenQuestNpc(npcLocation, w.Id)) return 1;
            if (step == 0)
            {
                // quest này chưa nhận , h chạy về chỗ Npc để nhận Q

                Log.Debug("QUEST", $"Bắt đầu thực hiện quest :{q.NameQuest} ({q.Config.LevelQuest}) ");

                _qEvent = QuestEvent.Accept;
                // Nhận cái quest
                Main.GsConnect.Send(new S0083Quest(Main.PlayerSessionId).SendAccept(q.IdQuest));

                Wait();
                EndTalk((int) q.ConfigMainQuest.Npc.NpcId);
            }
            else
            {
                if (q.ConfigMainQuest.QuestStepCount == Main.Quest.Step)
                {
                    // hoàn thành quest
                    _qEvent = QuestEvent.Finish;
                    Main.GsConnect.Send(new S0083Quest(Main.PlayerSessionId).SendFinish(q.IdQuest));
                    Wait();
                    Main.QuestDoing.Remove(q.IdQuest);
                    Main.QuestDone.Add(q.IdQuest);
                    Main.Quest = null;
                    EndTalk((int) q.ConfigMainQuest.Npc.NpcId);
                    Wait();
                    Log.Success("QUEST", $"Hoàn thành quest :{q.NameQuest}");
                }
                else
                {
                    // thực hiện các bước quest ở đây;
                    if (Main.Quest.Step == 1)
                    {
                        // lựa chọn item yêu cầu đầu tiên
                        //var it = q.ConfigMainQuest.ListItemQuest.FirstOrDefault();
                        //if(it!=null)
                        //    Moveto(it.Npc[0].Position);
                        if (q.ConfigMainQuest.ListItemQuest.Count > 0)
                        {
                            var it = q.ConfigMainQuest.ListItemQuest.FirstOrDefault();
                            if (it.Npc[0].NpcId == 0)
                                Main.AttackQuest = true;
                        }
                    }
                }
            }

            //Break?.Invoke("Pause");
            return 1;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof(IdleBot);
        }

        public override void Clean()
        {
            Main.GsConnect.Unregister(this);
        }

        private bool OpenQuestNpc(IYulgangLocation location, int npcId)
        {
            if (!TalkWithNpc(location, npcId))
                return false;

            // mở bảng shop ra
            Main.GsConnect.Send(new S0090TalkWithNpc(Main.PlayerSessionId).Send(YulgangTalkNpcCommand.Openquest,
                npcId));
            Wait(15000);
            if (State == 0)
            {
                // như cẹc rồi , đóng trò chuyện

                Main.GsConnect.Send(new S0090TalkWithNpc(Main.PlayerSessionId).Send(YulgangTalkNpcCommand.Close,
                    npcId));
                return false;
            }
            return true;
        }
    }

    public class PlayerQuestActionOutput : IUnitOutput
    {
    }

    public class PlayerQuestActionConfig : IUnitConfig
    {
    }

    public class PlayerQuestActionInput : IUnitInput
    {
    }
}