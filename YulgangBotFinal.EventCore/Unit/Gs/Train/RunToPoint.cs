﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 5:19 PM 17/09/2016
// FILENAME: RunToPoint.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Train
{
    /// <summary>
    ///     chạy tới điểm cần đánh
    /// </summary>
    public class RunToPoint : UnitSequenceProc<RunToPointInput, RunToPointConfig, RunToPointOutput>
    {
        public RunToPoint(YbConfig cfg) : base(cfg)
        {
        }

        public override event Action<string> Break;

        public override void HandlePacket(IPacket p)
        {
            //if (p.Opcode == 0x79 && p.SessionId == Main.PlayerSessionId)
            //{
            //    var tmp = ParsePacket <R0079SetPlayerLocation>(p);
            //    Log.Debug(Main.Username, $"Player được thiết lập tới vị trí {tmp.Location}");
            //    Main.Location.X = tmp.Location.X;
            //    Main.Location.Y = tmp.Location.Y;
            //    Main.Location.Z = 15f;
            //    Main.Location.MapId = tmp.Location.MapId;
            //}
        }

        public override int Run()
        {
            //Main.GsConnect.Register(this, 0x79);
            if (Main.Location == null)
            {
                return -1;
            }
            // Main.GsConnect.Register(this, 0x65);
            var distacneSafePoint = Main.Location.DistanceTo(Input.DestionationPoint);
            IYulgangLocation toLocation = null;
            if (distacneSafePoint > Input.Range)
                toLocation = Input.DestionationPoint.Random();
            else toLocation = Main.RequestMove ??Input.DestionationPoint;
            if (toLocation == null)
                return 1; // ko cần chạy nữa vì chưa xác định dc điểm cần tới
            var distance = Main.Location.DistanceTo(toLocation);
            if (distance < Input.Range && Main.RequestMove == null)
                return 1; // oke đang trong khu vực cho phép

            Log.Debug(Main.Username, $"Di chuyển đến vị trí thiết lập {toLocation} ");
            // if (Main.RequestMove == null) // nếu di chuyển đánh quái
            //{
            //    Main.GsConnect.Send(new S0007PlayerMove(Main.PlayerSessionId).Send(Main.Location, toLocation));
            //    Thread.Sleep(2000);
            //}
            //else// di chuyển tới vị trí cấu hình sẵn (tọa độ đánh, npc mua máu , kho ...)
            Main.GsConnect.Send(new S0048SelecDieType(Main.PlayerSessionId).SendAdminMove(toLocation));

            var last = DateTime.Now;
            while (Main.Location.DistanceTo(toLocation) > 5)
            {
                // todo nên đặt timeout đề phòng ko send dc
                if ((DateTime.Now - last).TotalMilliseconds > 10000)
                {
                    Log.Warning(Main.Username, $"Move Fail ");
                    break;
                }
                Thread.Sleep(10);
            }
            Main.RequestMove = null;
            if (distacneSafePoint >= 500) // nếu khoảng cách bay quá xa thì nên reload lại
                return 1;
            return 1;
            // tính toán đường đi và chạy tới vị trí yêu cầu
            var arrayPoint = Main.PathFinder.Route(Main.Location, toLocation);
            var failCount = 0;
            IYulgangLocation old = null;
            for (var i = 0; i < arrayPoint.Count; i = i + 3)
            {
                var location = arrayPoint[i];
                location.MapId = Main.Location.MapId;
                Main.GsConnect.Send(new S0007PlayerMove(Main.PlayerSessionId).Send(Main.Location, location));
                Log.Debug(Main.Username, $"Di chuyển từ {Main.Location} đến {location}");

                var t = DateTime.Now;
                while (Main.Location.DistanceTo(location) > 5)
                {
                    // todo nên đặt timeout đề phòng ko send dc
                    if ((DateTime.Now - t).TotalMilliseconds > 10000)
                    {
                        Log.Warning(Main.Username, $"Move Fail  {failCount++}");
                        break;
                    }
                    Thread.Sleep(10);
                }
                Thread.Sleep(2000);
                old = location;
            }
            //_active = true;
            //foreach (var location in arrayPoint)
            //{
            //    Main.GsConnect.Send(new S0007PlayerMove(Main.PlayerSessionId).Send(old ?? Main.Location, location));
            //    Log.Debug(Main.Username, $"Di chuyển từ {old ?? Main.Location} đến {location}");

            //    var t = DateTime.Now;
            //    while (Main.Location.DistanceTo(location)>5)
            //    {
            //        // todo nên đặt timeout đề phòng ko send dc
            //        if ((DateTime.Now - t).TotalMilliseconds > 10000)
            //        {
            //            Log.Warning(Main.Username, $"Move Fail  {failCount++}");
            //            break;
            //        }
            //        Thread.Sleep(10);
            //    }
            //    old = location;
            //    //Thread.Sleep(1000);
            //}
            return 1;
            //_active = false;
            Wait(); // chạy tối da 5 phút
            return 1;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (IdleBot);
        }

        public override void Clean()
        {
            //Main.GsConnect?.Unregister(this);
        }
    }

    public class RunToPointInput : IUnitInput
    {
        /// <summary>
        ///     vị trí điểm cần tới
        /// </summary>
        //public IYulgangLocation DestionationPoint { get; set; } = new YulgangLocation(6765, 446, 201);
        //{X=712.1066, Y=-827.9486}
        //public IYulgangLocation DestionationPoint { get; set; } = new YulgangLocation(712.1066f, -827.9486f, 101);
        //{X=779.6292, Y=-1756.016}
        // public IYulgangLocation DestionationPoint { get; set; } = new YulgangLocation(779, -1756, 101);
        //{X=1000.417, Y=-94.31946}
        //public IYulgangLocation DestionationPoint { get; set; } = new YulgangLocation(1000, -94, 101);
        //public IYulgangLocation DestionationPoint { get; set; } = new YulgangLocation(1513, -1982, 101);
        public IYulgangLocation DestionationPoint { get; set; } = new YulgangLocation(5648, 1122, 201);
        //public IYulgangLocation DestionationPoint { get; set; } = new YulgangLocation(385, 1615, 101);
        //public IYulgangLocation DestionationPoint { get; set; } = new YulgangLocation(6132, 1937, 201);
        //public IYulgangLocation DestionationPoint { get; set; } = new YulgangLocation(5971, 1935, 201);

        /// <summary>
        ///     khoảng cách sai số cho phép
        /// </summary>
        public int Range { get; set; } = 200;
    }

    public class RunToPointConfig : IUnitConfig
    {
    }

    public class RunToPointOutput : IUnitOutput
    {
    }
}