﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 12:10 AM 25/09/2016
// FILENAME: BuySellItem.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;
using YulgangBotFinal.Resource;

#endregion

namespace YulgangBotFinal.EventCore.Unit.Gs.Train
{
    public enum ComfirmNpcBuySell
    {
        /// <summary>
        ///     không thể mua
        /// </summary>
        NotBuy = 0,

        /// <summary>
        ///     mua thành công
        /// </summary>
        BuySuccess = 1,
        SellSuccess = 2,
        SellFail = 3,

        /// <summary>
        ///     khoảng các quá xa với NPC nên ko thể mua
        /// </summary>
        NotEnoughRange = 10,

        /// <summary>
        ///     qua trọng Lương
        /// </summary>
        NotEnoughWeigth = 11,

        /// <summary>
        ///     không đủ tiền
        /// </summary>
        NotEnoughMoney = 13,

        /// <summary>
        ///     không đủ ô trống
        /// </summary>
        NotEnoughSpace = 14
    }

    public class BuySellItem : UnitSequenceProc<BuySellItemInput, BuySellItemConfig, BuySellItemOutput>
    {
        public BuySellItem(YbConfig cfg) : base(cfg)
        {
        }


        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public override void HandlePacket(IPacket p)
        {
            Debug.WriteLine($"{p.Opcode.ToString("X4")}");
            Debug.WriteLine(BitConverter.ToString(p.Data));
            if (p.Opcode == 0x91)
            {
                var tmp = ParsePacket<R0091TalkNpc>(p);
                State = 1;
            }

            if (p.Opcode == 0x93)
            {
                var tmp = ParsePacket<R0093BuyOrSellNpcItem>(p);
                var command = (ComfirmNpcBuySell) tmp.Command;
                switch (command)
                {
                    case ComfirmNpcBuySell.SellSuccess:
                    {
                        // bán đồ thành công
                        State = 1;
                        break;
                    }
                    case ComfirmNpcBuySell.BuySuccess:
                        State = 1;
                        break;
                    default: // lỗi đếu mua hay bán dc
                        State = 2;
                        break;
                }
            }
        }

        public override event Action<string> Break;

        public override int Run()
        {
            if (Main.PathFinder == null) return 1;
            Main.GsConnect?.Register(this, 0x91, 0x93);
            var command = 0;
            // check full item
            if (Main.Inventory.Count >= 36)
                command = 1; // bán item
            // not enought item recovery
            var items = new List<IYulgangGameItem>();
            using (var c = Main.Inventory.GetEnumerator())
                while (c.MoveNext())
                {
                    items.Add(c.Current.Value);
                }

            // tính toán số lượng item cần mua
            var buyItems = new List<BuyInfo>();
            foreach (var info in Config.ItemBuy)
            {
                var exist = items.FirstOrDefault(m => m.BasicInfo.Id == info.IdItem);
                if (exist == null || exist.Count < 10)
                {
                    var sub = exist?.Count ?? 0;
                    buyItems.Add(new BuyInfo
                    {
                        Count = (int) (info.Count - sub),
                        IdItem = info.IdItem,
                        IdNpc = info.IdNpc,
                        Location = info.Location
                    });
                }
            }

            if (buyItems.Count > 0)
                command = 2;

            if (command == 1)
            {
                // chạy tói thằng bán đồ , bán sạch những gì mình có
                Moveto(Config.NpcSellLocation);
                SellItems(items);
                // sau đó qua thằng kho cất đồ
                Moveto(Config.NpcStoreLocation);
                SaveItem();
            }

            if (command == 2)
            {
                // chạy tới thằng bán đồ , mua đầy đủ những thứ mình cần
                foreach (var buyItem in buyItems)
                {
                    Moveto(buyItem.Location);
                    BuyItem(buyItem);
                }
            }
            return 1;
        }

        public override Type GetSeekWhenFail()
        {
            return typeof (IdleBot);
        }

        public override void Clean()
        {
            Main.GsConnect?.Unregister(this);
        }


        private void SaveItem()
        {
            if (!TalkWithNpc(Config.NpcStoreLocation, Config.IdNpcStore))
                return;
            var items = new List<IYulgangGameItem>();
            var c = Main.Inventory.GetEnumerator();
            while (c.MoveNext())
            {
                items.Add(c.Current.Value);
            }
            // lấy danh sách các item cần cất vào kho
            //todo: chưa làm
            EndTalk(Config.IdNpcStore);
        }

        

        private bool OpenShopNpc(IYulgangLocation location, int npcId)
        {
            if (!TalkWithNpc(location, npcId))
                return false;

            // mở bảng shop ra
            Main.GsConnect.Send(new S0090TalkWithNpc(Main.PlayerSessionId).Send(YulgangTalkNpcCommand.OpenShop,
                npcId));
            Wait(15000);
            if (State == 0)
            {
                // như cẹc rồi , đóng trò chuyện

                Main.GsConnect.Send(new S0090TalkWithNpc(Main.PlayerSessionId).Send(YulgangTalkNpcCommand.Close,
                    npcId));
                return false;
            }
            return true;
        }

        private void BuyItem(BuyInfo buyItem)
        {
            if (!OpenShopNpc(buyItem.Location, buyItem.IdNpc))
                return;
            Main.GsConnect.Send(new S0092BuyOrSellNpcShop(Main.PlayerSessionId).BuyItem(
                new YulgangItemShopNpc
                {
                    Id = buyItem.IdItem
                },
                buyItem.Count));
            Wait(1000);
            if (State == 1)
                Log.Debug(Main.Username, "Mua thành công item ");
        }

        /// <summary>
        ///     bán các item đi
        /// </summary>
        /// <param name="items"></param>
        private void SellItems(List<IYulgangGameItem> items)
        {
            if (!OpenShopNpc(Config.NpcSellLocation, Config.IdNpcSell))
                return;

            // mọi thứ có vẻ trơn tru h bán item thoy
            foreach (var item in items)
            {
                Main.GsConnect.Send(new S0092BuyOrSellNpcShop(Main.PlayerSessionId).SellItem(item, (int) item.Count,
                    Main.Money));
                Wait(10000);
                Log.Debug(Main.Username,
                    State == 0 ? "Bán đếu được" : $"Bán thành công item {item.BasicInfo.Name} - {item.Count}");
            }

            EndTalk(Config.IdNpcSell);
        }

        /// <summary>
        ///     todo: hàm này sẽ tách ra làm 1 lớp chung cho nhiều thằng khách sử dụng
        ///     todo : hiện tại làm chạy test
        /// </summary>
        /// <param name="toLocation"></param>
        
    }

    public class BuySellItemConfig : IUnitConfig
    {
        public int IdNpcSell { get; set; } = 3;
        public int IdNpcStore { get; set; } = 1;
        public IYulgangLocation NpcSellLocation { get; set; } = new YulgangLocation(736, 1959, 101);
        // bình thập chỉ ở huyền bột phái
        public IYulgangLocation NpcStoreLocation { get; set; } = new YulgangLocation(156, 1790, 101);
        // Vi đại bảo ở huyền bột phái
        public IList<int> ItemNotSell { get; set; } = new List<int>();
        public IList<int> OptionNotSell { get; set; } = new List<int>();

        public IList<BuyInfo> ItemBuy { get; set; } = new List<BuyInfo>
        {
            new BuyInfo
            {
                Count = 100,
                IdItem = 1000000103,
                IdNpc = 3,
                Location = new YulgangLocation(736, 1959, 101)
            }, new BuyInfo
            {
                Count = 10,
                IdItem = 1000000112,
                IdNpc = 3,
                Location = new YulgangLocation(736, 1959, 101)
            }, new BuyInfo
            {
                Count = 10,
                IdItem = 1000000110,
                IdNpc = 3,
                Location = new YulgangLocation(736, 1959, 101)
            }
        };
    }

    public class BuyInfo
    {
        public int IdItem { get; set; }
        public int Count { get; set; }
        public int IdNpc { get; set; }
        public IYulgangLocation Location { get; set; }
    }

    public class BuySellItemOutput : IUnitOutput
    {
    }

    public class BuySellItemInput : IUnitInput
    {
    }
}