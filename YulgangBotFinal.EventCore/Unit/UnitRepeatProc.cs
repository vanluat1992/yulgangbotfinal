﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: UnitRepeatProc.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Collections.Generic;
using System.Threading;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Define;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Resource;
using YulgangBotFinal.Resource.Ybq;

#endregion

namespace YulgangBotFinal.EventCore.Unit
{
    public abstract class UnitRepeatProc<TConfig, TOutput> : IRepeatUnitProc
        where TOutput : class, IUnitOutput
        where TConfig : class, IUnitConfig
    {
        protected UnitRepeatProc(YbConfig cfg)
        {
            Ybi = cfg.Ybi;
            Log = cfg.Log;
            Quests = cfg.Quests;
            Config = Activator.CreateInstance<TConfig>();
            Output = Activator.CreateInstance<TOutput>();
            Main = new MainDefine();
        }
        protected IDictionary<int,Quest> Quests { get; }
        protected
            YBiHandle
                <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill, YulgangYbiGameSpell>
            Ybi { get; }

        protected ILog Log { get; }
        protected MainDefine Main { get; set; }
        protected DateTime LastRun { get; set; } = DateTime.Now;

        protected void Wait(int timeout = 60000)
        {
            State = 0;
            var evt = DateTime.Now;
            while (State == 0)
            {
                if ((DateTime.Now - evt).TotalMilliseconds > timeout)
                    break;
                Thread.Sleep(10);
            }
        }

        //public virtual TOutput GetOutput()
        //{
        //    var output = Output as TOutput;
        //    if (output != null)
        //        return output;
        //    throw new InvalidCastException("");
        //}

        //public virtual TConfig GetConfig()
        //{
        //    var cfg = Config as TConfig;
        //    if (cfg != null)
        //        return cfg;
        //    throw new InvalidCastException("");
        //}

        #region Implementation of IUnitProcess

        /// <summary>
        ///     Lấy thông tin đầu vào của process
        /// </summary>
        /// <returns></returns>
        /// <summary>
        ///     Lấy thông tin đầu ra của process
        /// </summary>
        /// <returns></returns>
        public TOutput Output { get; set; }

        /// <summary>
        ///     cài đặt thông tin cấu hình của process
        /// </summary>
        /// <returns></returns>
        public TConfig Config { get; set; }

        /// <summary>
        ///     Thông tin tiến trình xử lý được đưa tới cho process
        /// </summary>
        /// <param name="p"></param>
        public abstract void HandlePacket(IPacket p);

        public void SetMainDefine(MainDefine def)
        {
            Main = def;
        }

        public int State { get; set; }
        protected IProcScope Scope { get; private set; }
        public abstract void Clean();

        public void SetScopeProcess(IProcScope scope)
        {
            Scope = scope;
        }

        public T ParsePacket<T>(IPacket p) where T : RecvPacket
        {
            var t = (T) Activator.CreateInstance(typeof (T), p.Data, p.SessionId);
            t?.Parse();
            return (T) t;
        }

        #endregion

        #region Implementation of IRepeatUnitProc

        /// <summary>
        ///     hàm chạy tiến trình
        /// </summary>
        /// <param name="time"></param>
        public abstract void Run(DateTime time);

        public abstract void Init();

        public abstract Type RunAfter();
        public bool Alive { get; set; }

        #endregion
    }
}