﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YulgangBotFinal.EventCore.Utils
{
    public static class Pass2KoreaCreate
    {
        private static int WordLength = 8 * (sizeof(UInt32));
        public static string GeneratePassword2(this string pass2)
        {
            try
            {
                string pass2New = "^!" + pass2 + "^!";
                var length = pass2New.Length;

                var bytePass2 = Encoding.ASCII.GetBytes(pass2New);

                var newByte = new byte[64];
                bytePass2.CopyTo(newByte, 0);

                newByte[length] = 0x80; // Thêm byte 80 vào
                newByte[0x38] = (byte)(length * 8); // độ dài

                var listInt = new uint[16];
                for (int i = 0; i < 16; i++)
                {
                    listInt[i] = BitConverter.ToUInt32(newByte, i * 4);
                }

                // la chuoi 01234567|89ABCDEF|FEDCBA98|7654321 chia thanh 4 phan
                // 01.23.45.67 => dao nguoc thanh 67.45.23.01
                uint a2 = 0x67452301;
                uint v5 = 0xEFCDAB89;
                uint v6 = 0x98BADCFE;
                uint v7 = 0x10325476;

                uint v8 = ROL(listInt[0] + (v5 & v6 | v7 & ~v5) + a2 - 0x28955B88, 7);

                uint v9 = v5 + v8;
                uint v10 = ROL(listInt[1] + (v9 & v5 | v6 & ~v9) + v7 - 389564586, 12);
                uint v11 = v9 + v10;
                uint v12 = ROR(listInt[2] + (v9 & v11 | v5 & ~v11) + v6 + 606105819, 15);
                uint v13 = v11 + v12;
                uint v14 = ROR(listInt[3] + (v13 & v11 | v9 & ~v13) + v5 - 1044525330, 10);
                uint v15 = v13 + v14;
                uint v16 = ROL(listInt[4] + (v15 & v13 | v11 & ~v15) + v9 - 176418897, 7);
                uint v17 = v15 + v16;
                uint v18 = ROL(listInt[5] + (v17 & v15 | v13 & ~v17) + v11 + 1200080426, 12);
                uint v19 = v17 + v18;
                uint v20 = ROR(listInt[6] + (v17 & v19 | v15 & ~v19) + v13 - 1473231341, 15);
                uint v21 = v19 + v20;
                uint v22 = ROR(listInt[7] + (v21 & v19 | v17 & ~v21) + v15 - 45705983, 10);
                uint v23 = v21 + v22;
                uint v24 = ROL(listInt[8] + (v23 & v21 | v19 & ~v23) + v17 + 1770035416, 7);
                uint v25 = v23 + v24;
                uint v26 = ROL(listInt[9] + (v25 & v23 | v21 & ~v25) + v19 - 1958414417, 12);
                uint v27 = v25 + v26;
                uint v28 = ROR(listInt[10] + (v25 & v27 | v23 & ~v27) + v21 - 42063, 15);
                uint v29 = v27 + v28;
                uint v30 = ROR(listInt[11] + (v29 & v27 | v25 & ~v29) + v23 - 1990404162, 10);
                uint v31 = v29 + v30;
                uint v32 = ROL(listInt[12] + (v31 & v29 | v27 & ~v31) + v25 + 1804603682, 7);
                uint v33 = v31 + v32;
                uint v34 = ROL(listInt[13] + (v33 & v31 | v29 & ~v33) + v27 - 40341101, 12);
                uint v35 = v33 + v34;
                uint v36 = ROR(listInt[14] + (v33 & v35 | v31 & ~v35) + v29 - 1502002290, 15);
                uint v37 = v35 + v36;
                uint v38 = ROR(listInt[15] + (v37 & v35 | v33 & ~v37) + v31 + 1236535329, 10);
                uint v39 = v37 + v38;
                uint v40 = ROL(listInt[1] + (v39 & v35 | v37 & ~v35) + v33 - 165796510, 5);
                uint v41 = v39 + v40;
                uint v42 = ROL(listInt[6] + (v41 & v37 | v39 & ~v37) + v35 - 1069501632, 9);//
                uint v43 = v41 + v42;
                uint v44 = ROL(listInt[11] + (v39 & v43 | v41 & ~v39) + v37 + 643717713, 14);
                uint v45 = v43 + v44;
                uint v46 = ROR(listInt[0] + (v41 & v45 | v43 & ~v41) + v39 - 373897302, 12);
                uint v47 = v45 + v46;
                uint v48 = ROL(listInt[5] + (v47 & v43 | v45 & ~v43) + v41 - 701558691, 5);

                //uint v49 = (char*)&unk_2441453 + v147 + ((v47 + v48) & v45 | v47 & ~v45) + v43;
                uint v49 = 0x2441453 + listInt[10] + ((v47 + v48) & v45 | v47 & ~v45) + v43;
                uint v50 = v47 + v48;
                v49 = ROL(v49, 9);
                //var a = (char*)v49;
                var v51 = v49 + v50;

                uint v52 = ROL(listInt[15] + (v47 & v51 | v50 & ~v47) + v45 - 660478335, 14);
                uint v53 = v51 + v52;
                uint v54 = ROR(listInt[4] + (v50 & v53 | v51 & ~v50) + v47 - 405537848, 12);
                uint v55 = v53 + v54;
                uint v56 = ROL(listInt[9] + (v55 & v51 | v53 & ~v51) + v50 + 568446438, 5);
                uint v57 = listInt[14] + ((v55 + v56) & v53 | v55 & ~v53) + v51 - 1019803690;
                uint v58 = v55 + v56;
                v57 = ROL(v57, 9);
                uint v59 = v58 + v57;
                uint v60 = ROL(listInt[3] + (v55 & v59 | v58 & ~v55) + v53 - 187363961, 14);
                uint v61 = v59 + v60;
                uint v62 = ROR(listInt[8] + (v58 & v61 | v59 & ~v58) + v55 + 1163531501, 12);
                uint v63 = v61 + v62;
                uint v64 = ROL(listInt[13] + (v63 & v59 | v61 & ~v59) + v58 - 1444681467, 5);
                uint v65 = listInt[2] + ((v63 + v64) & v61 | v63 & ~v61) + v59 - 51403784;
                uint v66 = v63 + v64;
                v65 = ROL(v65, 9);
                uint v67 = v66 + v65;
                uint v68 = ROL(listInt[7] + (v63 & v67 | v66 & ~v63) + v61 + 1735328473, 14);

                uint v69 = v67 + v68;
                uint v70 = ROR(listInt[12] + (v66 & v69 | v67 & ~v66) + v63 - 1926607734, 12);
                uint v71 = v69 + v70;
                uint v72 = ROL(listInt[5] + (v71 ^ v69 ^ v67) + v66 - 378558, 4);
                uint v73 = v71 + v72;
                uint v74 = ROL(listInt[8] + (v73 ^ v71 ^ v69) + v67 - 2022574463, 11);
                uint v75 = v73 + v74;
                uint v76 = ROL(listInt[11] + (v73 ^ v71 ^ v75) + v69 + 1839030562, 16);
                uint v77 = v75 + v76;
                uint v78 = ROR(listInt[14] + (v73 ^ v77 ^ v75) + v71 - 35309556, 9);
                uint v79 = v77 + v78;
                uint v80 = ROL(listInt[1] + (v79 ^ v77 ^ v75) + v73 - 1530992060, 4);
                uint v81 = v79 + v80;
                uint v82 = ROL(listInt[4] + (v81 ^ v79 ^ v77) + v75 + 1272893353, 11);
                uint v83 = v81 + v82;
                uint v84 = ROL(listInt[7] + (v81 ^ v79 ^ v83) + v77 - 155497632, 16);
                uint v85 = v83 + v84;
                uint v86 = ROR(listInt[10] + (v81 ^ v85 ^ v83) + v79 - 1094730640, 9);
                uint v87 = v85 + v86;
                uint v88 = ROL(listInt[13] + (v87 ^ v85 ^ v83) + v81 + 681279174, 4);
                uint v89 = v87 + v88;
                uint v90 = ROL(listInt[0] + (v89 ^ v87 ^ v85) + v83 - 358537222, 11);
                uint v91 = v89 + v90;
                uint v92 = ROL(listInt[3] + (v89 ^ v87 ^ v91) + v85 - 722521979, 16);
                uint v93 = v91 + v92;
                uint v94 = ROR(listInt[6] + (v89 ^ v93 ^ v91) + v87 + 76029189, 9);
                uint v95 = v93 + v94;
                uint v96 = ROL(listInt[9] + (v95 ^ v93 ^ v91) + v89 - 640364487, 4);
                uint v97 = v95 + v96;
                uint v98 = ROL(listInt[12] + (v97 ^ v95 ^ v93) + v91 - 421815835, 11);
                uint v99 = v97 + v98;
                uint v100 = ROL(listInt[15] + (v97 ^ v95 ^ v99) + v93 + 530742520, 16);
                uint v101 = v99 + v100;
                uint v102 = ROR(listInt[2] + (v97 ^ v101 ^ v99) + v95 - 995338651, 9);
                uint v103 = v101 + v102;
                uint v104 = ROL(listInt[0] + (v101 ^ (v103 | ~v99)) + v97 - 198630844, 6);
                uint v105 = v103 + v104;
                uint v106 = ROL(listInt[7] + (v103 ^ (v105 | ~v101)) + v99 + 1126891415, 10);
                uint v107 = v105 + v106;
                uint v108 = ROL(listInt[14] + (v105 ^ (v107 | ~v103)) + v101 - 1416354905, 15);
                uint v109 = v107 + v108;
                uint v110 = ROR(listInt[5] + (v107 ^ (v109 | ~v105)) + v103 - 57434055, 11);
                uint v111 = v109 + v110;
                uint v112 = ROL(listInt[12] + (v109 ^ (v111 | ~v107)) + v105 + 1700485571, 6);
                uint v113 = v111 + v112;
                uint v114 = ROL(listInt[3] + (v111 ^ (v113 | ~v109)) + v107 - 1894986606, 10);
                uint v115 = v113 + v114;
                uint v116 = ROL(listInt[10] + (v113 ^ (v115 | ~v111)) + v109 - 1051523, 15);
                uint v117 = v115 + v116;
                uint v118 = ROR(listInt[1] + (v115 ^ (v117 | ~v113)) + v111 - 2054922799, 11);
                uint v119 = v117 + v118;
                uint v120 = ROL(listInt[8] + (v117 ^ (v119 | ~v115)) + v113 + 1873313359, 6);
                uint v121 = v119 + v120;
                uint v122 = ROL(listInt[15] + (v119 ^ (v121 | ~v117)) + v115 - 30611744, 10);
                uint v123 = v121 + v122;
                uint v124 = ROL(listInt[6] + (v121 ^ (v123 | ~v119)) + v117 - 1560198380, 15);
                uint v125 = v123 + v124;
                uint v126 = ROR(listInt[13] + (v123 ^ (v125 | ~v121)) + v119 + 1309151649, 11);
                uint v127 = v125 + v126;
                uint v128 = ROL(listInt[4] + (v125 ^ (v127 | ~v123)) + v121 - 145523070, 6);
                uint v129 = v127 + v128;
                uint v130 = ROL(listInt[11] + (v127 ^ (v129 | ~v125)) + v123 - 1120210379, 10);
                uint v131 = v129 + v130;
                uint v132 = a2;
                uint v133 = ROL(listInt[2] + (v129 ^ (v131 | ~v127)) + v125 + 718787259, 15);
                uint v134 = v131 + v133;

                uint v135 = ROR(listInt[6] + (v131 ^ (v134 | ~v129)) + v127 - 343485551, 11);

                a2 += v129;
                v5 += v134 + v135;
                v6 += v134;
                v7 += v131;

                var temp = BitConverter.GetBytes(a2);
                var strPassword2 = temp.Aggregate("", (current, b) => current + $"{b:x2}");

                temp = BitConverter.GetBytes(v5);
                strPassword2 += temp.Aggregate("", (current, b) => current + $"{b:x2}");

                temp = BitConverter.GetBytes(v6);
                strPassword2 += temp.Aggregate("", (current, b) => current + $"{b:x2}");

                temp = BitConverter.GetBytes(v7);
                strPassword2 += temp.Aggregate("", (current, b) => current + $"{b:x2}");
                //Console.WriteLine(strPassword2);

                //Console.WriteLine(v5);
                //Console.WriteLine(v6);
                //Console.WriteLine(v7);

                return strPassword2;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static uint ROL(uint value, int shift)
        {
            return (value << shift) | (value >> (WordLength - shift));
        }

        public static uint ROR(uint value, int shift)
        {
            return (value >> shift) | (value << (WordLength * 8 - shift));
        }
    }
}
