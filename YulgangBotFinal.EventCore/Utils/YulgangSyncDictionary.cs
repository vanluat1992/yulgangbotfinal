﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: YulgangSyncDictionary.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Collections.Concurrent;
using System.Collections.Generic;

#endregion

namespace YulgangBotFinal.EventCore.Utils
{
    public interface IYulgangSyncDictionary<TKey, TValue> where TValue : class
    {
        int Count { get; }
        void Update(TKey key, TValue val);
        bool Set(TKey key, TValue val);
        TValue Get(TKey key);
        bool Remove(TKey key);
        void Clear();
        IList<TValue> Get();
    }

    public class YulgangSyncDictionary<TKey, TValue> : IYulgangSyncDictionary<TKey, TValue> where TValue : class
    {
        private readonly ConcurrentDictionary<TKey, TValue> _concurrent = new ConcurrentDictionary<TKey, TValue>();

        public void Update(TKey key, TValue val)
        {
            if (!_concurrent.ContainsKey(key)) return;
            _concurrent[key] = val;
        }

        public bool Set(TKey key, TValue val)
        {
            if (_concurrent.ContainsKey(key)) return false;
            return _concurrent.TryAdd(key, val);
        }

        public TValue Get(TKey key)
        {
            if (!_concurrent.ContainsKey(key)) return null;
            TValue result = null;
            _concurrent.TryGetValue(key, out result);
            return result;
        }

        public bool Remove(TKey key)
        {
            TValue tmp;
            return _concurrent.TryRemove(key, out tmp);
        }

        public void Clear()
        {
            _concurrent.Clear();
        }

        public int Count => _concurrent.Count;

        public IList<TValue> Get()
        {
            var result = new List<TValue>();
            var tmp = _concurrent.GetEnumerator();
            while (tmp.MoveNext())
            {
                result.Add(tmp.Current.Value);
            }
            return result;
        }
    }
}