﻿using System.Collections.Generic;
using Log;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Define;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.EventCore.Procedure.Units;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Network;
using YulgangBotFinal.Resource;

namespace YulgangBotFinal.EventCore.Procedure
{
    public class GameProcedure:IHookRecvPacket
    {
        private IProcedure _currentProc;
        private IProcedure _prevProc;
        private EventManager _evtManager;
        private ILog _log;
        private MainDefine _main;
        private YBiHandle<YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill, YulgangYbiGameSpell> _ybi;
        private IList<YulgangPlayer> _allChars = new List<YulgangPlayer>();
        public void Init()
        {
            // load resource
            _evtManager = new EventManager();
            _log = new LogManager {LogLevel = LogType.Dum};
            _log.InstallAttackLog(new LogFiles());
            _log.InstallAttackLog(new ConsoleLog());
            _ybi =
                new YBiHandle
                <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill,
                    YulgangYbiGameSpell>(15);
            _ybi.Load("Resource\\Ybi.cfg");
            _main =new MainDefine();
            _main.AuthIp = "121.52.200.68";
            _main.AuthPort = 16044;
            _main.IdServer = 1;
            _main.IdChannel = 1;
            _main.Username = "hmt1992";
            _main.Pass = "123456";
            _main.Pass2 = "999999";
            _main.IndexCharacter = 1;
            _main.Auto = true;
            _evtManager.RegisterEvent(GEvent.EnterPwd2Success, LoginAuthentSuccess, this);
            _evtManager.RegisterEvent<string, int>(GEvent.EnterChannelSuccess, EnterChannelSuccess, this);
            _evtManager.RegisterEvent<IList<YulgangPlayer>>(GEvent.AddCharactorComplete, LoadCharactorComplete, this);
            _evtManager.RegisterEvent<int>(GEvent.SelectCharEnterWorld, SelectCharEnterWorld, this);
            _evtManager.RegisterEvent(GEvent.BackToSelectChar, BackToSelectChar, this);
            _evtManager.RegisterEvent(GEvent.LogOut, LogOut, this);
            _evtManager.RegisterEvent(GEvent.Login, LogIn, this);
        }

        private void LogIn()
        {
            _main.IsEnterGame = false;
            _main.Reload = false;
            SetProc(new LoginProcedure());
        }

        private void LogOut()
        {
            _main.IsEnterGame = false;
            _main.Reload = false;
            if (_main.Auto)
            {
                _evtManager.PushEvent(GEvent.Login);
            }
        }

        private void BackToSelectChar()
        {
            _main.Reload = true;
            SetProc(new SelectCharactorProcedure());
        }

        private void SelectCharEnterWorld(int index)
        {
            _main.IndexCharacter = index;
            _main.Player = _allChars[index];
            SetProc(new EnterGameProcedure());
        }

        private void LoadCharactorComplete(IList<YulgangPlayer> all)
        {
            _allChars = all;
            if (all.Count == 0) return;
            if (_main.Auto)
            {
                _log.Debug(_main.Username, $"Tự động đăng nhập nhân vật {_main.IndexCharacter}");
                _evtManager.PushEvent(GEvent.SelectCharEnterWorld, _main.IndexCharacter);
            }
            else _log.Debug(_main.Username, "Vui lòng chọn nhân vật !");
        }

        private void EnterChannelSuccess(string ip, int port)
        {
            _log.Debug(_main.Username,$"Đóng kết nối với máy chủ authen");
            _main.AuthConnect.Release();
            _log.Debug(_main.Username, $"Mở kết nối qua máy chủ Gs {ip}:{port}");
            _main.GsConnect = new GsConnect(_log);
            _main.GsConnect.Connect(ip, port);
            _log.Success(_main.Username, $"Kết nối qua máy chủ gs thành công");
            SetProc(new SelectCharactorProcedure());
            _main.GsConnect.Register(this, 4);
        }

        private void LoginAuthentSuccess()
        {
            SetProc(new SelectChannelProcedure());
        }

        public void SetProc(IProcedure proc)
        {
            _prevProc = _currentProc;
            _prevProc?.Release();
            _currentProc = proc;
            _currentProc.Init(new ProcedureConfigParams(_log,_evtManager,_evtManager,_main));
        }

        public void Release()
        {
            // release resource
        }

        public void MainLoop()
        {
            while (true)
            {
                _currentProc.Tick();
                _evtManager.Tick();
            }
        }

        public void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 4:
                {
                    _evtManager.PushEvent(GEvent.LogOut);
                    break;
                }
            }
        }
    }
}