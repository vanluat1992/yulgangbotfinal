﻿using System;

namespace YulgangBotFinal.EventCore.Procedure
{
    public interface IRegisterEvent
    {
        long RegisterEvent(GEvent eventId, Action fun, object owner);
        long RegisterEvent<T1>(GEvent eventId, Action<T1> fun, object owner);
        long RegisterEvent<T1, T2>(GEvent eventId, Action<T1, T2> fun, object owner);
        long RegisterEvent<T1, T2, T3>(GEvent eventId, Action<T1, T2, T3> fun, object owner);
        long RegisterEvent<T1, T2, T3, T4>(GEvent eventId, Action<T1, T2, T3, T4> fun, object owner);
        long RegisterEvent<T1, T2, T3, T4, T5>(GEvent eventId, Action<T1, T2, T3, T4, T5> fun, object owner);
        long RegisterEvent<T1, T2, T3, T4, T5, T6>(GEvent eventId, Action<T1, T2, T3, T4, T5, T6> fun, object owner);
        long RegisterEvent<T1, T2, T3, T4, T5, T6, T7>(GEvent eventId, Action<T1, T2, T3, T4, T5, T6, T7> fun,object owner);
        long RegisterEvent<T1, T2, T3, T4, T5, T6, T7, T8>(GEvent eventId,Action<T1, T2, T3, T4, T5, T6, T7, T8> fun, object owner);
        bool UnRegisterEvent(GEvent eventId, long id);
    }
}