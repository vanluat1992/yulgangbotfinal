﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace YulgangBotFinal.EventCore.Procedure
{
    public class EventManager : IRegisterEvent, IExecEvent
    {
        private readonly ConcurrentDictionary<GEvent, ConcurrentDictionary<long, Delegate>> _allListEvent =
            new ConcurrentDictionary<GEvent, ConcurrentDictionary<long, Delegate>>();

        private readonly ConcurrentQueue<EventEntity> _evtQueue = new ConcurrentQueue<EventEntity>();

        private readonly object _syslock = new object();

        private long _evtIdIndex;

        public long RegisterEvent(GEvent eventId, Action fun, object owner)
        {
            return Register(eventId, fun, owner);
        }

        public long RegisterEvent<T1>(GEvent eventId, Action<T1> fun, object owner)
        {
            return Register(eventId, fun, owner);
        }

        public long RegisterEvent<T1, T2>(GEvent eventId, Action<T1, T2> fun, object owner)
        {
            return Register(eventId, fun, owner);
        }

        public long RegisterEvent<T1, T2, T3>(GEvent eventId, Action<T1, T2, T3> fun, object owner)
        {
            return Register(eventId, fun, owner);
        }

        public long RegisterEvent<T1, T2, T3, T4>(GEvent eventId, Action<T1, T2, T3, T4> fun, object owner)
        {
            return Register(eventId, fun, owner);
        }

        public long RegisterEvent<T1, T2, T3, T4, T5>(GEvent eventId, Action<T1, T2, T3, T4, T5> fun, object owner)
        {
            return Register(eventId, fun, owner);
        }

        public long RegisterEvent<T1, T2, T3, T4, T5, T6>(GEvent eventId, Action<T1, T2, T3, T4, T5, T6> fun, object owner)
        {
            return Register(eventId, fun, owner);
        }

        public long RegisterEvent<T1, T2, T3, T4, T5, T6, T7>(GEvent eventId, Action<T1, T2, T3, T4, T5, T6, T7> fun,
            object owner)
        {
            return Register(eventId, fun, owner);
        }

        public long RegisterEvent<T1, T2, T3, T4, T5, T6, T7, T8>(GEvent eventId,
            Action<T1, T2, T3, T4, T5, T6, T7, T8> fun, object owner)
        {
            return Register(eventId, fun, owner);
        }

        public bool UnRegisterEvent(GEvent eventId, long id)
        {
            if (_allListEvent.TryGetValue(eventId, out var val))
            {
                return val.TryRemove(id, out _);
            }

            return false;
        }

        private long Register(GEvent eventId, Delegate fun, object owner)
        {
            ConcurrentDictionary<long, Delegate> val;
            if (!_allListEvent.TryGetValue(eventId, out val))
            {
                val = new ConcurrentDictionary<long, Delegate>();
                _allListEvent.TryAdd(eventId, val);
            }

            long id = 0;
            lock (_syslock)
            {
                id = _evtIdIndex++;
            }

            val.TryAdd(id, fun);
            return id;
        }

        public void Tick()
        {
            while (_evtQueue.Count > 0)
            {
                EventEntity evt;
                if (_evtQueue.TryDequeue(out evt)) evt.Func.DynamicInvoke(evt.Args.ToArray());
            }
        }

        private void Push(GEvent idEvt, params object[] args)
        {
            ConcurrentDictionary<long, Delegate> val;
            if (_allListEvent.TryGetValue(idEvt, out val))
                foreach (var t in val)
                {
                    var e = new EventEntity(t.Value, args);
                    _evtQueue.Enqueue(e);
                }
            else throw new NotSupportedException($"Event {idEvt} not support");
        }

        public void PushEvent<T1, T2, T3, T4, T5, T6, T7, T8>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
            T6 arg6, T7 arg7, T8 arg8)
        {
            Push(idEvt, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
        }

        public void PushEvent<T1, T2, T3, T4, T5, T6, T7>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
            T6 arg6, T7 arg7)
        {
            Push(idEvt, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
        }

        public void PushEvent<T1, T2, T3, T4, T5, T6>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,
            T6 arg6)
        {
            Push(idEvt, arg1, arg2, arg3, arg4, arg5, arg6);
        }

        public void PushEvent<T1, T2, T3, T4, T5>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            Push(idEvt, arg1, arg2, arg3, arg4, arg5);
        }

        public void PushEvent<T1, T2, T3, T4>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            Push(idEvt, arg1, arg2, arg3, arg4);
        }

        public void PushEvent<T1, T2, T3>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3)
        {
            Push(idEvt, arg1, arg2, arg3);
        }

        public void PushEvent<T1, T2>(GEvent idEvt, T1 arg1, T2 arg2)
        {
            Push(idEvt, arg1, arg2);
        }

        public void PushEvent<T1>(GEvent idEvt, T1 arg1)
        {
            Push(idEvt, arg1);
        }

        public void PushEvent(GEvent idEvt)
        {
            Push(idEvt);
        }
    }
}