﻿using System;
using System.Collections.Generic;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;

namespace YulgangBotFinal.EventCore.Procedure.Units
{
    public class SelectCharactorProcedure:BaseProcedure,IHookRecvPacket
    {
        private DateTime _lastReciveCharactor;
        private IList<YulgangPlayer> _allPlayers = new List<YulgangPlayer>();
        public override void Tick()
        {
            switch (CurrentState)
            {
                case GEvent.AddCharactor:
                    if ((DateTime.Now - _lastReciveCharactor).Seconds >= 5)
                    {
                        Config.ExecEvent.PushEvent(GEvent.AddCharactorComplete, _allPlayers);
                        CurrentState = GEvent.None;
                    }
                    break;
            }
        }

        public override void Init()
        {
            _allPlayers.Clear();
            Log.Debug(Main.Username,"Kích hoạt Select charactor procedure");
            Main.GsConnect.Register(this, 2, 0x11);
            RegisterEvent<int>(GEvent.EnterWelcomeSuccess, EnterWelcome);
            RegisterEvent(GEvent.EmptyCharactor,EmptyCharactor);
            RegisterEvent<YulgangPlayer>(GEvent.AddCharactor, AddCharactor);
            RegisterEvent<IList<YulgangPlayer>>(GEvent.AddCharactorComplete, LoadCharactorListComplete);
            if (!Main.Reload)
            {
                Main.GsConnect.Send(new S0001FirstLogin(Main.PlayerSessionId).Send(Main.DzoUsername));
            }
            else
            {
                Config.ExecEvent.PushEvent(GEvent.EnterWelcomeSuccess, Main.PlayerSessionId);
            }
            Main.IsEnterGame = false;
        }

        private void LoadCharactorListComplete(IList<YulgangPlayer> allChar)
        {
            Log.Success(Main.Username, $"Load thành công danh sách nhân vật ({allChar.Count}) của tài khoản ");
        }

        private void AddCharactor(YulgangPlayer c)
        {
            Log.Debug(Main.Username, $"Load thành công thông tin nhân vật {c.Name}");
            _allPlayers.Add(c);
            _lastReciveCharactor = DateTime.Now;
        }

        private void EmptyCharactor()
        {
            Log.Warning(Main.Username, "Chưa có nhân vật được tạo ở máy chủ này");
        }

        private void EnterWelcome(int sessionId)
        {
            Log.Debug(Main.Username, $"Tài khoản đăng nhập được cấp phát sessionId : {sessionId}");
            Main.PlayerSessionId = sessionId;
            Main.GsConnect.Send(new S1638(Main.PlayerSessionId));
            Main.GsConnect.Send(new S1606(Main.PlayerSessionId));
            Main.GsConnect.Send(new S0010ListCharacter(Main.PlayerSessionId));
            _lastReciveCharactor = DateTime.Now;
            CurrentState = GEvent.AddCharactor;
        }

        public override void Dispose()
        {
            Main.GsConnect.Unregister(this);
            Log.Debug(Main.Username, "Dispose select charactor procedure");
        }

        public void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 2:
                    Config.ExecEvent.PushEvent(GEvent.EnterWelcomeSuccess, p.SessionId);
                    break;
                case 0x11:
                    var tmp = ParsePacket<R0011GetListCharacter>(p);
                    if (tmp.EmptyCharacter)
                    {
                        Config.ExecEvent.PushEvent(GEvent.EmptyCharactor);
                        return;
                    }

                    Config.ExecEvent.PushEvent(GEvent.AddCharactor, tmp.Player);
                    break;
            }
        }
    }
}