﻿using System.Collections.Generic;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Model;
using YulgangBotFinal.Network.Packet.Auth.Recv;
using YulgangBotFinal.Network.Packet.Auth.Send;

namespace YulgangBotFinal.EventCore.Procedure.Units
{
    public class SelectChannelProcedure:BaseProcedure,IHookRecvPacket
    {
        public override void Tick()
        {
            switch (CurrentState)
            {
                case GEvent.GetServerList:
                    Main.AuthConnect.Send(new S8016GetServerList());
                    CurrentState = GEvent.None;
                    break;
            }
        }

        public override void Init()
        {
            Main.AuthConnect.Register(this, 0x8017, 0x8064);
            RegisterEvent<IDictionary<int, Model.InfoGroupServer>>(GEvent.GetServerListSuccess, GetServerListSuccess);
            RegisterEvent<int,int>(GEvent.EnterChannel,EnterChannel);
            Log.Debug(Main.Username, "Kích hoạt SelectChannel Procedure");
            CurrentState = GEvent.GetServerList;
        }

        private void EnterChannel(int idServer, int idChannel)
        {
            Main.IdServer = idServer;
            Main.IdChannel = idChannel;
            Main.AuthConnect.Send(new S800CEnterChannel().Send(idServer, idChannel));
        }

        private void GetServerListSuccess(IDictionary<int, InfoGroupServer> svs)
        {
            Main.ServerList = svs;
            foreach (var channel in svs)
            {
                Log.Debug(Main.Username, $"Server name:{channel.Value.Id} - {channel.Value.Name} ");
                foreach (var channel1 in channel.Value.Chanels)
                {
                    Log.Debug(Main.Username,
                        $"   {channel1.Value.Id} - {channel1.Value.Name}:{channel1.Value.Percent}%");
                }
            }

            if (Main.Auto)
                Config.ExecEvent.PushEvent(GEvent.EnterChannel, Main.IdServer, Main.IdChannel);
        }

        public override void Dispose()
        {
            Main.AuthConnect?.Unregister(this);
            Log.Debug(Main.Username,"Dispose Select Channel procedure");
        }

        public void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 0x8017:
                    var tmp = ParsePacket<R8017ServerList>(p);
                    Config.ExecEvent.PushEvent(GEvent.GetServerListSuccess, tmp.Servers);
                    break;
                case 0x8064:
                    {
                        var p8064 = ParsePacket<R8064EnterChannel>(p);
                      
                        Log.Debug(Main.Username, $"Ip GameServer : {p8064.Ip}:{p8064.Port}");
                        Config.ExecEvent.PushEvent(GEvent.EnterChannelSuccess, p8064.Ip, p8064.Port);
                        break;
                    }
            }
        }
    }
}
