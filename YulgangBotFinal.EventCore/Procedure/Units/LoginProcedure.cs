﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Network;
using YulgangBotFinal.Network.Packet.Auth.Recv;
using YulgangBotFinal.Network.Packet.Auth.Send;

namespace YulgangBotFinal.EventCore.Procedure.Units
{
    public class LoginProcedure:BaseProcedure,IHookRecvPacket
    {
        private string _username = "";
        private string _pwd = "";
        private string _gUsername = "";
        private string _gPwd = "";
        private IConnection _authConnect;
        public override void Tick()
        {
            switch (CurrentState)
            {
                //case GEvent.InputUsernameAndPwd:
                    
                //    CurrentState = GEvent.None;
                //    break;
                case GEvent.LoginWeb:
                    var tmp = ParseDzo(GetResponseFromDzoServer(_username, _pwd));

                    if (string.IsNullOrEmpty(tmp.Item1) || string.IsNullOrEmpty(tmp.Item2))
                    {
                        Config.ExecEvent.PushEvent(GEvent.LoginFail);
                    }
                    else
                    {
                        var dzoUser = tmp.Item1;
                        var dzoPass = tmp.Item2;
                        Config.ExecEvent.PushEvent(GEvent.LoginSuccess, dzoUser, dzoPass);
                    }

                    CurrentState = GEvent.None;
                    break;
                case GEvent.ConnectAuthent:
                    Log.Info(_username,"Bắt đầu kết nối qua máy chủ authent");

                    Main.AuthConnect = new AuthConnection(Log);
                    //Main.AuthConnect.Register(this, 0x8001);
                    try
                    {

                        Main.AuthConnect.Connect(Main.AuthIp, Main.AuthPort);
                        Config.ExecEvent.PushEvent(GEvent.ConnectAuthentSuccess, Main.AuthConnect);
                    }
                    catch (Exception e)
                    {
                        Log.Exception(_username, e, "Kết nối máy chủ authent ko thành công");
                        Config.ExecEvent.PushEvent(GEvent.ConnectAuthentFail);
                    }
                    CurrentState = GEvent.None;
                    break;
                case GEvent.LoginAuthentSuccess:
                    if (Main.Auto)
                    {
                        Log.Debug(Main.Username, $"Đăng nhập với pass 2 : {Main.Pass}");
                        Main.AuthConnect.Send(new S8048PassVerify().Send(Main.Pass2));
                    }

                    CurrentState = GEvent.None;
                    break;
            }
        }

        public override void Init()
        {
            Log.Debug("SYSTEM", "Kích hoạt login procedure");
            RegisterEvent<string,string>(GEvent.LoginSuccess, LoginSuccess);
            RegisterEvent<string,string>(GEvent.InputUsernameAndPwd, InputUserNameAndPwd);
            RegisterEvent(GEvent.LoginFail, LoginFail);
            RegisterEvent<IConnection>(GEvent.ConnectAuthentSuccess,ConnectAuthentSuccess);
            RegisterEvent(GEvent.ConnectAuthentFail,ConnectAuthentFail);
            RegisterEvent(GEvent.LoginAuthentSuccess, LoginAuthentSuccess);
            RegisterEvent(GEvent.EnterPwd2Success, EnterPwd2Success);
            if (Main.Auto)
            {

                Log.Info("SYSTEM", $"tự động đăng nhập username {Main.Username}");
                Config.ExecEvent.PushEvent(GEvent.InputUsernameAndPwd, Main.Username, Main.Pass);
            }
        }

        private void EnterPwd2Success()
        {
            Log.Success(Main.Username, $"Đăng nhập pwd2 thành công");
        }

        private void LoginAuthentSuccess()
        {
            CurrentState = GEvent.LoginAuthentSuccess;
        }

        private void ConnectAuthentFail()
        {
            Log.Error(Main.Username,$"Kết nối máy chủ authent không thành công");
        }

        private void ConnectAuthentSuccess(IConnection connect)
        {
            Main.AuthConnect = connect;
            _authConnect = connect;
            Log.Success(Main.Username,"kết nối máy chủ authent thành công");
            connect.Register(this, 0x8001, 0x8049);
            Log.Debug(Main.Username,"Send gói tin đăng nhập 8000");
            Main.AuthConnect.Send(new S8000FirstLogin().Send(Main.DzoUsername, Main.DzoPass));
        }

        private void LoginFail()
        {
            Log.Warning(_username, "Đăng nhập không thành công");
        }

        private void InputUserNameAndPwd(string username, string pwd)
        {
            _username = username;
            _pwd = pwd;
            Main.Username = username;
            Main.Pass = pwd;
            CurrentState = GEvent.LoginWeb;
        }

        private void LoginSuccess(string gUser,string gPwd)
        {

            Log.Debug(_username, $"Đăng nhập thành công {gUser}----{gPwd}");
            _gUsername = gUser;
            _gPwd = gPwd;
            Main.DzoUsername = gUser;
            Main.DzoPass = gPwd;
            CurrentState = GEvent.ConnectAuthent;

        }

        public override void Dispose()
        {
            _authConnect?.Unregister(this);
            Log.Debug(Main.Username, "Dispose Login Procedure");
        }
        private Tuple<string, string> ParseDzo(string data)
        {
            var split = data.Split(',');
            var username = "";
            var pass = "";
            foreach (var para in split.Select(s => s.Replace("\"", ""))
                .Select(st => st.Split(':')))
            {
                if (para[0] == "user_id")
                {
                    username = para[1];
                }
                if (para[0] == "user_otp")
                {
                    pass = para[1];
                }
            }
            return new Tuple<string, string>(username, pass);
        }

        private string GetResponseFromDzoServer(string user, string pass)
        {
            const string url = "http://authen.dzogame.com:8080/LauncherLogin.aspx?gid=200";
            var myCookieContainer = new CookieContainer();

            // Convert the text into the url encoding string

            // Concat the string data which will be submit
            const string formatString =
                "ScriptManager1=UpdatePanel1%7CbtnLogin&__EVENTTARGET=btnLogin&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUJNTU3MDU3Mzk1D2QWAgIDD2QWAgIDD2QWAgIBD2QWAmYPZBYCAgsPZBYGAgEPDxYCHgtOYXZpZ2F0ZVVybAUdU05Mb2dpbi5hc3B4P2dpZD0yMDAmc3R5cGU9eWhkZAIDDw8WAh8ABR5TTkxvZ2luLmFzcHg%2FZ2lkPTIwMCZzdHlwZT1nbGVkZAIFDw8WAh8ABR1TTkxvZ2luLmFzcHg%2FZ2lkPTIwMCZzdHlwZT1mYmRkZA%3D%3D&__EVENTVALIDATION=%2FwEWBQLj4%2F3ECgLWn9eKCALq85jtCQKC3IeGDALPqpnxDQ%3D%3D&tbxUserName={0}&tbxPassword={1}&hdfGameID=200&__ASYNCPOST=true&";
            var postString = string.Format(formatString, user, pass);

            // Convert the submit string data into the byte array
            var postData = Encoding.ASCII.GetBytes(postString);

            // Set the request parameters
            var request = WebRequest.Create(url) as HttpWebRequest;
            if (request != null)
            {
                request.MaximumAutomaticRedirections = 4;
                request.MaximumResponseHeadersLength = 4;
                // Set credentials to use for this request.
                request.Credentials = CredentialCache.DefaultCredentials;

                request.Method = "POST";
                request.Referer = url;
                request.KeepAlive = true;
                request.UserAgent =
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.2; CIBA)";
                request.ContentType = "application/x-www-form-urlencoded";
                request.CookieContainer = myCookieContainer;
                request.ContentLength = postData.Length;

                // Submit the request data
                var outputStream = request.GetRequestStream();

                request.AllowAutoRedirect = true;
                outputStream.Write(postData, 0, postData.Length);
                outputStream.Close();

                // Get the return data
                var response = request.GetResponse() as HttpWebResponse;
                if (response != null)
                {
                    var responseStream = response.GetResponseStream();
                    //Console.WriteLine(srcString);
                    response.Close();


                    request = WebRequest.Create("http://authen.dzogame.com:8080/Notification.aspx") as HttpWebRequest;
                    if (request != null)
                    {
                        request.CookieContainer = myCookieContainer;
                        request.Method = "GET";
                        request.KeepAlive = true;
                        {
                            var response1 = request.GetResponse() as HttpWebResponse;
                            if (response1 != null)
                            {
                                new StreamReader(response1.GetResponseStream(), Encoding.UTF8)
                                    .ReadToEnd
                                    ();
                                response1.Close();
                            }
                        }
                    }

                    request = WebRequest.Create("http://authen.dzogame.com:8080/ReturnLogin.ashx") as HttpWebRequest;
                    if (request != null)
                    {
                        request.Method = "POST";
                        request.Referer = "http://authen.dzogame.com:8080/Notification.aspx";
                        request.KeepAlive = true;
                        request.UserAgent =
                            "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.2; CIBA)";
                        request.ContentType = "application/x-www-form-urlencoded";
                        request.CookieContainer = myCookieContainer;

                        request.ContentLength = 0;

                        // Get the return data
                        response = request.GetResponse() as HttpWebResponse;
                    }
                    if (response != null) responseStream = response.GetResponseStream();
                    if (responseStream != null)
                    {
                        var reader = new StreamReader(responseStream, Encoding.UTF8);
                        var srcString = reader.ReadToEnd();
                        reader.Dispose();
                        responseStream.Close();
                        response.Close();
                        request.Abort();

                        return srcString;
                    }
                }
            }
            return "";
        }

        public void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 0x8001:

                    var parsePacket = ParsePacket<R8001FirstLogin>(p);
                    if (parsePacket.Valid)
                    {
                        Config.ExecEvent.PushEvent(GEvent.LoginAuthentSuccess);
                    }
                    else Config.ExecEvent.PushEvent(GEvent.LoginAuthentFail);
                    break;
                case 0x8049:
                    var p8049 = ParsePacket<R8049PassVerify>(p);
                    if (p8049.Valid)
                        Config.ExecEvent.PushEvent(GEvent.EnterPwd2Success);
                    else Config.ExecEvent.PushEvent(GEvent.EnterPwd2Fail);
                    break;
                default:
                    Log.Warning(Main.Username, $"Chưa xử lý gói tin opcode {p.Opcode}");
                    break;
            }

        }
    }
}