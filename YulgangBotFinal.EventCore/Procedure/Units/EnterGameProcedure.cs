﻿using System.Collections.Generic;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.EventCore.Procedure.Units.Proc;
using YulgangBotFinal.Network.Packet.Gs.Send;

namespace YulgangBotFinal.EventCore.Procedure.Units
{
    public class EnterGameProcedure:BaseProcedure,IHookRecvPacket
    {
        private readonly IList<BaseProcedure> _allProc = new List<BaseProcedure>();
        public override void Tick()
        {
            using (var e = _allProc.GetEnumerator())
                while (e.MoveNext())
                {
                    e.Current?.Tick();
                }

        }

        public override void Init()
        {
            Log.Debug(Main.Username, "Kích hoạt EnterWorld Procedure");
            Main.GsConnect.Register(this, 0x20, 0x57);

            RegisterEvent(GEvent.EnterWorldFake, EnterWorldFake);
            RegisterEvent(GEvent.EnterWorldSuccess, EnterWorldSuccess);

            _allProc.Add(new SysntimeProc());
            _allProc.Add(new HealthyMonitorProc());
            Main.GsConnect.Send(new S0016GameOption(Main.PlayerSessionId).Send());
            Main.GsConnect.Send(new S0180(Main.PlayerSessionId));
            Main.GsConnect.Send(new S008F(Main.PlayerSessionId));
            Main.GsConnect.Send(new S0005LoadCharacter(Main.PlayerSessionId).Send((byte)Main.IndexCharacter));

            foreach (var p in _allProc)
            {
                p.Init(Config);
            }
        }

        private void EnterWorldSuccess()
        {
            
        }

        private void EnterWorldFake()
        {
            Main.GsConnect.Send(new S0020EnterWorld(Main.PlayerSessionId));
            Main.IsEnterGame = true;
            if (Main.Reload)
                Config.ExecEvent.PushEvent(GEvent.EnterWorldSuccess);
            else
            {

                Main.GsConnect.Send(new S0056UndoToSelectCharacter(Main.PlayerSessionId));
            }

        }

        public override void Dispose()
        {

            Log.Debug(Main.Username, "Dispose EnterWorld Procedure");
            using (var e = _allProc.GetEnumerator())
                while (e.MoveNext())
                {
                    e.Current?.Release();
                }
        }

        public void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 0x20:
                    Config.ExecEvent.PushEvent(GEvent.EnterWorldFake);
                    break;
                case 0x57:
                    Config.ExecEvent.PushEvent(GEvent.BackToSelectChar);
                    break;
            }
        }
    }
}