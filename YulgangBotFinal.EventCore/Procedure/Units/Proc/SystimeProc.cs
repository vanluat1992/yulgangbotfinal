﻿using System;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Network.Packet.Gs.Recv;
using YulgangBotFinal.Network.Packet.Gs.Send;

namespace YulgangBotFinal.EventCore.Procedure.Units.Proc
{
    public class SysntimeProc: BaseProcedure, IHookRecvPacket
    {
        private int _time;

        public override void Tick()
        {
            if(Main.IsEnterGame)
            if ((DateTime.Now - _lastRun).TotalMilliseconds > 10000)
            {
                _lastRun = DateTime.Now;
                try
                {
                    var t = Environment.TickCount;
                    var result = t & 0xF000000;
                    _time = (int)(((result | ((0xF00000 & t | (t >> 4) & 0xF000000) >> 8)) >>
                                   12) | ((t & 0xFF0F | 16 * (t & 0xFFFF0000 | ((t & 0xF0) << 8))) << 8));

                    Log.Debug(Main.Username, $"SyncTime : {_time:X4}");
                    Main.GsConnect.Send(new S00B0SyscServerTime(Main.PlayerSessionId).Send(_time));
                }
                catch (Exception ex)
                {
                    Log.Debug(Main.Username, "Send 0xB : Rớt kết nối , yêu cầu đăng nhập lại");
                    Main.RequestLoginAgain = true;
                }
            }
        }

        public DateTime _lastRun { get; set; }

        public override void Init()
        {
            Log.Debug(Main.Username,"Khích hoạt SyncTime Procedure");
            Main.GsConnect.Register(this, 0x80, 0x6);
        }

        public override void Dispose()
        {
            Log.Debug(Main.Username, "Dispose SyncTime Procedure");
            Main.GsConnect.Unregister(this);
        }

        public void HandlePacket(IPacket p)
        {
            if (p.Opcode == 0x80)
            {
                var tmp = ParsePacket<R0080SyncServerTime>(p);
                _time = tmp.TimeTick;
            }
            if (p.Opcode == 0x6)
            {
                // send 1606
                Main.GsConnect.Send(new S1606(Main.PlayerSessionId));
                Main.GsConnect.Send(new S0211(Main.PlayerSessionId));
                Main.GsConnect.Send(new S003CUsingSpellBuff(Main.PlayerSessionId).SendDefault(Main.Player.Location));
            }
        }
    }
}