﻿using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Model;
using YulgangBotFinal.Network.Packet.Gs.Recv;

namespace YulgangBotFinal.EventCore.Procedure.Units.Proc
{
    public class HealthyMonitorProc:BaseProcedure,IHookRecvPacket
    {
        public override void Tick()
        {
            
        }

        public override void Init()
        {
            Log.Debug(Main.Username, "Kích hoạt Healthy monitor process");
            Main.GsConnect.Register(this,0x69);
            RegisterEvent<int, Model.BasicInfo>(GEvent.UpdateHealthy, UpdateHealthy);
        }

        private void UpdateHealthy(int sessionId, BasicInfo info)
        {
            if (Main.PlayerSessionId == sessionId)
            {
                Log.Debug(Main.Username, $"Hp : {info.CurrentHp}/{info.MaxHp}");
                Log.Debug(Main.Username, $"Hp : {info.CurrentMp}/{info.MaxMp}");
                Log.Debug(Main.Username, $"Hp : {info.CurrentSp}/{info.MaxSp}");
            }
        }

        public override void Dispose()
        {
            Log.Debug(Main.Username, "Dispose Healthly monitor process");
            Main.GsConnect.Unregister(this);
        }

        public void HandlePacket(IPacket p)
        {
            switch (p.Opcode)
            {
                case 0x69:
                    var tmp = ParsePacket<R0069UpgradeHpMpSp>(p);
                    Config.ExecEvent.PushEvent(GEvent.UpdateHealthy,tmp.SessionId, tmp.Info);
                    break;
            }
        }
    }
}