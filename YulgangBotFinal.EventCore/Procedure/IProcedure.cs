﻿namespace YulgangBotFinal.EventCore.Procedure
{
    public interface IProcedure
    {
        void Tick();
        void Init(ProcedureConfigParams cfg);
        void Release();
    }
}