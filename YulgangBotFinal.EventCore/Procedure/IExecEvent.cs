﻿namespace YulgangBotFinal.EventCore.Procedure
{
    public interface IExecEvent
    {
        void PushEvent<T1, T2, T3, T4, T5, T6, T7, T8>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5,T6 arg6, T7 arg7, T8 arg8);
        void PushEvent<T1, T2, T3, T4, T5, T6, T7>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7);
        void PushEvent<T1, T2, T3, T4, T5, T6>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);
        void PushEvent<T1, T2, T3, T4, T5>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
        void PushEvent<T1, T2, T3, T4>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3, T4 arg4);
        void PushEvent<T1, T2, T3>(GEvent idEvt, T1 arg1, T2 arg2, T3 arg3);
        void PushEvent<T1, T2>(GEvent idEvt, T1 arg1, T2 arg2);
        void PushEvent<T1>(GEvent idEvt, T1 arg1);
        void PushEvent(GEvent idEvt);
    }
}