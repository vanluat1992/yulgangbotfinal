﻿using System;
using System.Collections.Generic;

namespace YulgangBotFinal.EventCore.Procedure
{
    public class EventEntity
    {
        public EventEntity(Delegate func, params object[] args)
        {
            Func = func;
            var types = func.GetType().GenericTypeArguments;
            if (types.Length != args.Length)
                throw new Exception("parameter invalid");
            for (var i = 0; i < types.Length; i++)
            {
                if (!types[i].IsInstanceOfType(args[i]))
                    throw new Exception(
                        $"wrong type parameter {i} , require {types[i]} , you type {args[i].GetType()} ");
                Args.Add(args[i]);
            }
        }

        public string Name { get; }
        public Delegate Func { get; }
        public IList<object> Args { get; } = new List<object>();
    }
}