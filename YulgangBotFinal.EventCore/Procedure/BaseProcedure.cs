﻿using System;
using System.Collections.Generic;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Define;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;

namespace YulgangBotFinal.EventCore.Procedure
{
    public abstract class BaseProcedure : IProcedure
    {
        protected readonly IDictionary<long, GEvent> AllEventRegister = new Dictionary<long, GEvent>();
        protected ProcedureConfigParams Config { get; private set; }
        protected GEvent CurrentState { get; set; }
        protected ILog Log { get; private set; }
        protected MainDefine Main { get; private set; }
        public abstract void Tick();
        public abstract void Init();
        public void Init(ProcedureConfigParams cfg)
        {
            Config = cfg;
            Log = cfg.Log;
            Main = cfg.Main;
            Init();
        }

        protected void RegisterEvent(GEvent eventId, Action fun)
        {
            AllEventRegister.Add(Config.REvent.RegisterEvent(eventId, fun, this), eventId);
        }
        protected void RegisterEvent<T1>(GEvent eventId, Action<T1> fun)
        {
            AllEventRegister.Add(Config.REvent.RegisterEvent(eventId, fun, this), eventId);
        }
        protected void RegisterEvent<T1, T2>(GEvent eventId, Action<T1, T2> fun)
        {
            AllEventRegister.Add(Config.REvent.RegisterEvent(eventId, fun, this), eventId);
        }
        protected void RegisterEvent<T1, T2, T3>(GEvent eventId, Action<T1, T2, T3> fun)
        {
            AllEventRegister.Add(Config.REvent.RegisterEvent(eventId, fun, this), eventId);
        }
        protected void RegisterEvent<T1, T2, T3, T4>(GEvent eventId, Action<T1, T2, T3, T4> fun)
        {
            AllEventRegister.Add(Config.REvent.RegisterEvent(eventId, fun, this), eventId);
        }
        protected void RegisterEvent<T1, T2, T3, T4, T5>(GEvent eventId, Action<T1, T2, T3, T4, T5> fun)
        {
            AllEventRegister.Add(Config.REvent.RegisterEvent(eventId, fun, this), eventId);
        }
        protected void RegisterEvent<T1, T2, T3, T4, T5, T6>(GEvent eventId, Action<T1, T2, T3, T4, T5, T6> fun)
        {
            AllEventRegister.Add(Config.REvent.RegisterEvent(eventId, fun, this), eventId);
        }
        protected void RegisterEvent<T1, T2, T3, T4, T5, T6, T7>(GEvent eventId, Action<T1, T2, T3, T4, T5, T6, T7> fun)
        {
            AllEventRegister.Add(Config.REvent.RegisterEvent(eventId, fun, this), eventId);
        }
        protected void RegisterEvent<T1, T2, T3, T4, T5, T6, T7, T8>(GEvent eventId, Action<T1, T2, T3, T4, T5, T6, T7, T8> fun)
        {
            AllEventRegister.Add(Config.REvent.RegisterEvent(eventId, fun, this), eventId);
        }
        public void Release()
        {
            foreach (var e in AllEventRegister)
            {
                Config.REvent.UnRegisterEvent(e.Value, e.Key);
            }
            Dispose();
        }

        public abstract void Dispose();
        public T ParsePacket<T>(IPacket p) where T : RecvPacket
        {
            var t = (T)Activator.CreateInstance(typeof(T), p.Data, p.SessionId);
            t?.Parse();
            return (T)t;
        }
    }
}