﻿using YulgangBotFinal.Core.Define;
using YulgangBotFinal.Log;

namespace YulgangBotFinal.EventCore.Procedure
{
    public class ProcedureConfigParams
    {
        public ILog Log { get; }
        public IRegisterEvent REvent { get; }
        public IExecEvent ExecEvent { get; }
        public MainDefine Main { get; }
        public ProcedureConfigParams(ILog log,IRegisterEvent rEvent,IExecEvent exec,MainDefine main)
        {
            Log = log;
            REvent = rEvent;
            ExecEvent = exec;
            Main = main;
        }
    }
}