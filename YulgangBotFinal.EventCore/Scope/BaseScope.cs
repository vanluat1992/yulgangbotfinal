﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.EventCore
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: BaseScope.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Define;
using YulgangBotFinal.EventCore.Unit.Gs;
using YulgangBotFinal.Log;
using YulgangBotFinal.Network.Packet.Gs.Send;

#endregion

namespace YulgangBotFinal.EventCore.Scope
{
    public class BaseScope : IProcScope
    {
        private readonly IList<IRepeatUnitProc> _repeatProcs = new List<IRepeatUnitProc>();
        private BlockingCollection<RepeatProcInfo> _repeatProcsBuild = new BlockingCollection<RepeatProcInfo>();
        private readonly IList<ISquenceUnitProc> _sequenceProcs = new List<ISquenceUnitProc>();
        private CancellationTokenSource _cancel = new CancellationTokenSource();
        private Thread _mainTask;
        private int _step = 0;

        public BaseScope(ILog log)
        {
            Log = log;

            var t = new Thread(HandleRepeatProc);
            t.Start();
        }

        protected ILog Log { get; }

        #region Implementation of IProcScope

        public event Action<IProcScope> OnComplete;
        public MainDefine Main { get; set; }

        public int Step
        {
            get { return _step; }
            set { _step = value; }
        }

        public bool RegisterRepeatProc(IRepeatUnitProc proc)
        {
            _repeatProcs.Add(proc);

            return true;
        }

        public bool RegisterSequenceProc(ISquenceUnitProc proc)
        {
            _sequenceProcs.Add(proc);
            return true;
        }

        public bool RegisterProcScope(IProcScope scope)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     tạm dừng tiến trình
        /// </summary>
        public void Break()
        {
            if (_mainTask == null)
            {
                return;
            }

            _cancel.Cancel();
            while (_mainTask.IsAlive)
            {
                Thread.Sleep(10);
            }
            //_mainTask.Abort();
            _mainTask = null;
            State = ScopeProcessSate.Break;
        }

        /// <summary>
        ///     chạy lại tiến trình
        /// </summary>
        public void Resume()
        {
            if (State == ScopeProcessSate.Break)
                Run();
            else
            {
                Debug.WriteLine("Trạng thái không hợp lệ");
            }
        }

        /// <summary>
        ///     khởi động lại
        /// </summary>
        public void ReStart()
        {
            Break();
            _step = 0;
            State = ScopeProcessSate.Idle;
            Run();
        }

        /// <summary>
        ///     chọn vị trí tiếp tục chạy
        /// </summary>
        /// <param name="step"></param>
        public void Seek(int step)
        {
            _step = step;
        }

        public void Seek(Type step)
        {
            var procTarget = _sequenceProcs.FirstOrDefault(x => x.GetType() == step);
            if (procTarget != null)
            {
                _step = _sequenceProcs.IndexOf(procTarget);
            }
        }

        /// <summary>
        ///     khởi động tiến trình
        /// </summary>
        public void Run()
        {
            if (State == ScopeProcessSate.Run) return;
            State = ScopeProcessSate.Run;
            _cancel = new CancellationTokenSource();
            _mainTask = new Thread(HandleProc);
            _mainTask.Start();
            _repeatProcsBuild = new BlockingCollection<RepeatProcInfo>();
            foreach (var proc in _repeatProcs)
            {
                var procTarget = _sequenceProcs.FirstOrDefault(x => x.GetType() == proc.RunAfter());
                if (procTarget != null)
                {
                    _repeatProcsBuild.Add(new RepeatProcInfo(proc, _sequenceProcs.IndexOf(procTarget)));
                }
            }
        }

        private async void HandleRepeatProc()
        {
            try
            {
                while (true)
                {
                    try
                    {
                        var time = DateTime.Now;

                        var tmp = new List<RepeatProcInfo>(); // _repeatProcsBuild.Where(m => Step > m.Step).ToArray();
                        var relase = new List<RepeatProcInfo>();
                        // _repeatProcsBuild.Where(m => Step <= m.Step).ToArray();
                        foreach (var info in _repeatProcsBuild.ToArray())
                        {

                            if (Step > info.Step)
                                tmp.Add(info);
                            else
                            {
                                relase.Add(info);
                            }
                        }
                        foreach (var info in relase)
                        {
                            info.Proc.SetMainDefine(Main);
                            info.Proc.Clean();
                        }
                        foreach (var procInfo in tmp.Where(m => !m.Proc.Alive))
                        {
                            procInfo.Proc.SetMainDefine(Main);
                            procInfo.Proc.SetScopeProcess(this);
                            procInfo.Proc.Alive = true;
                            await Task.Factory.StartNew(() =>
                            {
                                try
                                {
                                    procInfo.Proc.Run(time);
                                }
                                catch (Exception e)
                                {
                                    Log.Exception(Main.Username, e, $"Run proc {procInfo.Proc.GetType().Name}");
                                }
                                procInfo.Proc.Alive = false;
                            });
                            //t.Start();
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Exception(Main.Username, ex, "Handle repeat proc fail");
                    }
                    Thread.Sleep(20);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                Log.Exception(Main.Username, e, "");
            }
        }

        private void HandleProc()
        {
            try
            {
                begin:
                for (var j = _step; j < _sequenceProcs.Count; j++)
                {
                    if (_cancel.IsCancellationRequested)
                    {
                        State = ScopeProcessSate.Off;
                        return;
                    }
                    if (Main.RequestLoginAgain)
                    {
                        return;
                        Main.RequestLoginAgain = false;
                        Log.Debug(Main.Username, "Xác nhận yêu cầu đăng nhập lại");
                        _step = 0;
                        Main.GsConnect.Release();
                        goto begin;
                    }
                    _step = j;
                    var proc = _sequenceProcs[j];
                    proc.SetMainDefine(Main);
                    proc.SetScopeProcess(this);
                    proc.State = 0;
                    var procresult = proc.Run();
                    proc.Clean();
                    switch (procresult)
                    {
                        case 0:
                            var tmp = proc.GetSeekWhenFail();
                            if (tmp == null)
                            {
                                State = ScopeProcessSate.Break;
                                OnComplete?.Invoke(this);
                                return;
                            }
                            var runPrev = _sequenceProcs.FirstOrDefault(m => m.GetType() == tmp);
                            if (runPrev == null)
                            {
                                State = ScopeProcessSate.Break;
                                OnComplete?.Invoke(this);
                                return;
                            }
                            _step = _sequenceProcs.IndexOf(runPrev);
                            goto begin;
                        case 1:
                            var prs = _repeatProcs.Where(m => m.RunAfter() == proc.GetType());
                            foreach (var p in prs)
                            {
                                p.SetMainDefine(Main);
                                p.Clean();
                                p.Init();
                            }
                            _step = j + 1;
                            break;
                        case -1:
                            // reload lại nhân vật
                            Main.GsConnect.Send(new S0056UndoToSelectCharacter(Main.PlayerSessionId));
                            _step =
                                _sequenceProcs.IndexOf(
                                    _sequenceProcs.FirstOrDefault(m => m.GetType() == typeof (GetListChar)));
                            goto begin;
                        default:
                            State = ScopeProcessSate.Break;
                            OnComplete?.Invoke(this);
                            return;
                    }
                }

                var idle = _sequenceProcs.FirstOrDefault(m => m.GetType() == typeof (IdleBot));
                if (idle != null)
                {
                    _step = _sequenceProcs.IndexOf(idle);
                    goto begin;
                }
            }
            catch (Exception e)
            {
                Log.Exception(Main.Username, e, "");
            }
            State = ScopeProcessSate.Off;
            OnComplete?.Invoke(this);
        }

        /// <summary>
        ///     trạng thái hiện tại
        /// </summary>
        public ScopeProcessSate State { get; private set; }

        #endregion
    }

    public class RepeatProcInfo
    {
        public RepeatProcInfo(IRepeatUnitProc proc, int step)
        {
            Proc = proc;
            Step = step;
        }

        public IRepeatUnitProc Proc { get; set; }
        public int Step { get; set; }
    }
}