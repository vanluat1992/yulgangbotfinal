﻿#region header

// /*********************************************************************************************/
// Project :Scs
// FileName : ScsServerBase.cs
// Time Create : 4:04 PM 18/02/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;
using Hik.Collections;
using Hik.Communication.Scs.Communication.Channels;
using Hik.Communication.Scs.Communication.Protocols;

#endregion

namespace Hik.Communication.Scs.Server
{
    /// <summary>
    ///     This class provides base functionality for server classes.
    /// </summary>
    internal abstract class ScsServerBase : IScsServer
    {
        #region Private properties

        /// <summary>
        ///     This object is used to listen incoming connections.
        /// </summary>
        private IConnectionListener _connectionListener;

        #endregion

        #region Constructor

        /// <summary>
        ///     Constructor.
        /// </summary>
        protected ScsServerBase()
        {
            Clients = new ThreadSafeSortedList<long, IScsServerClient>();
            WireProtocolFactory = WireProtocolManager.GetDefaultWireProtocolFactory();
            MaxConcurentConnect = 5000;
        }

        #endregion

        #region Protected abstract methods

        /// <summary>
        ///     This method is implemented by derived classes to create appropriate connection listener to listen incoming
        ///     connection requets.
        /// </summary>
        /// <returns></returns>
        protected abstract IConnectionListener CreateConnectionListener();

        #endregion

        #region Public events

        /// <summary>
        ///     This event is raised when a new client is connected.
        /// </summary>
        public event EventHandler<ServerClientEventArgs> ClientConnected;

        /// <summary>
        ///     This event is raised when a client disconnected from the server.
        /// </summary>
        public event EventHandler<ServerClientEventArgs> ClientDisconnected;

        #endregion

        #region Public properties

        /// <summary>
        ///     Gets/sets wire protocol that is used while reading and writing messages.
        /// </summary>
        public IScsWireProtocolFactory WireProtocolFactory { get; set; }

        /// <summary>
        ///     A collection of clients that are connected to the server.
        /// </summary>
        public ThreadSafeSortedList<long, IScsServerClient> Clients { get; private set; }

        #endregion

        #region Public methods

        /// <summary>
        ///     Starts the server.
        /// </summary>
        public virtual void Start()
        {
            _connectionListener = CreateConnectionListener();
            _connectionListener.CommunicationChannelConnected += ConnectionListener_CommunicationChannelConnected;
            _connectionListener.Start();
            _connectionListener.OnDebug += OnDebug;
        }

        /// <summary>
        ///     Stops the server.
        /// </summary>
        public virtual void Stop()
        {
            if (_connectionListener != null)
            {
                _connectionListener.Stop();
            }

            foreach (var client in Clients.GetAllItems())
            {
                client.Disconnect();
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        ///     Handles CommunicationChannelConnected event of _connectionListener object.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        private void ConnectionListener_CommunicationChannelConnected(object sender, CommunicationChannelEventArgs e)
        {
            var client = new ScsServerClient(e.Channel)
            {
                ClientId = ScsServerManager.GetClientId(),
                WireProtocol = WireProtocolFactory.CreateWireProtocol(),
                TimeConnect = DateTime.Now
            };
            try
            {
                if (Clients.Count > MaxConcurentConnect)
                {
                    client.Disconnect();
                    OnDebug?.Invoke("ConnectionListener_CommunicationChannelConnected  : Full Connection !");
                    return;
                }

                e.Channel.OnDetectRecvMessage += Channel_OnDetectRecvMessage; 
                client.Disconnected += Client_Disconnected;
                client.OnDebug += Client_OnDebug;
                Clients[client.ClientId] = client;
                OnClientConnected(client);
                e.Channel.Start();
            }
            catch (Exception exception)
            {
                OnDebug?.Invoke($"ConnectionListener_CommunicationChannelConnected : {exception}");
                client.Disconnect();
                OnClientDisconnected(client);
                Console.WriteLine(exception);
            }
        }

        private void Channel_OnDetectRecvMessage()
        {
            OnDetectMessageRecv?.Invoke();
        }

        private void Client_OnDebug(string obj)
        {
            OnDebug?.Invoke(obj);
        }

        /// <summary>
        ///     Handles Disconnected events of all connected clients.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        private void Client_Disconnected(object sender, EventArgs e)
        {
            var client = (IScsServerClient) sender;
            Clients.Remove(client.ClientId);
            OnClientDisconnected(client);
        }

        #endregion

        #region Event raising methods

        /// <summary>
        ///     Raises ClientConnected event.
        /// </summary>
        /// <param name="client">Connected client</param>
        protected virtual void OnClientConnected(IScsServerClient client)
        {
            ClientConnected?.Invoke(this, new ServerClientEventArgs(client));
        }

        /// <summary>
        ///     Raises ClientDisconnected event.
        /// </summary>
        /// <param name="client">Disconnected client</param>
        protected virtual void OnClientDisconnected(IScsServerClient client)
        {
            ClientDisconnected?.Invoke(this, new ServerClientEventArgs(client));
        }

        #endregion

        #region IScsServer Members

        public int MaxConcurentConnect { get; set; }
        public event Action<string> OnDebug;
        public event Action OnDetectMessageRecv;
        public bool IsRuning => _connectionListener?.IsRuning ?? false;

        #endregion
    }
}