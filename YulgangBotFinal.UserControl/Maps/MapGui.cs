﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using YulgangBotFinal.PathFinder;

namespace YulgangBotFinal.UserControl.Maps
{
    public interface IDrawMap
    {
        void Draw(Graphics g);
    }

    [Export(typeof (IPlugin))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public partial class MapGui : System.Windows.Forms.UserControl, IPlugin, IPartImportsSatisfiedNotification
    {
        private readonly bool _ready = true;
        private MapCaculator _backgroundManager;
        //[Import] private TempConfig _config;
        private SynchronizationContext _context;
        private readonly IList<IDrawMap> _drawHandle = new List<IDrawMap>();
        private BufferedGraphicsContext _graphicManager;
        //[Import] private LayerManager _layerManager;
        private Task _mainTaskRefresh;
        private BufferedGraphics _managedBackBuffer;
        private Point _mousePoint;
        //[Import] private PathFinderManager _pathFinderManager;
        public float Zoom { get; set; } = 3.056f;
        public event Action<PointF> OnMapMouseClick;
        public MapGui()
        {
            InitializeComponent();

            Caption = "Map";
            Group = "Hệ thống";
            Name = "Map";

            RegisterEventGui();

            RegisterDoubleBuffer();

            RegisterTaskRefeshMap();
        }

        public void OnImportsSatisfied()
        {
        }

        public string Caption { set; get; }
        public Control Me => this;
        public string Group { set; get; }

        public void SetMapManager(MapCaculator mng)
        {
            _backgroundManager = mng;
        }

        public void AddDrawer(IDrawMap dr)
        {
            _drawHandle.Add(dr);
        }

        private void RegisterTaskRefeshMap()
        {
            _mainTaskRefresh = new Task(TaskRefesh);
            _mainTaskRefresh.Start();
        }

        private void RegisterDoubleBuffer()
        {
            _context = SynchronizationContext.Current;
            SetStyle(
                ControlStyles.OptimizedDoubleBuffer | ControlStyles.ContainerControl |
                ControlStyles.AllPaintingInWmPaint, true);
            _graphicManager = BufferedGraphicsManager.Current;
            _graphicManager.MaximumBuffer = new Size(ClientRectangle.Width + 1, ClientRectangle.Height + 1);
        }

        private void RegisterEventGui()
        {
        }

        private void TaskRefesh()
        {
            while (_ready)
            {
                Invalidate();
                Thread.Sleep(10);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            _managedBackBuffer?.Dispose();
            _managedBackBuffer = _graphicManager.Allocate(e.Graphics, ClientRectangle);
            _managedBackBuffer.Graphics.Clear(Color.Brown);

            ////if (_config.EnterGame && _config.Player != null)
            //{
            //    DrawBackground(_managedBackBuffer.Graphics);

            //    DrawText(_managedBackBuffer.Graphics);

            //    DrawLayer(_managedBackBuffer.Graphics);

            //    _pathFinderManager.Draw(false, _managedBackBuffer.Graphics);
            //}
            foreach (var drawMap in _drawHandle)
            {
                drawMap.Draw(_managedBackBuffer.Graphics);
            }

            _managedBackBuffer.Render(e.Graphics);
        }

        //private void DrawLayer(Graphics g)
        //{
        //    IGameLayer main = null;
        //    foreach (var layer in _layerManager.GetAllLayer())
        //    {
        //        var point = _backgroundManager.PointMapToPointControl(new PointF(layer.Location.X, layer.Location.Y));
        //        var tmpsize = 5*_zoom/2;
        //        switch (layer.Type)
        //        {
        //            case GameLayerType.Player:
        //                g.FillRectangle(new SolidBrush(layer.IsTarget ? Color.Red : Color.MediumOrchid),
        //                    point.X - tmpsize, point.Y - tmpsize, 5*_zoom, 5*_zoom);
        //                break;
        //            case GameLayerType.Npc:
        //                g.FillRectangle(new SolidBrush(layer.IsTarget ? Color.Red : Color.Blue), point.X - tmpsize,
        //                    point.Y - tmpsize, 5*_zoom, 5*_zoom);
        //                break;
        //            case GameLayerType.Monster:
        //                g.FillRectangle(new SolidBrush(layer.IsTarget ? Color.Red : Color.Green), point.X - tmpsize,
        //                    point.Y - tmpsize, 5*_zoom, 5*_zoom);
        //                break;
        //            case GameLayerType.Item:
        //                g.FillRectangle(new SolidBrush(layer.IsTarget ? Color.YellowGreen : Color.Yellow),
        //                    point.X - tmpsize, point.Y - tmpsize, 5*_zoom, 5*_zoom);
        //                break;
        //            case GameLayerType.Party:
        //                g.FillRectangle(new SolidBrush(Color.Fuchsia),
        //                    point.X - tmpsize, point.Y - tmpsize, 5*_zoom, 5*_zoom);
        //                break;
        //            case GameLayerType.MainPlayer:
        //                main = layer;
        //                break;
        //        }
        //    }


        //    foreach (var layer in _layerManager.GetAllBootLayer())
        //    {
        //        var point = _backgroundManager.PointMapToPointControl(new PointF(layer.Location.X, layer.Location.Y));


        //        var radius1 =
        //            _backgroundManager.PointMapToPointControl(new PointF(layer.Location.X - layer.Radius,
        //                layer.Location.Y - layer.Radius));
        //        var radius2 =
        //            _backgroundManager.PointMapToPointControl(new PointF(layer.Location.X + layer.Radius,
        //                layer.Location.Y + layer.Radius));
        //        var tmpsize = 5 * _zoom / 2;
        //        var font = new Font(new FontFamily("Tahoma"), 10, FontStyle.Regular, GraphicsUnit.Pixel);
        //        switch (layer.Type)
        //        {
        //            case BootLayerType.AttackPoint:
        //                g.FillRectangle(new SolidBrush(Color.White), 
        //                    point.X - tmpsize, point.Y - tmpsize, 5 * _zoom, 5 * _zoom);

        //                g.DrawEllipse(new Pen(Color.Indigo), radius1.X,radius1.Y,
        //                   radius2.X-radius1.X,radius2.Y-radius1.Y);

        //                break;
        //            case BootLayerType.WraptGate:
        //                g.FillRectangle(new SolidBrush(Color.Turquoise),
        //                   point.X - tmpsize, point.Y - tmpsize, 5 * _zoom, 5 * _zoom);
        //                break;
        //        }

        //        g.DrawString(
        //            layer.Name, font,
        //            Brushes.Black, point.X + tmpsize + 3, point.Y);
        //    }

        //    if (main != null)
        //    {
        //        var tmpsize = 8 * _zoom / 2;
        //        var point = _backgroundManager.PointMapToPointControl(new PointF(main.Location.X, main.Location.Y));
        //        g.FillEllipse(new SolidBrush(Color.Coral), point.X - tmpsize, point.Y - tmpsize, 8 * _zoom, 8 * _zoom);
        //    }
        //}

        //private void DrawText(Graphics g)
        //{
        //    var font = new Font(new FontFamily("Tahoma"), 12, FontStyle.Regular, GraphicsUnit.Pixel);

        //    g.DrawString(
        //        _backgroundManager.PointControlToPointMap(new PointF(_mousePoint.X, _mousePoint.Y)).ToString(), font,
        //        Brushes.Black, 10, 10);
        //}

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            _backgroundManager.OnmouseWheel(e);
            if (e.Delta > 0)
            {
                Zoom *= 1.5f;
            }
            else
            {
                Zoom /= 1.5f;
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            _backgroundManager?.BackupCenterPoint(new Point(e.X, e.Y));
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Debug.WriteLine($"{_backgroundManager.PointControlToPointMap(new PointF(e.X, e.Y))}");
                contextMenuStrip1.Show(this, _mousePoint);
            }
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                //if (_config.EnterGame && _config.Player != null)
                // _pathFinderManager.Move(_config.Player.Location, new Point(e.X, e.Y));
                OnMapMouseClick?.Invoke(_backgroundManager.PointControlToPointMap(new PointF(e.X, e.Y)));
            }
        }

        protected override void OnResize(EventArgs e)
        {
            _backgroundManager?.SetClientSize(ClientRectangle);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            _mousePoint = new Point(e.X, e.Y);
            //_pathFinderManager.FocusCellFromPointControl(_mousePoint);
            if (e.Button == MouseButtons.Left)
             _backgroundManager?.Move(new Point(e.X, e.Y));
        }

        //private void DrawBackground(Graphics g)
        //{
        //    /*Lấy thông tin bản đồ nhân vật đang đứng*/

        //    if (_config.Player == null) return;
        //    if (_backgroundManager.MapId != _config.Player.Location.MapId)
        //    {
        //        _backgroundManager.SetBackground(_config.Player.Location.MapId, ClientRectangle);
        //        _backgroundManager.SetCenterPointFromMapPoing(new PointF(_config.Player.Location.X,
        //            _config.Player.Location.Y));
        //    }

        //    /*Lấy diện tích dc hiển thị*/
        //    g.DrawImage(_backgroundManager.GetBackgroundImage(), _backgroundManager.GetClientRect(),
        //        _backgroundManager.GetImageRect(), GraphicsUnit.Pixel);
        //}

        private void mmSetAttackPoint_Click(object sender, EventArgs e)
        {
            /*Thiết lập điểm đánh */
        }
    }
}