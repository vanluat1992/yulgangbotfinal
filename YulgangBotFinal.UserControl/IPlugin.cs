﻿using System.Windows.Forms;

namespace YulgangBotFinal.UserControl
{
    /// <summary>
    ///
    /// </summary>
    public interface IPlugin
    {
        /// <summary>
        /// Tên của plugin
        /// </summary>
        string Name { set; get; }

        /// <summary>
        /// Tên hiển thị
        /// </summary>
        string Caption { set; get; }

        /// <summary>
        /// đối tượng xử lý
        /// </summary>
        Control Me { get; }

        /// <summary>
        /// Nhóm
        /// </summary>
        string Group { set; get; }
    }
}