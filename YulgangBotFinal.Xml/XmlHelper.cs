﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace YulgangBotFinal.Xml
{
    public static class XmlHelper
    {
        public static bool Deserializer<T>(this T obj, string filename)
        {
            if (!File.Exists(filename))
            {
                return false;
            }
            var reader = new StreamReader(filename);
            obj = (T) new XmlSerializer(typeof (T)).Deserialize(reader);
            reader.Dispose();
            return true;
        }

        public static bool Deserializer(object obj, Type t, string filename)
        {
            if (!File.Exists(filename))
            {
                return false;
            }
            var reader = new StreamReader(filename);
            obj = new XmlSerializer(t).Deserialize(reader);
            reader.Dispose();
            return true;
        }

        public static void Serializer<T>(this T obj, string filename)
        {
            var writer = new StreamWriter(filename);
            new XmlSerializer(typeof (T)).Serialize(writer, obj);
            writer.Dispose();
        }

        public static void Serializer(this object obj, Type t, string filename)
        {
            var writer = new StreamWriter(filename);
            new XmlSerializer(t).Serialize(writer, obj);
            writer.Dispose();
        }
    }
}