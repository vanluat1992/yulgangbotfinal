﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel.Composition;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using YulgangBotFinal.Log;

namespace Log
{
    public class LogFiles : IAttackLog,IDisposable
    {
        private readonly ConcurrentQueue<Tuple<LogType, string>> _queue = new ConcurrentQueue<Tuple<LogType, string>>();
        private readonly CancellationTokenSource _cancelTaskHandle = new CancellationTokenSource();
        private string _path = Environment.CurrentDirectory;//.MapPath("~");
        private string GetFileNameFromType(LogType type)
        {
            var timePRefix = $"log{DateTime.Now.Day}_{DateTime.Now.Month}_{DateTime.Now.Year}";

            var path =_path  + "\\Logs";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            var fileName = string.Format("{2}\\{0}  {1}.txt", timePRefix, Enum.GetName(typeof (LogType), type), path);
            if (!File.Exists(fileName))
                File.Create(fileName).Close();
            return fileName;
        }


#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task<bool> Writer(string tag,LogType type, string log)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            _queue.Enqueue(new Tuple<LogType, string>(type, log));
            return true;
        }

        public void SetPath(string path)
        {
            _path = path;
        }

        public LogFiles()
        {
            _path = Path.GetDirectoryName(Assembly.GetAssembly(GetType()).Location);
            Task.Factory.StartNew(HandleLog);
        }
        private async void HandleLog()
        {
            while (true)
            {
                if (!_queue.IsEmpty)
                {
                    Tuple<LogType, string> data;
                    if (_queue.TryDequeue(out data))
                    {
                        try
                        {
                            var file = GetFileNameFromType(data.Item1);
                            var f = new StreamWriter(file,true);
                            f.WriteLine(data.Item2);
                            f.Flush();
                            f.Dispose();
                        }
                        catch
                        {
                        }
                        
                    }
                }
                if(_cancelTaskHandle.IsCancellationRequested)
                    return;
                await Task.Delay(100);
            }
        }

        public void Dispose()
        {
            _cancelTaskHandle.Cancel();
        }
    }
}