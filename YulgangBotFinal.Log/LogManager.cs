﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Log
// FileName : LogManager.cs
// Time Create : 7:51 AM 19/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

#endregion

namespace YulgangBotFinal.Log
{
    /// <summary>
    ///     The log manager.
    /// </summary>
    [Export(typeof (ILog))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class LogManager : ILog, IPartImportsSatisfiedNotification
    {
        /// <summary>
        ///     danh sách các attack log
        /// </summary>
        //[ImportMany(typeof(IAttackLog))]
        private readonly IList<IAttackLog> _attaclLogs = new List<IAttackLog>();

        /// <summary>
        ///     Lấy lớp attack log
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetAttackLog<T>() where T : class, IAttackLog
        {
            return (T) _attaclLogs.FirstOrDefault(m => m.GetType() == typeof (T));
        }

        public LogType LogLevel { set; get; }

        public void InstallAttackLog(IAttackLog at)
        {
            _attaclLogs.Add(at);
        }

        /// <summary>
        ///     debug log
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="format">
        ///     format
        /// </param>
        /// <param name="arg">
        ///     argument
        /// </param>
        public void Debug(string tag, string format, params object[] arg)
        {
            //StackTrace stackTrace = new StackTrace(); 
            //StackFrame[] stackFrames = stackTrace.GetFrames();

            //StackFrame callingFrame = stackFrames[1];

            //MethodInfo method = (MethodInfo)callingFrame.GetMethod();
            //this.Writer(tag, LogType.Debug, $"{method.DeclaringType.Name}.{method.Name}", format, arg);
            Writer(tag, LogType.Debug, "", format, arg);
        }

        /// <summary>
        ///     info log
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="format">
        ///     format
        /// </param>
        /// <param name="arg">
        ///     argument
        /// </param>
        public void Info(string tag, string format, params object[] arg)
        {
            //StackTrace stackTrace = new StackTrace();
            //StackFrame[] stackFrames = stackTrace.GetFrames();

            //StackFrame callingFrame = stackFrames[1];
            //if (callingFrame.GetMethod() is MethodInfo)
            {
                //MethodInfo method = (MethodInfo) callingFrame.GetMethod();
                Writer(tag, LogType.Info, "", format, arg);
            }
            //else
            //{
            //    this.Writer(tag, LogType.Info, $"UK", format, arg);
            //}
        }

        /// <summary>
        ///     success log
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="format">
        ///     format
        /// </param>
        /// <param name="arg">
        ///     argument
        /// </param>
        public void Success(string tag, string format, params object[] arg)
        {
            //StackTrace stackTrace = new StackTrace();
            //StackFrame[] stackFrames = stackTrace.GetFrames();

            //StackFrame callingFrame = stackFrames[1];

            //MethodInfo method = (MethodInfo)callingFrame.GetMethod();
            //this.Writer(tag, LogType.Success, $"{method.DeclaringType.Name}.{method.Name}", format, arg);
            Writer(tag, LogType.Success, "", format, arg);
        }

        /// <summary>
        ///     Error log
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="format">
        ///     format
        /// </param>
        /// <param name="arg">
        ///     argument
        /// </param>
        public void Error(string tag, string format, params object[] arg)
        {
            //StackTrace stackTrace = new StackTrace();
            //StackFrame[] stackFrames = stackTrace.GetFrames();

            //StackFrame callingFrame = stackFrames[1];

            //MethodInfo method = (MethodInfo)callingFrame.GetMethod();
            // this.Writer(tag, LogType.Error, $"{method.DeclaringType.Name}.{method.Name}", format, arg);
            Writer(tag, LogType.Error, "", format, arg);
        }

        /// <summary>
        ///     Warning log
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="format">
        ///     format
        /// </param>
        /// <param name="arg">
        ///     argument
        /// </param>
        public void Warning(string tag, string format, params object[] arg)
        {
            //StackTrace stackTrace = new StackTrace();
            //StackFrame[] stackFrames = stackTrace.GetFrames();

            //StackFrame callingFrame = stackFrames[1];

            //MethodInfo method = (MethodInfo)callingFrame.GetMethod();
            //this.Writer(tag, LogType.Warning, $"{method.DeclaringType.Name}.{method.Name}", format, arg);
            Writer(tag, LogType.Warning, "", format, arg);
        }

        /// <summary>
        ///     Fatal log
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="format">
        ///     format
        /// </param>
        /// <param name="arg">
        ///     argument
        /// </param>
        public void Fatal(string tag, string format, params object[] arg)
        {
            //StackTrace stackTrace = new StackTrace();
            //StackFrame[] stackFrames = stackTrace.GetFrames();

            //StackFrame callingFrame = stackFrames[1];

            //MethodInfo method = (MethodInfo)callingFrame.GetMethod();
            //this.Writer(tag, LogType.Fatal, $"{method.DeclaringType.Name}.{method.Name}", format, arg);
            Writer(tag, LogType.Fatal, "", format, arg);
        }

        /// <summary>
        ///     Exception log
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="e">
        ///     Exception
        /// </param>
        /// <param name="format">
        ///     format
        /// </param>
        /// <param name="arg">
        ///     argument
        /// </param>
        public void Exception(string tag, Exception e, string format, params object[] arg)
        {
            //StackTrace stackTrace = new StackTrace();
            //StackFrame[] stackFrames = stackTrace.GetFrames();

            //StackFrame callingFrame = stackFrames[1];

            //MethodInfo method = (MethodInfo)callingFrame.GetMethod();
            //this.Writer(tag,
            //    LogType.Exception,
            //    $"{method.DeclaringType.Name}.{method.Name}",
            //    string.Format(string.Format(format, arg) + " :{0}", e.Message + "\r\n" + e.StackTrace));
            Writer(tag,
                LogType.Exception,
                "",
                string.Format(string.Format(format, arg) + " :{0}", e.Message + "\r\n" + e.StackTrace));
        }

        /// <summary>
        /// </summary>
        /// <param name="data"></param>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void Dum(byte[] data,string format,params object[] args)
        {
            var tp = "";//"offset   |                        data                         |        ascii \r\n";
            var t16 = data.Length % 16;
            var div = (data.Length - t16) / 16;

            string tran;
            for (var i = 0; i < div; i++)
            {
                tp += $"{i.ToString("X8")} : ";
                tran = "";
                for (var j = 0; j < 16; j++)
                {
                    var val = data[i * 16 + j];
                    tp += $" {data[i * 16 + j].ToString("X2")}";
                    tran += $"{(val > 31 && val < 127 ? Convert.ToChar(val) : '.')}";
                }
                tp += "    ;   " + tran + "\r\n";
            }
            if (t16 > 0)
            {
                tp += $"{(div + 1).ToString("X8")} : ";
                tran = "";
                for (var i = 0; i < 16; i++)
                {
                    if (div * 16 + i < data.Length)
                    {
                        var val = data[i + div * 16];
                        tp += $" {val.ToString("X2")}";
                        tran += $"{(val > 31 && val < 127 ? Convert.ToChar(val) : '.')}";
                    }
                    else
                        tp += "   ";
                }
                tp += "    ;   " + tran;
            }
            Writer("DUMP", LogType.Dum, "", $"{string.Format(format, args)}\r\n{tp}");
        }

        /// <summary>
        ///     Called when a part's imports have been satisfied and it is safe to use.
        /// </summary>
        public void OnImportsSatisfied()
        {
        }

        /// <summary>
        ///     The writer.
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="type">
        ///     The type.
        /// </param>
        /// <param name="source">
        ///     source call log method
        /// </param>
        /// <param name="format">
        ///     The format.
        /// </param>
        /// <param name="arg">
        ///     The arg.
        /// </param>
        private async void Writer(string tag, LogType type, string source, string format, params object[] arg)
        {
            try
            {
                if ((type & LogLevel) == type)
                {
                    return;
                }

                var data = string.Format(
                    "{0} [{4}] [{1}][{2}]: {3}",
                    DateTime.Now.ToString("dd-MM HH:mm:ss"),
                    type.ToString().ToUpper(),
                    source,
                    string.Format(format, arg), tag);
                foreach (var attaclLog in _attaclLogs)
                {
                    try
                    {
                        await attaclLog.Writer(tag, type, data);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}