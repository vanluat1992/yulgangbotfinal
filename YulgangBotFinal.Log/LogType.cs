﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Log
// FileName : LogType.cs
// Time Create : 7:51 AM 19/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;

#endregion

namespace YulgangBotFinal.Log
{
    [Flags]
    public enum LogType
    {
        /// <summary>
        ///     The debug.
        /// </summary>
        Debug = 1,

        /// <summary>
        ///     The info.
        /// </summary>
        Info = 1 << 1,

        /// <summary>
        ///     The fatal.
        /// </summary>
        Fatal = 1 << 2,

        /// <summary>
        ///     The warning.
        /// </summary>
        Warning = 1 << 3,

        /// <summary>
        ///     The error.
        /// </summary>
        Error = 1 << 4,

        /// <summary>
        ///     The success.
        /// </summary>
        Success = 1 << 5,

        /// <summary>
        ///     The exception.
        /// </summary>
        Exception = 1 << 6,
        Dum = 1 << 7
    }
}