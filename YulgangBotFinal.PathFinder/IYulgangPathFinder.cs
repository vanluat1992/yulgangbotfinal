﻿using System.Collections.Generic;
using System.Drawing;
using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.PathFinder
{
    public interface IYulgangPathFinder
    {
        IList<IYulgangLocation> Route(IYulgangLocation from, IYulgangLocation to);
        int GetMaxSize();
        byte[,] GetGrid();
    }
}