﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Resource;

namespace YulgangBotFinal.PathFinder
{
    public class MapCaculator
    {
        private PointF _backupCenterPoint;
        private PointF _centerPoint;
        private RectangleF _clientDisplay;
        private RectangleF _currentDisplay;
        private Image _image;
        private Size _imageSize;
        private IYulgangMapRectangle _mapRect;
        private PointF _oldCenterPoint;

        private readonly YBiHandle
            <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill, YulgangYbiGameSpell>
            _ybi;

        private int _zoom = -448;

        public MapCaculator(
            YBiHandle
                <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill, YulgangYbiGameSpell>
                ybi)
        {
            _ybi = ybi;
        }

        public int MapId { get; private set; }

        /// <summary>
        ///     Called when a part's imports have been satisfied and it is safe to use.
        /// </summary>
        public void OnImportsSatisfied()
        {
            _zoom = -448;
            _centerPoint = new PointF(256, 256);
        }

        /// <summary>
        ///     Lấy kích thước khu vực ảnh cần vẽ.
        /// </summary>
        /// <returns></returns>
        public RectangleF GetImageRect()
        {
            _currentDisplay = GetImageRect(_centerPoint);
            FixCenterPoint();
            return _currentDisplay;
        }

        public IYulgangMapRectangle GetMapRect()
        {
            return _mapRect;
        }

        public float ConvertMapToControl(float val)
        {
            var valImg = ConvertMapToImage(val);
            return Math.Abs(ConvertImageToControl(valImg));
        }

        private float ConvertMapToImage(float val)
        {
            return val* _imageSize.Width/_mapRect.Width;
        }

        private float ConvertImageToControl(float val)
        {
            return val*_clientDisplay.Width/ _imageSize.Width;
        }

        private void FixCenterPoint()
        {
            if (_currentDisplay.X < 0)
            {
                var v = Math.Abs(_currentDisplay.X);
                _currentDisplay.X = 0;
                _centerPoint.X += v;
                //_currentDisplay.Width -= v;
            }

            if (_currentDisplay.Y < 0)
            {
                var v = Math.Abs(_currentDisplay.Y);
                _currentDisplay.Y = 0;
                _centerPoint.Y += v;
                // _currentDisplay.Height -= v; 
            }
        }

        private RectangleF GetImageRect(PointF center)
        {
            var tmp = new RectangleF(0, 0, _imageSize.Width + _zoom, _imageSize.Height + _zoom);
            var tmpCenterDisplay = new PointF(tmp.Width/2, tmp.Height/2);

            tmp.X = center.X - tmpCenterDisplay.X;
            tmp.Y = center.Y - tmpCenterDisplay.Y;
            return tmp;
        }

        public RectangleF GetClientRect()
        {
            return _clientDisplay;
        }

        public Image GetBackgroundImage()
        {
            return _image;
        }

        public void SetBackground(int mapid, Rectangle client)
        {
            var file = $"Resource\\Maps\\{mapid}.jpg";
            if (!File.Exists(file))
                throw new FileNotFoundException("Không tìm thấy map file !");
            _image = Image.FromFile(file);
            _imageSize = new Size(_image.Width, _image.Height);
            MapId = mapid;
            SetClientSize(client);

            IYulgangYbiGameMap map;
            if (!_ybi.DicMaps.TryGetValue(mapid, out map))
                throw new Exception("Hệ thống ko hỗ trợ bản đồ này !");

            _mapRect = map.MapRectangle; //new RectangleF(map.X1, map.Y2, map.X2 - map.X1, map.Y2 - map.Y1);
        }

        public void OnmouseWheel(MouseEventArgs arg)
        {
            if (_image == null) return;
            _zoom = arg.Delta < 0 ? _zoom + 2 : _zoom - 2;
            if (_zoom > 0) _zoom = 0;
            //Console.WriteLine(_zoom);
            //_centerPoint.X -= arg.X*_currentDisplay.Width/_clientDisplay.Width;
            //_centerPoint.Y -= arg.Y*_currentDisplay.Height/_clientDisplay.Height;
        }

        public void Move(Point p)
        {
            // _centerPoint 
            var xmove = (p.X - _oldCenterPoint.X)*_currentDisplay.Width/_clientDisplay.Width;
            var ymove = (p.Y - _oldCenterPoint.Y)*_currentDisplay.Height/_clientDisplay.Height;
            if (ValidMove((int) xmove, (int) ymove))
            {
                _centerPoint.X = _backupCenterPoint.X - xmove;
                _centerPoint.Y = _backupCenterPoint.Y - ymove;
                BackupCenterPoint(p);
                //Console.WriteLine(_centerPoint);
            }
        }

        private bool ValidMove(int xmove, int ymove)
        {
            //X1
            //|------------|
            //|            |
            //|            |
            //|------------|
            //            X2
            // tính tọa độ x1 và x2 xem có chạm ngưỡng hay chưa
            // (0,0) (512,512) ngưỡng của image.
            var tmpPoint = new PointF {X = _backupCenterPoint.X - xmove, Y = _backupCenterPoint.Y - ymove};
            var tmp = GetImageRect(tmpPoint);
            //if (tmp.X < 0) return false;
            //if (tmp.Y < 0) return false;
            if (tmp.Width + tmp.X > _imageSize.Width) return false;
            if (tmp.Height + tmp.Y > _imageSize.Height) return false;
            return true;
        }

        public PointF GetCenterPoint()
        {
            return _centerPoint;
        }

        public void SetCenterPointFromMapPoing(PointF p)
        {
            _centerPoint = PointMapToPointImage(p);
        }

        public PointF PointControlToPointMap(PointF p)
        {
            var pointImage = PointControlToPointImage(p);
            return PointImageToPointMap(pointImage);
        }

        public PointF PointMapToPointControl(PointF p)
        {
            var pointImage = PointMapToPointImage(p);
            var pointControl = PointImageToPointControl(pointImage);
            return pointControl;
        }

        private PointF PointMapToPointImage(PointF p)
        {
            var x = (p.X - _mapRect.X1)* _imageSize.Width/_mapRect.Width;
            var y = (p.Y - _mapRect.Y1)* _imageSize.Height/_mapRect.Height;
            return new PointF(x, y);
        }

        private PointF PointImageToPointMap(PointF p)
        {
            var x = p.X*_mapRect.Width/ _imageSize.Width + _mapRect.X1;
            var y = p.Y*_mapRect.Height/ _imageSize.Height + _mapRect.Y1;
            return new PointF(x, y);
        }

        private PointF PointImageToPointControl(PointF p)
        {
            var x = (p.X - _currentDisplay.X)*_clientDisplay.Width/_currentDisplay.Width;
            var y = (p.Y - _currentDisplay.Y)*_clientDisplay.Height/_currentDisplay.Height;
            return new PointF(x, y);
        }

        private PointF PointControlToPointImage(PointF p)
        {
            var x = p.X*_currentDisplay.Width/_clientDisplay.Width + _currentDisplay.X;
            var y = p.Y*_currentDisplay.Height/_clientDisplay.Height + _currentDisplay.Y;
            return new PointF(x, y);
        }

        public void BackupCenterPoint(Point p)
        {
            _oldCenterPoint = new PointF(p.X, p.Y);
            _backupCenterPoint = new PointF(_centerPoint.X, _centerPoint.Y);
        }

        public void SetClientSize(Rectangle client)
        {
            _clientDisplay = new RectangleF(client.X, client.Y, client.Width, client.Height);
        }
    }
}