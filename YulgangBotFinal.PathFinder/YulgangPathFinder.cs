﻿using System;
using System.Collections.Generic;
using System.Drawing;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.PathFinder.Algorithms;

namespace YulgangBotFinal.PathFinder
{
    public class YulgangPathFinder:IYulgangPathFinder
    {
        private readonly MapCaculator _map;

        private const int MaxSize = 2048;
        private readonly byte[,] _grid = new byte[MaxSize, MaxSize];

        public YulgangPathFinder(MapCaculator map)
        {
            _map = map;
            RegisterArray();
        }

        private void RegisterArray()
        {
            for (var i = 0; i < MaxSize; i++)
            {
                for (var j = 0; j < MaxSize; j++)
                {
                    _grid[i, j] = 1;
                }
            }
            // _grid[0, 0] = 0;
        }
        
        public IList<IYulgangLocation> Route(IYulgangLocation @from, IYulgangLocation to)
        {
            //todo: hack
            return new List<IYulgangLocation>() {to};
            var mapArea = _map.GetMapRect();
            if (mapArea == null)
            {
                throw new NullReferenceException("Mapcaculator null");
            }
            var cellWidth = (mapArea.Width / MaxSize);
            var cellHeight = (mapArea.Height / MaxSize);

            var begin = new Point((int)((@from.X - mapArea.X1) / cellWidth), (int)((@from.Y - mapArea.Y1) / cellHeight));
            var end = new Point((int)((to.X - mapArea.X1) / cellWidth), (int)((to.Y - mapArea.Y1) / cellHeight));

            var mPathFinder = new PathFinderFast(_grid)
            {
                Formula = HeuristicFormula.EuclideanNoSQR,
                Diagonals = true,
                HeavyDiagonals = true,
                HeuristicEstimate = 30,
                PunishChangeDirection = false,
                TieBreaker = false,
                SearchLimit = 50000,
                DebugProgress = false,
                ReopenCloseNodes = false,
                DebugFoundPath = false
            };


            var path = mPathFinder.FindPath(begin, end).Result;
            if (path == null)
            {
                throw new NullReferenceException("Can't find path");
            }

            return ConvertPathPoint(from, to, path);
        }

        public int GetMaxSize()
        {
            return MaxSize;
        }

        public byte[,] GetGrid()
        {
            return _grid;
        }

        private IList<IYulgangLocation> ConvertPathPoint(IYulgangLocation begin, IYulgangLocation end, List<PathFinderNode> path)
        {
            var result = new List<IYulgangLocation>();
            var map = _map.GetMapRect();
            var cellWidth = map.Width / MaxSize;
            var cellHeight = map.Height / MaxSize;
            result.Add(new YulgangLocation(end.X, end.Y,end.MapId));
            foreach (var pathFinderNode in path)
            {
                var p1 = GetControlPointFormPathLocation(pathFinderNode.X, pathFinderNode.Y, cellWidth, cellHeight);
                var p2 = GetControlPointFormPathLocation(pathFinderNode.X + 1, pathFinderNode.Y + 1, cellWidth,
                    cellHeight);
                var point = new YulgangLocation((p1.X + p2.X)/2, (p1.Y + p2.Y)/2, begin.MapId);
                result.Add(point);
            }
            result.Add(new YulgangLocation(begin.X, begin.Y,begin.MapId));
            result.Reverse();
            return result;
        }
        private PointF GetControlPointFormPathLocation(int i, int j, float cellw, float cellh)
        {
            var map = _map.GetMapRect();
            return new PointF(i * cellw + map.X1, j * cellh + map.Y1);
        }
    }
}