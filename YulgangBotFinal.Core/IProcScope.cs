﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Core
// FileName : IProcScope.cs
// Time Create : 1:31 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;
using YulgangBotFinal.Core.Define;

#endregion

namespace YulgangBotFinal.Core
{
    public interface IProcScope
    {
        MainDefine Main { get; set; }
        int Step { get; set; }

        /// <summary>
        ///     trạng thái hiện tại
        /// </summary>
        ScopeProcessSate State { get; }

        event Action<IProcScope> OnComplete;
        bool RegisterRepeatProc(IRepeatUnitProc proc);
        bool RegisterSequenceProc(ISquenceUnitProc proc);
        bool RegisterProcScope(IProcScope scope);

        /// <summary>
        ///     tạm dừng tiến trình
        /// </summary>
        void Break();

        /// <summary>
        ///     chạy lại tiến trình
        /// </summary>
        void Resume();

        /// <summary>
        ///     khởi động lại
        /// </summary>
        void ReStart();

        /// <summary>
        ///     chọn vị trí tiếp tục chạy
        /// </summary>
        /// <param name="step"></param>
        void Seek(int step);
        void Seek(Type step);

        /// <summary>
        ///     khởi động tiến trình
        /// </summary>
        void Run();
    }

    public enum ScopeProcessSate
    {
        Idle,
        Run,
        Break,
        Off
    }
}