﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Core
// FileName : ISquenceUnitProc.cs
// Time Create : 1:31 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;
using YulgangBotFinal.Core.Define;

#endregion

namespace YulgangBotFinal.Core
{
    /// <summary>
    ///     các tiểu tiến trình được chạy tuần tự
    /// </summary>
    public interface ISquenceUnitProc : IUnitProcess
    {
        int Run();
        Type GetSeekWhenFail();
    }
}