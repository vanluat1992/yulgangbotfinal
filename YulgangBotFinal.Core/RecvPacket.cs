﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Core
// FileName : RecvPacket.cs
// Time Create : 3:10 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System.Text;
using YulgangBotFinal.Core.Utils;

#endregion

namespace YulgangBotFinal.Core
{
    public abstract class RecvPacket : ReadBuff
    {
        public RecvPacket(byte[] data) : base(data)
        {
            //Parse();
        }

        public RecvPacket(byte[] data, Encoding en) : base(data, en)
        {
            //Parse();
        }

        public abstract void Parse();
    }
}