﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.Network
// FileName : IConnection.cs
// Time Create : 1:28 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion

using System;
using YulgangBotFinal.Core.Packet;

namespace YulgangBotFinal.Core
{
    public interface IConnection
    {
        void Connect(string ip, int port);
        void Register(IHookRecvPacket proc, params int[] opcodes);
        void Send(IPacket p);
        void Send(SendPacket p);
        void Release();
        void Unregister(IHookRecvPacket proc);
        event Action Disconnect;
    }
}