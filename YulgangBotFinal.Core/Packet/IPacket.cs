﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.Network
// FileName : IPacket.cs
// Time Create : 10:59 AM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion
namespace YulgangBotFinal.Core.Packet
{
    public interface IPacket
    {
        int Opcode { get; set; }
        int SessionId { get; set; }
        byte[] Data { get; set; } 
    }
}