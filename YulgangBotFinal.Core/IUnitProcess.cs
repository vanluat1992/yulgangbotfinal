﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Core
// FileName : IUnitProcess.cs
// Time Create : 1:31 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;
using YulgangBotFinal.Core.Define;
using YulgangBotFinal.Core.Packet;

#endregion

namespace YulgangBotFinal.Core
{
    public interface IHookRecvPacket
    {

        void HandlePacket(IPacket p);
    }
    /// <summary>
    ///     Xử lý các phần trong quá trinh vận hành
    /// </summary>
    public interface IUnitProcess:IHookRecvPacket
    {
    //    /// <summary>
    //    ///     Lấy thông tin đầu vào của process
    //    /// </summary>
    //    /// <returns></returns>
    //    /// <summary>
    //    ///     Lấy thông tin đầu ra của process
    //    /// </summary>
    //    /// <returns></returns>
    //    IUnitOutput Output { set; }

    //    /// <summary>
    //    ///     cài đặt thông tin cấu hình của process
    //    /// </summary>
    //    /// <returns></returns>
    //    IUnitConfig Config { set; }
        void SetMainDefine(MainDefine def);
        int State { get; set; }
        void Clean();
        void SetScopeProcess(IProcScope scope);
        T ParsePacket<T>(IPacket p) where T:RecvPacket;
    }
}