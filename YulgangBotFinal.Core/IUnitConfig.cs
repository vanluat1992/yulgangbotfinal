﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Core
// FileName : IUnitConfig.cs
// Time Create : 1:31 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

namespace YulgangBotFinal.Core
{
    /// <summary>
    ///     chứa thông tin cấu hình của 1 process
    /// </summary>
    public interface IUnitConfig
    {
    }
}