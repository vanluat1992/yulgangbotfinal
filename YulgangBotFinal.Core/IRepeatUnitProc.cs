﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Core
// FileName : IRepeatUnitProc.cs
// Time Create : 1:31 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;

#endregion

namespace YulgangBotFinal.Core
{
    /// <summary>
    ///     các tién trình được chạy lặp lại theo chu kỳ
    ///     không yêu cầu các dữ liệu đầu vào
    /// </summary>
    public interface IRepeatUnitProc : IUnitProcess
    {
        /// <summary>
        ///     hàm chạy tiến trình
        /// </summary>
        /// <param name="time"></param>
        void Run(DateTime time);

        void Init();
        Type RunAfter();
        bool Alive { get; set; }
    }
}