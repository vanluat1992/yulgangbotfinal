﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Core
// FileName : MainDefine.cs
// Time Create : 7:34 AM 20/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using YulgangBotFinal.Model;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.PathFinder;

#endregion

namespace YulgangBotFinal.Core.Define
{
    public class MainDefine
    {
        public string Username { get; set; }
        public string Pass { get; set; }
        public string Pass2 { get; set; }
        public string DzoUsername { get; set; }
        public string DzoPass { get; set; }
        public string AuthIp { get; set; }
        public int AuthPort { get; set; }
        public string GsIp { get; set; }
        public int GsPort { get; set; }
        public IConnection AuthConnect { get; set; }
        public IConnection GsConnect { get; set; }
        public IDictionary<int, InfoGroupServer> ServerList { get; set; }
        public int PlayerSessionId { get; set; }
        public YulgangPlayer Player { get; set; }

        /// <summary>
        ///     chứa thông tin item trong túi đồ
        /// </summary>
        public ConcurrentDictionary<int, IYulgangGameItem> Inventory { get; } =
            new ConcurrentDictionary<int, IYulgangGameItem>();

        /// <summary>
        ///     Chứa thông tin item đeo trên người
        /// </summary>
        public ConcurrentDictionary<int, IYulgangGameItem> Equipment { get; } =
            new ConcurrentDictionary<int, IYulgangGameItem>();

        /// <summary>
        ///     Chứa thông tin quái vật và npc xung quanh nhân vật
        /// </summary>
        public ConcurrentDictionary<long, IYulgangNpc> Npcs { get; } = new ConcurrentDictionary<long, IYulgangNpc>();

        public long Money { get; set; }
        public ScrollInfo Weight { get; } = new ScrollInfo();
        public ScrollInfo Hp { get; } = new ScrollInfo();
        public ScrollInfo Mp { get; } = new ScrollInfo();
        public ScrollInfo Sp { get; } = new ScrollInfo();
        public ScrollInfo Exp { get; } = new ScrollInfo();
        public IYulgangLocation Location { get; set; } = new YulgangLocation();
        public MapCaculator MapCaculartor { get; set; }
        public IYulgangPathFinder PathFinder { get; set; }
        public IYulgangNpc MobTarget { get; set; }
        public IYulgangLocation RequestMove { get; set; }
        public bool RequestLoginAgain { get; set; }

        public ConcurrentDictionary<int, YulgangPartyMember> Partys { get; set; } =
            new ConcurrentDictionary<int, YulgangPartyMember>();
        public YulgangPartyMember Leader { get; set; }
        /// <summary>
        /// Tất cả các người chơi xung quanh nhân vật
        /// </summary>
        public ConcurrentDictionary<long, IYulgangPlayer> AllPlayer { get; } =
            new ConcurrentDictionary<long, IYulgangPlayer>();
        /// <summary>
        /// Tất cả các item rơi xung quanh nhân vật
        /// </summary>
        public ConcurrentDictionary<long, IYulgangItemDrop> ItemDrop { get; } =
            new ConcurrentDictionary<long, IYulgangItemDrop>();
        /// <summary>
        /// danh sách buff
        /// </summary>
        public ConcurrentDictionary<int, DateTime> Buff { get; set; } = new ConcurrentDictionary<int, DateTime>();
        /// <summary>
        /// dánh sách các quest đã hoàn thành
        /// </summary>
        public IList<int> QuestDone { get; set; }=new List<int>();
        /// <summary>
        /// danh sách các quest đang làm
        /// </summary>
        public Dictionary<int, IYulgangPlayerQuest> QuestDoing { get; set; }=new Dictionary<int, IYulgangPlayerQuest>();
        public bool LoadPlayerQuestOk { get; set; }
        public IYulgangPlayerQuest Quest { get; set; }
        public bool AttackQuest { get; set; }
        public bool FirstLogin { get; set; }
        public int IdServer { get; set; }
        public int IdChannel { get; set; }
        public bool Auto { get; set; } = true;
        public bool IsEnterGame { get; set; }
        public int IndexCharacter { get; set; }
        public bool Reload { get; set; }
    }

    public class ScrollInfo
    {
        public int Current { get; set; }
        public int Limit { get; set; }

        public float Percent
        {
            get
            {
                if (Limit == 0)
                    Limit = 1;
                return (float) Current/(float) Limit*100.0F;
            }
        }

        public override string ToString()
        {
            return $"{Current} / {Limit}";
        }
    }
}