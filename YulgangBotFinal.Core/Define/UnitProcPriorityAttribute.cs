﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Core
// FileName : UnitProcPriorityAttribute.cs
// Time Create : 1:31 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;

#endregion

namespace YulgangBotFinal.Core.Define
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class UnitProcPriorityAttribute : Attribute
    {
        public UnitProcPriorityAttribute(int level, bool isPass)
        {
            Level = level;
            IsPass = isPass;
        }

        public int Level { get; }
        public bool IsPass { get; }
    }
}