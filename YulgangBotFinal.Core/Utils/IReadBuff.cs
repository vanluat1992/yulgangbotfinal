﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.Network
// FileName : IReadBuff.cs
// Time Create : 1:58 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion
namespace YulgangBotFinal.Core.Utils
{
    public interface IReadBuff
    {
        char ReadChar();
        byte ReadByte();
        short ReadInt16();
        ushort ReadUInt16();
        int ReadInt32();
        uint ReadUInt32();
        long ReadInt64();
        ulong ReadUInt64();
        float ReadFloat();
        byte[] ReadBytes(int len);
        byte[] ReadBytes(int start, int len);
        string ReadString();
        string ReadString(int len);
        string ReadString(int start, int len);
    }
}