﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.Network
// FileName : IWriteBuff.cs
// Time Create : 1:58 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion
namespace YulgangBotFinal.Core.Utils
{
    public interface IWriteBuff
    {
        void WriteInt32(int val);
        void WriteInt16(short val);
        void WriteChar(char val);
        void WriteByte(byte val);
        void WriteInt64(long val);
        void WriteBytes(byte[] data);
        void WriteBytes(byte[] data, int len);
        void WriteBytes(byte[] data, int start, int len);
        void WriteBytes(byte[] data, int start, int len, int skip);
        void WriteUInt16(ushort val);
        void WriteUInt32(uint val);
        void WriteUInt64(ulong val);
        void WriteString(string data);
        void WriteString(string data, int len);
        void WriteString(string data, int start, int len);
        void WriteString(string data, int start, int len, int skip);
        void WriteFloat(float val);
        byte[] ToArray();
    }
}