﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Network
// FileName : ReadBuff.cs
// Time Create : 2:00 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;
using System.IO;
using System.Text;

#endregion

namespace YulgangBotFinal.Core.Utils
{
    public class ReadBuff : IReadBuff, IDisposable
    {
        private readonly Encoding _encoding;
        private readonly MemoryStream _memory;
        private readonly BinaryReader _reader;

        public ReadBuff(byte[] data) : this(data, Encoding.GetEncoding(1258))
        {
        }

        public ReadBuff(byte[] data, Encoding en)
        {
            _encoding = en;
            _memory = new MemoryStream(data);
            _reader = new BinaryReader(_memory);
        }

        public void Dispose()
        {
            _reader.Dispose();
            _memory.Dispose();
        }

        public char ReadChar()
        {
            if (_memory.Length - _reader.BaseStream.Position >= 1)
                return _reader.ReadChar();
            //Log.Error("Index out of range RecvPacket.ReadChar()  !");
            return '@';
        }

        public byte ReadByte()
        {
            if (_memory.Length - _reader.BaseStream.Position >= 1)
                return _reader.ReadByte();
            //Log.Error("Index out of range RecvPacket.ReadByte()  !");
            return 0;
        }

        public short ReadInt16()
        {
            if (_memory.Length - _reader.BaseStream.Position >= 2)
                return _reader.ReadInt16();
            //Log.Error("Index out of range RecvPacket.ReadInt16()  !");
            return 0;
        }

        public ushort ReadUInt16()
        {
            if (_memory.Length - _reader.BaseStream.Position >= 2)
                return _reader.ReadUInt16();
            //Log.Error("Index out of range RecvPacket.ReadUInt16()  !");
            return 0;
        }

        public int ReadInt32()
        {
            if (_memory.Length - _reader.BaseStream.Position >= 4)
                return _reader.ReadInt32();
            //Log.Error("Index out of range RecvPacket.ReadInt32()  !");
            return 0;
        }

        public uint ReadUInt32()
        {
            if (_memory.Length - _reader.BaseStream.Position >= 4)
                return _reader.ReadUInt32();
            //Log.Error("Index out of range RecvPacket.ReadUInt32()  !");
            return 0;
        }

        public long ReadInt64()
        {
            if (_memory.Length - _reader.BaseStream.Position >= 8)
                return _reader.ReadInt64();
            //Log.Error("Index out of range RecvPacket.ReadInt64()  !");
            return 0;
        }

        public ulong ReadUInt64()
        {
            if (_memory.Length - _reader.BaseStream.Position >= 8)
                return _reader.ReadUInt64();
            //Log.Error("Index out of range RecvPacket.ReadUInt64()  !");
            return 0;
        }

        public float ReadFloat()
        {
            if (_memory.Length - _reader.BaseStream.Position >= 4)
                return _reader.ReadSingle();
            //Log.Error("Index out of range RecvPacket.ReadFloat()  !");
            return 0;
        }

        public byte[] ReadBytes(int len)
        {
            if (_memory.Length - _reader.BaseStream.Position >= len)
                return _reader.ReadBytes(len);
            //Log.Error("Index out of range RecvPacket.ReadBytes(int len)  !");
            return null;
        }

        public byte[] ReadBytes(int start, int len)
        {
            if (_memory.Length - (_reader.BaseStream.Position + start) >= len)
            {
                _reader.BaseStream.Position += start;
                return _reader.ReadBytes(len);
            }
            //Log.Error("Index out of range RecvPacket.ReadBytes(int start, int len)  !");
            return null;
        }

        public string ReadString()
        {
            if (_memory.Length - _reader.BaseStream.Position >= 1)
                return _reader.ReadString();
            //Log.Error("Index out of range RecvPacket.ReadString()  !");
            return null;
        }

        public string ReadString(int len)
        {
            if (_memory.Length - _reader.BaseStream.Position >= len)
                return _encoding.GetString(ReadBytes(len)).Replace("\0", "");
            //Log.Error("Index out of range RecvPacket.ReadString(int len)  !");
            return null;
        }

        public string ReadString(int start, int len)
        {
            if (_memory.Length - (_reader.BaseStream.Position + start) >= len)
            {
                _reader.BaseStream.Position += start;
                return _encoding.GetString(ReadBytes(start, len));
            }
            //Log.Error("Index out of range RecvPacket.ReadString(int start, int len)  !");
            return null;
        }
    }
}