﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.Network
// FileName : WriteBuff.cs
// Time Create : 2:04 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion

using System;
using System.IO;
using System.Text;
using YulgangBotFinal.Core.Utils;

namespace YulgangBotFinal.Network.Utils
{
    public class WriteBuff:IWriteBuff,IDisposable
    {
        private readonly MemoryStream _stream;
        private readonly BinaryWriter _writer;
        private readonly Encoding _encode;

        public WriteBuff():this(Encoding.GetEncoding(1258))
        {
            
        }
        public WriteBuff(Encoding en)
        {
            _encode = en;
            _stream = new MemoryStream();
            _writer=new BinaryWriter(_stream);
        }
        public void Dispose()
        {
            _writer.Dispose();
            _stream.Dispose();
        }

        public void WriteInt32(int val)
        {
            _writer.Write(val);
        }

        public void WriteInt16(short val)
        {

            _writer.Write(val);
        }

        public void WriteChar(char val)
        {
            _writer.Write(val);
        }

        public void WriteByte(byte val)
        {
            _writer.Write(val);
        }

        public void WriteInt64(long val)
        {
            _writer.Write(val);
        }

        public void WriteBytes(byte[] data)
        {
            _writer.Write(data);
        }

        public void WriteBytes(byte[] data, int len)
        {
            _writer.Write(data, 0, len);
        }

        public void WriteBytes(byte[] data, int start, int len)
        {
            _writer.Write(data, start, len);
        }


        public void WriteBytes(byte[] data, int start, int len, int skip)
        {
            var add = new byte[skip];
            WriteBytes(data, start, len);
            WriteBytes(add);
        }

        public void WriteUInt16(ushort val)
        {
            _writer.Write(val);
        }

        public void WriteUInt32(uint val)
        {
            _writer.Write(val);
        }

        public void WriteUInt64(ulong val)
        {
            _writer.Write(val);
        }

        public void WriteString(string data)
        {
            _writer.Write(_encode.GetBytes(data));
        }

        public void WriteString(string data, int len)
        {
            _writer.Write(_encode.GetBytes(data.ToCharArray(), 0, len));
        }

        public void WriteString(string data, int start, int len)
        {
            _writer.Write(_encode.GetBytes(data.ToCharArray(), start, len));
        }


        public void WriteFloat(float val)
        {
            _writer.Write(val);
        }

        public byte[] ToArray()
        {
            return _stream.ToArray();
        }

        public void WriteString(string data, int start, int len, int skip)
        {
            var add = new byte[skip];
            WriteString(data, start, len);
            WriteBytes(add);
        }
    }
}