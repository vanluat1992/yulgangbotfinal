﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.Network
// FileName : SendPacket.cs
// Time Create : 2:28 PM 15/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion

using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Core
{
    public abstract class SendPacket:WriteBuff
    {
        public abstract IPacket BuildPacket();
    }
}