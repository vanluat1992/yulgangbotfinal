﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: ManaProcWithOpcode.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Collections.Generic;
using YulgangBotFinal.Core;

#endregion

namespace YulgangBotFinal.Network
{
    public class ManaProcWithOpcode
    {
        public ManaProcWithOpcode(IHookRecvPacket proc, params int[] ops)
        {
            Proc = proc;
            Opcode = new List<int>();
            foreach (var op in ops)
            {
                Opcode.Add(op);
            }
        }

        public IList<int> Opcode { get; private set; }
        public IHookRecvPacket Proc { get; private set; }
    }
}