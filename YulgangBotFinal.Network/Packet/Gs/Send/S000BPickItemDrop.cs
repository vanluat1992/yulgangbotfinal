﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 10:52 PM 21/09/2016
// FILENAME: S000BPickItemDrop.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0xB)]
    public class S000BPickItemDrop : GsSend
    {
        public S000BPickItemDrop(int sesionId) : base(sesionId)
        {
        }

        public S000BPickItemDrop Send(long id)
        {
            WriteInt64(id);
            return this;
        }
    }
}