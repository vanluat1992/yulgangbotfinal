﻿#region header
// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 7:24 PM 29/09/2016
// FILENAME: S003CUsingSpellBuff.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************
#endregion

using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x3C)]
    public class S003CUsingSpellBuff:GsSend
    {
        public S003CUsingSpellBuff(int sesionId) : base(sesionId)
        {
        }

        public S003CUsingSpellBuff SendDefault(IYulgangLocation location)
        {
            WriteInt32(2);
            WriteByte(0);
            WriteByte(0);
            WriteInt16(0);
            WriteInt32(0);
            WriteFloat(location.X);
            WriteFloat(15);
            WriteFloat(location.Y);
            WriteInt32(0);
            return this;
        }
    }
}