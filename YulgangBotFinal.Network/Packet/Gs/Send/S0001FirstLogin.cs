﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: S0001FirstLogin.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.IO;
using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(1)]
    public class S0001FirstLogin : GsSend
    {
        public S0001FirstLogin(int sesionId) : base(sesionId)
        {
        }

        public S0001FirstLogin SendLoginKorea(string username)
        {
            WriteString(username, 0, username.Length, 31 - username.Length); //31
            WriteString("0", 0, 1, 2); //34
            WriteBytes(new byte[30]); //64
            WriteInt16(0x7c);
            WriteInt16(2); //68

            var ip = "";
            const string s = "0123456789ABCDEF";
            var ran1 = new Random((int)DateTime.Now.Ticks);
            ip += ran1.Next(1, 254);
            ip += ".";
            ip += ran1.Next(1, 254);
            ip += ".";
            ip += ran1.Next(1, 254);
            ip += ".";
            ip += ran1.Next(1, 254);
            //var mac = "";
            //var ran = new Random((int)DateTime.Now.Ticks);
            //for (var i = 0; i < 13; i++)
            //{
            //    mac += s[ran.Next(0, 15)];
            //}

            ip = "192.168.116.1";
            WriteString(ip, 0, ip.Length, 16 - ip.Length);
            // 32 byte check sum phần data hệ thống

            using (var st=File.OpenRead("dhme_esea.d2s"))
            {
                using (var bRead=new BinaryReader(st))
                {
                    var uk1 = bRead.ReadInt32();
                    var data = bRead.ReadBytes(32);
                    for (int i = 0; i < data.Length; i++)
                    {
                        data[i] ^= 0x5B;
                    }

                    // lấy user bỏ vào 1 mảng 80byte
                    var userData = new byte[80];
                    var userbyte = Encoding.ASCII.GetBytes(username.ToUpper());
                    Buffer.BlockCopy(userbyte, 0, userData, 0, userbyte.Length);
                    var index = 0;
                    var result = 0;
                    do
                    {
                        var v3 = userData[index];
                        if (v3==0)
                            break;
                        result += v3;
                        var v4 = userData[index+1];
                        if (v4==0)
                            break;
                        result += v4;
                        var v5 = userData[index + 2];
                        if (v5==0)
                            break;
                        result += v5;
                        var v6 = userData[index + 3];
                        if (v6==0)
                            break;
                        result += v6;
                        var v7 = userData[index + 4];
                        if (v7==0)
                            break;
                        index += 5;
                        result += v7;
                    }
                    while (index < 80);
                    for (int i = 0; i < data.Length; i++)
                    {
                        data[i] ^= (byte)result;
                    }
                    WriteBytes(data, 32);
                }
            }

            WriteInt32(0);
            return this;
        }
        public S0001FirstLogin Send(string username)
        {
            WriteString(username, 0, username.Length, 31 - username.Length); //31
            WriteString("242",3); //34
            WriteBytes(new byte[30]); //64
            WriteInt16(0xF2);
            WriteInt16(4); //68

            var ip = "";
            const string s = "0123456789ABCDEF";
            var ran1 = new Random((int) DateTime.Now.Ticks);
            ip += ran1.Next(1, 254);
            ip += ".";
            ip += ran1.Next(1, 254);
            ip += ".";
            ip += ran1.Next(1, 254);
            ip += ".";
            ip += ran1.Next(1, 254);
            var mac = "";
            var ran = new Random((int) DateTime.Now.Ticks);
            for (var i = 0; i < 13; i++)
            {
                mac += s[ran.Next(0, 15)];
            }


            WriteString(ip, 0, ip.Length, 16 - ip.Length);
            WriteString(mac, 0, mac.Length, 16 - mac.Length);
            return this;
        }
    }
}