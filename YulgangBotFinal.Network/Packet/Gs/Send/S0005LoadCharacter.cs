﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: S0005LoadCharacter.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(5)]
    public class S0005LoadCharacter : GsSend
    {
        public S0005LoadCharacter(int sesionId) : base(sesionId)
        {
        }

        public S0005LoadCharacter Send(byte index)
        {
            WriteByte(index);
            var sessionid = SesionId;
            if (SesionId >= 0 && SesionId <= 9999)
                sessionid += 9999;
            var tmp = (SesionId + 0xF) ^ 10*0xF*sessionid;
            WriteInt32(tmp);

            return this;
        }
    }
}