﻿#region header
// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:38 AM 27/09/2016
// FILENAME: S0016GameOption.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************
#endregion

using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x16)]
    public class S0016GameOption:GsSend
    {
        public S0016GameOption(int sesionId) : base(sesionId)
        {
        }

        public S0016GameOption Send()
        {
            WriteByte(1);
            WriteByte(1);
            WriteByte(1);
            WriteByte(1);
            WriteByte(1);
            WriteByte(0);
            WriteByte(0);
            WriteByte(0x63);
            WriteByte(1);
            WriteByte(1);
            WriteByte(1);
            WriteByte(1);
            WriteByte(0x32);
            WriteByte(0);
            return this;
        }
    }
}