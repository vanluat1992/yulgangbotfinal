﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 1:46 PM 17/09/2016
// FILENAME: S003AUseItem.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x3A)]
    public class S003AUseItem : GsSend
    {
        public S003AUseItem(int sesionId) : base(sesionId)
        {
        }

        public S003AUseItem Send(int idcmd, int idItem, int count, int index)
        {
            WriteByte((byte) idcmd);
            WriteByte((byte) index);
            WriteInt16(0);
            WriteInt64(idItem);
            WriteInt32(count);
            WriteInt64(0);
            WriteInt32(0);
            return this;
        }
    }
}