﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:39 PM 22/09/2016
// FILENAME: S0090TalkWithNpc.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x90)]
    public class S0090TalkWithNpc : GsSend
    {
        public S0090TalkWithNpc(int sesionId) : base(sesionId)
        {
        }

        public S0090TalkWithNpc Send(YulgangTalkNpcCommand command, int idNpc)
        {
            WriteInt64((int) command);
            WriteInt64(idNpc);
            WriteInt64((int) command <= 2 ? 0 : 1);
            return this;
        }
    }
}