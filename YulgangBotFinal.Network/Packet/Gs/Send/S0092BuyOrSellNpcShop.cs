﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:39 PM 22/09/2016
// FILENAME: S0092BuyOrSellNpcShop.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x92)]
    public class S0092BuyOrSellNpcShop : GsSend
    {
        public S0092BuyOrSellNpcShop(int sesionId) : base(sesionId)
        {
        }

        public S0092BuyOrSellNpcShop BuyItem(IYulgangItemShopNpc it, int count)
        {
            //aa558a00000092008400010000000000000066ca9a3b00000000070000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000055aa
            WriteInt32(1);
            WriteInt32(0);
            WriteInt64(it.Id);
            WriteInt64(count);
            WriteBytes(new byte[108]);
            return this;
        }

        public S0092BuyOrSellNpcShop SellItem(IYulgangGameItem it, int count, long playermoney)
        {
            //aa558a00000092008400020000000000000066ca9a3b0000000007000000000000003040a07200000000bb3d6df88da0240166ca9a3b0000000007000000000000000106000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000055aa

            WriteInt32(2);
            WriteInt32(0);
            WriteInt64(it.BasicInfo.Id);
            WriteInt64(count);
            WriteInt64(playermoney);
            WriteInt64(it.Id);
            WriteInt64(it.BasicInfo.Id);
            WriteInt64(count);
            WriteByte(1);
            WriteByte((byte) it.Index);
            WriteBytes(new byte[74]);

            return this;
        }
    }
}