﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 2:47 PM 18/09/2016
// FILENAME: S1088TargetObject.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x1088)]
    public class S1088TargetObject : GsSend
    {
        public S1088TargetObject(int sesionId, long id) : base(sesionId)
        {
            WriteInt64(id);
        }
    }
}