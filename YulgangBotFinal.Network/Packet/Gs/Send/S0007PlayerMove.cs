﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 1:50 PM 17/09/2016
// FILENAME: S0007PlayerMove.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x7)]
    public class S0007PlayerMove : GsSend
    {
        public S0007PlayerMove(int sesionId) : base(sesionId)
        {
        }

        public S0007PlayerMove Send(IYulgangLocation from, IYulgangLocation to)
        {
            WriteInt32(2);
            WriteFloat(to.X);
            WriteFloat(to.Z);
            WriteFloat(to.Y);

            WriteFloat(from.X);
            WriteFloat(from.Z);
            WriteFloat(from.Y);

            WriteInt32(1);
            var distan =
                Math.Sqrt(Math.Pow(@from.X - to.X, 2) +
                          Math.Pow(@from.Y - to.Y, 2));

            WriteFloat((float) distan);
            WriteUInt16(0xFFFF); //session taget
            WriteUInt16(0);
            return this;
        }
    }
}