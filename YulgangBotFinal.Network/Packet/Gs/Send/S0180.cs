﻿#region header
// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:41 AM 27/09/2016
// FILENAME: S0180.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************
#endregion

using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x180)]
    public class S0180:GsSend
    {
        public S0180(int sesionId) : base(sesionId)
        {
            WriteInt32(0x18);
            WriteInt16(0x0);
        }
    }
}