﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:38 PM 19/09/2016
// FILENAME: S0034ConfirmInviteParty.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x34)]
    public class S0034ConfirmInviteParty : GsSend
    {
        public S0034ConfirmInviteParty(int sesionId) : base(sesionId)
        {
        }

        public S0034ConfirmInviteParty Send(int command, int result, int session, string name)
        {
            WriteInt16((short) command);
            WriteInt16((short) result);
            WriteInt16((short) session);
            WriteBytes(new byte[15]);
            return this;
        }
    }
}