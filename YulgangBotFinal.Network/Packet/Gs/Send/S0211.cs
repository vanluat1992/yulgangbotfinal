﻿#region header
// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:36 PM 29/09/2016
// FILENAME: S0211.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************
#endregion

using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x211)]
    public class S0211:GsSend
    {
        public S0211(int sesionId) : base(sesionId)
        {
            WriteInt32(1);
            WriteInt16(0);
        }
    }
}