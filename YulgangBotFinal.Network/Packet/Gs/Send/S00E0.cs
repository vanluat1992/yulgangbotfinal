﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0xE0)]
    public class S00E0:GsSend
    {
        public S00E0(int sesionId) : base(sesionId)
        {
        }
    }
}
