﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: S00B0SyscServerTime.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x80)]
    public class S00B0SyscServerTime : GsSend
    {
        public S00B0SyscServerTime(int sesionId) : base(sesionId)
        {
        }

        public S00B0SyscServerTime Send(int time)
        {
            WriteInt32(time);
            return this;
        }
    }
}