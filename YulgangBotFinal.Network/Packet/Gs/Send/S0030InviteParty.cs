﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:37 PM 19/09/2016
// FILENAME: S0030InviteParty.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x30)]
    public class S0030InviteParty : GsSend
    {
        public S0030InviteParty(int sesionId) : base(sesionId)
        {
        }

        public S0030InviteParty Send(int session, int config, string name)
        {
            WriteInt16(1);
            WriteInt16(1);
            WriteInt16((short) session);
            WriteInt16((short) config);
            WriteBytes(new byte[15]);
            //WriteString(name, 15);
            return this;
        }
    }
}