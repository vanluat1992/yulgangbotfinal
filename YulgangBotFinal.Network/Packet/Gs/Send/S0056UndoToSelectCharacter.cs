﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 9:07 PM 16/09/2016
// FILENAME: S0056UndoToSelectCharacter.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x56)]
    public class S0056UndoToSelectCharacter : GsSend
    {
        public S0056UndoToSelectCharacter(int sesionId) : base(sesionId)
        {
        }
    }
}