﻿

using System;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x1638)]
    public class S1638: GsSend
    {
        public S1638(int sesionId) : base(sesionId)
        {
        }
    }
}