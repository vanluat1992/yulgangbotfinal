﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 1:55 PM 18/09/2016
// FILENAME: S0048SelecDieType.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x48)]
    public class S0048SelecDieType : GsSend
    {
        public S0048SelecDieType(int sesionId) : base(sesionId)
        {
        }

        public S0048SelecDieType SendAdminMove(IYulgangLocation location)
        {
            WriteInt32(0x51);
            WriteFloat(location.X);
            WriteFloat(15f);
            WriteFloat(location.Y);
            WriteInt32(location.MapId);
            WriteBytes(new byte[14]);
            return this;
        }

        public S0048SelecDieType SendDie(IYulgangLocation location)
        {
            WriteInt32(0x63);
            WriteFloat(location.X);
            WriteFloat(15f);
            WriteFloat(location.Y);
            WriteInt32(location.MapId);
            WriteBytes(new byte[14]);
            return this;
        }
    }
}