﻿// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Network
// FILENAME : S0083Quest.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 30/03/2017 1:20 AM
// **************************************************************************

using YulgangBotFinal.Model;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x83)]
    public class S0083Quest:GsSend
    {
        public S0083Quest(int sesionId) : base(sesionId)
        {
        }

        public S0083Quest SendAccept(int id)
        {
            WriteInt16((short) id);
            WriteInt16((short) QuestEvent.Accept);
            WriteInt16(0);
            return this;
        }
        public S0083Quest SendFinish(int id)
        {
            WriteInt16((short) id);
            WriteInt16((short) QuestEvent.Finish);
            WriteInt16(0);
            return this;
        }
    }

  
}