﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 3:05 PM 02/10/2016
// FILENAME: S001D.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x1D)]
    public class S001D : GsSend
    {
        public S001D(int sesionId) : base(sesionId)
        {
        }

        public S001D Send(IYulgangLocation location)
        {
            WriteInt32(255);
            WriteFloat(location.X);
            WriteFloat(1);
            WriteFloat(location.Y);
            WriteFloat(location.X);
            WriteFloat(15);
            WriteFloat(location.Y);
            WriteInt32(0);
            WriteInt32(0xFFFF);
            return this;
        }
    }
}