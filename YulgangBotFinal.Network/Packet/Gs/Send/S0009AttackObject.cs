﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 3:21 PM 18/09/2016
// FILENAME: S0009AttackObject.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x09)]
    public class S0009AttackObject : GsSend
    {
        public S0009AttackObject(int sesionId) : base(sesionId)
        {
        }

        public S0009AttackObject Send(long id, int spellId, IYulgangLocation location)
        {
            WriteInt16((short) id);
            WriteInt16(0);
            WriteInt32(spellId);
            WriteFloat(location.X);
            WriteFloat(15f);
            WriteFloat(location.Y);
            WriteInt32(0);
            return this;
        }
        public S0009AttackObject Send(long id,int z, int spellId, IYulgangLocation location)
        {
            WriteInt16((short) id);
            WriteInt16((short) z);
            WriteInt32(spellId);
            WriteFloat(location.X);
            WriteFloat(15f);
            WriteFloat(location.Y);
            WriteInt32(0);
            return this;
        }
    }
}