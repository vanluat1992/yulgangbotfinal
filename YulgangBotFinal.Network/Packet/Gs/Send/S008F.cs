﻿#region header
// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:43 AM 27/09/2016
// FILENAME: S008F.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************
#endregion

using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Send
{
    [Opcode(0x8f)]
    public class S008F:GsSend
    {
        public S008F(int sesionId) : base(sesionId)
        {
        }
    }
}