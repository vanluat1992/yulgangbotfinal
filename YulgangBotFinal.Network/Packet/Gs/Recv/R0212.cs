﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x212)]
    public class R0212:GsRecv
    {
        public R0212(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0212(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public override void Parse()
        {
            //todo: chưa làm
        }
    }
}
