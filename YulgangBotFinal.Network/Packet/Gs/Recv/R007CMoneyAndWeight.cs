﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 1:29 PM 17/09/2016
// FILENAME: R007CMoneyAndWeight.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x7C)]
    public class R007CMoneyAndWeight : GsRecv
    {
        public R007CMoneyAndWeight(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R007CMoneyAndWeight(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public long Money { get; set; }
        public int CurrentW { get; set; }
        public int MaxW { get; set; }

        public override void Parse()
        {
            Money = ReadInt64();
            CurrentW = ReadInt32();
            MaxW = ReadInt32();
        }
    }
}