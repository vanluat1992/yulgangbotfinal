﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:33 PM 19/09/2016
// FILENAME: R0031InviteParty.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x31)]
    public class R0031InviteParty : GsRecv
    {
        public R0031InviteParty(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0031InviteParty(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public short SessionLead { get; set; }

        public string NameLeader { get; set; }

        public short Flag { get; set; }

        public short Result { get; set; }

        public short Command { get; set; }

        public override void Parse()
        {
            Command = ReadInt16();
            Result = ReadInt16();
            if (Result == 1)
            {
                var unk = ReadInt16();
                SessionLead = ReadInt16(); /*session cua nguoi dc moi*/
                Flag = ReadInt16();
                ReadUInt16();
                NameLeader = ReadString(15);
                //if (nameleader.Trim() != TmpConfig.Player.Name)
                //    Connection.Send(new S0034ConfirmInviteParty(command, result, flag, nameleader));
            }
        }
    }
}