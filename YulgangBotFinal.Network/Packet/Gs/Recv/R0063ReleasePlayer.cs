﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: R0063ReleasePlayer.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Collections.Generic;
using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x63)]
    public class R0063ReleasePlayer : GsRecv
    {
        public R0063ReleasePlayer(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0063ReleasePlayer(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public IList<long> PlayerIds { get; } = new List<long>();

        #region Overrides of RecvPacket

        public override void Parse()
        {
            var count = ReadInt32();
            for (var i = 0; i < count; i++)
            {
                var ss = ReadUInt16();
                PlayerIds.Add(ss);
                ReadUInt16();
            }
        }

        #endregion
    }
}