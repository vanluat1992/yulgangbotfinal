﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 2:07 PM 18/09/2016
// FILENAME: R0079SetPlayerLocation.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x79)]
    public class R0079SetPlayerLocation : GsRecv
    {
        public R0079SetPlayerLocation(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0079SetPlayerLocation(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public IYulgangLocation Location { get; } = new YulgangLocation();

        public override void Parse()
        {
            //00-80-F9-43-00-00-70-41-00-00-F9-44-65-00-00-00-00-00-00-00
            Location.X = ReadFloat();
            Location.Z = ReadFloat();
            Location.Y = ReadFloat();
            Location.MapId = ReadInt32();
        }
    }
}