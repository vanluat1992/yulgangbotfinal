﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: R0067InitializerNpc.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Collections.Generic;
using System.Text;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x67)]
    public class R0067InitializerNpc : GsRecv
    {
        public R0067InitializerNpc(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0067InitializerNpc(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public IDictionary<long, IYulgangNpc> Npcs { get; } = new Dictionary<long, IYulgangNpc>();

        #region Overrides of RecvPacket

        public override void Parse()
        {
            var count = ReadInt32();
            for (var i = 0; i < count; i++)
            {
                var msession1 = ReadUInt16();
                var msession2 = ReadUInt16();
                var mId = ReadInt16();
                ReadInt32();
                var mCurrentHp = ReadInt32();
                var mMaxHp = ReadInt32();
                var mX = ReadFloat();
                var mZ = ReadFloat();
                var mY = ReadFloat();
                ReadInt32();
                var face1 = ReadFloat();
                var face2 = ReadFloat();
                var mX1 = ReadFloat();
                var mZ1 = ReadFloat();
                var mY2 = ReadFloat();
                ReadBytes(16);
                ReadInt32();
                //IYulgangYbiGameMonster monster;
                //if (Ybi.DicMonsters.TryGetValue((uint)mId, out monster))
                //{
                var npc = new YulgangNpc()
                {
                    Id = msession1,
                    BasicInfo = new YulgangYbiGameMonster() {Id = mId},
                    CurrentHp = mCurrentHp,
                    MaxHp = mMaxHp,
                    Location = new YulgangLocation()
                    {
                        X = mX,
                        Y = mY,
                        Z = mZ
                    }
                };
                Npcs.Add(npc.Id, npc);
                //    EventManeger.CallBackGuiEvent(GuiEvent.InitNpc, npc);
                //}

                //var monster = new Monster
                //{
                //    Session = msession1,
                //    Id = mId,
                //    MaxHp = mMaxHp,
                //    CurrentHp = mCurrentHp,
                //    Location =
                //    {
                //        X = mX,
                //        Y = mY,
                //        Z = mZ,
                //    }
                //};
            }
        }

        #endregion
    }
}