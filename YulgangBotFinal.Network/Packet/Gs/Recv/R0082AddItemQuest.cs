﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.Network
// FileName : R0082AddItemQuest.cs
// Time Create : 2:33 PM 30/03/2017
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion

using System.Text;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x82)]
    public class R0082AddItemQuest:GsRecv
    {
        public long SessionId { get; set; }
        public long ItemId { get; set; }
        public int Count { get; set; }
        public R0082AddItemQuest(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0082AddItemQuest(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            SessionId = ReadInt64();
            ItemId = ReadInt64();
            Count = ReadInt32();
        }

        #endregion
    }
}