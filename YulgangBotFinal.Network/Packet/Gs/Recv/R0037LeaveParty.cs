﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 7:06 PM 20/09/2016
// FILENAME: R0037LeaveParty.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x37)]
    public class R0037LeaveParty : GsRecv
    {
        public R0037LeaveParty(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0037LeaveParty(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            Command = ReadInt16();
            Result = ReadInt16();
            Session = ReadInt16();
        }

        public short Session { get; set; }

        public short Result { get; set; }

        public short Command { get; set; }

        #endregion
    }
}