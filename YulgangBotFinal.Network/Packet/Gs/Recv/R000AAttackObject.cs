﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 3:23 PM 18/09/2016
// FILENAME: R000AAttackObject.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0xA)]
    public class R000AAttackObject : GsRecv
    {
        public R000AAttackObject(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R000AAttackObject(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public int MobId { get; set; }
        public int Dame { get; set; }

        public override void Parse()
        {
            var npcSession = ReadInt16();
            var arrow = ReadInt16();
            var dam1 = ReadInt32();
            var dam2 = ReadInt32();
            var dame3 = ReadInt32();
            var dame4 = ReadInt32();
            var idSpell = ReadInt32();
            var effect = ReadInt32();
            var x = ReadFloat();
            var z = ReadFloat();
            var y = ReadFloat();
            ReadByte();
            var hit = ReadByte();
            var crit = ReadByte();
            ReadByte();
            var npcHp = ReadInt32();
            ReadInt32();
            ReadInt32();
            ReadInt32();
            ReadInt32();
            Dame = dam1 + dam2 + dame3 + dame4;
            MobId = npcSession;
        }
    }
}