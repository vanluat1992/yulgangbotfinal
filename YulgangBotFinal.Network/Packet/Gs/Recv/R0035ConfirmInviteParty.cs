﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:35 PM 19/09/2016
// FILENAME: R0035ConfirmInviteParty.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x35)]
    public class R0035ConfirmInviteParty : GsRecv
    {
        public R0035ConfirmInviteParty(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0035ConfirmInviteParty(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public short Result { get; set; }

        public override void Parse()
        {
            var command = ReadInt16();
            Result = ReadInt16();
            if (Result == 1)
            {
                var session = ReadInt16();
                var name = ReadString(15);
                // Log.Debug("Session {0}-{1}", session, Connection.Session);
                //if (TmpConfig.Party.Leader != null && TmpConfig.Party.Leader.Id != Connection.Session)
                //{
                //    /* mình là đội trưởng*/
                //    Log.Debug("Người chơi {0} đã gia nhập đội ngũ", name);

                //}
                //else
                //{
                //    Log.Debug("Bạn đã gia nhập đội của {0} ", name);

                //}

                //EventManeger.CallBackGuiEvent(GuiEvent.AddMemberToParty, session, name);
            }
        }
    }
}