﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 10:47 PM 21/09/2016
// FILENAME: R0073ReleaseItemDrop.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x73)]
    public class R0073ReleaseItemDrop : GsRecv
    {
        public R0073ReleaseItemDrop(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0073ReleaseItemDrop(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public long SessionItem { get; set; }

        public override void Parse()
        {
            SessionItem = ReadInt64();
        }
    }
}