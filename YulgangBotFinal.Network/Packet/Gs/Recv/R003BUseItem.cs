﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 1:49 PM 17/09/2016
// FILENAME: R003BUseItem.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x3B)]
    public class R003BUseItem : GsRecv
    {
        public R003BUseItem(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R003BUseItem(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public byte Command { get; set; }

        public int CountExist { get; set; }

        public int Count { get; set; }

        public long IdItem { get; set; }

        public byte Index { get; set; }

        public override void Parse()
        {
            Command = ReadByte(); // 1 là sử dụng item
            Index = ReadByte();
            ReadInt16();
            IdItem = ReadInt64();
            Count = ReadInt32();
            CountExist = ReadInt32();
        }
    }
}