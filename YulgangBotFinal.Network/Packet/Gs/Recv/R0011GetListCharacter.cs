﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: R0011GetListCharacter.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Text;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.Helper;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x11)]
    public class R0011GetListCharacter : GsRecv
    {
        public bool EmptyCharacter { get; private set; }
        public R0011GetListCharacter(byte[] data, int sessionId) : base(data, sessionId)
        {
            //Debug.WriteLine(BitConverter.ToString(data).Replace("-",""));
        }

        public R0011GetListCharacter(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public YulgangPlayer Player { get; private set; }

        public override void Parse()
        {
            var index = ReadByte();
            if (index == 255)
            {
                Console.WriteLine("Chưa có nhân vật");
                EmptyCharacter = true;
                //Client.Devt.Send(FirstLoginEventType.ResponseListCharacter, this, new InfoPlayerEvent(null));
                //Log.Debug("Chưa có nhân vật dc tạo");
                //Client.Evt.ActiveEvent(EventType.ShowFormListCharacter, this, new YulgangEvent(null));
                ////Client.EventManaged.ActiveEvent(EventType.EndSelectCharacter, this, new YulgangEvent(this));
                return;
            }

            var name = ReadString(15);
            ReadByte();
            ReadInt32();
            var gName = ReadString(15);
            ReadByte();
            ReadInt16();
            var forces = ReadInt16();
            var level = ReadInt16();
            var joblevel = ReadInt16();
            var job = ReadByte();
            ReadByte();

            ReadBytes(7); //khuôn mặt
            var sex = ReadByte();
            ReadInt16();
            var x = ReadFloat();
            var z = ReadFloat();
            var y = ReadFloat();
            var mId = ReadInt32();

            ReadBytes(12);
            ReadBytes(48);

            var tam = ReadByte();
            var khi = ReadByte();
            var the = ReadByte();
            var hon = ReadByte();

            var maxHp = ReadInt16();
            var maxMp = ReadInt16();
            var maxSp = ReadInt32();

            var maxExpNm = ReadInt32();
            var maxExpVip = ReadInt32();

            var cHp = ReadInt16();
            var cMp = ReadInt16();
            var cSp = ReadInt32();
            var cExpNm = ReadInt32();
            var cExpVip = ReadInt32();

            var expSpell = ReadInt32();

            for (var i = 0; i < 15; i++)
            {
                ReadBytes(88);
            }
            for (var i = 0; i < 15; i++)
            {
                ReadBytes(88);
            }
            var speed = ReadInt32();
            ReadBytes(15);
            Player = new YulgangPlayer()
            {
                Id = SessionId,
                Name = name,
                Index = index,
                Level = level,
                Forces = (YulgangGameForce) forces,
                Job = ((int) job).GetJobGame(),
                Sex = (YulgangGameSex) sex,
                Upgrade = joblevel,
                Location = new YulgangLocation()
                {
                    X = x,
                    Y = y,
                    Z = z,
                    MapId = mId
                }
            };
        }
    }
}