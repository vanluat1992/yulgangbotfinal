﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:39 PM 22/09/2016
// FILENAME: R0091TalkNpc.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x91)]
    public class R0091TalkNpc : GsRecv
    {
        public R0091TalkNpc(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0091TalkNpc(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            var cmd = ReadInt32();
            var cmd1 = ReadInt32();
            var idNpc = ReadInt32();
            switch (cmd1)
            {
                case (int) YulgangTalkNpcCommand.OpenShop:
                {
                    var count = ReadInt64();
                    ReadInt64();

                    for (var j = 0; j < count; j++)
                    {
                        var id = ReadInt64();
                        var optionCount = ReadInt64();
                        var item = new YulgangItemShopNpc {Id = (int) id};

                        for (var i = 0; i < optionCount; i++)
                        {
                            var magic = ReadInt64();
                            switch (i)
                            {
                                case 0:
                                    item.Magic1 = (int) magic;
                                    break;
                                case 1:
                                    item.Magic2 = (int) magic;
                                    break;
                                case 2:
                                    item.Magic3 = (int) magic;
                                    break;
                                case 3:
                                    item.Magic4 = (int) magic;
                                    break;
                            }
                        }
                        ReadInt64();
                        IYulgangYbiGameItem itbasic;
                        //if (Ybi.DicItems.TryGetValue(id, out itbasic))
                        {
                            //    item.BasicInfo = itbasic;
                            //    item.Name = itbasic.Name;
                            //    EventManeger.CallBackGuiEvent(GuiEvent.AddItemNpcShop, item);
                        }
                    }
                }
                    break;
            }
        }

        #endregion
    }
}