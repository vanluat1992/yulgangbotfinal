﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 1:54 PM 17/09/2016
// FILENAME: R0022DeleteBagItem.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x22)]
    public class R0022DeleteBagItem : GsRecv
    {
        public R0022DeleteBagItem(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0022DeleteBagItem(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public int CountExist { get; set; }

        public int Count { get; set; }

        public long IdItem { get; set; }

        public byte Index { get; set; }

        public override void Parse()
        {
            ReadByte();
            Index = ReadByte();
            ReadInt16();
            IdItem = ReadInt64();
            Count = ReadInt32();
            CountExist = ReadInt32();
        }
    }
}