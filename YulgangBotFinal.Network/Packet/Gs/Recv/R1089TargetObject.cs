﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 2:48 PM 18/09/2016
// FILENAME: R1089TargetObject.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x1089)]
    public class R1089TargetObject : GsRecv
    {
        public R1089TargetObject(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R1089TargetObject(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public int TargetId { get; set; }

        public override void Parse()
        {
            TargetId = ReadInt32();
        }
    }
}