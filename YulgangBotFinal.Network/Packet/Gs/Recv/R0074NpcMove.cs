﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: R0074NpcMove.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x74)]
    public class R0074NpcMove : GsRecv
    {
        public R0074NpcMove(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0074NpcMove(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public NpcMoveInfo NpcMoves { get; private set; }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            var mX = ReadFloat();
            var mY = ReadFloat();
            var mZ = ReadFloat();
            ReadInt32();
            var isRun = ReadInt32();
            var distance = ReadFloat();
            var hp = ReadInt32();
            NpcMoves = new NpcMoveInfo()
            {
                Hp = hp,
                Location = new YulgangLocation()
                {
                    X = mX,
                    Y = mY,
                    Z = mZ,
                },
                RunType = isRun
            };
        }

        #endregion
    }

    public class NpcMoveInfo
    {
        public long Id { get; set; }
        public IYulgangLocation Location { get; set; }
        public int Hp { get; set; }
        public int RunType { get; set; }
    }
}