﻿#region header
// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 12:32 PM 25/09/2016
// FILENAME: R0087ActiveBuff.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************
#endregion

using System.Text;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x87)]
    public class R0087ActiveBuff:GsRecv
    {
        public R0087ActiveBuff(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0087ActiveBuff(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public override void Parse()
        {
            Id = (int)ReadInt64();
            ReadInt32();
            Active = ReadInt32() == 1;
            Exist = ReadInt32();
            ReadInt32();
        }

        public int Exist { get; set; }

        public bool Active { get; set; }

        public int Id { get; set; }
    }
}