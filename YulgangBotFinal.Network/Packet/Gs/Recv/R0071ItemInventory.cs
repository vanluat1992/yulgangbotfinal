﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 9:56 PM 16/09/2016
// FILENAME: R0071ItemInventory.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Collections.Generic;
using System.Text;
using YulgangBotFinal.Model.Helper;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x71)]
    public class R0071ItemInventory : GsRecv
    {
        public R0071ItemInventory(byte[] data, int sessionId) : base(data, sessionId)
        {
            // Debug.WriteLine(BitConverter.ToString(data).Replace("-",""));
        }

        public R0071ItemInventory(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public int Type { get; set; }

        public IDictionary<long, IYulgangGameItem> Items { get; } = new Dictionary<long, IYulgangGameItem>();

        public override void Parse()
        {
            Type = ReadByte();

            var job = ReadByte();
            ReadInt16();
            var hasSubBag = ReadInt32();
            if (Type == 0x1)
                for (var i = 0; i < 66; i++)
                {
                    var bdata = ReadBytes(92);
                    var it = bdata?.GetGameItem();
                    if (it?.Id > 0)
                    {
                        // IYulgangYbiGameItem item;
                        // if (Ybi.DicItems.TryGetValue(it.BasicInfo.Id, out item))
                        {
                            //it.BasicInfo = item;
                            //it.Name = item.Name;
                            //it.Forces = item.Force; 1008000033
                            it.Index = i;
                        }
                        if (!Items.ContainsKey(it.Id))
                            Items.Add(it.Id, it);
                    }
                }
        }
    }
}