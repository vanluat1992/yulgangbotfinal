﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: R0064InitCharacterOrPet.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Collections.Generic;
using System.Text;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.Helper;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x64)]
    public class R0064InitCharacterOrPet : GsRecv
    {
        private byte[] _data;

        public R0064InitCharacterOrPet(byte[] data, int sessionId) : base(data, sessionId)
        {
            Players = new Dictionary<long, YulgangPlayer>();
            _data = data;

        }

        public R0064InitCharacterOrPet(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
            Players = new Dictionary<long, YulgangPlayer>();
            _data = data;
        }

        public IDictionary<long, YulgangPlayer> Players { get; set; }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            var count = ReadInt32();
            //Debug.WriteLine($"Recv player {count}");
            //Debug.WriteLine(BitConverter.ToString(_data).Replace("-", ""));
            //Log.Debug("Nhận packet 64 : Số lượng nhân vật có trong packet  {0} ",count);
            for (var i = 0; i < count; i++)
            {
                var session = ReadInt32(); //4

                var name = ReadString(15);
                var uk = ReadByte(); //20

                var gId = ReadInt32(); //24
                var gName = ReadString(15);
                var gLevel = ReadByte(); //40
                var gImage = ReadInt16();
                var cforces = ReadByte();
                var cLevel = ReadByte();
                var cJobLevel = ReadByte();
                var cJob = ReadByte();
                var cFamous = ReadByte();

                // khuôn mặt và tóc
                ReadByte();
                var cHairColor = ReadByte();
                var cHairStyle = ReadByte();
                var cFace = ReadByte();
                ReadByte();
                var cVoiceId = ReadByte();
                var cSex = ReadByte(); //54
                ReadInt16(); //56
                var cX = ReadFloat(); //60
                var cZ = ReadFloat(); //64
                var cY = ReadFloat(); //68
                var cMapId = ReadInt32(); //72

                ReadInt64(); //80
                ReadInt64(); //88
                ReadInt64(); //96
                ReadInt64(); //104
                ReadInt64(); //112
                ReadInt64(); //120
                ReadInt32(); //124
                ReadInt64(); //132

                ReadInt32(); //Effect 136
                var cX1 = ReadFloat(); //140
                var cZ1 = ReadFloat(); //144
                var cY1 = ReadFloat(); //148
                ReadInt32(); //152
                ReadInt32(); //156


                ReadInt64(); //164
                ReadInt64(); //172
                ReadInt16(); //174
                ReadInt16(); //176

                var cPk = ReadInt32(); //180
                var cKarma = ReadInt32(); //184
                ReadInt32(); //188

                ReadInt32(); //192
                ReadInt32(); //196
                var cNameStyle = ReadBytes(48); //244

                ReadInt32();
                ReadInt32(); //252

                var cIsMarris = ReadByte(); //253
                var cMarrieName = ReadString(16); //269
                ReadInt32(); //273
                ReadByte(); //274
                ReadByte();
                ReadByte(); //276
                ReadInt16(); //278
                ReadInt16(); //280
                ReadBytes(76);
                ReadBytes(32);
                ReadInt32();
                ReadInt32();
               // ReadInt32();
                var player = new YulgangPlayer()
                {
                    Id = session,
                    Name = name,
                    Level = cLevel,
                    Forces = (YulgangGameForce) cforces,
                    Guide = new YulgangGuide()
                    {
                        Name = gName,
                        Level = gLevel
                    },
                    Job = ((int) cJob).GetJobGame(),
                    PkPoint = cPk,
                    KarmaPoint = cKarma,
                    Sex = (YulgangGameSex) cSex,
                    Upgrade = cJobLevel,
                    Location = new YulgangLocation()
                    {
                        X = cX,
                        Y = cY,
                        Z = cZ,
                        MapId = cMapId
                    }
                };
                //Log.Debug("Packet Session {0} , Character Session {1} , Name {2}", Session, session, name);
                //Debug.WriteLine("AddPlayer Id :"+player.Id);
                Players.Add(player.Id, player);
            }
        }

        #endregion
    }
}