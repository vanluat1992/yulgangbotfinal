﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 10:45 PM 21/09/2016
// FILENAME: R0072InitializerItemDrop.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Collections.Generic;
using System.Text;
using YulgangBotFinal.Model.Helper;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x72)]
    public class R0072InitializerItemDrop : GsRecv
    {
        public R0072InitializerItemDrop(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0072InitializerItemDrop(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public IList<IYulgangItemDrop> Items { get; } = new List<IYulgangItemDrop>();

        public override void Parse()
        {
            var count = ReadInt32();
            for (var i = 0; i < count; i++)
            {
                var item = ReadBytes(96).GetItemDrop();
                if (item == null)
                {
                    continue;
                }
                Items.Add(item);
            }
        }
    }
}