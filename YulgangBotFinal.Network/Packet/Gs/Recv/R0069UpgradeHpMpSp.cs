﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: R0069UpgradeHpMpSp.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Model;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x69)]
    public class R0069UpgradeHpMpSp : GsRecv
    {
        public R0069UpgradeHpMpSp(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0069UpgradeHpMpSp(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            ReadInt32();
            Info = new BasicInfo
            {
                CurrentHp = ReadInt32(),
                CurrentMp = ReadInt32(),
                CurrentSp = ReadInt32(),
                MaxHp = ReadInt32(),
                MaxMp = ReadInt32(),
                MaxSp = ReadInt32()
            };
        }

        public BasicInfo Info { get; set; }

        #endregion
    }
}