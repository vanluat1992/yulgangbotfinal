﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: R0080SyncServerTime.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x80)]
    public class R0080SyncServerTime : GsRecv
    {
        public R0080SyncServerTime(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0080SyncServerTime(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public int TimeTick { get; set; }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            TimeTick = ReadInt32();
        }

        #endregion
    }
}