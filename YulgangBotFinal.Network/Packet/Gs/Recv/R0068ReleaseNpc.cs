﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:22 PM 16/09/2016
// FILENAME: R0068ReleaseNpc.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Collections.Generic;
using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(68)]
    public class R0068ReleaseNpc : GsRecv
    {
        public R0068ReleaseNpc(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0068ReleaseNpc(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public IList<long> NpcIds { get; } = new List<long>();

        #region Overrides of RecvPacket

        public override void Parse()
        {
            var count = ReadInt32();
            for (var i = 0; i < count; i++)
            {
                var session = ReadUInt16();
                if (SessionId != session)
                {
                    var session1 = ReadUInt16();
                    var mId = ReadInt16();
                    ReadInt32();
                    var mCurrentHp = ReadInt32();
                    var mMaxHp = ReadInt32();
                    var mX = ReadFloat();
                    var mZ = ReadFloat();
                    var mY = ReadFloat();
                    ReadInt32();
                    var face1 = ReadFloat();
                    var face2 = ReadFloat();
                    var mX1 = ReadFloat();
                    var mZ1 = ReadFloat();
                    var mY2 = ReadFloat();
                    ReadBytes(16);
                }
                NpcIds.Add(session);
            }
        }

        #endregion
    }
}