﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 1:55 PM 17/09/2016
// FILENAME: R006AExpInfo.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x6A)]
    public class R006AExpInfo : GsRecv
    {
        public R006AExpInfo(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R006AExpInfo(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public int Spellpoint { get; set; }

        public long MaxExp { get; set; }

        public long CurrentExp { get; set; }

        public override void Parse()
        {
            if (SessionId < 40000)
            {
                CurrentExp = ReadInt64();
                MaxExp = ReadInt64();
                ReadInt32();
                Spellpoint = ReadInt32();
                ReadInt16();
                ReadInt16();
                ReadInt32();

                var expVip = ReadInt32();
                var maxExpVip = ReadInt32();
            }
        }
    }
}