﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 1:59 PM 17/09/2016
// FILENAME: R0088HideGameObject.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x88)]
    public class R0088HideGameObject : GsRecv
    {
        public R0088HideGameObject(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0088HideGameObject(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public override void Parse()
        {
            /*player*/
            //if (PacketSession < 10000)
            //{
            //    EventManeger.CallBackGuiEvent(GuiEvent.ReleasePlayer, PacketSession);
            //}
            /*Npc*/
            //if (PacketSession >= 10000 && PacketSession < 40000)
            //{
            //    EventManeger.CallBackGuiEvent(GuiEvent.ReleaseNpc, PacketSession);
            //}
        }
    }
}