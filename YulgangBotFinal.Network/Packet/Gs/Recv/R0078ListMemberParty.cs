﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 12:27 AM 20/09/2016
// FILENAME: R0078ListMemberParty.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Collections.Generic;
using System.Text;
using YulgangBotFinal.Model;
using YulgangBotFinal.Model.Helper;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x78)]
    public class R0078ListMemberParty : GsRecv
    {
        public R0078ListMemberParty(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0078ListMemberParty(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public YulgangPartyMember Leader { get; set; }
        public IList<YulgangPartyMember> Members { get; set; } = new List<YulgangPartyMember>();

        public override void Parse()
        {
            ReadByte();
            ReadInt16();
            var count = ReadByte();
            ReadInt16();
            var list = new List<long>();
            var lsession = ReadInt16();
            var lcurrentHp = ReadInt16();
            var lcurrnetMp = ReadInt16();
            var lmaxHp = ReadInt16();
            var lmaxMp = ReadInt16();
            var lname = ReadString(15);
            var uk = ReadByte();
            var uk1 = ReadInt16();
            var lJob = ReadInt32();

            Leader = new YulgangPartyMember
            {
                Id = lsession,
                CurrentHp = lcurrentHp,
                CurrentMp = lcurrnetMp,
                MaxHp = lmaxHp,
                MaxMp = lmaxMp,
                Name = lname,
                Job = lJob.GetJobGame()
            };


            for (var i = 0; i < count - 1; i++)
            {
                var session = ReadInt16();
                var currentHp = ReadInt16();
                var currnetMp = ReadInt16();
                var maxHp = ReadInt16();
                var maxMp = ReadInt16();
                var name = ReadString(15);
                var uk3 = ReadByte();
                var uk2 = ReadInt16();
                var job = ReadInt16();

                var member = new YulgangPartyMember();
                member.Id = session;
                member.CurrentHp = currentHp;
                member.CurrentMp = currnetMp;
                member.MaxHp = maxHp;
                member.MaxMp = maxMp;
                member.Name = name;
                member.Job = ((int) job).GetJobGame();

                if (i < count - 2)
                    ReadUInt16();
                Members.Add(member);
            }

            //var remove = partyMember.Where(m => !list.Contains(m.Key)).ToList();

            //foreach (var member in remove)
            //{
            //    EventManeger.CallBackGuiEvent(GuiEvent.RemoveMemberToParty, member.Key);
            //}
        }
    }
}