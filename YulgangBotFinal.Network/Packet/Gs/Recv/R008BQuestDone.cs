﻿// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Network
// FILENAME : R0085QuestDone.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 9:10 PM
// **************************************************************************

using System.Collections.Generic;
using System.Text;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x8B)]
    public class R008BQuestDone:GsRecv
    {
        public IList<int> QuestDone { get; set; }=new List<int>();
        public R008BQuestDone(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R008BQuestDone(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public override void Parse()
        {
            var count = ReadInt32();
            for (int i = 0; i < count; i++)
            {
                var idQuest = ReadInt16();
                QuestDone.Add(idQuest);
            }
        }
    }
}