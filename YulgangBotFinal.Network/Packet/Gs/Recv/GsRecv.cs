﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: GsRecv.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Core;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    public abstract class GsRecv : RecvPacket
    {
        public GsRecv(byte[] data, int sessionId) : base(data)
        {
            SessionId = sessionId;
        }

        public GsRecv(byte[] data, Encoding en, int sessionId) : base(data, en)
        {
            SessionId = sessionId;
        }

        public int SessionId { get; }
    }
}