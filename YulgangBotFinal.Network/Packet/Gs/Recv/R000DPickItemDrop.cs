﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 10:49 PM 21/09/2016
// FILENAME: R000DPickItemDrop.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x0D)]
    public class R000DPickItemDrop : GsRecv
    {
        public R000DPickItemDrop(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R000DPickItemDrop(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public IYulgangGameItem Item { get; set; }

        public override void Parse()
        {
            var command = ReadInt32();
            var id = ReadInt64();
            if (command == 1)
            {
                var itemId = ReadInt64();
                var count = ReadInt64();
                var uk = ReadByte();
                var index = ReadByte();
                //IYulgangYbiGameItem item;
                //if (Ybi.DicItems.TryGetValue(itemId, out item))
                {
                    Item = new YulgangGameItem
                    {
                        Id = id,
                        BasicInfo = new YulgangYbiGameItem() {Id = itemId},
                        // Name = item.Name,
                        // Forces = item.Force,
                        Index = index,
                        Count = count
                    };

                    //if (it.BasicInfo.Id != 2000000000)
                    //{
                    //    Log.Fatal("Nhặt được item :{0}[{1}]", it.Name, it.Count);
                    //    EventManeger.CallBackGuiEvent(GuiEvent.AddGameItemInventory, it);
                    //}
                    //else
                    //{
                    //    Log.Fatal("Nhận được : {0} lượng", it.Count);
                    //}
                    /*lọc đồ theo option ngọc*/
                }
            }
            // if (PacketSession == Connection.Session)
            //    EventManeger.CallBackGuiEvent(GuiEvent.PickItem, id, command);
        }
    }
}