﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 6:39 PM 22/09/2016
// FILENAME: R0093BuyOrSellNpcItem.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x93)]
    public class R0093BuyOrSellNpcItem : GsRecv
    {
        public R0093BuyOrSellNpcItem(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0093BuyOrSellNpcItem(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            var rCommand = ReadInt32();
            Command = ReadInt32();
            if (Command != rCommand)
            {
            }
            else
            {
                var id = ReadInt64();
                var count = ReadInt64();
                var money = ReadInt64();
                var itemsession = ReadInt64();
                var itemid = ReadInt64();
                var ittemcount = ReadInt64();
                ReadByte();
                var index = ReadByte();
                Item = new YulgangGameItem()
                {
                    Count = count,
                    Id = itemsession,
                    Index = index,
                    BasicInfo = new YulgangYbiGameItem() {Id = id}
                };
                //IYulgangYbiGameItem baseIt;
                //if (Ybi.DicItems.TryGetValue(id, out baseIt))
                //{
                //    item.BasicInfo = baseIt;
                //    item.Name = baseIt.Name;
                //    item.Forces = baseIt.Force;
                //    if (id != 2000000000)
                //    {
                //        switch ((YulgangNpcBySellCommand)command)
                //        {
                //            case YulgangNpcBySellCommand.Buy:
                //                Log.Debug("Mua Item {0} [{1}]", item.BasicInfo.Id, ittemcount);
                //                item.Count = ittemcount;
                //                EventManeger.CallBackGuiEvent(GuiEvent.AddGameItemInventory, item);
                //                break;
                //            case YulgangNpcBySellCommand.Sell:
                //                item.Count = 0 - item.Count;
                //                // EventManeger.CallBackGuiEvent(GuiEvent.BuyOrSellItemNpc, item);
                //                EventManeger.CallBackGuiEvent(GuiEvent.AddGameItemInventory, item);
                //                break;
                //        }
                //    }
                //}
            }
        }

        public int Command { get; set; }

        public IYulgangGameItem Item { get; set; }

        #endregion
    }
}