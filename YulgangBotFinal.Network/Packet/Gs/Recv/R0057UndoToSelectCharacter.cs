﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 9:08 PM 16/09/2016
// FILENAME: R0057UndoToSelectCharacter.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x57)]
    public class R0057UndoToSelectCharacter : GsRecv
    {
        public R0057UndoToSelectCharacter(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0057UndoToSelectCharacter(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public override void Parse()
        {
        }
    }
}