﻿// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Network
// FILENAME : R0084Quest.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 8:23 PM
// **************************************************************************

using System.Text;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x84)]
    public class R0084Quest : GsRecv
    {
        public R0084Quest(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0084Quest(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public int IdQuest { get; set; }
        public int EventQuest { get; set; }
        public int Step { get; set; }

        public override void Parse()
        {
            IdQuest = ReadInt16();
            EventQuest = ReadInt16();
            Step = ReadInt16();
        }
    }
}