﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 1:52 PM 17/09/2016
// FILENAME: R0065PlayerMove.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x65)]
    public class R0065PlayerMove : GsRecv
    {
        public R0065PlayerMove(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0065PlayerMove(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public IYulgangLocation ToLocation { get; set; }

        public override void Parse()
        {
            ReadUInt32();
            var x = ReadFloat();
            var z = ReadFloat();
            var y = ReadFloat();

            ToLocation = new YulgangLocation {X = x, Y = y};
        }
    }
}