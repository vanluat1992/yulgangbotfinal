﻿// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Network
// FILENAME : R0085QuestDoing.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 9:02 PM
// **************************************************************************

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(0x85)]
    public class R0085QuestDoing:GsRecv
    {
        public IDictionary<int,IYulgangPlayerQuest> Quest { get; set; }=new Dictionary<int, IYulgangPlayerQuest>();
        public R0085QuestDoing(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0085QuestDoing(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public override void Parse()
        {
            var count = ReadInt32();
            for (int i = 0; i < count; i++)
            {
                var idQuest = ReadInt16();
                var step = ReadInt16();
                Quest.Add(idQuest, new YulgangPlayerQuest() {Id = idQuest, Step = step});
            }
            ReadInt32();
        }
    }
}