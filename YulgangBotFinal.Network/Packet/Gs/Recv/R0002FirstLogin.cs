﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: R0002FirstLogin.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Gs.Recv
{
    [Opcode(2)]
    public class R0002FirstLogin : GsRecv
    {
        public R0002FirstLogin(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R0002FirstLogin(byte[] data, Encoding en, int sessionId) : base(data, en, sessionId)
        {
        }

        public override void Parse()
        {
        }
    }
}