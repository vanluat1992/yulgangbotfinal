﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.Network
// FileName : S804C.cs
// Time Create : 8:01 AM 25/03/2017
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion

using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Auth.Send
{
    [Opcode(0x804C)]
    public class S804C:AuthSendPacket
    {
        public S804C Send(byte type)
        {
            WriteByte(type);
            return this;
        }
    }
}