﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: AuthSendPacket.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Reflection;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Auth.Send
{
    public class AuthSendPacket : SendPacket
    {
        #region Overrides of SendPacket

        public override IPacket BuildPacket()
        {
            var opcode = GetType().GetCustomAttribute<OpcodeAttribute>();
            if (opcode == null)
                throw new NotSupportedException("Gói tin chưa định nghĩa opcode");
            return new BasicPacket
            {
                Opcode = opcode.Opcode,
                Data = ToArray()
            };
        }

        #endregion
    }
}