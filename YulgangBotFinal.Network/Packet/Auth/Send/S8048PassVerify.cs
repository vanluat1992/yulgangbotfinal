﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: S8048PassVerify.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Auth.Send
{
    [Opcode(0x8048)]
    public class S8048PassVerify : AuthSendPacket
    {
        public S8048PassVerify Send(string pass)
        {
            WriteInt16((short) pass.Length);
            WriteString(pass);
            return this;
        }
    }
}