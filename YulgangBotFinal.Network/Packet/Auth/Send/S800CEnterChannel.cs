﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: S800CEnterChannel.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Auth.Send
{
    [Opcode(0x800C)]
    public class S800CEnterChannel : AuthSendPacket
    {
        public S800CEnterChannel Send(int idServer, int idChannel)
        {
            WriteInt32(idServer);
            WriteInt32(idChannel);
            WriteByte(0xE3);
            const string s = "0123456789ABCDEF";
            var m = "60A44C2E81B8";
            var mac = "";
            var ran = new Random(DateTime.Now.Millisecond);
            for (var i = 0; i < 11; i++)
            {
                mac += s[ran.Next(0, 15)];
            }
            WriteInt16((short) mac.Length);
            WriteString(mac);
            return this;
        }
        public S800CEnterChannel SendKorea(int idServer, int idChannel)
        {
            WriteInt32(idServer);
            WriteInt32(idChannel);
            WriteByte(0xE8);
            //const string s = "0123456789ABCDEF";
            //var m = "60A44C2E81B8";
            //var mac = "";
            //var ran = new Random(DateTime.Now.Millisecond);
            //for (var i = 0; i < 11; i++)
            //{
            //    mac += s[ran.Next(0, 15)];
            //}
            //WriteInt16((short) mac.Length);
            //WriteString(mac);
            return this;
        }
    }
}