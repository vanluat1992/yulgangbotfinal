﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: S8000FirstLogin.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Auth.Send
{
    [Opcode(0x8000)]
    public class S8000FirstLogin : AuthSendPacket
    {
        public S8000FirstLogin Send(string username, string pass)
        {
            WriteInt16((short) username.Length);
            WriteString(username);
            WriteInt16((short) pass.Length);
            WriteString(pass);
            WriteBytes(new byte[16]);
            return this;
        }
        public S8000FirstLogin SendKorea(string username, string pass)
        {
            WriteInt16((short)username.Length);
            WriteString(username);
            WriteInt16((short)pass.Length);
            WriteString(pass);
            WriteInt16(1);
            WriteInt16(0x30);
            WriteBytes(new byte[11]);
            return this;
        }
    }
}