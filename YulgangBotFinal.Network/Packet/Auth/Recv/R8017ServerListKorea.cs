﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.Network
// FileName : R8017ServerListKorea.cs
// Time Create : 8:40 AM 25/03/2017
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion

using System.Collections.Generic;
using System.Text;
using YulgangBotFinal.Model;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Auth.Recv
{
    [Opcode(0x8017)]
    public class R8017ServerListKorea:AuthRecvPacket
    {
        public IDictionary<int, InfoGroupServer> Servers = new Dictionary<int, InfoGroupServer>();
        public R8017ServerListKorea(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R8017ServerListKorea(byte[] data, Encoding en) : base(data, en)
        {
        }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            var result = new Dictionary<int, InfoGroupServer>();
            var scount = ReadInt16();
            for (var i = 0; i < scount; i++)
            {
                var sId = ReadInt16();
                var snlen = ReadInt16();
                var sName = ReadString(snlen);
                var uk1 = ReadInt16();
                var uk2 = ReadInt16();
                var uk3 = ReadInt16();
                var gr = new InfoGroupServer { Id = sId, Name = sName };
                var cCount = ReadInt16();
                var uk4 = ReadInt16();
                for (var j = 0; j < cCount; j++)
                {
                    var cId = ReadInt16();
                    var cnlen = ReadInt16();
                    var cName = ReadString(cnlen);
                    var cPercen = ReadInt16();
                    var cuk1 = ReadInt16();
                    var ch = new InfoChannel() { Id = cId, Name = cName, Percent = cPercen };
                    gr.Chanels.Add(ch.Id, ch);
                }
                result.Add(gr.Id, gr);
            }
            Servers = result;
        }

        #endregion
    }
}