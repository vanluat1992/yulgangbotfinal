﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: R8064EnterChannel.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Auth.Recv
{
    [Opcode(0x8064)]
    public class R8064EnterChannel : AuthRecvPacket
    {
        public R8064EnterChannel(byte[] data, int s) : base(data, s)
        {
        }

        public R8064EnterChannel(byte[] data, Encoding en) : base(data, en)
        {
        }

        public string Ip { get; private set; }
        public string User { get; private set; }
        public int Port { get; set; }

        public override void Parse()
        {
            var iplen = ReadInt16();
            var ipGs = ReadString(iplen);
            var port = ReadInt16();
            var ulen = ReadInt16();
            var user = ReadString(ulen);
            Ip = ipGs;
            Port = port;
            User = user;
        }
    }
}