﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: AuthRecvPacket.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Core;

#endregion

namespace YulgangBotFinal.Network.Packet.Auth.Recv
{
    public abstract class AuthRecvPacket : RecvPacket
    {
        public AuthRecvPacket(byte[] data, int sessionId) : base(data)
        {
        }

        public AuthRecvPacket(byte[] data, Encoding en) : base(data, en)
        {
        }

        #region Overrides of RecvPacket

        #endregion
    }
}