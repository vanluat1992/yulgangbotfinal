﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: R8001FirstLogin.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Auth.Recv
{
    [Opcode(0x8001)]
    public class R8001FirstLogin : AuthRecvPacket
    {
        public R8001FirstLogin(byte[] data, int s) : base(data, s)
        {
        }

        public R8001FirstLogin(byte[] data, Encoding en) : base(data, en)
        {
        }

        public bool Valid { get; set; }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            var resultType = ReadInt16();
            var resultvalue = ReadInt16();
            ReadInt16();
            if (resultType == 0)
            {
                if (resultvalue == 0)
                {
                    var ulen = ReadInt16();
                    var user = ReadString(ulen);
                    var up1 = ReadInt16();
                    var p1 = ReadString(up1);
                    var up2 = ReadInt16();
                    var p2 = ReadString(up2);

                    //kiểm tra giá trị
                    Valid =  p1.Equals(p2);
                }
            }
        }

        #endregion
    }
}