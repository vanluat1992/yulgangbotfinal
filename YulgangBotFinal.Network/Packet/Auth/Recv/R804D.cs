﻿#region header
// /*********************************************************************************************/
// Project :YulgangBotFinal.Network
// FileName : R804D.cs
// Time Create : 8:01 AM 25/03/2017
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/
#endregion

using System.Text;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.Network.Packet.Auth.Recv
{
    [Opcode(0x804D)]
    public class R804D:AuthRecvPacket
    {
        public int Type { get; set; }
        public R804D(byte[] data, int sessionId) : base(data, sessionId)
        {
        }

        public R804D(byte[] data, Encoding en) : base(data, en)
        {
        }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            Type = ReadInt32();
        }

        #endregion
    }
}