﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: R8049PassVerify.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System.Text;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network.Packet.Auth.Recv
{
    [Opcode(0x8049)]
    public class R8049PassVerify : AuthRecvPacket
    {
        public R8049PassVerify(byte[] data, int s) : base(data, s)
        {
        }

        public R8049PassVerify(byte[] data, Encoding en) : base(data, en)
        {
        }

        public bool Valid { get; set; }

        #region Overrides of RecvPacket

        public override void Parse()
        {
            var chk = ReadInt16();
            var invalidcount = ReadInt16();
            Valid = chk == 1;
        }

        #endregion
    }
}