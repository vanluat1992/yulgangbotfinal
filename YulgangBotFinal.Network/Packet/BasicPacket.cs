﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: BasicPacket.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using YulgangBotFinal.Core.Packet;

#endregion

namespace YulgangBotFinal.Network.Packet
{
    public class BasicPacket : IPacket
    {
        #region Implementation of IPacket

        public int Opcode { get; set; }
        public int SessionId { get; set; }
        public byte[] Data { get; set; }

        #endregion
    }
}