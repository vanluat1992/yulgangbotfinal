﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: GsConnect.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Hik.Communication.Scs.Client;
using Hik.Communication.Scs.Communication.EndPoints.Tcp;
using Hik.Communication.Scs.Communication.Messages;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Network.Packet;
using YulgangBotFinal.Network.Utils;

#endregion

namespace YulgangBotFinal.Network
{
    public class GsConnect : IConnection
    {
        private readonly ConcurrentDictionary<Type, ManaProcWithOpcode> _allProc =
            new ConcurrentDictionary<Type, ManaProcWithOpcode>();

        private readonly CancellationTokenSource _cancel;

        private readonly Queue<byte[]> _queueUnhandlePacket;
        private IScsClient _connect;
        private byte[] _dBytes;

        public GsConnect(ILog log)
        {
            Log = log;
            _queueUnhandlePacket = new Queue<byte[]>();
            _cancel = new CancellationTokenSource();
            Task.Factory.StartNew(ProcessData, _cancel.Token);
        }

        protected ILog Log { get; }

        public void Connect(string ip, int port)
        {
            _connect = ScsClientFactory.CreateClient(new ScsTcpEndPoint(ip, port));
            _connect.WireProtocol = new StreamWireProtocol();
            _connect.MessageReceived += Connect_MessageReceived;
            _connect.Disconnected += _connect_Disconnected;
            _connect.Connect();
        }

        public void Register(IHookRecvPacket proc, params int[] opcodes)
        {
            if (!_allProc.ContainsKey(proc.GetType()))
                while (!_allProc.TryAdd(proc.GetType(), new ManaProcWithOpcode(proc, opcodes)))
                {
                    Thread.Sleep(10);
                }
        }

        public void Send(IPacket p)
        {
            byte[] data = null;
            using (var plainStream = new MemoryStream())
            {
                using (var w = new BinaryWriter(plainStream))
                {
                    w.Write((ushort) p.SessionId);
                    w.Write((ushort) p.Opcode);
                    w.Write((ushort) p.Data.Length);
                    w.Write(p.Data);
                }
                data = plainStream.ToArray();
            }
            //Log.Dum(data, $"Send : {p.Opcode.ToString("X4")}");
            //Debug.WriteLine(BitConverter.ToString(data));
            var key = (ushort) DateTime.Now.Ticks;
            var ecryptedKey = DzoEncrypt.CreateKey(key);
            DzoEncrypt.Encrypt(data, ecryptedKey);
            using (var plainStream = new MemoryStream())
            {
                using (var w = new BinaryWriter(plainStream))
                {
                    w.Write((byte) 0xAA);
                    w.Write((byte) 0x55);
                    w.Write((short) (data.Length + 2));
                    w.Write((ushort) key);
                    w.Write(data);
                    w.Write((byte) 0x55);
                    w.Write((byte) 0xAA);
                }
                DzoEncrypt.Descrypt(data, ecryptedKey);
                _connect.SendMessage(new ScsRawDataMessage(plainStream.ToArray()));
            }
        }

        public void Send(SendPacket p)
        {
            Send(p.BuildPacket());
        }

        public void Release()
        {
            _allProc.Clear();
            _connect.Disconnect();
            _connect.Dispose();
            _cancel.Cancel();
        }

        public void Unregister(IHookRecvPacket proc)
        {
            if (_allProc.ContainsKey(proc.GetType()))
            {
                ManaProcWithOpcode tmp;
                while (!_allProc.TryRemove(proc.GetType(), out tmp))
                {
                    Thread.Sleep(10);
                }
            }
        }

        public event Action Disconnect;

        private void ProcessData()
        {
            try
            {
                while (true)
                {
                    if (_cancel.IsCancellationRequested)
                        return;
                    if (_queueUnhandlePacket.Count > 0)
                    {
                        var bytOriginal = _queueUnhandlePacket.Dequeue();

                        if (_dBytes == null)
                        {
                            _dBytes = new byte[bytOriginal.Length];
                            Array.Copy(bytOriginal, 0, _dBytes, 0, bytOriginal.Length);
                        }
                        else
                        {
                            var iCurrentLeng = _dBytes.Length;
                            Array.Resize(ref _dBytes, iCurrentLeng + bytOriginal.Length);
                            Array.Copy(bytOriginal, 0, _dBytes, iCurrentLeng, bytOriginal.Length);
                        }

                        var bContinue = true;

                        while (bContinue)
                        {
                            var length = _dBytes.Length;
                            if (length <= 9)
                            {
                                bContinue = false;
                                continue;
                            }

                            var iBegin = _dBytes.FindBeginYulgang();
                            // txtLog.WriteLine("Find Begin Yulgang : {0} ", iBegin);
                            if (iBegin > -1)
                            {
                                int iLeng = BitConverter.ToUInt16(_dBytes, iBegin + 2); // Length
                                // txtLog.WriteLine("Lenght Packet : {0} ", iLeng);
                                if (iBegin + 6 <= length)
                                {
                                    var iEnd = _dBytes.FindEndYulgang(iBegin + iLeng + 4);
                                    // txtLog.WriteLine("Find End Yulgang : {0} ", iEnd);
                                    if (iEnd <= -1)
                                    {
                                        bContinue = false;
                                        continue;
                                    }
                                    var iBody = iEnd - iBegin - 4;
                                    //txtLog.WriteLine("Body Lenght : {0} ", iBody);
                                    if (iBody == iLeng)
                                    {
                                        var bytClearly = new byte[iBody + 6];
                                        Array.Copy(_dBytes, iBegin, bytClearly, 0, iBody + 6);

                                        var s = BitConverter.ToString(bytClearly).Replace("-", "");
                                        //txtLog.WriteLine("data After handle: ");
                                        //txtLog.WriteLine(s);

                                        // Frmain.DepatchPacketForPlugins();
                                        // lock (QueueHandledPacket)
                                        {
                                            //Log.Fatal("Before Decrypt",
                                            //    BitConverter.ToString(bytClearly).Replace("-", ""));
                                            var mstrem = new MemoryStream(bytClearly);
                                            var bRead = new BinaryReader(mstrem);
                                            if (bRead.ReadByte() == 0xAA && bRead.ReadByte() == 0x55)
                                            {
                                                //txtLog.WriteLine("Check Valid header packet ");
                                                var len = bRead.ReadInt16();
                                                var key = bRead.ReadUInt16();
                                                var packData = bRead.ReadBytes(len - 2);

                                                var deKey = DzoEncrypt.CreateKey(key);
                                                //Console.WriteLine($"Key : {key.ToString("X4")}");
                                                //Console.WriteLine(
                                                //    $"Encrypt Data : {BitConverter.ToString(packData).Replace("-", "")}");
                                                DzoEncrypt.Descrypt(packData, deKey);

                                                //Log.Fatal("After Decrypt",
                                                //    BitConverter.ToString(packData).Replace("-", ""));
                                                //Console.WriteLine(
                                                //    $"Decrypt Data: {BitConverter.ToString(packData).Replace("-", "")}");
                                                // create packet
                                                bytClearly = new byte[len + 4];

                                                bRead.Dispose();
                                                mstrem.Dispose();
                                                mstrem = new MemoryStream(bytClearly);
                                                var w = new BinaryWriter(mstrem);
                                                w.Write((byte) 0xAA);
                                                w.Write((byte) 0x55);
                                                w.Write((short) (len + 4));
                                                w.Write(packData);

                                                w.Write((byte) 0x55);
                                                w.Write((byte) 0xAA);
                                                //Console.WriteLine(
                                                //   $"RECV: {BitConverter.ToString(bytClearly).Replace("-", "")}");
                                                w.Dispose();
                                                mstrem.Dispose();
                                                mstrem = new MemoryStream(bytClearly);
                                                bRead = new BinaryReader(mstrem);
                                                bRead.ReadInt16();
                                                var plen = bRead.ReadInt16();
                                                var sesionId = bRead.ReadUInt16();
                                                var opcode = bRead.ReadInt16();
                                                var dlen = bRead.ReadInt16();
                                                var pdata = bRead.ReadBytes(dlen);
                                                var p = new BasicPacket
                                                {
                                                    SessionId = sesionId,
                                                    Opcode = opcode,
                                                    Data = pdata
                                                };
                                                bRead.ReadByte();
                                                bRead.ReadByte();
                                                //txtLog.WriteLine("Opcode : {0} ", p.OpCode.ToString("X4"));
                                                //txtLog.Flush();
                                                if (p.Opcode != 0xC0 && (p.Opcode < 0x240 || p.Opcode > 0x27E))
                                                {
                                                    HandlePacket(p);
                                                }
                                                else
                                                {
                                                    DecryptPacket.ExtractPacket(bytClearly, HandlePacket);
                                                }
                                            }
                                            bRead.Dispose();
                                            mstrem.Dispose();
                                        }
                                        // txtLog.WriteLine("--------------------------END---------------------------");
                                        //txtLog.Flush();
                                        iBegin = iEnd + 2;

                                        Array.Reverse(_dBytes);
                                        Array.Resize(ref _dBytes, length - iBegin);
                                        Array.Reverse(_dBytes);

                                        bContinue = _dBytes.Length > 9;
                                        continue;
                                    }
                                }
                                else
                                {
                                    Log.Error("", "Fail parse packet");
                                }
                                bContinue = false;
                                continue;
                            }
                            bContinue = false;
                        }
                    }
                    Thread.Sleep(10);
                }
            }
            catch (Exception e)
            {
                Log.Exception("", e, "");
                //bIsConnected = false;
            }
        }

        private void _connect_Disconnected(object sender, EventArgs e)
        {
            Disconnect?.Invoke();
        }

        private async void HandlePacket(IPacket p)
        {
            //Log.Fatal("", $"Opcode {p.Opcode.ToString("X4")}");

            //Log.Dum(p.Data, $"Recv : {p.Opcode.ToString("X4")}");
            var clone = new List<ManaProcWithOpcode>();
            using (var tmpEnu = _allProc.GetEnumerator())
                while (tmpEnu.MoveNext())
                {
                    if (tmpEnu.Current.Value != null)
                        clone.Add(tmpEnu.Current.Value);
                }
            var tmp = clone.Where(m => m.Opcode.Contains(p.Opcode)).Select(m => m.Proc).ToList();
            foreach (var proc in tmp)
            {
                
                //await Task.Factory.StartNew(() =>
                {
                    proc.HandlePacket(p);
                } //);
            }
            if (tmp.Count == 0)
                Log.Warning("",
                    $"Gói tin chưa có Process xử lý : {p.Opcode:X2}");
        }

        private void Connect_MessageReceived(object sender, MessageEventArgs e)
        {
            var raw = e.Message as ScsRawDataMessage;
            if (raw == null) return;
            //Log.Fatal("", BitConverter.ToString(raw.MessageData).Replace("-", ""));
            _queueUnhandlePacket.Enqueue(raw.MessageData);
        }
    }
}