﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: AuthConnection.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Hik.Communication.Scs.Client;
using Hik.Communication.Scs.Communication.EndPoints.Tcp;
using Hik.Communication.Scs.Communication.Messages;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Log;
using YulgangBotFinal.Network.Packet;

#endregion

namespace YulgangBotFinal.Network
{
    public class AuthConnection : IConnection
    {
        private ConcurrentDictionary<Type, ManaProcWithOpcode> _allProc =
            new ConcurrentDictionary<Type, ManaProcWithOpcode>();

        private IScsClient _connect = null;

        public AuthConnection(ILog log)
        {
            Log = log;
            //var types = Assembly.GetAssembly(GetType()).GetTypes().Where(m =>
            //{
            //    var attr = m.GetCustomAttribute<OpcodeAttribute>();
            //    if (attr == null) return false;
            //    if (!m.IsSubclassOf(typeof (RecvPacket))) return false;

            //    return true;
            //});

            //foreach (var type in types)
            //{
            //    var attr = type.GetCustomAttribute<OpcodeAttribute>();
            //    _allRecvPacket.Add(attr.Opcode, type);
            //}
        }

        //private IDictionary<int, Type> _allRecvPacket = new Dictionary<int, Type>();
        protected ILog Log { get; private set; }

        #region Implementation of IConnection

        public void Connect(string ip, int port)
        {
            _connect = ScsClientFactory.CreateClient(new ScsTcpEndPoint(ip, port));
            _connect.WireProtocol = new StreamWireProtocol();
            _connect.MessageReceived += Connect_MessageReceived;
            _connect.Disconnected += _connect_Disconnected;
            _connect.Connect();
        }

        private void _connect_Disconnected(object sender, EventArgs e)
        {
            Disconnect?.Invoke();
        }

        private void Connect_MessageReceived(object sender, MessageEventArgs e)
        {
            var raw = e.Message as ScsRawDataMessage;
            if (raw == null) return;
            using (var stream = new MemoryStream(raw.MessageData))
            {
                using (var reader = new BinaryReader(stream))
                {
                    var opcode = reader.ReadUInt16();
                    var len = reader.ReadUInt16();
                    var data = reader.ReadBytes(len);
                    IPacket p = new BasicPacket
                    {
                        Opcode = opcode,
                        Data = data
                    };
                    // chạy task xử lý
                    var tmp = _allProc.Values.Where(m => m.Opcode.Contains(opcode)).Select(m => m.Proc).ToList();
                    foreach (var proc in tmp)
                    {
                        Task.Factory.StartNew(() => { proc.HandlePacket(p); });
                    }
                    if (tmp.Count == 0)
                        Log.Warning("", $"Gói tin chưa có Process xử lý : {p.Opcode:X2}");
                }
            }
        }

        public void Register(IHookRecvPacket proc, params int[] opcodes)
        {
            if (!_allProc.ContainsKey(proc.GetType()))
                while (!_allProc.TryAdd(proc.GetType(), new ManaProcWithOpcode(proc, opcodes)))
                {
                    Thread.Sleep(10);
                }
        }

        public void Send(IPacket p)
        {
            using (var stream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(stream))
                {
                    writer.Write((ushort) p.Opcode);
                    writer.Write((ushort) p.Data.Length);
                    writer.Write(p.Data);
                    var data = stream.ToArray();
                    _connect.SendMessage(new ScsRawDataMessage(data));
                }
            }
        }

        public void Send(SendPacket p)
        {
            Send(p.BuildPacket());
        }

        public void Release()
        {
            _allProc.Clear();
            _connect.Disconnect();
            _connect.Dispose();
        }

        public void Unregister(IHookRecvPacket proc)
        {
            if (_allProc.ContainsKey(proc.GetType()))
            {
                ManaProcWithOpcode tmp;
                while (!_allProc.TryRemove(proc.GetType(), out tmp))
                {
                    Thread.Sleep(10);
                }
            }
        }

        public event Action Disconnect;

        #endregion
    }
}