﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: DecryptPacket.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Diagnostics;
using System.IO;
using YulgangBotFinal.Core.Packet;
using YulgangBotFinal.Network.Packet;

#endregion

namespace YulgangBotFinal.Network.Utils
{
    public class DecryptPacket
    {
        private static readonly int[] _secret = {0xC9, 0x27, 0x93, 0x01, 0xA2, 0x6C, 0x31, 0x97};

        private static readonly int[] encryptTable =
        {
            04, 0x3B, 0xC2, 0x7C, 0xEC, 0xEB, 0x3F, 0x8B, 0x34, 0x83, 0x5B, 0x24, 0x43, 0xF8, 0x6A, 0x20,
            0xC1, 0xE7, 0x02, 0xE8, 0x5C, 0xED, 0xF1, 0xFF, 0x59, 0x8B, 0x0D, 0x08, 0x33, 0x41, 0x00, 0x89
        };

        public static void ExtractPacket(byte[] packet, Action<IPacket> callback)
        {
            try
            {
                int iLeng = BitConverter.ToInt16(packet, Config.BeginSession + 6);
                int iNewLeng = BitConverter.ToInt16(packet, Config.BeginSession + 8);
                //iNewLeng--;
                iNewLeng += 10;

                var bCompress = new byte[iLeng];
                var bUnCompress = new byte[iNewLeng];
                Array.Copy(packet, Config.BeginSession + 10, bCompress, 0, iLeng);

                ExpandPacket(bCompress, iLeng, bUnCompress, iNewLeng);

                var iSourceIndex = 0;
                while (iSourceIndex + 6 <= iNewLeng)
                {
                    var iSizePacket = BitConverter.ToInt16(bUnCompress, iSourceIndex + Config.BeginSession) + 6;
                        // 6 for Session, OpCode, DataLength
                    var opCode = BitConverter.ToInt16(bUnCompress, iSourceIndex + Config.BeginSession - 2);
                        // 6 for Session, OpCode, DataLength

                    if (opCode > 0)
                    {
                        // Debug.WriteLine(opCode.ToString("X4"));
                        var bytTemp = new byte[iSizePacket + Config.BeginSession + 2]; // 6 for AA55-Length-55AA
                        bytTemp[0] = 0xAA;
                        bytTemp[1] = 0x55;

                        var plength = BitConverter.GetBytes(iSizePacket);
                        bytTemp[2] = plength[0];
                        bytTemp[3] = plength[1];
                        Array.Copy(bUnCompress, iSourceIndex, bytTemp, 4, iSizePacket);
                        bytTemp[iSizePacket + Config.BeginSession] = 0x55;
                        bytTemp[iSizePacket + Config.BeginSession + 1] = 0xAA;

                        if (bytTemp.Length >= 12)
                        {
                            var mstrem = new MemoryStream(bytTemp);
                            var bRead = new BinaryReader(mstrem);
                            if (bRead.ReadByte() == 0xAA && bRead.ReadByte() == 0x55)
                            {
                                var len = bRead.ReadInt16();
                                var session = bRead.ReadInt16();
                                var opcode = bRead.ReadInt16();
                                var dlen = bRead.ReadInt16();
                                var data = bRead.ReadBytes(dlen);
                                var p = new BasicPacket
                                {
                                    SessionId = session,
                                    Opcode = opcode,
                                    Data = data
                                };
                                bRead.ReadByte();
                                bRead.ReadByte();
                                if (p.Opcode < 0x240 || p.Opcode > 0x27E)
                                    callback(p);
                                else
                                {
                                    ExtractPacket(bytTemp, callback);
                                }
                            }
                            bRead.Dispose();
                            mstrem.Dispose();
                        }

                        /*
                        lock (QueueHandledPacket)
                        {
                            if (bytTemp.Length >= 12)
                                QueueHandledPacket.Enqueue(bytTemp); 
                        }
                         * */
                    }

                    iSourceIndex += iSizePacket;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public static void ExpandPacket(byte[] pCompressedData, int iLeng, byte[] pExpandedData, int iNewLeng)
        {
            var index = 0;
            var index2 = 0;
            //int num2 = 0;
            do
            {
                int len = pCompressedData[index++];

                if (len >= 0x20)
                {
                    var temp = len >> 5;
                    if (temp == 7)
                        temp = pCompressedData[index++] + 7;

                    var v11 = index2 - ((len & 0x1F) << 8) - 1 - pCompressedData[index++];
                    if (temp + index2 + 2 > iNewLeng)
                    {
                        return;
                    }
                    if (v11 < 0)
                    {
                        return;
                    }

                    pExpandedData[index2] = pExpandedData[v11];
                    var nIdx = index2 + 1;
                    pExpandedData[nIdx] = pExpandedData[v11 + 1];
                    index2 = nIdx + 1;
                    var v13 = v11 + 2;

                    do
                    {
                        pExpandedData[index2++] = pExpandedData[v13++];
                        --temp;
                    } while (temp > 0);
                }
                else
                {
                    var v9 = len + 1;
                    if (index2 + v9 > iNewLeng)
                    {
                        Console.WriteLine("ERROR 7");
                        return;
                    }

                    do
                    {
                        pExpandedData[index2++] = pCompressedData[index++];
                        --v9;
                    } while (v9 > 0);
                }
            } while ((index2 < iNewLeng) && (index < iLeng));
        }

        public static byte[] DeCryptRev(byte[] data, int length)
        {
            var mStream = new MemoryStream(data);
            var biRead = new BinaryReader(mStream);
            biRead.ReadUInt16();
            int iLength = biRead.ReadUInt16();
            int iCheckSum = biRead.ReadUInt16();
            int iDecode = biRead.ReadByte();
            biRead.ReadBytes(7); // next 8byte

            int b1 = biRead.ReadByte();

            var previousChar = 0;
            var al = 0;
            var cl = 0;
            var index = 0;
            var decryptCharacter = 0;

            var decryptBytes = new byte[iLength + 6];
            var mStreamWrite = new MemoryStream();
            mStreamWrite.WriteByte(0xaa);
            mStreamWrite.WriteByte(0x55);
            mStreamWrite.WriteByte((byte) ((iLength - 10) & 0xFF));
            mStreamWrite.WriteByte((byte) ((iLength - 10) >> 8));
            // Bắt đầu giải mã
            for (var j = 0; j < iLength - 10; j++)
            {
                al = previousChar;
                index = j & 0x1F; // đảm bảo không dài quá 32 kí tự
                cl = encryptTable[index] ^ al;
                cl = cl ^ iDecode;
                cl = cl << 8;
                var code = CompareHash.compareHashTable[cl];
                while (code != b1)
                {
                    cl++;
                    decryptCharacter++;
                    code = CompareHash.compareHashTable[cl];
                }

                // Tìm được kí tự giải mã
                previousChar = decryptCharacter;
                //decryptBytes[j + 4] = (byte)decryptCharacter;
                mStreamWrite.WriteByte((byte) decryptCharacter);
                //encrypt += string.Format(" {0:X2}", decryptCharacter);
                // Debug.Write(string.Format(" {0:X2}", decryptCharacter));
                decryptCharacter = 0;
                b1 = biRead.ReadByte();
            }
            biRead.Dispose();
            mStream.Dispose();
            mStreamWrite.WriteByte(0x55);
            mStreamWrite.WriteByte(0xAA);
            Debug.WriteLine(BitConverter.ToString(mStreamWrite.ToArray()).Replace("-", "") + Environment.NewLine);
            return mStreamWrite.ToArray();
        }

        public static class Config
        {
            public static int BeginSession = 4;
        }
    }
}