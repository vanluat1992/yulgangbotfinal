﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: ByteExtension.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;

#endregion

namespace YulgangBotFinal.Network.Utils
{
    public static class ByteExtension
    {
        public static int FindBeginYulgang(this byte[] oBytes, int start = 0)
        {
            try
            {
                return oBytes.SearchBytes(0xAA, 0x55,start);
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public static int FindEndYulgang(this byte[] oBytes, int start = 0)
        {
            try
            {
                return oBytes.SearchBytes(0x55, 0xAA, start);
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public static int SearchBytes(this byte[] oBytes, byte bOne, byte bTwo, int start = 0)
        {
            try
            {
                int i;
                for (i = start; i < oBytes.Length - 1; i++)
                    if ((oBytes[i] == bOne) && (oBytes[i + 1] == bTwo))
                        return i;
                return -1;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}