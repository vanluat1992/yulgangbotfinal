﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: Opcode.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;

#endregion

namespace YulgangBotFinal.Network.Utils
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class OpcodeAttribute : Attribute
    {
        public OpcodeAttribute(int opcode)
        {
            Opcode = opcode;
        }

        public int Opcode { get; private set; }
    }
}