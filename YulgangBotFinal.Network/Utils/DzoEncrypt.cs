﻿#region header

// **********************************************************************
// SOLUTION: YulgangBotFinal
// PROJECT: YulgangBotFinal.Network
// TIME CREATE : 11:39 PM 15/09/2016
// FILENAME: DzoEncrypt.cs
// AUTHOR: Văn Luật (vanluat1992@gmail.com)
// -----------------------------------
// Copyrights 2016  - All Rights Reserved.
// **********************************************************************

#endregion

#region

using System;
using System.Diagnostics;

#endregion

namespace YulgangBotFinal.Network.Utils
{
    public class DzoEncrypt
    {
        public static uint CreateKey(ushort a1)
        {
            var t0 = a1 > short.MaxValue ? a1 | 0xFFFF0000 : a1;
            var t1 = t0 ^ ((t0 ^ 0x3D0000) >> 16);
            var t2 = 9*t1 ^ (9*t1 >> 4);
            return (0x73C2EB2D*t2) ^ (0x73C2EB2D*t2 >> 15);
        }

        public static void Descrypt(byte[] data, uint key)
        {
            var tmp = data.Length >> 2;
            var abc = BitConverter.GetBytes(key);
            var id = 0;
            do
            {
                //var abc = bRead.ReadUInt32() ^ key;
                //.Write(abc.ToString("X4"));
                //var aaa = BitConverter.GetBytes(abc);
                data[id] ^= abc[0];
                data[id + 1] ^= abc[1];
                data[id + 2] ^= abc[2];
                data[id + 3] ^= abc[3];
                id += 4;
                --tmp;
            } while (tmp != 0);
            var conlai = data.Length - 4*(data.Length >> 2);
            //Console.WriteLine($"CONLAI :{conlai}");
            switch (conlai)
            {
                case 1:
                    data[id] ^= abc[0];
                    break;
                case 2:
                    data[id] ^= abc[0];
                    data[id + 1] ^= abc[1];
                    break;
                case 3:
                    data[id] ^= abc[0];
                    data[id + 1] ^= abc[1];
                    data[id + 2] ^= abc[2];
                    break;
            }
            //Debug.Write("\r\n");
            //Debug.WriteLine(BitConverter.ToString(data));
        }

        public static void Encrypt(byte[] data, uint key)
        {
            var abc = BitConverter.GetBytes(key);
            var id = 0;
            var conlai = data.Length;
            if (data.Length >= 4)
            {
                var tmp = data.Length >> 2;
                conlai = data.Length - 4*(data.Length >> 2);
                do
                {
                    //var abc = bRead.ReadUInt32() ^ key;
                    //.Write(abc.ToString("X4"));
                    //var aaa = BitConverter.GetBytes(abc);
                    data[id] ^= abc[0];
                    data[id + 1] ^= abc[1];
                    data[id + 2] ^= abc[2];
                    data[id + 3] ^= abc[3];
                    id += 4;
                    --tmp;
                } while (tmp != 0);
            }
            //Console.WriteLine($"CONLAI :{conlai}");
            switch (conlai)
            {
                case 1:
                    data[id] ^= abc[0];
                    break;
                case 2:
                    data[id] ^= abc[0];
                    data[id + 1] ^= abc[1];
                    break;
                case 3:
                    data[id] ^= abc[0];
                    data[id + 1] ^= abc[1];
                    data[id + 2] ^= abc[2];
                    break;
            }
        }
    }
}