﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.ConsoleApp
// FileName : Program.cs
// Time Create : 7:50 AM 16/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Log;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Define;
using YulgangBotFinal.EventCore.Procedure;
using YulgangBotFinal.EventCore.Procedure.Units;
using YulgangBotFinal.EventCore.Scope;
using YulgangBotFinal.EventCore.Unit;
using YulgangBotFinal.EventCore.Unit.Auth;
using YulgangBotFinal.EventCore.Unit.Gs;
using YulgangBotFinal.EventCore.Unit.Gs.Repeat;
using YulgangBotFinal.EventCore.Unit.Gs.Train;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Resource;
using YulgangBotFinal.Resource.Ybq;

#endregion

namespace YulgangBotFinal.ConsoleApp
{
    public class AccountInfo
    {
        public AccountInfo(string username, string pass, string pass2)
        {
            Username = username;
            Pass = pass;
            Pass2 = pass2;
        }

        public string Username { get; set; }
        public string Pass { get; set; }
        public string Pass2 { get; set; }
        public string GsIp { get; set; } = "210.211.109.130";
        public int GsPort { get; set; } = 15001;
    }

    public static class Program
    {
        private static RunToPoint _runToPoint;

        private static readonly IList<AccountInfo> _accounts = new List<AccountInfo>
        {
            new AccountInfo("tinh123", "tamdaica1", "28091994"),
            new AccountInfo("liemhd", "@lananh123", "999999oO!"),
            new AccountInfo("hmt1991", "123456", "999999"),
            new AccountInfo("hmt1993", "123456", "999999"),
            new AccountInfo("hmt1994", "123456", "999999"),
            new AccountInfo("hmt1995", "123456", "999999"),// {GsIp = "210.211.109.79"},
            new AccountInfo("hmt1996", "123456", "999999"),// {GsIp = "210.211.109.79"},
            new AccountInfo("hmt1997", "123456", "999999"),// {GsIp = "210.211.109.79"},
            new AccountInfo("hmt1998", "123456", "999999"),// {GsIp = "210.211.109.79"}
        };

        /// <summary>
        /// 
        /// </summary>
        [STAThread]
        public static void Main()
        {
            var procedure = new GameProcedure();
            procedure.Init();
            procedure.SetProc(new LoginProcedure());
            procedure.MainLoop();
            procedure.Release();
            return;
            //var fMap = new FMap();
            var log = new LogManager();
            log.LogLevel = LogType.Dum;
            var ybi =
                new YBiHandle
                    <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill,
                        YulgangYbiGameSpell>(14);
            ybi.Load("Resource\\Ybi.cfg");
            var ybq = YbqUtils.ReadQuest("Resource\\Ybq.cfg");
            var wr = new Wrap();
            XmlSerializer xs = new XmlSerializer(typeof(Wrap));
            using(var f= File.OpenText("Resource\\map.xl"))
            using (StringReader stream = new StringReader(f.ReadToEnd()))
            {
                wr = (Wrap) xs.Deserialize(stream);
            }
            var cfg = new YbConfig(ybi, ybq, log,wr);
            log.InstallAttackLog(new ConsoleLog());
            log.InstallAttackLog(new LogFiles());
            var index =2;//int.Parse(Console.ReadLine())-1;

            //for (var i= index; i<_accounts.Count;i++)
            {
                var info = _accounts[index];
                Task.Factory.StartNew(() =>
                {
                    LoginServerType login = LoginServerType.Dzo;
                    IProcScope scope = new BaseScope(log);
                    //fMap.SetMapManage(scope.Main.MapCaculartor);
                    if (login == LoginServerType.Korea)
                    {
                        scope.Main = new MainDefine
                        {
                            Username = info.Username,
                            Pass = info.Pass,
                            Pass2 = info.Pass2,
                            AuthIp = "119.205.224.140",// "121.52.200.68",
                            AuthPort = 16044,
                            GsIp = info.GsIp,
                            GsPort = info.GsPort
                        };
                        scope.RegisterSequenceProc(new KoreaAccountLogin(cfg)
                        {
                            Config = new KoreaAccountLoginConfig() { Proxy = "211.110.165.238:3128" }
                        });
                    }
                    if (login == LoginServerType.Dzo)
                    {
                        scope.Main = new MainDefine
                        {
                            Username = info.Username,
                            Pass = info.Pass,
                            Pass2 = info.Pass2,
                            AuthIp = "121.52.200.68",
                            AuthPort = 16044,
                            GsIp = info.GsIp,
                            GsPort = info.GsPort
                        };
                        scope.RegisterSequenceProc(new AccountLogin(cfg));
                    }
                    scope.RegisterSequenceProc(new GameLogin(cfg) { Config = new GameLoginConfig() {LoginType = login } });
                    scope.RegisterSequenceProc(new Pwd2Enter(cfg) { Config = new Pwd2Config() {LoginType = login } });
                    scope.RegisterSequenceProc(new GetServerList(cfg) { Config = new GetServerConfig() {LoginType = login } });
                    scope.RegisterSequenceProc(new EnterChannel(cfg) { Config = new EnterChannelConfig { IdChanel = 2, IdServer =1, LoginType = login } });
                    scope.RegisterSequenceProc(new Welcome(cfg) { Config = new WelcomeConfig() { LoginType = login } });
                    scope.RegisterSequenceProc(new GetListChar(cfg));
                    var enterW = new EnterWorld(cfg);
                    enterW.Output.OnValidMap += () =>
                    {
                        //fMap.SetMapManage(scope.Main.MapCaculartor); 
                        
                    };
                    scope.RegisterSequenceProc(enterW);
                    scope.RegisterSequenceProc(new IdleBot(cfg));
                    _runToPoint = new RunToPoint(cfg);
                   /* fMap.OnDbClickInMap += (p) =>
                    {
                        _runToPoint.Input.DestionationPoint.X = p.X;
                        _runToPoint.Input.DestionationPoint.Y = p.Y;
                    };*/
                    //scope.RegisterSequenceProc(_runToPoint);

                    var target = new TargetMonster(cfg);
                    //target.Output.TargetNpc += fMap.TargetNpc;
                    //target.Output.OnUntargetNpc += fMap.UnTargetNpc;
                    scope.RegisterSequenceProc(target);
                    //scope.RegisterSequenceProc(new BuySellItem(log, ybi));
                    scope.RegisterSequenceProc(new UsingSupportSpell(cfg));
                    scope.RegisterSequenceProc(new PickItem(cfg));
                    //scope.RegisterSequenceProc(new AttackMob(cfg));
                    //scope.RegisterSequenceProc(new PlayerQuestAction(cfg));



                    scope.RegisterRepeatProc(new PlayerHpMpMonitor(cfg));

                    var monster = new MonsterMonitor(cfg);
                    //monster.Output.OnAddNpc += fMap.AddNpc;
                    //monster.Output.OnRemoveNpc += fMap.RemoveNpc;
                    //monster.Output.OnUpdateNpc += fMap.UpdateNpc;
                    scope.RegisterRepeatProc(monster);


                    var pMonitor = new PlayerMonitor(cfg);
                    //pMonitor.Output.UpdateMainPlayer += fMap.UpdateMainPlayer;
                    //pMonitor.Output.OnAddPlayer += fMap.AddPlayer;
                    //pMonitor.Output.OnRemovePlayer += fMap.RemoverPlayer;
                    //pMonitor.Output.OnUpdatePlayer += fMap.UpdatePlayer;
                    scope.RegisterRepeatProc(pMonitor);
                    scope.RegisterRepeatProc(new SyncTime(cfg));
                    scope.RegisterRepeatProc(new InventoryMonitor(cfg));
                    scope.RegisterRepeatProc(new ForceLogout(cfg));
                    scope.RegisterRepeatProc(new PartyMonitor(cfg));
                    scope.RegisterRepeatProc(new BuffMonitor(cfg));
                    scope.RegisterRepeatProc(new DropItemMonitor(cfg));
                    scope.RegisterRepeatProc(new PlayerQuestMonitor(cfg));

                    log.Debug("APP", $" Khởi tạo thành công scope quản lý account {info.Username}");
                    scope.Run();
                }).Wait();
            }

           // Application.Run(fMap);
            Console.Read();
            //HandleInput();
        }

        private static void HandleInput()
        {
            do
            {
                var cmd = Console.ReadLine();
                var sp = cmd?.Split('|') ?? new[] {""};
                switch (sp[0].ToUpper())
                {
                    case "SETPOINT":
                        if (sp.Length > 3)
                        {
                            var location = new YulgangLocation(float.Parse(sp[1]), float.Parse(sp[2]), int.Parse(sp[3]));

                            _runToPoint.Input.DestionationPoint = location;
                            Console.WriteLine("OK");
                        }
                        else
                        {
                            Console.WriteLine("Command invalid");
                        }
                        break;
                }
                Thread.Sleep(10);
            } while (true);
        }

        private static void Scope_OnComplete(IProcScope obj)
        {
        }
    }
}