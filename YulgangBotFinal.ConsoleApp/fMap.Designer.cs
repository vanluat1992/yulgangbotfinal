﻿namespace YulgangBotFinal.ConsoleApp
{
    partial class FMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mapGui1 = new YulgangBotFinal.UserControl.Maps.MapGui();
            this.SuspendLayout();
            // 
            // mapGui1
            // 
            this.mapGui1.Caption = "Map";
            this.mapGui1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapGui1.Group = "Hệ thống";
            this.mapGui1.Location = new System.Drawing.Point(0, 0);
            this.mapGui1.Name = "mapGui1";
            this.mapGui1.Size = new System.Drawing.Size(284, 261);
            this.mapGui1.TabIndex = 0;
            // 
            // fMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.mapGui1);
            this.Name = "FMap";
            this.Text = "fMap";
            this.ResumeLayout(false);

        }

        #endregion

        private UserControl.Maps.MapGui mapGui1;
    }
}