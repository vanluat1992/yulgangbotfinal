﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.ConsoleApp
// FileName : FMap.cs
// Time Create : 10:17 AM 28/03/2017
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;
using System.Collections.Concurrent;
using System.Drawing;
using System.Windows.Forms;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.PathFinder;
using YulgangBotFinal.UserControl.Maps;

#endregion

namespace YulgangBotFinal.ConsoleApp
{
    public partial class FMap : Form, IDrawMap
    {
        private readonly ConcurrentDictionary<long, MapLayer> _allLayer = new ConcurrentDictionary<long, MapLayer>();
        private MapCaculator _mng;
        public event Action<IYulgangLocation> OnDbClickInMap;
        public FMap()
        {
            InitializeComponent();
            mapGui1.AddDrawer(this);
            mapGui1.Zoom = 5;
            mapGui1.OnMapMouseClick += MapGui1_OnMapMouseClick;
        }

        private void MapGui1_OnMapMouseClick(PointF obj)
        {
            OnDbClickInMap?.Invoke(new YulgangLocation() {X = obj.X, Y = obj.Y});
        }

        #region Implementation of IDrawMap

        public void Draw(Graphics g)
        {
            if (_mng != null)
            {
                g.DrawImage(_mng.GetBackgroundImage(), _mng.GetClientRect(),
                    _mng.GetImageRect(), GraphicsUnit.Pixel);
                // draw main player
                DrawLayer(g);
            }
        }

        #endregion

        public void UpdateMainPlayer(IYulgangPlayer main)
        {
            _mng.SetCenterPointFromMapPoing(new PointF(main.Location.X, main.Location.Y));
            if (!_allLayer.TryGetValue(main.Id,out MapLayer ly))
            {
                _allLayer.TryAdd(main.Id,
                    new MapLayer() {Type = GameLayerType.MainPlayer, IsTarget = false, Location = main.Location});
            }
            else
            {
                ly.Location = main.Location;
            }
        }

        private void DrawLayer(Graphics g)
        {
            var zoom =1;
            using (var em = _allLayer.GetEnumerator())
            {
                while (em.MoveNext())
                {
                    var layer = em.Current.Value;
                    var point = _mng.PointMapToPointControl(new PointF(layer.Location.X, layer.Location.Y));
                    var tmpsize = 5 * zoom / 2;
                    switch (layer.Type)
                    {
                        case GameLayerType.Player:
                            g.FillRectangle(new SolidBrush(layer.IsTarget ? Color.Red : Color.MediumOrchid),
                                point.X - tmpsize, point.Y - tmpsize, 5 * zoom, 5 * zoom);
                            break;
                        case GameLayerType.Npc:
                            g.FillRectangle(new SolidBrush(layer.IsTarget ? Color.Red : Color.Blue), point.X - tmpsize,
                                point.Y - tmpsize, 5 * zoom, 5 * zoom);
                            break;
                        case GameLayerType.Monster:
                            g.FillRectangle(new SolidBrush(layer.IsTarget ? Color.Red : Color.Green), point.X - tmpsize,
                                point.Y - tmpsize, 5 * zoom, 5 * zoom);
                            break;
                        case GameLayerType.Item:
                            g.FillRectangle(new SolidBrush(layer.IsTarget ? Color.YellowGreen : Color.Yellow),
                                point.X - tmpsize, point.Y - tmpsize, 5 * zoom, 5 * zoom);
                            break;
                        case GameLayerType.Party:
                            g.FillRectangle(new SolidBrush(Color.Fuchsia),
                                point.X - tmpsize, point.Y - tmpsize, 5 * zoom, 5 * zoom);
                            break;
                        case GameLayerType.MainPlayer:
                            g.FillRectangle(new SolidBrush(Color.DarkGoldenrod),
                                point.X - tmpsize, point.Y - tmpsize, 10 * zoom, 10 * zoom);
                            break;
                    }
                }
            }
        }

        public void SetMapManage(MapCaculator mng)
        {
            _mng = mng;
            mapGui1.SetMapManager(mng);
        }

        public void AddNpc(IYulgangNpc obj)
        {
            if (!_allLayer.ContainsKey(obj.Id))
            {
                _allLayer.TryAdd(obj.Id, new MapLayer() {Type = GameLayerType.Npc, Location = obj.Location});
            }

        }

        public void RemoveNpc(IYulgangNpc obj)
        {
            _allLayer.TryRemove(obj.Id, out MapLayer tmp);
        }

        public void UpdateNpc(IYulgangNpc obj)
        {
            if (_allLayer.TryGetValue(obj.Id, out MapLayer tmp))
            {
                tmp.Location = obj.Location;
            }
        }

        public void AddPlayer(IYulgangPlayer obj)
        {
            if (!_allLayer.ContainsKey(obj.Id))
            {
                _allLayer.TryAdd(obj.Id, new MapLayer() {Type = GameLayerType.Player, Location = obj.Location});
            }
        }

        public void RemoverPlayer(IYulgangPlayer obj)
        {
            _allLayer.TryRemove(obj.Id, out MapLayer tmp);
        }

        public void UpdatePlayer(IYulgangPlayer obj)
        {

            if (_allLayer.TryRemove(obj.Id, out MapLayer tmp))
            {
                tmp.Location = obj.Location;
            }
        }

        public void TargetNpc(IYulgangNpc obj)
        {
            if (obj != null && _allLayer.TryGetValue(obj.Id, out MapLayer tmp))
            {
                tmp.IsTarget = true;
            }
        }

        public void UnTargetNpc(IYulgangNpc obj)
        {
            if (_allLayer.TryGetValue(obj.Id, out MapLayer tmp))
            {
                tmp.IsTarget = false;
            }
        }
    }

    public enum GameLayerType
    {
        Player,
        Npc,
        Monster,
        Item,
        Party,
        MainPlayer
    }

    public class MapLayer
    {
        public IYulgangLocation Location { get; set; }
        public GameLayerType Type { get; set; }
        public int Id { get; set; }
        public bool IsTarget { get; set; }
    }
}