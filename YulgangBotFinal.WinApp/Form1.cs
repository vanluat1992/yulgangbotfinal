﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010.Views;
using DevExpress.XtraBars.Docking2010.Views.Widget;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Resource;
using YulgangBotFinal.WinApp.UserControl;

namespace YulgangBotFinal.WinApp
{
    public partial class Form1 : Form
    {
        private readonly ILog _log;

        private readonly
            YBiHandle
                <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill, YulgangYbiGameSpell>
            _ybi;

        private readonly Dictionary<string, PlayerDetail> _allViewPlayer = new Dictionary<string, PlayerDetail>();

        public Form1()
        {
            InitializeComponent();
            // Handling the QueryControl event that will populate all automatically generated Documents
        }

        public Form1(
            YBiHandle
                <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill, YulgangYbiGameSpell>
                ybi, ILog log) : this()
        {
            _ybi = ybi;
            _log = log;

            // đọc config

            // tạo các component view tương ứng

            documentManager1.View.QueryControl += View_QueryControl;
            //this.widgetView1.QueryControl += widgetView1_QueryControl;
            widgetView1.AllowDocumentStateChangeAnimation = DefaultBoolean.True;
            foreach (var account in Program.Accounts)
            {
                var doc = new Document {ControlName = account.Username, Caption = account.Username, Height = 120};
                
                widgetView1.Documents.Add(doc);
                stackGroup1.Items.Add(doc);
                _allViewPlayer.Add(account.Username, new PlayerDetail(account,log,ybi));
            }
            //// 
            //// widgetView1
            //// 
            //this.widgetView1.Documents.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseDocument[] {
            //this.document7,
            //this.document8,
            //this.document9});
            //this.widgetView1.StackGroups.AddRange(new DevExpress.XtraBars.Docking2010.Views.Widget.StackGroup[] {
            //this.stackGroup1});
            //// 
            //// stackGroup1
            //// 
            //this.stackGroup1.Items.AddRange(new DevExpress.XtraBars.Docking2010.Views.Widget.Document[] {
            //this.document7,
            //this.document8,
            //this.document9});
        }

        private void View_QueryControl(object sender, QueryControlEventArgs e)
        {
            PlayerDetail pDetail;
            _allViewPlayer.TryGetValue(e.Document.ControlName, out pDetail);
            e.Control = pDetail;
        }


        // Assigning a required content for each auto generated Document
        private void widgetView1_QueryControl(object sender, QueryControlEventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}