﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace YulgangBotFinal.WinApp.UserControl
{
    [Description("Color Progress Bar")]
    [ToolboxBitmap(typeof (ProgressBar))]
    public class ColorProgressBar : Control
    {
        public enum FillStyles
        {
            Solid,
            Dashed
        }

        private Color _barColor = Color.FromArgb(255, 128, 128);
        private Color _borderColor = Color.Black;

        private FillStyles _FillStyle = FillStyles.Solid;
        private int _maximum = 100;

        private int _minimum;
        private int _step = 10;

        private string _text = "";
        //
        // set default values
        //
        private int _value;

        public ColorProgressBar()
        {
            Size = new Size(150, 15);
            SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.ResizeRedraw |
                ControlStyles.DoubleBuffer,
                true
                );
        }

        [Description("ColorProgressBar color")]
        [Category("ColorProgressBar")]
        public Color BarColor
        {
            get { return _barColor; }
            set
            {
                _barColor = value;
                Invalidate();
            }
        }

        [Description("ColorProgressBar fill style")]
        [Category("ColorProgressBar")]
        public FillStyles FillStyle
        {
            get { return _FillStyle; }
            set
            {
                _FillStyle = value;
                Invalidate();
            }
        }

        [Description("The current value for the ColorProgressBar, " +
                     "in the range specified by the Minimum and Maximum properties.")]
        [Category("ColorProgressBar")]
        // the rest of the Properties windows must be updated when this peroperty is changed.
        [RefreshProperties(RefreshProperties.All)]
        public int Value
        {
            get { return _value; }
            set
            {
                if (value < _minimum)
                {
                    throw new ArgumentException("'" + value + "' is not a valid value for 'Value'.\n" +
                                                "'Value' must be between 'Minimum' and 'Maximum'.");
                }

                if (value > _maximum)
                {
                    throw new ArgumentException("'" + value + "' is not a valid value for 'Value'.\n" +
                                                "'Value' must be between 'Minimum' and 'Maximum'.");
                }

                _value = value;
                Invalidate();
            }
        }

        [Description("The lower bound of the range this ColorProgressbar is working with.")]
        [Category("ColorProgressBar")]
        [RefreshProperties(RefreshProperties.All)]
        public int Minimum
        {
            get { return _minimum; }
            set
            {
                _minimum = value;

                if (_minimum > _maximum)
                    _maximum = _minimum;
                if (_minimum > _value)
                    _value = _minimum;

                Invalidate();
            }
        }

        [Description("The uppper bound of the range this ColorProgressbar is working with.")]
        [Category("ColorProgressBar")]
        [RefreshProperties(RefreshProperties.All)]
        public int Maximum
        {
            get { return _maximum; }
            set
            {
                _maximum = value;

                if (_maximum < _value)
                    _value = _maximum;
                if (_maximum < _minimum)
                    _minimum = _maximum;

                Invalidate();
            }
        }

        [Description("The amount to jump the current value of the control by when the Step() method is called.")]
        [Category("ColorProgressBar")]
        public int Step
        {
            get { return _step; }
            set
            {
                _step = value;
                Invalidate();
            }
        }

        [Description("The border color of ColorProgressBar")]
        [Category("ColorProgressBar")]
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;
                Invalidate();
            }
        }

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        public override string Text
        {
            set
            {
                _text = value;
                Invalidate();
            }
            get { return _text; }
        }

        //
        // Call the PerformStep() method to increase the value displayed by the amount set in the Step property
        //
        public void PerformStep()
        {
            if (_value < _maximum)
                _value += _step;
            else
                _value = _maximum;

            Invalidate();
        }

        //
        // Call the PerformStepBack() method to decrease the value displayed by the amount set in the Step property
        //
        public void PerformStepBack()
        {
            if (_value > _minimum)
                _value -= _step;
            else
                _value = _minimum;

            Invalidate();
        }

        //
        // Call the Increment() method to increase the value displayed by an integer you specify
        //
        public void Increment(int value)
        {
            if (_value < _maximum)
                _value += value;
            else
                _value = _maximum;

            Invalidate();
        }

        //
        // Call the Decrement() method to decrease the value displayed by an integer you specify
        //
        public void Decrement(int value)
        {
            if (_value > _minimum)
                _value -= value;
            else
                _value = _minimum;

            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //
            // Calculate matching colors
            //
            var darkColor = ControlPaint.Dark(_barColor);
            var bgColor = BackColor; //ControlPaint.Dark(_barColor);

            //
            // Fill background
            //
            var bgBrush = new SolidBrush(bgColor);
            e.Graphics.FillRectangle(bgBrush, ClientRectangle);
            bgBrush.Dispose();

            //
            // Check for value
            //
            if (_maximum == _minimum || _value == 0)
            {
                // Draw border only and exit;
                DrawBorder(e.Graphics);
                DrawText(e.Graphics);
                return;
            }

            //
            // The following is the width of the bar. This will vary with each value.
            //
            var fillWidth = Width*_value/(_maximum - _minimum);

            //
            // GDI+ doesn't like rectangles 0px wide or high
            //
            if (fillWidth == 0)
            {
                // Draw border only and exti;
                DrawBorder(e.Graphics);
                DrawText(e.Graphics);
                return;
            }

            //
            // Rectangles for upper and lower half of bar
            //
            var topRect = new Rectangle(0, 0, fillWidth, Height/2);
            var buttomRect = new Rectangle(0, Height/2, fillWidth, Height/2);

            //
            // The gradient brush
            //
            LinearGradientBrush brush;

            //
            // Paint upper half
            //
            brush = new LinearGradientBrush(new Point(0, 0),
                new Point(0, Height/2), darkColor, _barColor);
            e.Graphics.FillRectangle(brush, topRect);
            brush.Dispose();

            //
            // Paint lower half
            // (this.Height/2 - 1 because there would be a dark line in the middle of the bar)
            //
            brush = new LinearGradientBrush(new Point(0, Height/2 - 1),
                new Point(0, Height), _barColor, darkColor);
            e.Graphics.FillRectangle(brush, buttomRect);
            brush.Dispose();

            //
            // Calculate separator's setting
            //
            var sepWidth = (int) (Height*.67);
            var sepCount = fillWidth/sepWidth;
            var sepColor = ControlPaint.LightLight(_barColor);

            //
            // Paint separators
            //
            switch (_FillStyle)
            {
                case FillStyles.Dashed:
                    // Draw each separator line
                    for (var i = 1; i <= sepCount; i++)
                    {
                        e.Graphics.DrawLine(new Pen(sepColor, 1),
                            sepWidth*i, 0, sepWidth*i, Height);
                    }
                    break;

                case FillStyles.Solid:
                    // Draw nothing
                    break;

                default:
                    break;
            }

            //
            // Draw border and exit
            //
            DrawBorder(e.Graphics);

            //
            // Draw text
            //
            DrawText(e.Graphics);
        }

        //
        // Draw border
        //
        private void DrawBorder(Graphics g)
        {
            var borderRect = new Rectangle(0, 0,
                ClientRectangle.Width - 1, ClientRectangle.Height - 1);
            g.DrawRectangle(new Pen(_borderColor, 1), borderRect);
        }

        private void DrawText(Graphics g)
        {
            if (!string.IsNullOrEmpty(_text))
            {
                /*Caculator point x */
                var textSize = TextRenderer.MeasureText(_text, Font);
                g.DrawString(Text, Font, new SolidBrush(ForeColor),
                    new PointF((Size.Width - textSize.Width)/2F, (Size.Height - textSize.Height)/2F));
            }
        }
    }
}