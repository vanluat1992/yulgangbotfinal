﻿namespace YulgangBotFinal.WinApp.UserControl
{
    partial class PlayerDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btRun = new DevExpress.XtraEditors.SimpleButton();
            this.clpPlatyerHp = new YulgangBotFinal.WinApp.UserControl.ColorProgressBar();
            this.clpPlayerMp = new YulgangBotFinal.WinApp.UserControl.ColorProgressBar();
            this.clpNpcHp = new YulgangBotFinal.WinApp.UserControl.ColorProgressBar();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lbPlayerLevel = new DevExpress.XtraEditors.LabelControl();
            this.lbUsername = new DevExpress.XtraEditors.LabelControl();
            this.lbPlayerJob = new DevExpress.XtraEditors.LabelControl();
            this.lbNpcTargetName = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.clbPlayerExp = new YulgangBotFinal.WinApp.UserControl.ColorProgressBar();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.lbPlayerName = new DevExpress.XtraEditors.LabelControl();
            this.lbPlayerMoney = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(4, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(58, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Username : ";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(99, 25);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(35, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Level : ";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(240, 7);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(27, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Job : ";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(2, 75);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(42, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Target : ";
            // 
            // btRun
            // 
            this.btRun.Location = new System.Drawing.Point(471, 46);
            this.btRun.Name = "btRun";
            this.btRun.Size = new System.Drawing.Size(75, 43);
            this.btRun.TabIndex = 8;
            this.btRun.Text = "Run";
            this.btRun.Click += new System.EventHandler(this.btRun_Click);
            // 
            // clpPlatyerHp
            // 
            this.clpPlatyerHp.BarColor = System.Drawing.Color.Red;
            this.clpPlatyerHp.BorderColor = System.Drawing.Color.Black;
            this.clpPlatyerHp.FillStyle = YulgangBotFinal.WinApp.UserControl.ColorProgressBar.FillStyles.Solid;
            this.clpPlatyerHp.Location = new System.Drawing.Point(26, 46);
            this.clpPlatyerHp.Maximum = 100;
            this.clpPlatyerHp.Minimum = 0;
            this.clpPlatyerHp.Name = "clpPlatyerHp";
            this.clpPlatyerHp.Size = new System.Drawing.Size(178, 15);
            this.clpPlatyerHp.Step = 1;
            this.clpPlatyerHp.TabIndex = 9;
            this.clpPlatyerHp.Text = "HP";
            this.clpPlatyerHp.Value = 80;
            // 
            // clpPlayerMp
            // 
            this.clpPlayerMp.BarColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.clpPlayerMp.BorderColor = System.Drawing.Color.Black;
            this.clpPlayerMp.FillStyle = YulgangBotFinal.WinApp.UserControl.ColorProgressBar.FillStyles.Solid;
            this.clpPlayerMp.Location = new System.Drawing.Point(240, 46);
            this.clpPlayerMp.Maximum = 100;
            this.clpPlayerMp.Minimum = 0;
            this.clpPlayerMp.Name = "clpPlayerMp";
            this.clpPlayerMp.Size = new System.Drawing.Size(196, 15);
            this.clpPlayerMp.Step = 40;
            this.clpPlayerMp.TabIndex = 10;
            this.clpPlayerMp.Text = "MP";
            this.clpPlayerMp.Value = 80;
            // 
            // clpNpcHp
            // 
            this.clpNpcHp.BarColor = System.Drawing.Color.Green;
            this.clpNpcHp.BorderColor = System.Drawing.Color.Black;
            this.clpNpcHp.FillStyle = YulgangBotFinal.WinApp.UserControl.ColorProgressBar.FillStyles.Solid;
            this.clpNpcHp.Location = new System.Drawing.Point(239, 73);
            this.clpNpcHp.Maximum = 100;
            this.clpNpcHp.Minimum = 0;
            this.clpNpcHp.Name = "clpNpcHp";
            this.clpNpcHp.Size = new System.Drawing.Size(226, 15);
            this.clpNpcHp.Step = 40;
            this.clpNpcHp.TabIndex = 11;
            this.clpNpcHp.Text = "MP";
            this.clpNpcHp.Value = 80;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(4, 47);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(20, 13);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "HP: ";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(210, 47);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(21, 13);
            this.labelControl7.TabIndex = 13;
            this.labelControl7.Text = "MP: ";
            // 
            // lbPlayerLevel
            // 
            this.lbPlayerLevel.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPlayerLevel.Location = new System.Drawing.Point(140, 22);
            this.lbPlayerLevel.Name = "lbPlayerLevel";
            this.lbPlayerLevel.Size = new System.Drawing.Size(20, 19);
            this.lbPlayerLevel.TabIndex = 14;
            this.lbPlayerLevel.Text = "12";
            // 
            // lbUsername
            // 
            this.lbUsername.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUsername.Location = new System.Drawing.Point(69, 4);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(50, 16);
            this.lbUsername.TabIndex = 15;
            this.lbUsername.Text = "hmt1991";
            // 
            // lbPlayerJob
            // 
            this.lbPlayerJob.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPlayerJob.Appearance.ForeColor = System.Drawing.Color.Maroon;
            this.lbPlayerJob.Location = new System.Drawing.Point(279, 4);
            this.lbPlayerJob.Name = "lbPlayerJob";
            this.lbPlayerJob.Size = new System.Drawing.Size(79, 16);
            this.lbPlayerJob.TabIndex = 16;
            this.lbPlayerJob.Text = "Hàn bảo quân";
            // 
            // lbNpcTargetName
            // 
            this.lbNpcTargetName.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNpcTargetName.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lbNpcTargetName.Location = new System.Drawing.Point(50, 73);
            this.lbNpcTargetName.Name = "lbNpcTargetName";
            this.lbNpcTargetName.Size = new System.Drawing.Size(104, 16);
            this.lbNpcTargetName.TabIndex = 18;
            this.lbNpcTargetName.Text = "Huyết Lang Vương";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(5, 25);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(45, 13);
            this.labelControl8.TabIndex = 19;
            this.labelControl8.Text = "Thế lực : ";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(140, 7);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(28, 13);
            this.labelControl10.TabIndex = 20;
            this.labelControl10.Text = "Tên : ";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(379, 7);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(30, 13);
            this.labelControl11.TabIndex = 21;
            this.labelControl11.Text = "Tiền : ";
            // 
            // clbPlayerExp
            // 
            this.clbPlayerExp.BarColor = System.Drawing.Color.Aqua;
            this.clbPlayerExp.BorderColor = System.Drawing.Color.Black;
            this.clbPlayerExp.FillStyle = YulgangBotFinal.WinApp.UserControl.ColorProgressBar.FillStyles.Solid;
            this.clbPlayerExp.Location = new System.Drawing.Point(216, 23);
            this.clbPlayerExp.Maximum = 100;
            this.clbPlayerExp.Minimum = 0;
            this.clbPlayerExp.Name = "clbPlayerExp";
            this.clbPlayerExp.Size = new System.Drawing.Size(330, 15);
            this.clbPlayerExp.Step = 10;
            this.clbPlayerExp.TabIndex = 22;
            this.clbPlayerExp.Text = "EXP";
            this.clbPlayerExp.Value = 10;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(186, 23);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(28, 13);
            this.labelControl12.TabIndex = 23;
            this.labelControl12.Text = "Exp : ";
            // 
            // lbPlayerName
            // 
            this.lbPlayerName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPlayerName.Location = new System.Drawing.Point(174, 6);
            this.lbPlayerName.Name = "lbPlayerName";
            this.lbPlayerName.Size = new System.Drawing.Size(30, 13);
            this.lbPlayerName.TabIndex = 24;
            this.lbPlayerName.Text = "hmt1";
            // 
            // lbPlayerMoney
            // 
            this.lbPlayerMoney.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPlayerMoney.Appearance.ForeColor = System.Drawing.Color.Chocolate;
            this.lbPlayerMoney.Location = new System.Drawing.Point(415, 6);
            this.lbPlayerMoney.Name = "lbPlayerMoney";
            this.lbPlayerMoney.Size = new System.Drawing.Size(120, 14);
            this.lbPlayerMoney.TabIndex = 25;
            this.lbPlayerMoney.Text = "100.000.000 lượng";
            this.lbPlayerMoney.UseMnemonic = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(552, 7);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(41, 81);
            this.simpleButton1.TabIndex = 26;
            this.simpleButton1.Text = ">>";
            // 
            // PlayerDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.lbPlayerMoney);
            this.Controls.Add(this.lbPlayerName);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.clbPlayerExp);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.lbNpcTargetName);
            this.Controls.Add(this.lbPlayerJob);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.lbPlayerLevel);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.clpNpcHp);
            this.Controls.Add(this.clpPlayerMp);
            this.Controls.Add(this.clpPlatyerHp);
            this.Controls.Add(this.btRun);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "PlayerDetail";
            this.Size = new System.Drawing.Size(605, 100);
            this.Load += new System.EventHandler(this.PlayerDetail_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btRun;
        private ColorProgressBar clpPlatyerHp;
        private ColorProgressBar clpPlayerMp;
        private ColorProgressBar clpNpcHp;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl lbPlayerLevel;
        private DevExpress.XtraEditors.LabelControl lbUsername;
        private DevExpress.XtraEditors.LabelControl lbPlayerJob;
        private DevExpress.XtraEditors.LabelControl lbNpcTargetName;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private ColorProgressBar clbPlayerExp;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl lbPlayerName;
        private DevExpress.XtraEditors.LabelControl lbPlayerMoney;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
