﻿using System;
using System.Threading;
using System.Threading.Tasks;
using YulgangBotFinal.Core;
using YulgangBotFinal.Core.Define;
using YulgangBotFinal.EventCore.Scope;
using YulgangBotFinal.EventCore.Unit.Auth;
using YulgangBotFinal.EventCore.Unit.Gs;
using YulgangBotFinal.EventCore.Unit.Gs.Repeat;
using YulgangBotFinal.EventCore.Unit.Gs.Train;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;
using YulgangBotFinal.Resource;

namespace YulgangBotFinal.WinApp.UserControl
{
    public partial class PlayerDetail : System.Windows.Forms.UserControl
    {
        private readonly AccountInfo _acc;
        private readonly ILog _log;

        private readonly
            YBiHandle
                <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill, YulgangYbiGameSpell>
            _ybi;

        private readonly SynchronizationContext _context; //=new WindowsFormsSynchronizationContext();

        public PlayerDetail()
        {
            InitializeComponent();
            _context = SynchronizationContext.Current;
        }

        public PlayerDetail(AccountInfo acc, ILog log,
            YBiHandle
                <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill, YulgangYbiGameSpell>
                ybi) : this()
        {
            _acc = acc;
            _log = log;
            _ybi = ybi;
        }

        private void PlayerDetail_Load(object sender, EventArgs e)
        {
        }

        private void btRun_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                _context.Post(m => { lbUsername.Text = _acc.Username; }, null);
                IProcScope scope = new BaseScope(_log);
                scope.Main = new MainDefine
                {
                    Username = _acc.Username,
                    Pass = _acc.Pass,
                    Pass2 = _acc.Pass2,
                    AuthIp = "121.52.200.68",
                    AuthPort = 16044,
                    GsIp = _acc.GsIp,
                    GsPort = _acc.GsPort
                };
                scope.RegisterSequenceProc(new AccountLogin(_log, _ybi));
                scope.RegisterSequenceProc(new GameLogin(_log, _ybi));
                scope.RegisterSequenceProc(new Pwd2Enter(_log, _ybi));
                scope.RegisterSequenceProc(new GetServerList(_log, _ybi));
                scope.RegisterSequenceProc(new EnterChannel(_log, _ybi));
                scope.RegisterSequenceProc(new Welcome(_log, _ybi));
                var sGetListChar = new GetListChar(_log, _ybi);
                sGetListChar.Output.SelectPlayer += PlayerDetail_SelectPlayer;
                scope.RegisterSequenceProc(sGetListChar);
                scope.RegisterSequenceProc(new EnterWorld(_log, _ybi));
                scope.RegisterSequenceProc(new IdleBot(_log, _ybi));
                scope.RegisterSequenceProc(new RunToPoint(_log, _ybi));
                scope.RegisterSequenceProc(new BuySellItem(_log, _ybi));
                var sTargetMonster = new TargetMonster(_log, _ybi);
                sTargetMonster.Output.TargetNpc += PlayerDetail_TargetNpc;
                scope.RegisterSequenceProc(sTargetMonster);
                if (_acc.Username != "hmt1996")
                    scope.RegisterSequenceProc(new AttackMob(_log, _ybi));
                scope.RegisterSequenceProc(new PickItem(_log, _ybi));
                scope.RegisterSequenceProc(new UsingSupportSpell(_log, _ybi));





                var rHpMpSp = new PlayerHpMpMonitor(_log, _ybi);
                rHpMpSp.Output.UpdateHpMp += PlayerDetail_UpdateHpMp;
                rHpMpSp.Output.UpdateExp += PlayerDetail_UpdateExp;
                scope.RegisterRepeatProc(rHpMpSp);

                var rMonster = new MonsterMonitor(_log, _ybi);
                rMonster.Output.UpdateTargetHp += PlayerDetail_UpdateTargetHp;
                scope.RegisterRepeatProc(rMonster);

                var rPlayer = new PlayerMonitor(_log, _ybi);
                rPlayer.Output.UpdateMainPlayer += PlayerDetail_UpdateMainPlayer;
                scope.RegisterRepeatProc(rPlayer);
                scope.RegisterRepeatProc(new SyncTime(_log, _ybi));

                var rInventory = new InventoryMonitor(_log, _ybi);
                rInventory.Output.UpdateMoney += PlayerDetail_UpdateMoney;
                scope.RegisterRepeatProc(rInventory);
                scope.RegisterRepeatProc(new ForceLogout(_log, _ybi));
                scope.RegisterRepeatProc(new PartyMonitor(_log, _ybi));
                scope.RegisterRepeatProc(new DropItemMonitor(_log, _ybi));
                scope.RegisterRepeatProc(new BuffMonitor(_log, _ybi));

                _log.Debug("APP", $" Khởi tạo thành công scope quản lý account {_acc.Username}");
                scope.Run();
            });
            btRun.Enabled = false;
            btRun.Text = "Logout";
        }

        private void PlayerDetail_UpdateMoney(int obj)
        {
            _context.Post(m =>
            {
                lbPlayerMoney.Text = $"{obj.ToString("N0")} Lượng";
            }, null);
        }

        private void PlayerDetail_UpdateExp(ScrollInfo p)
        {
            _context.Post(m =>
            {
                clbPlayerExp.Maximum = (int) 100;
                clbPlayerExp.Value = (int)p.Percent;
                clbPlayerExp.Text = $"{p} ({p.Percent}) ";
            }, null);
        }

        private void PlayerDetail_UpdateMainPlayer(IYulgangPlayer p)
        {
            _context.Post(m =>
            {
                lbPlayerLevel.Text = p.Level.ToString();
                lbPlayerJob.Text = GetJob(p.Job);
                //clbPlayerExp.Maximum = (int) p.MaxExpNormal;
                //clbPlayerExp.Value = (int) p.CurrentExpNormal;
                //clbPlayerExp.Text = $"{(float) p.CurrentExpNormal/(float) p.MaxExpNormal*100.0}";
            }, null);
        }

        private string GetJob(YulgangCharaterJobs job)
        {
            switch (job)
            {
                case YulgangCharaterJobs.Blade:
                    return "Đao";
                case YulgangCharaterJobs.Bow:
                    return "Cung";
                case YulgangCharaterJobs.Boxing:
                    return "Quyền sư";
                case YulgangCharaterJobs.Busker:
                    return "Đàn";
                case YulgangCharaterJobs.Healer:
                    return "Đại phu";
                case YulgangCharaterJobs.Ninja:
                    return "Ninja";
                case YulgangCharaterJobs.Spear:
                    return "Thương";
                case YulgangCharaterJobs.SuperBlade:
                    return "Hàn bảo quân";
                case YulgangCharaterJobs.SuperSword:
                    return "Đàm hoa liên";
                case YulgangCharaterJobs.Sword:
                    return "Kiếm";
                case YulgangCharaterJobs.None:
                default:
                    return "Đếu biết";
            }
        }

        private void PlayerDetail_UpdateTargetHp(int obj)
        {
            _context.Post(m =>
            {
                clpNpcHp.Value = obj;
                clpNpcHp.Text = $"{obj}/{clpNpcHp.Maximum}";
            }, null);
        }

        private void PlayerDetail_TargetNpc(IYulgangNpc obj)
        {
            _context.Post(m =>
            {
                if (obj == null)
                {
                    lbNpcTargetName.Text = "Không tìm thấy";
                    clpNpcHp.Value = 0;
                    clpNpcHp.Text = "--";
                }
                else
                {
                    lbNpcTargetName.Text = obj.BasicInfo.Name + $"( {obj.BasicInfo.Level} .lv)";
                    clpNpcHp.Maximum = obj.MaxHp;
                    clpNpcHp.Value = obj.CurrentHp;
                    clpNpcHp.Text = $"{obj.CurrentHp}/{obj.MaxHp}";
                }
            }, null);
        }

        private void PlayerDetail_UpdateHpMp(ScrollInfo hp, ScrollInfo mp)
        {
            _context.Post(m =>
            {
                clpPlatyerHp.Text = hp.ToString();
                clpPlatyerHp.Maximum = hp.Limit;
                clpPlatyerHp.Value = hp.Current;

                clpPlayerMp.Text = mp.ToString();
                clpPlayerMp.Maximum = mp.Limit;
                clpPlayerMp.Value = mp.Current;
            }, null);
        }

        private void PlayerDetail_SelectPlayer(string name)
        {
            _context.Post(m => { lbPlayerName.Text = name; }, null);
        }
    }
}