﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using YulgangBotFinal.Log;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Resource;

namespace YulgangBotFinal.WinApp
{
    public class AccountInfo
    {
        public AccountInfo(string username, string pass, string pass2)
        {
            Username = username;
            Pass = pass;
            Pass2 = pass2;
        }

        public string Username { get; set; }
        public string Pass { get; set; }
        public string Pass2 { get; set; }
        public string GsIp { get; set; } = "210.211.109.130";
        public int GsPort { get; set; } = 15001;
    }

    public static class Program
    {
        public static IList<AccountInfo> Accounts { get; } = new List<AccountInfo>
        {
            new AccountInfo("hmt1991", "123456", "999999"),
            new AccountInfo("hmt1992", "123456", "999999"),
            new AccountInfo("hmt1993", "123456", "999999"),
            new AccountInfo("hmt1994", "123456", "999999"),
            new AccountInfo("hmt1995", "123456", "999999") {GsIp = "210.211.109.79"},
            new AccountInfo("hmt1996", "123456", "999999") {GsIp = "210.211.109.79"},
            new AccountInfo("hmt1997", "123456", "999999") {GsIp = "210.211.109.79"},
            new AccountInfo("hmt1998", "123456", "999999") {GsIp = "210.211.109.79"}
        };
        public static ILog Log { get; private set; }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Log = new LogManager();
            //log.LogLevel = LogType.Debug;
            var ybi =
                new YBiHandle
                    <YulgangYbiGameItem, YulgangYbiGameMonster, YulgangYbiGameMap, YulgangYbiGameSkill,
                        YulgangYbiGameSpell>();
            ybi.Load("Resource\\Ybi.cfg");


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(ybi, Log));
        }
    }
}
