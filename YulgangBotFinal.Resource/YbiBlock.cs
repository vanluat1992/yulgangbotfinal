﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace YulgangBotFinal.Resource
{
    public class YBiBlock
    {
        private int Size { set; get; }

        private int Count { set; get; }

        private Dictionary<long, byte[]> ListBlockByte { set; get; }

        public YBiBlock(int iCount, int iSize)
        {
            Size = iSize;
            Count = iCount;
            ListBlockByte = new Dictionary<long, byte[]>();
        }

        /*
                public void Initialize(BinaryReader biRead)
                {
                    for (var i = 0; i < Count; i++)
                    {
                        Add(biRead.ReadBytes(Size));
                    }
                }
        */

        public void Save(BinaryWriter write, bool isEnc)
        {
            var lv = ListBlockByte.Values.ToList();

            for (var i = 0; i < Count; i++)
            {
                if (i >= lv.Count)
                    write.Write(new byte[Size]);
                else
                {
                    var data = lv[i].ToArray();
                    if (isEnc)
                    {
                        if (Enc(data))
                            write.Write(data);
                    }
                    else
                        write.Write(data);
                }
            }
        }

        public void Initialize(BinaryReader biRead, bool isDec, bool isUseId)
        {
            for (var i = 0; i < Count; i++)
            {
                Add(biRead.ReadBytes(Size), isDec, isUseId);
            }
        }

        /*
                public void Change(long id, byte[] data, bool IsENC)
                {
                    if (listBlockByte.ContainsKey(id))
                        if (IsENC)
                        {
                            if (Enc(data))
                                listBlockByte[id] = data;
                        }
                        else
                        {
                            listBlockByte[id] = data;
                        }
                }
        */

        /*
                private void Add(byte[] byValue)
                {
                    if (listBlockByte.Count < Count)
                    {
                        if (Dec(byValue))
                        {
                            if (!listBlockByte.ContainsKey(BitConverter.ToInt32(byValue, 0)))
                                listBlockByte.Add(BitConverter.ToInt32(byValue, 0), byValue);
                        }
                    }
                }
        */

        private void Add(byte[] byValue, bool isDec, bool isUseId)
        {
            if (ListBlockByte.Count >= Count) return;
            if (isDec)
            {
                if (Dec(byValue))
                {
                    if (isUseId)
                    {
                        if (!ListBlockByte.ContainsKey(BitConverter.ToInt32(byValue, 0)))
                            ListBlockByte.Add(BitConverter.ToInt32(byValue, 0), byValue);
                    }
                    else
                    {
                        ListBlockByte.Add(ListBlockByte.Count, byValue);
                    }
                }
            }
            else
            {
                ListBlockByte.Add(ListBlockByte.Count, byValue);
            }
        }

        public IEnumerable<byte[]> GetDec()
        {
            return ListBlockByte.Values.ToList();
        }

        public Dictionary<long, byte[]> GetDictionarydec()
        {
            return ListBlockByte;
        }

        /*
                public List<byte[]> Getenc()
                {
                    var rValues = new List<byte[]>();
                    for (var i = 0; i < listBlockByte.Count; i++)
                    {
                        var obj = listBlockByte[i].ToArray();
                        if (Enc(obj))
                            rValues.Add(obj);
                    }
                    return null;
                }
        */

        public static bool Dec(byte[] value)
        {
            try
            {
                for (var i = 0; i < value.Length; i += 4)
                {
                    int src = BitConverter.ToInt32(value, i);
                    int num3 = 0;
                    num3 |= MoveBit(src, 0x1a, 0);
                    num3 |= MoveBit(src, 0x1f, 1);
                    num3 |= MoveBit(src, 0x11, 2);
                    num3 |= MoveBit(src, 10, 3);
                    num3 |= MoveBit(src, 30, 4);
                    num3 |= MoveBit(src, 0x10, 5);
                    num3 |= MoveBit(src, 0x18, 6);
                    num3 |= MoveBit(src, 2, 7);
                    num3 |= MoveBit(src, 0x1d, 8);
                    num3 |= MoveBit(src, 8, 9);
                    num3 |= MoveBit(src, 20, 10);
                    num3 |= MoveBit(src, 15, 11);
                    num3 |= MoveBit(src, 0x1c, 12);
                    num3 |= MoveBit(src, 11, 13);
                    num3 |= MoveBit(src, 13, 14);
                    num3 |= MoveBit(src, 4, 15);
                    num3 |= MoveBit(src, 0x13, 0x10);
                    num3 |= MoveBit(src, 0x17, 0x11);
                    num3 |= MoveBit(src, 0, 0x12);
                    num3 |= MoveBit(src, 12, 0x13);
                    num3 |= MoveBit(src, 14, 20);
                    num3 |= MoveBit(src, 0x1b, 0x15);
                    num3 |= MoveBit(src, 6, 0x16);
                    num3 |= MoveBit(src, 0x12, 0x17);
                    num3 |= MoveBit(src, 0x15, 0x18);
                    num3 |= MoveBit(src, 3, 0x19);
                    num3 |= MoveBit(src, 9, 0x1a);
                    num3 |= MoveBit(src, 7, 0x1b);
                    num3 |= MoveBit(src, 0x16, 0x1c);
                    num3 |= MoveBit(src, 1, 0x1d);
                    num3 |= MoveBit(src, 0x19, 30);
                    num3 |= MoveBit(src, 5, 0x1f);
                    value[i] = (byte)(num3 & 0xff);
                    value[i + 1] = (byte)((num3 >> 8) & 0xff);
                    value[i + 2] = (byte)((num3 >> 0x10) & 0xff);
                    value[i + 3] = (byte)((num3 >> 0x18) & 0xff);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }
            return true;
        }

        private static bool Enc(byte[] value)
        {
            try
            {
                for (int i = 0; i < value.Length; i += 4)
                {
                    int src = BitConverter.ToInt32(value, i);
                    int num3 = 0;
                    num3 |= MoveBit(src, 0, 0x1a);
                    num3 |= MoveBit(src, 1, 0x1f);
                    num3 |= MoveBit(src, 2, 0x11);
                    num3 |= MoveBit(src, 3, 10);
                    num3 |= MoveBit(src, 4, 30);
                    num3 |= MoveBit(src, 5, 0x10);
                    num3 |= MoveBit(src, 6, 0x18);
                    num3 |= MoveBit(src, 7, 2);
                    num3 |= MoveBit(src, 8, 0x1d);
                    num3 |= MoveBit(src, 9, 8);
                    num3 |= MoveBit(src, 10, 20);
                    num3 |= MoveBit(src, 11, 15);
                    num3 |= MoveBit(src, 12, 0x1c);
                    num3 |= MoveBit(src, 13, 11);
                    num3 |= MoveBit(src, 14, 13);
                    num3 |= MoveBit(src, 15, 4);
                    num3 |= MoveBit(src, 0x10, 0x13);
                    num3 |= MoveBit(src, 0x11, 0x17);
                    num3 |= MoveBit(src, 0x12, 0);
                    num3 |= MoveBit(src, 0x13, 12);
                    num3 |= MoveBit(src, 20, 14);
                    num3 |= MoveBit(src, 0x15, 0x1b);
                    num3 |= MoveBit(src, 0x16, 6);
                    num3 |= MoveBit(src, 0x17, 0x12);
                    num3 |= MoveBit(src, 0x18, 0x15);
                    num3 |= MoveBit(src, 0x19, 3);
                    num3 |= MoveBit(src, 0x1a, 9);
                    num3 |= MoveBit(src, 0x1b, 7);
                    num3 |= MoveBit(src, 0x1c, 0x16);
                    num3 |= MoveBit(src, 0x1d, 1);
                    num3 |= MoveBit(src, 30, 0x19);
                    num3 |= MoveBit(src, 0x1f, 5);
                    value[i] = (byte)(num3 & 0xff);
                    value[i + 1] = (byte)((num3 >> 8) & 0xff);
                    value[i + 2] = (byte)((num3 >> 0x10) & 0xff);
                    value[i + 3] = (byte)((num3 >> 0x18) & 0xff);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
            return true;
        }

        private static int MoveBit(int src, int oldLoc, int newLoc)
        {
            return (((src >> (oldLoc & 0x1f)) & 1) << newLoc);
        }
    }
}