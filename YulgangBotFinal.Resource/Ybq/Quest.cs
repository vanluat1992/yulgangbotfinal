// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Resource
// FILENAME : Quest.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 6:37 PM
// **************************************************************************

using System.Collections.Generic;

namespace YulgangBotFinal.Resource.Ybq
{
    public class Quest
    {
        public Quest()
        {
            NameQuest = new Sentence();
            AcceptQuest = new Sentence();
            CancelQuest = new Sentence();
            AcceptQuest1 = new Sentence();
            NotEnough = new Sentence();
            ConfigMainQuest = new ConfigMainQuest();
            MainQuest = new MainQuest();
            StepQuest = new List<StepQuest>();
            //ConfigSpecial = new ConfigSpecial();
            Config = new ConfigQuest();
        }


        public int IdQuest { get; set; }


        public Sentence NameQuest { get; set; }


        public ConfigQuest Config { get; set; }


        public Sentence AcceptQuest { get; set; }


        public Sentence CancelQuest { get; set; }


        public Sentence AcceptQuest1 { get; set; }


        public Sentence NotEnough { get; set; }


        // public ConfigSpecial ConfigSpecial { get; set; }


        public ConfigMainQuest ConfigMainQuest { get; set; }


        public MainQuest MainQuest { get; set; }


        public List<StepQuest> StepQuest { get; set; }
        //{
        //    NameQuest.Data.Replace("\\", "-");
        //    var str = NameQuest.Data.Replace("/", "-");
        //    if (str.Contains("?"))
        //    {
        //        Console.WriteLine("Skip quest: {0}", (object) str);
        //    }
        //    else
        //    {
        //        var xmlSerializer = new XmlSerializer(typeof(Quest));
        //        if (


        //public void SaveXml()
        //            File.Exists(string.Format("Xml\\[{0}]{1}-{2}.xml", (object) Config.LevelQuest, (object) str,
        //                (object) Config.Forces)))
        //            File.Delete(string.Format("Xml\\[{0}]{1}-{2}.xml", (object) Config.LevelQuest, (object) str,
        //                (object) Config.Forces));
        //        var streamWriter =
        //            new StreamWriter(string.Format("Xml\\[{0}]{1}-{2}.xml", (object) Config.LevelQuest, (object) str,
        //                (object) Config.Forces));
        //        xmlSerializer.Serialize((TextWriter) streamWriter, (object) this);
        //        streamWriter.Dispose();
        //    }
        //}
    }
}