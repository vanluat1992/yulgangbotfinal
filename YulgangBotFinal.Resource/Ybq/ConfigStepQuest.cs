// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Resource
// FILENAME : ConfigStepQuest.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 6:40 PM
// **************************************************************************

using System.Collections.Generic;

namespace YulgangBotFinal.Resource.Ybq
{
    public class ConfigStepQuest
    {
        public ConfigStepQuest()
        {
            Npc = new InfoNpc();
            Items = new List<ItemQuest>();
        }

        public InfoNpc Npc { get; set; }


        public List<ItemQuest> Items { get; set; }

        public int ItemCount => Items.Count;

        public void AddItem(ItemQuest itemQuest)
        {
            Items.Add(itemQuest);
        }
    }
}