// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Resource
// FILENAME : InfoNpc.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 6:40 PM
// **************************************************************************

using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Resource.Ybq
{
    public class InfoNpc
    {
        public InfoNpc()
        {
            Position = new YulgangLocation();
        }

        public int Unknown { get; set; }


        public uint NpcId { get; set; }


        public IYulgangLocation Position { get; set; }
    }
}