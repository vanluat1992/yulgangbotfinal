// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Resource
// FILENAME : ItemQuest.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 6:39 PM
// **************************************************************************

using System.Collections.Generic;

namespace YulgangBotFinal.Resource.Ybq
{
    public class ItemQuest
    {
        public ItemQuest()
        {
            Item = new ItemPrize();
            Npc = new List<InfoNpc>();
        }

        public int QuestId { get; set; }


        public int StepId { get; set; }


        public ItemPrize Item { get; set; }


        public List<InfoNpc> Npc { get; set; }
    }
}