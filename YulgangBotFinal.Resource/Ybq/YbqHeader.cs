namespace YulgangBotFinal.Resource.Ybq
{
    public class YbqHeader
    {
        public string From { get; set; }

        public int QuestCount { get; set; }
    }
}