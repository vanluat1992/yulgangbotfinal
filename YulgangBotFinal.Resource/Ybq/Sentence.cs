using System.Xml.Serialization;

namespace YulgangBotFinal.Resource.Ybq
{
    public class Sentence
    {
        private string s;


        public int Length { get; set; }


        public string Data
        {
            get
            {
                return this.s;
            }
            set
            {
                this.s = value;
                this.Length = value.Length;
            }
        }

        public Sentence()
        {
            this.Data = " ";
        }

        public void SetData(string value)
        {
            this.Data = value;
            this.Length = value.Length;
        }

        public void SetData(Sentence value)
        {
            this.Length = value.Length;
            this.Data = value.Data;
        }

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return Data;
        }
    }
}