// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Resource
// FILENAME : StepQuest.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 6:40 PM
// **************************************************************************

namespace YulgangBotFinal.Resource.Ybq
{
    public class StepQuest
    {
        public StepQuest()
        {
            Config = new ConfigStepQuest();
            AcceptQuest = new Sentence();
            NotEnoughCondition = new Sentence();
            QuestInfo = new Sentence();
            AS1 = new Sentence();
            AS2 = new Sentence();
            AS3 = new Sentence();
            AS4 = new Sentence();
            NS1 = new Sentence();
            NS2 = new Sentence();
            NS3 = new Sentence();
            NS4 = new Sentence();
        }

        public int QuestId { get; set; }


        public int StepId { get; set; }


        public ConfigStepQuest Config { get; set; }


        public Sentence AcceptQuest { get; set; }


        public Sentence NotEnoughCondition { get; set; }


        public Sentence QuestInfo { get; set; }

        public Sentence AS1 { get; set; }

        public Sentence AS2 { get; set; }

        public Sentence AS3 { get; set; }

        public Sentence AS4 { get; set; }

        public Sentence NS1 { get; set; }

        public Sentence NS2 { get; set; }

        public Sentence NS3 { get; set; }

        public Sentence NS4 { get; set; }
    }
}