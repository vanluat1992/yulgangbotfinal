// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Resource
// FILENAME : ItemPrize.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 6:39 PM
// **************************************************************************

namespace YulgangBotFinal.Resource.Ybq
{
    public class ItemPrize
    {
        public long Id { get; set; }


        public long Count { get; set; }
    }
}