// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Resource
// FILENAME : ConfigMainQuest.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 6:39 PM
// **************************************************************************

using System.Collections.Generic;

namespace YulgangBotFinal.Resource.Ybq
{
    public class ConfigMainQuest
    {
        public ConfigMainQuest()
        {
            ListItemBonus = new List<ItemPrize>();
            ListItemQuest = new List<ItemQuest>();
            Npc = new InfoNpc();
        }

        public List<ItemPrize> ListItemBonus { get; set; }

        public InfoNpc Npc { get; set; }

        public int ItemBonusCount => ListItemBonus.Count;

        public int QuestStepCount { get; set; }

        public int ItemQuestCount => ListItemQuest.Count;


        public List<ItemQuest> ListItemQuest { get; set; }

        public void AddItemBonus(ItemPrize it)
        {
            ListItemBonus.Add(it);
        }

        public void AddItemQuest(ItemQuest it)
        {
            ListItemQuest.Add(it);
        }
    }
}