using System.Collections.Generic;
using System.Xml.Serialization;

namespace YulgangBotFinal.Resource.Ybq
{
    public class ConfigQuest
    {

        public int LevelQuest { get; set; }


        public int Job { get; set; }


        public int Forces { get; set; }


        public int Upgrade { get; set; }


        public int Sex { get; set; }


        public int IsDelete { get; set; }


        public int Unknown5 { get; set; }


        public int Type { get; set; }




        public List<ItemPrize> RequiredItems { get; set; }




        public ConfigQuest()
        {
            this.RequiredItems = new List<ItemPrize>();
        }
    }
}