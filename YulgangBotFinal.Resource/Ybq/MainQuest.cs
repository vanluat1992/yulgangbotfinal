// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Resource
// FILENAME : MainQuest.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 6:39 PM
// **************************************************************************

namespace YulgangBotFinal.Resource.Ybq
{
    public class MainQuest
    {
        public MainQuest()
        {
            QuestInfo = new Sentence();
            AccepQuest = new Sentence();
            CancelQuest = new Sentence();
            AS1 = new Sentence();
            AS2 = new Sentence();
            AS3 = new Sentence();
            AS4 = new Sentence();
            NS1 = new Sentence();
            NS2 = new Sentence();
            NS3 = new Sentence();
            NS4 = new Sentence();
        }

        public Sentence QuestInfo { get; set; }


        public Sentence AccepQuest { get; set; }


        public Sentence CancelQuest { get; set; }

        public Sentence AS1 { get; set; }

        public Sentence AS2 { get; set; }

        public Sentence AS3 { get; set; }

        public Sentence AS4 { get; set; }

        public Sentence NS1 { get; set; }

        public Sentence NS2 { get; set; }

        public Sentence NS3 { get; set; }

        public Sentence NS4 { get; set; }
    }
}