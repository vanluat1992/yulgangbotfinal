﻿// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Resource
// FILENAME : YbqUtils.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 6:38 PM
// **************************************************************************

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace YulgangBotFinal.Resource.Ybq
{
    public class YbqUtils
    {
        private static readonly int[] Dtable = new int[256]
        {
            0,
            2,
            4,
            6,
            32,
            34,
            36,
            38,
            128,
            130,
            132,
            134,
            160,
            162,
            164,
            166,
            1,
            3,
            5,
            7,
            33,
            35,
            37,
            39,
            129,
            131,
            133,
            135,
            161,
            163,
            165,
            167,
            8,
            10,
            12,
            14,
            40,
            42,
            44,
            46,
            136,
            138,
            140,
            142,
            168,
            170,
            172,
            174,
            9,
            11,
            13,
            15,
            41,
            43,
            45,
            47,
            137,
            139,
            141,
            143,
            169,
            171,
            173,
            175,
            16,
            18,
            20,
            22,
            48,
            50,
            52,
            54,
            144,
            146,
            148,
            150,
            176,
            178,
            180,
            182,
            17,
            19,
            21,
            23,
            49,
            51,
            53,
            55,
            145,
            147,
            149,
            151,
            177,
            179,
            181,
            183,
            24,
            26,
            28,
            30,
            56,
            58,
            60,
            62,
            152,
            154,
            156,
            158,
            184,
            186,
            188,
            190,
            25,
            27,
            29,
            31,
            57,
            59,
            61,
            63,
            153,
            155,
            157,
            159,
            185,
            187,
            189,
            191,
            64,
            66,
            68,
            70,
            96,
            98,
            100,
            102,
            192,
            194,
            196,
            198,
            224,
            226,
            228,
            230,
            65,
            67,
            69,
            71,
            97,
            99,
            101,
            103,
            193,
            195,
            197,
            199,
            225,
            227,
            229,
            231,
            72,
            74,
            76,
            78,
            104,
            106,
            108,
            110,
            200,
            202,
            204,
            206,
            232,
            234,
            236,
            238,
            73,
            75,
            77,
            79,
            105,
            107,
            109,
            111,
            201,
            203,
            205,
            207,
            233,
            235,
            237,
            239,
            80,
            82,
            84,
            86,
            112,
            114,
            116,
            118,
            208,
            210,
            212,
            214,
            240,
            242,
            244,
            246,
            81,
            83,
            85,
            87,
            113,
            115,
            117,
            119,
            209,
            211,
            213,
            215,
            241,
            243,
            245,
            247,
            88,
            90,
            92,
            94,
            120,
            122,
            124,
            126,
            216,
            218,
            220,
            222,
            248,
            250,
            252,
            254,
            89,
            91,
            93,
            95,
            121,
            123,
            125,
            (int) sbyte.MaxValue,
            217,
            219,
            221,
            223,
            249,
            251,
            253,
            (int) byte.MaxValue
        };

        public static Encoding QuestEncoding { get; set; } = Encoding.GetEncoding(1258);

        public static byte[] Unpack(byte[] data)
        {
            for (var index = 0; index < data.Length; ++index)
                data[index] = Convert.ToByte(Dtable[(int) Convert.ToInt16(data[index])]);
            return data;
        }

        public static byte[] Pack(byte[] data)
        {
            for (var index1 = 0; index1 < data.Length; ++index1)
            {
                var index2 = 0;
                while (Dtable[index2] != (int) Convert.ToInt16(data[index1]))
                    ++index2;
                data[index1] = Convert.ToByte(index2);
            }
            return data;
        }

        public static byte[] Unpack(string file, out YbqHeader header)
        {
            var fileStream = new FileStream(file, FileMode.Open);
            var ybqHeader = new YbqHeader();
            var num1 = 0;
            var memoryStream1 = new MemoryStream();
            var binaryWriter1 = new BinaryWriter((Stream) memoryStream1);
            while (true)
            {
                ++num1;
                var num2 = fileStream.ReadByte();
                if (num2 != 10)
                    binaryWriter1.Write((byte) num2);
                else
                    break;
            }
            ybqHeader.From = Encoding.Default.GetString(memoryStream1.ToArray());
            var memoryStream2 = new MemoryStream();
            var binaryWriter2 = new BinaryWriter((Stream) memoryStream2);
            while (true)
            {
                ++num1;
                var num2 = fileStream.ReadByte();
                if (num2 != 32)
                    binaryWriter2.Write((byte) num2);
                else
                    break;
            }
            ybqHeader.QuestCount = int.Parse(Encoding.Default.GetString(memoryStream2.ToArray()));
            binaryWriter2.Dispose();
            memoryStream2.Dispose();
            header = ybqHeader;
            byte[] numArray1;
            using (var binaryReader = new BinaryReader((Stream) fileStream))
            {
                var memoryStream3 = new MemoryStream();
                fileStream.Seek((long) num1, SeekOrigin.Begin);
                var numArray2 = new byte[binaryReader.BaseStream.Length - (long) num1];
                binaryReader.Read(numArray2, 0, numArray2.Length);
                numArray1 = Unpack(numArray2);
                memoryStream3.Dispose();
            }
            fileStream.Dispose();
            return numArray1;
        }

        public static IDictionary<int, Quest> ReadQuest(string path)
        {
            var dictionary = new Dictionary<int, Quest>();
            YbqHeader header;
            var numArray = Unpack(path, out header);
            //var s = Encoding.GetEncoding(1258).GetString(numArray);
            var bread = new BinaryReader(new MemoryStream(numArray));
            for (var index1 = 0; index1 < header.QuestCount; ++index1)
            {
                var quest = new Quest();
                try
                {
                    quest.IdQuest = ReadInt(bread.BaseStream.Position, numArray, bread);
                    quest.NameQuest = ReadString(bread.BaseStream.Position, numArray, bread);
                    quest.Config.LevelQuest = ReadInt(bread.BaseStream.Position, numArray, bread);
                    quest.Config.Forces = ReadInt(bread.BaseStream.Position, numArray, bread);
                    quest.Config.Job = ReadInt(bread.BaseStream.Position, numArray, bread);
                    quest.Config.Upgrade = ReadInt(bread.BaseStream.Position, numArray, bread);
                    quest.Config.Sex = ReadInt(bread.BaseStream.Position, numArray, bread);
                    quest.Config.IsDelete = ReadInt(bread.BaseStream.Position, numArray, bread);
                    quest.Config.Unknown5 = ReadInt(bread.BaseStream.Position, numArray, bread);
                    quest.Config.Type = ReadInt(bread.BaseStream.Position, numArray, bread);
                    quest.AcceptQuest = ReadString(bread.BaseStream.Position, numArray, bread);
                    quest.NotEnough = ReadString(bread.BaseStream.Position, numArray, bread);
                    quest.AcceptQuest1 = ReadString(bread.BaseStream.Position, numArray, bread);
                    quest.CancelQuest = ReadString(bread.BaseStream.Position, numArray, bread);
                    var num1 = ReadInt(bread.BaseStream.Position, numArray, bread);
                    for (var index2 = 0; index2 < num1; ++index2)
                    {
                        var it = new ItemPrize()
                        {
                            Id = (long) ReadInt(bread.BaseStream.Position, numArray, bread),
                            Count = (long) ReadInt(bread.BaseStream.Position, numArray, bread)
                        };
                        quest.ConfigMainQuest.AddItemBonus(it);
                    }
                    quest.ConfigMainQuest.QuestStepCount = ReadInt(bread.BaseStream.Position, numArray, bread);
                    if (quest.ConfigMainQuest.QuestStepCount != 0)
                    {
                        quest.ConfigMainQuest.Npc.NpcId = (uint) ReadInt(bread.BaseStream.Position, numArray, bread);
                        quest.ConfigMainQuest.Npc.Unknown = ReadInt(bread.BaseStream.Position, numArray, bread);
                        quest.ConfigMainQuest.Npc.Position.MapId = ReadInt(bread.BaseStream.Position, numArray, bread);
                        quest.ConfigMainQuest.Npc.Position.X = ReadInt(bread.BaseStream.Position, numArray, bread);
                        quest.ConfigMainQuest.Npc.Position.Y = ReadInt(bread.BaseStream.Position, numArray, bread);
                        var num2 = ReadInt(bread.BaseStream.Position, numArray, bread);
                        for (var index2 = 0; index2 < num2; ++index2)
                        {
                            var itemQuest = new ItemQuest();
                            itemQuest.Item.Id = ReadInt(bread.BaseStream.Position, numArray, bread);
                            itemQuest.Item.Count = ReadInt(bread.BaseStream.Position, numArray, bread);
                            var idQuest = quest.IdQuest;
                            itemQuest.QuestId = idQuest;
                            var num3 = 0;
                            itemQuest.StepId = num3;
                            var it = itemQuest;
                            var infoNpc = new InfoNpc()
                            {
                                Position =
                                {
                                    MapId = ReadInt(bread.BaseStream.Position, numArray, bread),
                                    X = ReadInt(bread.BaseStream.Position, numArray, bread),
                                    Y = ReadInt(bread.BaseStream.Position, numArray, bread)
                                }
                            };
                            it.Npc = new List<InfoNpc>();
                            it.Npc.Add(infoNpc);
                            quest.ConfigMainQuest.AddItemQuest(it);
                        }
                        quest.MainQuest.AccepQuest = ReadString(bread.BaseStream.Position, numArray, bread);
                        ReadString(bread.BaseStream.Position, numArray, bread);
                        ReadString(bread.BaseStream.Position, numArray, bread);
                        ReadString(bread.BaseStream.Position, numArray, bread);
                        ReadString(bread.BaseStream.Position, numArray, bread);
                        quest.MainQuest.CancelQuest = ReadString(bread.BaseStream.Position, numArray, bread);
                        ReadString(bread.BaseStream.Position, numArray, bread);
                        ReadString(bread.BaseStream.Position, numArray, bread);
                        ReadString(bread.BaseStream.Position, numArray, bread);
                        ReadString(bread.BaseStream.Position, numArray, bread);
                        quest.MainQuest.QuestInfo = ReadString(bread.BaseStream.Position, numArray, bread);
                        for (var index2 = 0; index2 < quest.ConfigMainQuest.QuestStepCount - 1; ++index2)
                        {
                            var stepQuest = new StepQuest()
                            {
                                QuestId = quest.IdQuest,
                                StepId = index2 + 1
                            };
                            stepQuest.Config.Npc.NpcId = (uint) ReadInt(bread.BaseStream.Position, numArray, bread);
                            stepQuest.Config.Npc.Unknown = ReadInt(bread.BaseStream.Position, numArray, bread);
                            stepQuest.Config.Npc.Position.MapId = ReadInt(bread.BaseStream.Position, numArray, bread);
                            stepQuest.Config.Npc.Position.X = ReadInt(bread.BaseStream.Position, numArray, bread);
                            stepQuest.Config.Npc.Position.Y = ReadInt(bread.BaseStream.Position, numArray, bread);
                            var num3 = ReadInt(bread.BaseStream.Position, numArray, bread);
                            for (var index3 = 0; index3 < num3; ++index3)
                            {
                                var itemQuest1 = new ItemQuest();
                                itemQuest1.Item.Id = ReadInt(bread.BaseStream.Position, numArray, bread);
                                itemQuest1.Item.Count = ReadInt(bread.BaseStream.Position, numArray, bread);
                                var idQuest = quest.IdQuest;
                                itemQuest1.QuestId = idQuest;
                                var num4 = index2 + 1;
                                itemQuest1.StepId = num4;
                                var itemQuest2 = itemQuest1;
                                var infoNpc = new InfoNpc()
                                {
                                    Position =
                                    {
                                        MapId = ReadInt(bread.BaseStream.Position, numArray, bread),
                                        X = ReadInt(bread.BaseStream.Position, numArray, bread),
                                        Y = ReadInt(bread.BaseStream.Position, numArray, bread)
                                    }
                                };
                                itemQuest2.Npc = new List<InfoNpc>();
                                itemQuest2.Npc.Add(infoNpc);
                                stepQuest.Config.AddItem(itemQuest2);
                            }
                            stepQuest.AcceptQuest = ReadString(bread.BaseStream.Position, numArray, bread);
                            ReadString(bread.BaseStream.Position, numArray, bread);
                            ReadString(bread.BaseStream.Position, numArray, bread);
                            ReadString(bread.BaseStream.Position, numArray, bread);
                            ReadString(bread.BaseStream.Position, numArray, bread);
                            stepQuest.NotEnoughCondition = ReadString(bread.BaseStream.Position, numArray, bread);
                            ReadString(bread.BaseStream.Position, numArray, bread);
                            ReadString(bread.BaseStream.Position, numArray, bread);
                            ReadString(bread.BaseStream.Position, numArray, bread);
                            ReadString(bread.BaseStream.Position, numArray, bread);
                            stepQuest.QuestInfo = ReadString(bread.BaseStream.Position, numArray, bread);
                            quest.StepQuest.Add(stepQuest);
                        }
                        dictionary.Add(quest.IdQuest, quest);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return (IDictionary<int, Quest>) dictionary;
        }

        public static void WriteQuest(IDictionary<int, Quest> qlisQuests)
        {
            //var questEncoding = Encoding.GetEncoding(1258);
            var ybqHeader1 = new YbqHeader();
            ybqHeader1.From = "vietnam 20100210";
            var count = qlisQuests.Count;
            ybqHeader1.QuestCount = count;
            var ybqHeader2 = ybqHeader1;
            var fileStream = new FileStream("Out\\Ybq.cfg", FileMode.Create);
            var binaryWriter = new BinaryWriter((Stream) fileStream, QuestEncoding);
            binaryWriter.Write(ybqHeader2.From.ToCharArray());
            binaryWriter.Write("\r\n".ToCharArray());
            binaryWriter.Write(
                ybqHeader2.QuestCount.ToString(CultureInfo.InvariantCulture).ToCharArray());
            binaryWriter.Write(' ');
            binaryWriter.Flush();
            var memoryStream = new MemoryStream();
            var bwrite = new BinaryWriter((Stream) memoryStream, QuestEncoding);
            foreach (var quest in qlisQuests.Values)
            {
                WriteInt(bwrite, quest.IdQuest);
                WriteString(bwrite, quest.NameQuest);
                WriteInt(bwrite, quest.Config.LevelQuest);
                WriteInt(bwrite, quest.Config.Forces);
                WriteInt(bwrite, quest.Config.Job);
                WriteInt(bwrite, quest.Config.Upgrade);
                WriteInt(bwrite, quest.Config.Sex);
                WriteInt(bwrite, quest.Config.IsDelete);
                WriteInt(bwrite, quest.Config.Unknown5);
                WriteInt(bwrite, quest.Config.Type);
                WriteString(bwrite, quest.AcceptQuest);
                WriteString(bwrite, quest.NotEnough);
                WriteString(bwrite, quest.AcceptQuest1);
                WriteString(bwrite, quest.CancelQuest);
                WriteInt(bwrite, quest.ConfigMainQuest.ItemBonusCount);
                foreach (var listItemBonu in quest.ConfigMainQuest.ListItemBonus)
                {
                    WriteInt(bwrite, (int) listItemBonu.Id);
                    WriteInt(bwrite, (int) listItemBonu.Count);
                }
                WriteInt(bwrite, quest.ConfigMainQuest.QuestStepCount);
                WriteInt(bwrite, (int) quest.ConfigMainQuest.Npc.NpcId);
                WriteInt(bwrite, quest.ConfigMainQuest.Npc.Unknown);
                WriteInt(bwrite, quest.ConfigMainQuest.Npc.Position.MapId);
                WriteInt(bwrite, (int) quest.ConfigMainQuest.Npc.Position.X);
                WriteInt(bwrite, (int) quest.ConfigMainQuest.Npc.Position.Y);
                WriteInt(bwrite, quest.ConfigMainQuest.ItemQuestCount);
                foreach (var itemQuest in quest.ConfigMainQuest.ListItemQuest)
                {
                    WriteInt(bwrite, (int) itemQuest.Item.Id);
                    WriteInt(bwrite, (int) itemQuest.Item.Count);
                    var infoNpc = itemQuest.Npc.FirstOrDefault<InfoNpc>();
                    WriteInt(bwrite, infoNpc != null ? infoNpc.Position.MapId : 0);
                    WriteInt(bwrite, (int) (infoNpc != null ? infoNpc.Position.X : 0));
                    WriteInt(bwrite, (int) (infoNpc != null ? infoNpc.Position.Y : 0));
                }
                if (quest.ConfigMainQuest.QuestStepCount != 0)
                {
                    WriteString(bwrite, quest.MainQuest.AccepQuest);
                    WriteInt(bwrite, 0);
                    WriteInt(bwrite, 0);
                    WriteInt(bwrite, 0);
                    WriteInt(bwrite, 0);
                    WriteString(bwrite, quest.MainQuest.CancelQuest);
                    WriteInt(bwrite, 0);
                    WriteInt(bwrite, 0);
                    WriteInt(bwrite, 0);
                    WriteInt(bwrite, 0);
                    WriteString(bwrite, quest.MainQuest.QuestInfo);
                    foreach (var stepQuest in quest.StepQuest)
                    {
                        WriteInt(bwrite, (int) stepQuest.Config.Npc.NpcId);
                        WriteInt(bwrite, stepQuest.Config.Npc.Unknown);
                        WriteInt(bwrite, stepQuest.Config.Npc.Position.MapId);
                        WriteInt(bwrite, (int) stepQuest.Config.Npc.Position.X);
                        WriteInt(bwrite, (int) stepQuest.Config.Npc.Position.Y);
                        WriteInt(bwrite, stepQuest.Config.ItemCount);
                        foreach (var itemQuest in stepQuest.Config.Items)
                        {
                            WriteInt(bwrite, (int) itemQuest.Item.Id);
                            WriteInt(bwrite, (int) itemQuest.Item.Count);
                            var infoNpc = itemQuest.Npc.FirstOrDefault<InfoNpc>();
                            WriteInt(bwrite, infoNpc != null ? infoNpc.Position.MapId : 0);
                            WriteInt(bwrite, (int) (infoNpc != null ? infoNpc.Position.X : 0));
                            WriteInt(bwrite, (int) (infoNpc != null ? infoNpc.Position.Y : 0));
                        }
                        WriteString(bwrite, stepQuest.AcceptQuest);
                        WriteInt(bwrite, 0);
                        WriteInt(bwrite, 0);
                        WriteInt(bwrite, 0);
                        WriteInt(bwrite, 0);
                        WriteString(bwrite, stepQuest.NotEnoughCondition);
                        WriteInt(bwrite, 0);
                        WriteInt(bwrite, 0);
                        WriteInt(bwrite, 0);
                        WriteInt(bwrite, 0);
                        WriteString(bwrite, stepQuest.QuestInfo);
                    }
                }
            }
            var buffer = Pack(memoryStream.ToArray());
            bwrite.Dispose();
            memoryStream.Dispose();
            binaryWriter.Write(buffer);
            binaryWriter.Flush();
            binaryWriter.Dispose();
            fileStream.Dispose();
            Console.WriteLine("Completed");
        }

        private static int ReadInt(long begin, byte[] data, BinaryReader bread)
        {
            var space = FindSpace(begin, data);
            return int.Parse(QuestEncoding.GetString(bread.ReadBytes((int) space)));
        }

        private static void WriteInt(BinaryWriter bwrite, int val)
        {
            bwrite.Write(val.ToString(CultureInfo.InvariantCulture).ToCharArray());
            bwrite.Write(' ');
            bwrite.Flush();
        }

        private static Sentence ReadString(long begin, byte[] data, BinaryReader bread)
        {
            var sentence = new Sentence();
            var num = ReadInt(begin, data, bread);
            var str = QuestEncoding.GetString(bread.ReadBytes(num + 1));
            sentence.SetData(str);
            return sentence;
        }

        private static void WriteString(BinaryWriter bwrite, Sentence data)
        {
            WriteInt(bwrite, data.Length);
            bwrite.Write(data.Data.ToArray<char>());
            bwrite.Write(' ');
            bwrite.Flush();
        }

        private static long FindSpace(long begin, byte[] data)
        {
            for (var index = begin; index < (long) data.Length; ++index)
                if ((int) data[index] == 32 || (int) data[index] == 10)
                    return index - begin + 1L;
            return -1;
        }
    }
}