﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Resource
// FileName : YBiHandle.cs
// Time Create : 10:35 AM 16/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.Helper;
using YulgangBotFinal.Model.IModels;

#endregion

namespace YulgangBotFinal.Resource
{
    public class YBiHandle<TItem, TMonster, TMap, TSkill, TSpell> where TItem : IYulgangYbiGameItem
        where TMonster : IYulgangYbiGameMonster
        where TMap : IYulgangYbiGameMap
        where TSkill : IYulgangYbiGameSkill
        where TSpell : IYulgangYbiGameSpell
    {
        public readonly YBiBlock Badsentence;
        public readonly YBiBlock Badword;
        public readonly Encoding BlockEncoding = Encoding.GetEncoding(1258);
        public readonly YBiBlock Famous;
        //public Dictionary<UInt64, Database.Famous> DicFamous { get; private set; }

        public readonly YBiBlock FileSize;
        public readonly YBiBlock Infochar;
        public readonly YBiBlock Item;
        public readonly YBiBlock Map;
        public readonly YBiBlock Npc;
        public readonly YBiBlock Skill;
        public readonly YBiBlock Spell;
        public readonly YBiBlock Unknow1;
        public readonly YBiBlock Unknow2;
        public readonly YBiBlock Vesion;


        public YBiHandle()
            : this(9)
        {
        }

        public YBiHandle(int ver)
        {
            if (ver >= 15)
            {
                FileSize = new YBiBlock(1, 4);
                Vesion = new YBiBlock(1, 4);
                Item = new YBiBlock(0x1BD9+1, 0x350);
                Unknow1 = new YBiBlock(1, 64);
                Spell = new YBiBlock(0x400, 0x1a50);
                Skill = new YBiBlock(0x400, 0xB94);
                Famous = new YBiBlock(0x100, 0x48);
                Badword = new YBiBlock(0x800, 0x40);
                Badsentence = new YBiBlock(0x800, 0x80);
                Npc = new YBiBlock(0x800, 0x1EB4);
                Infochar = new YBiBlock(6, 0x10);
                Map = new YBiBlock(0x400, 0x2E8);
                Unknow2 = new YBiBlock(0x400, 0x60);

                DicItems = new Dictionary<long, IYulgangYbiGameItem>();
                DicMonsters = new Dictionary<long, IYulgangYbiGameMonster>();
                DicMaps = new Dictionary<long, IYulgangYbiGameMap>();
                DicSkills = new Dictionary<long, IYulgangYbiGameSkill>();
                DicSpells = new Dictionary<long, IYulgangYbiGameSpell>();
            }
            else if (ver >= 14)
            {
                FileSize = new YBiBlock(1, 4);
                Vesion = new YBiBlock(1, 4);
                Item = new YBiBlock(0x2000, 0x350);
                Unknow1 = new YBiBlock(1, 64);
                Spell = new YBiBlock(0x400, 0x1a50);
                Skill = new YBiBlock(0x400, 0xB94);
                Famous = new YBiBlock(0x100, 0x48);
                Badword = new YBiBlock(0x800, 0x40);
                Badsentence = new YBiBlock(0x800, 0x80);
                Npc = new YBiBlock(0x800, 0x1EB4);
                Infochar = new YBiBlock(6, 0x10);
                Map = new YBiBlock(0x400, 0x68);
                Unknow2 = new YBiBlock(0x400, 0x60);

                DicItems = new Dictionary<long, IYulgangYbiGameItem>();
                DicMonsters = new Dictionary<long, IYulgangYbiGameMonster>();
                DicMaps = new Dictionary<long, IYulgangYbiGameMap>();
                DicSkills = new Dictionary<long, IYulgangYbiGameSkill>();
                DicSpells = new Dictionary<long, IYulgangYbiGameSpell>();
            }
            else if (ver > 8)
            {
                FileSize = new YBiBlock(1, 4);
                Vesion = new YBiBlock(1, 4);
                Item = new YBiBlock(0x2000, 0x350);
                Unknow1 = new YBiBlock(1, 64);
                Spell = new YBiBlock(0x400, 0x1a50);
                Skill = new YBiBlock(0x400, 0xB94);
                Famous = new YBiBlock(0x80, 0x48);
                Badword = new YBiBlock(0x800, 0x40);
                Badsentence = new YBiBlock(0x800, 0x80);
                Npc = new YBiBlock(0x800, 0x1EB4);
                Infochar = new YBiBlock(6, 0x10);
                Map = new YBiBlock(0x400, 0x68);
                Unknow2 = new YBiBlock(0x400, 0x60);

                DicItems = new Dictionary<long, IYulgangYbiGameItem>();
                DicMonsters = new Dictionary<long, IYulgangYbiGameMonster>();
                DicMaps = new Dictionary<long, IYulgangYbiGameMap>();
                DicSkills = new Dictionary<long, IYulgangYbiGameSkill>();
                DicSpells = new Dictionary<long, IYulgangYbiGameSpell>();
                // DicFamous = new Dictionary<UInt64, Database.Famous>();
            }
            else
            {
                FileSize = new YBiBlock(1, 4);
                Vesion = new YBiBlock(1, 4);
                Item = new YBiBlock(0x1000, 0x328);
                Unknow1 = new YBiBlock(1, 0x40);
                Spell = new YBiBlock(0x400, 0x1a50);
                Skill = new YBiBlock(0x400, 0x974);
                Famous = new YBiBlock(0x80, 0x48);
                Badword = new YBiBlock(0x800, 0x40);
                Badsentence = new YBiBlock(0x800, 0x80);
                Npc = new YBiBlock(0x800, 0x1EB4);
                Infochar = new YBiBlock(6, 0x10);
                Map = new YBiBlock(0x400, 0x68);
                Unknow2 = new YBiBlock(0x400, 0x60);

                DicItems = new Dictionary<long, IYulgangYbiGameItem>();
                DicMonsters = new Dictionary<long, IYulgangYbiGameMonster>();
                DicMaps = new Dictionary<long, IYulgangYbiGameMap>();
                DicSkills = new Dictionary<long, IYulgangYbiGameSkill>();
                DicSpells = new Dictionary<long, IYulgangYbiGameSpell>();
                //DicFamous = new Dictionary<UInt64, Database.Famous>();
            }
        }

        public Dictionary<long, IYulgangYbiGameItem> DicItems { get; }
        public Dictionary<long, IYulgangYbiGameMonster> DicMonsters { get; }
        public Dictionary<long, IYulgangYbiGameMap> DicMaps { get; }
        public Dictionary<long, IYulgangYbiGameSkill> DicSkills { get; }
        public Dictionary<long, IYulgangYbiGameSpell> DicSpells { get; }


        public void Load(string path)
        {
            try
            {
                var fStream = new FileStream(path, FileMode.Open);
                var biRead = new BinaryReader(fStream);

                FileSize.Initialize(biRead, false, false);
                Vesion.Initialize(biRead, false, false);
                Item.Initialize(biRead, true, true);
                Unknow1.Initialize(biRead, true, false);
                Spell.Initialize(biRead, true, true);
                Skill.Initialize(biRead, true, true);
                Famous.Initialize(biRead, true, false);
                Badword.Initialize(biRead, true, false);
                Badsentence.Initialize(biRead, true, false);
                Npc.Initialize(biRead, true, true);

                //SAVENPCDATA();

                Infochar.Initialize(biRead, true, false);
                //biRead.BaseStream.Seek(0x102000, SeekOrigin.Current);
                Map.Initialize(biRead, true, true);
                Unknow2.Initialize(biRead, true, false);

                biRead.Close();
                biRead.Dispose();
                fStream.Close();
                fStream.Dispose();

                Handle();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }


        public void SaveTo(YBiHandle<TItem, TMonster, TMap, TSkill, TSpell> ybiOther)
        {
            foreach (var obj in Item.GetDictionarydec())
            {
                var obj1 = obj;
                foreach (var objOther in ybiOther.Item.GetDictionarydec().Where(objOther => objOther.Key == obj1.Key))
                {
                    obj.Value.CopyTo(objOther.Value, 0);

                    break;
                }
            }
            foreach (var obj in Npc.GetDictionarydec())
            {
                var obj1 = obj;
                foreach (var objOther in ybiOther.Npc.GetDictionarydec().Where(objOther => objOther.Key == obj1.Key))
                {
                    obj.Value.CopyTo(objOther.Value, 0);
                    break;
                }
            }
            foreach (var obj in Skill.GetDictionarydec())
            {
                var obj1 = obj;
                foreach (var objOther in ybiOther.Skill.GetDictionarydec().Where(objOther => objOther.Key == obj1.Key))
                {
                    obj.Value.CopyTo(objOther.Value, 0);
                    break;
                }
            }
            foreach (var obj in Spell.GetDictionarydec())
            {
                var obj1 = obj;
                foreach (var objOther in ybiOther.Spell.GetDictionarydec().Where(objOther => objOther.Key == obj1.Key))
                {
                    obj.Value.CopyTo(objOther.Value, 0);
                    break;
                }
            }
            foreach (var obj in Map.GetDictionarydec())
            {
                var obj1 = obj;
                foreach (var objOther in ybiOther.Map.GetDictionarydec().Where(objOther => objOther.Key == obj1.Key))
                {
                    obj.Value.CopyTo(objOther.Value, 0);
                    break;
                }
            }
            foreach (var famouse in Famous.GetDictionarydec())
            {
                foreach (
                    var bytese in
                        ybiOther.Famous.GetDictionarydec()
                            .Where(
                                bytese =>
                                    BitConverter.ToInt64(famouse.Value, 64) == BitConverter.ToInt64(bytese.Value, 64)))
                {
                    famouse.Value.CopyTo(bytese.Value, 0);
                }
            }
        }

        //public void Save(string path, bool chk)
        //{
        //    if (File.Exists(path))
        //        File.Delete(path);

        //    var fStream = new FileStream(path, FileMode.Create);
        //    var write = new BinaryWriter(fStream);
        //    if (chk)
        //    {
        //        SaveItem();
        //        SaveMap();
        //        SaveNPC();
        //        SaveSkill();
        //        SaveSpell();
        //    }
        //    FileSize.Save(write, false);
        //    Vesion.Save(write, false);
        //    ITEM.Save(write, true);
        //    UNKNOW1.Save(write, true);
        //    SPELL.Save(write, true);
        //    SKILL.Save(write, true);
        //    FAMOUS.Save(write, true);
        //    BADWORD.Save(write, true);
        //    BADSENTENCE.Save(write, true);
        //    NPC.Save(write, true);
        //    INFOCHAR.Save(write, true);
        //    write.Write(new byte[0x102000]);
        //    MAP.Save(write, true);
        //    UNKNOW2.Save(write, true);
        //    write.Close();
        //    write.Dispose();
        //    fStream.Close();
        //    fStream.Dispose();
        //}


        public void SAVENPCDATA(int id)
        {
            try
            {
                var fStream = new FileStream(string.Format(@"NpcYbi\{0}.dat", id), FileMode.Create);
                var vWrite = new BinaryWriter(fStream);
                byte[] data;
                Npc.GetDictionarydec().TryGetValue(id, out data);
                if (data != null)
                    vWrite.Write(data);
                vWrite.Dispose();
                fStream.Dispose();
            }
            catch (Exception Ex)
            {
                Debug.WriteLine(Ex);
                throw;
            }
        }

        public void SAVESpell(int id)
        {
            try
            {
                var fStream = new FileStream(string.Format(@"SpellYbi\{0}.dat", id), FileMode.Create);
                var vWrite = new BinaryWriter(fStream);
                byte[] data;
                Spell.GetDictionarydec().TryGetValue(id & 0xFFFFFFFC, out data);
                if (data != null)
                    vWrite.Write(data);
                vWrite.Dispose();
                fStream.Dispose();
            }
            catch (Exception Ex)
            {
                Debug.WriteLine(Ex);
                throw;
            }
        }

        public void SaveItem(int ID)
        {
            try
            {
                var fStream = new FileStream(string.Format(@"ItemYbi\{0}.dat", ID), FileMode.Create);
                var vWrite = new BinaryWriter(fStream);
                byte[] data;
                Item.GetDictionarydec().TryGetValue(ID, out data);
                if (data != null)
                    vWrite.Write(data);
                vWrite.Dispose();
                fStream.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void Handle()
        {
            HandleItem();
            HandleNPC();
            HandleSkill();
            HandleSpell();
            HandleMAP();
            //HandleFamous();
        }


        private void HandleItem()
        {
            foreach (var vItem in Item.GetDec())
            {
                var sStream = new MemoryStream(vItem);
                var biRead = new BinaryReader(sStream);
                var tmpItem = Activator.CreateInstance<TItem>();
                //var tmpItem = new T1
                //{
                //    Id = (int)biRead.ReadUInt64()
                //};
                tmpItem.Id = (int) biRead.ReadUInt64();
                if (tmpItem.Id == 0) break;
                tmpItem.Name = ConvertGetString(biRead.ReadBytes(64));
                biRead.ReadByte();
                tmpItem.Force = (YulgangGameForce) biRead.ReadByte();
                tmpItem.Job = YulgangConvert.GetJobYbi(biRead.ReadInt16());
                tmpItem.Level = biRead.ReadUInt16();
                tmpItem.Upgrade = biRead.ReadByte();
                tmpItem.Sex = (YulgangGameSex) biRead.ReadByte();

                var iType = biRead.ReadByte();
                biRead.ReadByte();
                tmpItem.Weight = biRead.ReadInt16();
                tmpItem.MaxAttackPoint = biRead.ReadInt16();
                tmpItem.MinAttackPoint = biRead.ReadInt16();
                tmpItem.Defense = biRead.ReadInt16();
                tmpItem.Accuracy = biRead.ReadInt16();
                tmpItem.Dodge = biRead.ReadInt16();
                tmpItem.MaxDurability = biRead.ReadInt16();
                var unk10Byte = biRead.ReadBytes(4);

                tmpItem.SellMoney = biRead.ReadInt64();
                tmpItem.BuyMoney = biRead.ReadInt64();
                /*tmpItem.Type1 =*/
                biRead.ReadByte();
                /*tmpItem.Type2 =*/
                biRead.ReadByte();
                /*tmpItem.Type3 =*/
                biRead.ReadByte();
                /*tmpItem.Type4 =*/
                biRead.ReadByte();
                /*tmpItem.Type5 = */
                biRead.ReadByte();
                //if (iType >= 1 && iType <= 5) tmpItem.ItemType = ItemType.Weapon;
                //else
                //tmpItem.Type = iType;
                biRead.ReadBytes(35);

                tmpItem.Description = ConvertGetString(biRead.ReadBytes(256));
                // doc 17 byte
                biRead.ReadBytes(16);
                // 160 byte phan giai
                // Phân giải
                // Tối đa 10 items
                // 8 bytes ID, 8 bytes số lượng
                for (var i = 0; i < 10; i++)
                {
                    var id = biRead.ReadInt64();
                    var count = biRead.ReadInt64();
                    //if (id > 0)
                    //    tmpItem.Descontructs.Add(new ItemDescontruct()
                    //    {
                    //        ItemId = (int)id,
                    //        Count = (int)count,
                    //        Reference = tmpItem
                    //    });
                }
                biRead.ReadBytes(160);
                // 2 byte cut cho
                /* tmpItem.Type6 =*/
                biRead.ReadByte();
                /*tmpItem.Type7 =*/
                biRead.ReadByte();
                // 2 byte trong
                biRead.ReadBytes(2);
                // 256
                biRead.ReadBytes(256);
                if (!DicItems.ContainsKey(tmpItem.Id) && tmpItem.Id != 0)
                    DicItems.Add(tmpItem.Id, tmpItem);
                biRead.Dispose();
                sStream.Dispose();
            }
        }


        private void HandleNPC()
        {
            foreach (var vObj in Npc.GetDec())
            {
                var sStream = new MemoryStream(vObj);
                var biRead = new BinaryReader(sStream);
                var tmpMonster = Activator.CreateInstance<TMonster>(); // new Monster { Id = biRead.ReadInt32() };
                tmpMonster.Id = biRead.ReadInt32();
                if (tmpMonster.Id == 0) return;
                tmpMonster.Name = ConvertGetString(biRead.ReadBytes(64));
                tmpMonster.Forces = (YulgangGameForce) biRead.ReadInt16();
                tmpMonster.Level = biRead.ReadInt16();
                tmpMonster.MaxHp = biRead.ReadInt32();
                tmpMonster.Description = ConvertGetString(biRead.ReadBytes(1024));

                // 16 byte Menu
                tmpMonster.Menu1 = biRead.ReadInt32();
                tmpMonster.Menu2 = biRead.ReadInt32();
                tmpMonster.Menu3 = biRead.ReadInt32();
                tmpMonster.Menu4 = biRead.ReadInt32();

                if (!DicMonsters.ContainsKey((uint) tmpMonster.Id))
                    DicMonsters.Add((uint) tmpMonster.Id, tmpMonster);
                //sStream.Seek(-5, SeekOrigin.End);
                //tmpMonster.MapId = biRead.ReadInt32();
                //tmpMonster.IsActive = biRead.ReadByte() == 1;
                biRead.Dispose();
                sStream.Dispose();
            }
        }

        private void HandleSpell()
        {
            foreach (var vObj in Spell.GetDec())
            {
                var sStream = new MemoryStream(vObj);
                var biRead = new BinaryReader(sStream);
                var id = biRead.ReadInt32();
                if (id == 0) break;
                //var name = ConvertGetString(biRead.ReadBytes(64));
                //var iUnknow = biRead.ReadByte();
                //var side = biRead.ReadByte();
                //var iOccupation = biRead.ReadUInt16();
                //var iLevel = biRead.ReadUInt16();
                //var upgrade = biRead.ReadByte();
                //var iUnknow01 = biRead.ReadByte();
                //var des = ConvertGetString(biRead.ReadBytes(260));
                //var spell = new Spell()
                //    {
                //        ID = (int)id,
                //        Name = name,
                //        Level = iLevel,
                //        JobLevel = upgrade,
                //        Description = des,
                //        Job = iOccupation,
                //        Forces = side
                //    };
                var spell = Activator.CreateInstance<TSpell>();
                spell.Id = id;
                spell.Name = ConvertGetString(biRead.ReadBytes(64)); //68
                biRead.ReadByte(); //69
                spell.Forces = (YulgangGameForce) biRead.ReadByte(); //70
                spell.Job = YulgangConvert.GetJobYbi(biRead.ReadInt16()); //72
                spell.Level = biRead.ReadInt16(); //74
                spell.Upgrade = biRead.ReadByte(); //75
                biRead.ReadByte(); //76
                biRead.ReadByte(); //77
                spell.Description = ConvertGetString(biRead.ReadBytes(256)); //333
                biRead.ReadBytes(3); // 2 byte là số lượng võ công, 1 byte éo biết   336
                spell.IsBook = true;
                DicSpells.Add((int) id, spell);
                for (var i = 0; i < 16; i++) // 400 bytes x 16 = 6400
                {
                    id = biRead.ReadInt32();
                    if (id == 0) break;

                    var spell1 = Activator.CreateInstance<TSpell>();
                    spell1.Id = id;
                    spell1.Name = ConvertGetString(biRead.ReadBytes(64));

                    biRead.ReadByte();
                    spell1.Forces = (YulgangGameForce) biRead.ReadByte();
                    spell1.Job = YulgangConvert.GetJobYbi(biRead.ReadInt16());
                    spell1.Level = biRead.ReadInt16();
                    spell1.Upgrade = biRead.ReadInt16();
                    spell1.ExpNeed = biRead.ReadInt32();
                    spell1.MpNeed = biRead.ReadInt16();
                    spell1.AttackPoint = biRead.ReadInt16();
                    biRead.ReadInt32();
                    spell1.Delay = biRead.ReadInt32();
                    spell1.TimeExist = biRead.ReadInt32();
                    biRead.ReadInt16();
                    spell1.Effect = biRead.ReadInt32();
                    //biRead.ReadInt16();
                    biRead.ReadByte();
                    spell1.Index = biRead.ReadByte();
                    spell1.Description = ConvertGetString(biRead.ReadBytes(256));
                    biRead.ReadBytes(40);
                    DicSpells.Add((int) id, spell1);
                }
                biRead.Dispose();
                sStream.Dispose();
            }
        }

        private void HandleSkill()
        {
            foreach (var vObj in Skill.GetDec())
            {
                var sStream = new MemoryStream(vObj);
                var biRead = new BinaryReader(sStream);


                var id = biRead.ReadInt32();
                if (id == 0)
                {
                    break;
                }
                else
                {
                    var name = ConvertGetString(biRead.ReadBytes(64));
                    var iU01 = biRead.ReadUInt16();
                    var iOccupation = biRead.ReadUInt16();
                    var iLevel = biRead.ReadUInt16();
                    var iUpgrade = biRead.ReadByte();
                    var iU00 = biRead.ReadByte();

                    biRead.ReadBytes(6);
                    var iU02 = biRead.ReadUInt32();
                    biRead.ReadBytes(4);
                    var iU03 = biRead.ReadUInt32();
                    biRead.ReadBytes(4);
                    var iU04 = biRead.ReadUInt32();
                    biRead.ReadBytes(4 + 10 + 32);

                    var mean = ConvertGetString(biRead.ReadBytes(256));
                    var how = ConvertGetString(biRead.ReadBytes(256));
                    var des = ConvertGetString(biRead.ReadBytes(256));
                    var t1 = ConvertGetString(biRead.ReadBytes(256));

                    var skill = Activator.CreateInstance<TSkill>();
                    {
                        skill.Id = id;
                        skill.Name = name;
                        skill.Description = des;
                        skill.Level = iLevel;
                        skill.Job = YulgangConvert.GetJobYbi(iOccupation);
                        skill.Upgrade = iUpgrade;
                    }
                    ;
                    DicSkills.Add(id, skill);
                }
                biRead.Dispose();
                sStream.Dispose();
            }
        }

        private void HandleMAP()
        {
            foreach (var vObj in Map.GetDec())
            {
                var sStream = new MemoryStream(vObj);
                var biRead = new BinaryReader(sStream);

                var id = biRead.ReadUInt32();
                if (id == 0)
                {
                    continue;
                }
                var name = ConvertGetString(biRead.ReadBytes(64));
                var x1 = biRead.ReadSingle();
                biRead.ReadBytes(4);
                var y2 = biRead.ReadSingle();
                var x2 = biRead.ReadSingle();
                biRead.ReadBytes(4);
                var y1 = biRead.ReadSingle();
                var map = Activator.CreateInstance<TMap>();
                map.Id = (int) id;
                map.Name = name;
                map.X1 = x1;
                map.Y1 = y1;
                map.X2 = x2;
                map.Y2 = y2;
                map.MapRectangle = new YulgangMapRectangle()
                {
                    X1 = x1,
                    Y1 = y1,
                    X2 = x2,
                    Y2 = y2
                };

                DicMaps.Add((int) id, map);
                biRead.Dispose();
                sStream.Dispose();
            }
        }

        private void HandleFamous()
        {
            //foreach (var vObj in FAMOUS.GetDec())
            //{
            //    MemoryStream sStream = new MemoryStream(vObj);
            //    BinaryReader biRead = new BinaryReader(sStream);

            //    var obj = new Database.Famous();
            //    obj.Name = ConvertGetString(biRead.ReadBytes(64));
            //    obj.Side = biRead.ReadInt16();
            //    obj.Job = biRead.ReadInt16();
            //    obj.Level = biRead.ReadInt16();
            //    obj.Upgrade = biRead.ReadInt16();
            //    sStream.Seek(64, SeekOrigin.Begin);
            //    obj.ID = biRead.ReadUInt64();

            //    DicFamous.Add(obj.ID, obj);
            //    biRead.Dispose();
            //    sStream.Dispose();
            //}
        }

        //private void SaveItem()
        //{
        //    foreach (var dicSpell in ITEM.GetDictionarydec())
        //    {
        //        IYulgangGameItem obj;
        //        if (DicItems.TryGetValue((int)dicSpell.Key, out obj))
        //        {
        //            MemoryStream stream = new MemoryStream(dicSpell.Value);
        //            BinaryWriter writer = new BinaryWriter(stream);

        //            stream.Position = 0;
        //            writer.Write((long)obj.Id);
        //            byte[] name = BlockEncoding.GetBytes(obj.Name);
        //            if (name.Length >= 64)
        //                writer.Write(name, 0, 64);
        //            else
        //            {
        //                writer.Write(name);
        //                writer.Write(new byte[64 - name.Length]);
        //            }


        //            // 1 byte không biết la ji nên bỏ qua
        //            //stream.Position++;
        //            // writer.Write((byte) 0);
        //            // writer.Seek(2, SeekOrigin.Current);
        //            writer.BaseStream.Position += 1;
        //            writer.Write((byte)obj.Force);
        //            writer.Write((short)obj.Job);
        //            writer.Write((short)obj.Level);
        //            writer.Write((byte)obj.Upgrade);
        //            writer.Write((byte)obj.Sex);
        //            writer.Write((byte)obj.Type);

        //            // byte này củng chả biết là cái ji nên bỏ qua

        //            writer.Seek(1, SeekOrigin.Current);
        //            writer.Write((short)obj.Weight);
        //            writer.Write((short)obj.MaxAttack);
        //            writer.Write((short)obj.MinAttack);
        //            writer.Write((short)obj.Defense);
        //            writer.Write((short)obj.Accuracy);
        //            writer.Write((short)obj.Dodge);
        //            writer.Write((short)obj.MaxDurability);

        //            // 4 byte ko  biết

        //            //stream.Position += 4;
        //            //writer.Write(0);
        //            writer.Seek(4, SeekOrigin.Current);
        //            writer.Write((long)obj.SellMoney);
        //            writer.Write((long)obj.BuyMoney);
        //            writer.Write((byte)obj.Type1);
        //            writer.Write((byte)obj.Type2);
        //            writer.Write((byte)obj.Type3);
        //            writer.Write((byte)obj.Type4);
        //            writer.Write((byte)obj.Type5);

        //            // 35 byte ko biet
        //            //stream.Position += 35;
        //            //writer.Write(new byte[35]);
        //            writer.Seek(35, SeekOrigin.Current);
        //            byte[] dec = BlockEncoding.GetBytes(obj.Description);

        //            if (dec.Length >= 256)
        //                writer.Write(dec, 0, 256);
        //            else
        //            {
        //                writer.Write(dec);
        //                writer.Write(new byte[256 - dec.Length]);
        //            }

        //            // doc 16 byte

        //            //stream.Position += 16;
        //            //writer.Write(new byte[16]);
        //            writer.Seek(16, SeekOrigin.Current);
        //            // 160 byte phan giai
        //            // Phân giải
        //            // Tối đa 10 items
        //            // 8 bytes ID, 8 bytes số lượng
        //            //stream.Position += 160;
        //            writer.Seek(160, SeekOrigin.Current);
        //            //writer.Write(new byte[160]);
        //            // 2 byte cut cho
        //            writer.Write((byte)obj.Type6);
        //            writer.Write((byte)obj.Type7);

        //            // writer.Write(new byte[258]);

        //        }
        //    }
        //}

        //private void SaveSpell()
        //{
        //    foreach (var dicSpell in SPELL.GetDictionarydec())
        //    {
        //        // ghi 1 lan  0 spell

        //        for (int i = 0; i < 16; i++)
        //        {
        //            var index = 0;
        //            if (i == 0) index = 0;
        //            if (i == 1) index = 336;
        //            if (i > 1) index = 336 + 400 * (i - 1);
        //            Spell obj;
        //            if (DicSpells.TryGetValue((int)(dicSpell.Key + i), out obj))
        //            {
        //                byte[] name = BlockEncoding.GetBytes(obj.Name);
        //                Buffer.BlockCopy(name, 0, dicSpell.Value, index + 4, name.Length);
        //                Buffer.BlockCopy(new byte[64 - name.Length], 0, dicSpell.Value, index + 4 + name.Length, 64 - name.Length);// coppy ten

        //                // job
        //                dicSpell.Value[index + 70] = (byte)obj.Job;
        //                dicSpell.Value[index + 72] = (byte)obj.Level;
        //                dicSpell.Value[index + 74] = (byte)obj.JobLevel;
        //                //byte[] dec = BlockEncoding.GetBytes(obj.Description);
        //                //Buffer.BlockCopy(dec, 0, dicSpell.Value, 155, dec.Length);
        //                //Buffer.BlockCopy(new byte[256 - dec.Length], 0, dicSpell.Value, 155 + dec.Length, 256 - dec.Length);

        //            }
        //        }

        //    }
        //}

        //private void SaveMap()
        //{
        //    foreach (var dic in MAP.GetDictionarydec())
        //    {
        //        if (dic.Key == 401)
        //        {
        //            Console.WriteLine("aaa");
        //        }
        //        Map obj;
        //        if (DicMaps.TryGetValue((int)dic.Key, out obj))
        //        {
        //            var name = BlockEncoding.GetBytes(obj.Name);
        //            var db = new byte[64];
        //            Buffer.BlockCopy(name, 0, db, 0, name.Length);
        //            Buffer.BlockCopy(db, 0, dic.Value, 4, 64);
        //        }
        //    }
        //}

        //private void SaveNPC()
        //{
        //    foreach (var monster in NPC.GetDictionarydec())
        //    {
        //        Monster obj;
        //        if (DicMonsters.TryGetValue((uint)monster.Key, out obj))
        //        {
        //            var name = BlockEncoding.GetBytes(obj.Name);
        //            var Namedb = new byte[64];
        //            Buffer.BlockCopy(name, 0, Namedb, 0, name.Length);

        //            var des = BlockEncoding.GetBytes(obj.Description);
        //            var desDB = new byte[1024];
        //            Buffer.BlockCopy(des, 0, desDB, 0, des.Length);

        //            Buffer.BlockCopy(Namedb, 0, monster.Value, 4, 64);
        //            Buffer.BlockCopy(BitConverter.GetBytes(obj.Level), 0, monster.Value, 0x46, 2);
        //            Buffer.BlockCopy(BitConverter.GetBytes(obj.MaxHP), 0, monster.Value, 0x48, 4);
        //            Buffer.BlockCopy(BitConverter.GetBytes(obj.Menu1), 0, monster.Value, 0x44c, 4);
        //            Buffer.BlockCopy(BitConverter.GetBytes(obj.Menu2), 0, monster.Value, 0x44c + 4, 4);
        //            Buffer.BlockCopy(BitConverter.GetBytes(obj.Menu3), 0, monster.Value, 0x44c + 4 + 4, 4);
        //            Buffer.BlockCopy(BitConverter.GetBytes(obj.Menu4), 0, monster.Value, 0x44c + 4 + 4 + 4, 4);
        //            Buffer.BlockCopy(desDB, 0, monster.Value, 76, 1024);
        //            Buffer.BlockCopy(BitConverter.GetBytes(obj.MapId), 0, monster.Value, (int)(monster.Value.Length - 5), 4);
        //            Buffer.BlockCopy(BitConverter.GetBytes(obj.IsActive), 0, monster.Value, (int)(monster.Value.Length - 1), 1);

        //        }
        //    }
        //}

        //private void SaveSkill()
        //{
        //    foreach (var skill in SKILL.GetDictionarydec())
        //    {
        //        Skill obj;
        //        if (DicSkills.TryGetValue((int)skill.Key, out obj))
        //        {
        //            var name = BlockEncoding.GetBytes(obj.Name);
        //            var Namedb = new byte[64];
        //            Buffer.BlockCopy(name, 0, Namedb, 0, name.Length);

        //            var des = BlockEncoding.GetBytes(obj.Description);
        //            var desDB = new byte[256];
        //            Buffer.BlockCopy(des, 0, desDB, 0, des.Length);

        //            Buffer.BlockCopy(Namedb, 0, skill.Value, 4, 64);
        //            Buffer.BlockCopy(desDB, 0, skill.Value, 660, 256);
        //        }
        //    }
        //}
        private string ConvertGetString(IEnumerable<byte> data)
        {
            var stream = new MemoryStream();
            var write = new BinaryWriter(stream);
            foreach (var b in data)
            {
                if (b != 0x00)
                    write.Write((byte) b);
            }
            return BlockEncoding.GetString(stream.ToArray());
        }
    }
}