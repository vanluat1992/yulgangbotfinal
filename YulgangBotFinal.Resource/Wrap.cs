﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace YulgangBotFinal.Resource
{
    [XmlRoot("warp")]
    public class Wrap
    {
        [XmlElement("location")]
        public List<WLocation> WLocations { get; set; }
    }

    [XmlRoot("location")]
    public class WLocation
    {
        [XmlElement("com")]
        public int Id { get; set; }
        [XmlElement("zone")]
        public int Zone { get; set; }
        [XmlElement("x")]
        public int X { get; set; }
        [XmlElement("z")]
        public int Y { get; set; }
    }
}
