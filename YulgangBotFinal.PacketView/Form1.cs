﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YulgangBotFinal.Network.Utils;

namespace YulgangBotFinal.PacketView
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ofn = new OpenFileDialog();
            ofn.InitialDirectory = Environment.CurrentDirectory;
            ofn.Multiselect = false;
            ofn.Filter = $"Packet data |*.data";
            var result = ofn.ShowDialog(this);

            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                try
                {
                    using (var f = new FileStream(ofn.FileName, FileMode.Open))
                    {
                        using (TextReader read = new StreamReader(f))
                        {
                            var data = read.ReadToEnd();

                            var packets = JsonConvert.DeserializeObject<List<PacketData>>(data);
                            listView1.Items.AddRange(packets.Select(m => new ListViewItem(
                                new[]
                                {
                                    m.From.ToString(),
                                    m.OpCode.ToString("X4"),
                                    "0",
                                    BitConverter.ToString(m.Data).Replace("-", "")
                                }) {Tag = m}).ToArray());
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Read file Error : {ex}");
                }
            }
            ofn.Dispose();
            

        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                var p = listView1.SelectedItems[0].Tag as PacketData;
                if (p != null)
                {
                    using (var fView = new ParseView())
                    {
                        fView.SetObject(p);
                        fView.ShowDialog(this);
                    }
                    //MessageBox.Show(BitConverter.ToString(p.Data).Replace("-", ""));
                }
            }
        }
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
        private void _mmOpenWireShark_Click(object sender, EventArgs e)
        {
            using(var ofn=new OpenFileDialog())
            {
                ofn.Filter = "Wireshare export pdml |*.pdml";
                if (ofn.ShowDialog(this)==DialogResult.OK)
                {
                    var ct = new StreamReader(ofn.OpenFile()).ReadToEnd();
                    var doc = XDocument.Parse(ct);
                    var t = JsonConvert.SerializeXNode(doc);
                    var abc = (JObject)JsonConvert.DeserializeObject(t);
                    var listPacket = new List<Tuple<string, string,DateTime>>();
                    var packets = abc["pdml"]["packet"];
                    var tmpDst = "";
                    var tmpSrc = "";
                    var tmpData = "";
                    DateTime time=DateTime.Now;
                    foreach (var p in packets)
                    {
                        var ipDst = "";
                        var ipSrc = "";
                        var data = "";

                        foreach (var pr in p["proto"])
                        {
                            switch (pr["@name"].ToString())
                            {
                                case "fake-field-wrapper":
                                    {
                                        data = pr["field"]["@value"].ToString();
                                        break;
                                    }
                                case "ip":
                                    {
                                        foreach (var f in pr["field"])
                                        {
                                            switch (f["@name"].ToString())
                                            {
                                                case "ip.src":
                                                    ipSrc = f["@show"].ToString();
                                                    break;
                                                case "ip.dst":
                                                    ipDst = f["@show"].ToString();
                                                    break;
                                            }
                                        }
                                        break;
                                    }
                                case "geninfo":
                                    foreach (var f in pr["field"])
                                    {
                                        switch (f["@name"].ToString())
                                        {
                                            case "timestamp":
                                                time = UnixTimeStampToDateTime(double.Parse(f["@value"].ToString()));
                                                break;
                                        }
                                    }
                                    break;

                            }
                        }
                        if (string.IsNullOrEmpty(tmpSrc) && string.IsNullOrEmpty(tmpDst))
                        {
                            tmpSrc = ipSrc;
                            tmpDst = ipDst;
                        }
                        if (tmpDst == ipDst && tmpSrc == ipSrc)
                        {
                            tmpData += data;
                        }
                        else
                        {
                            listPacket.Add(new Tuple<string, string,DateTime>($"{tmpSrc} -> {tmpDst}", tmpData,time));
                            //Console.WriteLine($"{ipSrc} -> {ipDst}  : {tmpData}");
                            tmpData = data;
                        }
                        tmpSrc = ipSrc;
                        tmpDst = ipDst;

                    }
                    listPacket = listPacket.Where(m => !string.IsNullOrEmpty(m.Item2)).ToList();


                    // parse packet
                    var listPacketEx = new List<PacketData>();
                    foreach (var item in listPacket.GroupBy(m=>m.Item1))
                    {
                        using (var m = new MemoryStream())
                        {
                            void Print(byte[] data,DateTime timePacket)
                            {
                                var stream = new MemoryStream(data);
                                var bread = new BinaryReader(stream);
                                while (bread.BaseStream.Position != bread.BaseStream.Length)
                                {
                                    var begin = bread.ReadUInt16();
                                    if (begin != 0x55aa)
                                        throw new Exception();
                                    var len = bread.ReadInt16();
                                    var key = bread.ReadUInt16();
                                    var pdata = bread.ReadBytes(len - 2);
                                    var end = bread.ReadUInt16();
                                    if (end != 0xaa55)
                                    {
                                        var tt = data.FindBeginYulgang((int)bread.BaseStream.Position);
                                        stream.Position = tt;
                                        //throw new Exception();
                                    }
                                    var keyEcrpt = DzoEncrypt.CreateKey(key);
                                    DzoEncrypt.Descrypt(pdata, keyEcrpt);
                                    Debug.WriteLine($"{item.Key} : {BitConverter.ToString(pdata)}");
                                    listPacketEx.Add(new PacketData
                                    {
                                        From = item.Key,
                                        Data = pdata,
                                        OpCode = BitConverter.ToInt16(pdata, 2),
                                        Time=timePacket,
                                        SessionId= BitConverter.ToInt16(pdata, 0)
                                    });
                                }

                                Debug.WriteLine($"===================");
                            }
                            foreach (var it in item.OrderBy(mx=>mx.Item3))
                            {

                                var data =
                                StringToByteArray(it.Item2);
                                if (data[0] == 0xAA && data[1] == 0x55 && data[data.Length - 2] == 0x55 && data[data.Length - 1] == 0xaa)
                                {
                                    Print(data, it.Item3);
                                    m.Position = 0;
                                    m.SetLength(0);
                                }
                                else
                                {
                                    m.Write(data, 0, data.Length);
                                    if(data[data.Length - 2] == 0x55 && data[data.Length - 1] == 0xaa)
                                    {
                                        Print(m.ToArray(),it.Item3);
                                        m.Position = 0;
                                        m.SetLength(0);
                                    }
                                }
                            }
                           
                           
                        }
                        
                        
                    }

                    listPacketEx = listPacketEx.OrderBy(m => m.Time).ToList();
                    listView1.Items.AddRange(listPacketEx.Select(m => new ListViewItem(
                              new[]
                              {
                                    m.From.ToString(),
                                    m.OpCode.ToString("X4"),
                                    "0",
                                    BitConverter.ToString(m.Data).Replace("-", "")
                              })
                    { Tag = m }).ToArray());
                }


            }
        }
    }

    public class PacketData
    {
        public string From { get; set; }
        public int OpCode { get; set; }
        public byte[] Data { get; set; }
        public DateTime Time { get; set; }
        public int SessionId { get; set; }
    }
}
