﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.PacketView
// FileName : ParseView.cs
// Time Create : 4:27 PM 27/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using System.Windows.Forms;

#endregion

namespace YulgangBotFinal.PacketView
{
    public partial class ParseView : Form
    {
        public ParseView()
        {
            InitializeComponent();
        }

        public void SetObject(object obj)
        {
            pView.SelectedObject = obj;
        }
    }
}