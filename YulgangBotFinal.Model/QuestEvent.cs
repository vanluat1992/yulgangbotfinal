﻿namespace YulgangBotFinal.Model
{
    public enum QuestEvent
    {
        Working = 1,
        Accept = 2,
        Cancel = 3,
        Quit = 4,
        Finish = 5
    }
}