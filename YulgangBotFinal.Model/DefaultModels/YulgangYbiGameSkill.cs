﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangYbiGameSkill :IYulgangYbiGameSkill
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public IModels.YulgangGameForce Forces { get; set; }
        public YulgangCharaterJobs Job { get; set; }
        public int Level { get; set; }
        public string Description { get; set; }
        public int Upgrade { get; set; }
    }
}