﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangYbiGameItem :IYulgangYbiGameItem
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public IModels.YulgangGameForce Forces { get; set; }
        public IModels.YulgangGameForce Force { get; set; }
        public YulgangCharaterJobs Job { get; set; }
        public YulgangGameSex Sex { get; set; }
        public int Level { get; set; }
        public int Upgrade { get; set; }
        public int Weight { get; set; }
        public int MaxAttackPoint { get; set; }
        public int MinAttackPoint { get; set; }
        public int Defense { get; set; }
        public int Accuracy { get; set; }
        public int Dodge { get; set; }
        public int MaxDurability { get; set; }
        public long SellMoney { get; set; }
        public long BuyMoney { get; set; }
        public string Description { get; set; }
    }
}