﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangItemDrop :IYulgangItemDrop
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public IModels.YulgangGameForce Forces { get; set; }
        public IYulgangLocation Location { get; set; }
        public IYulgangYbiGameItem BasicInfo { get; set; }

        public int Count { set; get; }
    }
}