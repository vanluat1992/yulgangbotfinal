﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangGuide :IYulgangGuide
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public IModels.YulgangGameForce Forces { get; set; }
        public int Level { get; set; }
    }
}