﻿// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Model
// FILENAME : YulgangPlayerQuest.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 9:05 PM
// **************************************************************************

using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangPlayerQuest:IYulgangPlayerQuest
    {
        public int Id { get; set; }
        public int Step { get; set; }
        public bool IsComplete { get; set; }
    }
}