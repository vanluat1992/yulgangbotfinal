﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangBuffEffect :IYulgangBuffEffect
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int TimeExist { get; set; }
        public bool IsReady { get; set; }
    }
}