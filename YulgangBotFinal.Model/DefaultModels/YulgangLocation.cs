﻿using System;
using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangLocation :IYulgangLocation
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        public int MapId { get; set; }

        public YulgangLocation()
        {
            Z = 15f;
        }
        public YulgangLocation(float x, float y,int mapId)
        {
            X = x;
            Y = y;
            Z = 15f;
            MapId = mapId;
        }
        public float DistanceTo(IYulgangLocation to)
        {
            var distan =
               Math.Sqrt((Math.Pow(X - to.X, 2) +
                          Math.Pow(Y - to.Y, 2)));
            return (float)distan;
        }

        public IYulgangLocation Random()
        {
            var ran = new Random((int) DateTime.Now.Ticks);
            return new YulgangLocation(X + ran.Next(-5, 5), Y + ran.Next(-2, 2), MapId);
        }

        public override string ToString()
        {
            return $"{X},{Y} - {MapId}";
        }
    }
}