﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangItemShopNpc :IYulgangItemShopNpc
    {
        public int Id { get; set; }
        public IYulgangYbiGameItem BasicInfo { get; set; }
        public string Name { get; set; }
        public int Magic1 { get; set; }
        public int Magic2 { get; set; }
        public int Magic3 { get; set; }
        public int Magic4 { get; set; }
    }
}