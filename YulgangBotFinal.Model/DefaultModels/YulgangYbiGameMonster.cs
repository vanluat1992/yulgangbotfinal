﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangYbiGameMonster :IYulgangYbiGameMonster
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public IModels.YulgangGameForce Forces { get; set; }
        public int Level { get; set; }
        public int MaxHp { get; set; }
        public string Description { get; set; }
        public int Menu1 { get; set; }
        public int Menu2 { get; set; }
        public int Menu3 { get; set; }
        public int Menu4 { get; set; }
    }
}