﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangYbiGameMap :IYulgangYbiGameMap
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public IModels.YulgangGameForce Forces { get; set; }
        public float X1 { get; set; }
        public float Y1 { get; set; }
        public float X2 { get; set; }
        public float Y2 { get; set; }
        public IYulgangMapRectangle MapRectangle { get; set; }
    }
}