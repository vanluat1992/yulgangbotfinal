﻿#region header

// /*********************************************************************************************/
// Project :YulgangBotFinal.Model
// FileName : YulgangPlayer.cs
// Time Create : 10:15 AM 16/09/2016
// Author:  Văn Luật (vanluat1992@gmail.com)
// /********************************************************************************************/

#endregion

#region include

using YulgangBotFinal.Model.IModels;

#endregion

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangPlayer : IYulgangPlayer
    {
        public byte Index { get; set; }
        public string Name { get; set; }
        public long Id { get; set; }
        public YulgangGameForce Forces { get; set; }
        public int Level { get; set; }
        public int Upgrade { get; set; }
        public YulgangCharaterJobs Job { get; set; }
        public YulgangGameSex Sex { get; set; }
        public BasicInfo Info { get; set; }
        public int PkPoint { get; set; }
        public int KarmaPoint { get; set; }
        public IYulgangGuide Guide { get; set; }
        public IYulgangLocation Location { set; get; }
        public long Money { get; set; }
        public int CurrentWeight { get; set; }
        public int MaxWeight { get; set; }
        public long CurrentExpNormal { get; set; }
        public long MaxExpNormal { get; set; }
        public int SpellPoint { get; set; }
    }
}