﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangYbiGameSpell :IYulgangYbiGameSpell
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public IModels.YulgangGameForce Forces { get; set; }
        public YulgangCharaterJobs Job { get; set; }
        public int Level { get; set; }
        public int Upgrade { get; set; }
        public int ExpNeed { get; set; }
        public int MpNeed { get; set; }
        public int AttackPoint { get; set; }
        public int Delay { get; set; }
        public int TimeExist { get; set; }
        public int Effect { get; set; }
        public int Index { get; set; }
        public string Description { get; set; }
        public bool IsBook { get; set; }
    }
}