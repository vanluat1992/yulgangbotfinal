﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.DefaultModels
{
    public class YulgangGameItem :IYulgangGameItem
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public IModels.YulgangGameForce Forces { get; set; }
        public int Index { get; set; }
        public IYulgangYbiGameItem BasicInfo { get; set; }
        public long Count { get; set; }
    }
}