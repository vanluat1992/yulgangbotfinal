﻿namespace YulgangBotFinal.Model
{
    public class InfoChannel
    {
        public string Name { set; get; }
        public int Percent { set; get; }
        public int Id { set; get; }
    }
}