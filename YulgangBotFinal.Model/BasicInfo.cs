﻿namespace YulgangBotFinal.Model
{
    public class BasicInfo
    {
        public virtual int CurrentHp { set; get; }
        public virtual int MaxHp { set; get; }
        public virtual int CurrentMp { set; get; }
        public virtual int MaxMp { set; get; }
        public virtual int CurrentSp { set; get; }
        public virtual int MaxSp { set; get; }
    }
}