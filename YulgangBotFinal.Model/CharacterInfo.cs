﻿namespace YulgangBotFinal.Model
{
    public class CharacterInfo
    {
        public virtual string Name { set; get; }
        public virtual int Level { set; get; }
        public virtual BasicInfo Basic { set; get; }
    }
}