﻿using System.Collections.Generic;

namespace YulgangBotFinal.Model
{
    public class InfoGroupServer
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public Dictionary<int,InfoChannel> Chanels { set; get; }

        public InfoGroupServer()
        {
            Chanels=new Dictionary<int, InfoChannel>();
        }
    }
}