﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangNpc : IYulgangGameObject
    {
        IYulgangYbiGameMonster BasicInfo { set; get; }
        int Level { set; get; }
        int CurrentHp { set; get; }
        int MaxHp { set; get; }
        IYulgangLocation Location { set; get; }
    }
}