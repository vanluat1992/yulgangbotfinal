﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangPlayer : IYulgangGameObject
    {
        int Level { set; get; }
        int Upgrade { set; get; }
        YulgangCharaterJobs Job { set; get; }
        YulgangGameSex Sex { set; get; }
        BasicInfo Info { set; get; }
        int PkPoint { set; get; }
        int KarmaPoint { set; get; }
        IYulgangGuide Guide { set; get; }
        IYulgangLocation Location { set; get; }
        long Money { set; get; }
        int CurrentWeight { set; get; }
        int MaxWeight { set; get; }
        long CurrentExpNormal { set; get; }
        long MaxExpNormal { set; get; }
        int SpellPoint { set; get; }
    }
}