﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangYbiGameMap : IYulgangGameObject
    {
        float X1 { get; set; }

        float Y1 { get; set; }

        float X2 { get; set; }

        float Y2 { get; set; }

        IYulgangMapRectangle MapRectangle { set; get; }
    }
}