﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangYbiGameMonster : IYulgangGameObject
    {
        int Level { set; get; }

        int MaxHp { set; get; }

        string Description { set; get; }

        int Menu1 { set; get; }

        int Menu2 { set; get; }

        int Menu3 { set; get; }

        int Menu4 { set; get; }
    }
}