﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangMapRectangle
    {
        float X1 { set; get; }
        float Y1 { set; get; }
        float X2 { set; get; }
        float Y2 { set; get; }
        float Width {  get; }
        float Height {  get; } 
    }
}