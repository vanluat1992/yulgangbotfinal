﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangItemDrop :IYulgangGameObject
    {
        IYulgangLocation Location { set; get; } 
        IYulgangYbiGameItem BasicInfo { set; get; }
        int Count { set; get; }
    }
}