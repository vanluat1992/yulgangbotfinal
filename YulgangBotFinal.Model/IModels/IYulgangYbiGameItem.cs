﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangYbiGameItem : IYulgangGameObject
    {
        YulgangGameForce Force { set; get; }

        YulgangCharaterJobs Job { set; get; }

        YulgangGameSex Sex { set; get; }

        int Level { set; get; }

        int Upgrade { set; get; }

        int Weight { set; get; }

        int MaxAttackPoint { set; get; }

        int MinAttackPoint { set; get; }

        int Defense { set; get; }

        int Accuracy { set; get; }

        int Dodge { set; get; }

        int MaxDurability { set; get; }

        long SellMoney { set; get; }

        long BuyMoney { set; get; }

        string Description { set; get; }
    }
}