﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangGuide :IYulgangGameObject
    {
         int Level { set; get; }
    }
}