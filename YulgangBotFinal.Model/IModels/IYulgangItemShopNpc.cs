﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangItemShopNpc 
    {
        int Id { set; get; }
        IYulgangYbiGameItem BasicInfo { set; get; }
        string Name { set; get; }
        int Magic1 { set; get; } 
        int Magic2 { set; get; } 
        int Magic3 { set; get; } 
        int Magic4 { set; get; } 
    }
}