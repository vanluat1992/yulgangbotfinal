﻿// **************************************************************************
// SOLUTION : YulgangBotFinal
// PROJECT : YulgangBotFinal.Model
// FILENAME : IYulgangPlayerQuest.cs
// AUTHOR : Nguyen Van Luat
// CREATE DATE : 29/03/2017 9:05 PM
// **************************************************************************
namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangPlayerQuest
    {
        int Id { get; set; }
        int Step { get; set; }
        bool IsComplete { get; set; }
    }
}