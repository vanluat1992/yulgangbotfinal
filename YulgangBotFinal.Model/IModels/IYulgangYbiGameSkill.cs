﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangYbiGameSkill : IYulgangGameObject
    {
        YulgangCharaterJobs Job { set; get; }

        int Level { set; get; }

        string Description { set; get; }

        int Upgrade { set; get; }
    }
}