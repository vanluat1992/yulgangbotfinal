﻿using System.CodeDom;

namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangLocation
    {
        float X { set; get; }
        float Y { set; get; }
        float Z { set; get; }
        int MapId { set; get; }
        float DistanceTo(IYulgangLocation to);
        IYulgangLocation Random();
    }
}