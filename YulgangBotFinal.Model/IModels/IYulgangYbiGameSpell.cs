﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangYbiGameSpell : IYulgangGameObject
    {
        YulgangCharaterJobs Job { set; get; }

        int Level { set; get; }

        int Upgrade { set; get; }

        int ExpNeed { set; get; }

        int MpNeed { set; get; }

        int AttackPoint { set; get; }

        int Delay { set; get; }

        int TimeExist { set; get; }

        int Effect { set; get; }

        int Index { set; get; }

        string Description { set; get; }

        bool IsBook { set; get; }
    }
}