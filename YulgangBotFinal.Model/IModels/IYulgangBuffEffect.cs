﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangBuffEffect
    {
        int Id { set; get; }
        string Description { set; get; }
        int TimeExist { set; get; }
        bool IsReady { set; get; } 
    }
}