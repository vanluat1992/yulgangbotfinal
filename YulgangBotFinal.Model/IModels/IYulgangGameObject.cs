﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangGameObject
    {
        string Name { set; get; }
        long Id { set; get; }
        YulgangGameForce Forces { set; get; }
    }
}