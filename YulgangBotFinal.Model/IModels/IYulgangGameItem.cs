﻿namespace YulgangBotFinal.Model.IModels
{
    public interface IYulgangGameItem :IYulgangGameObject
    {
         int Index { set; get; }
        IYulgangYbiGameItem BasicInfo { set; get; }
        long Count { set; get; }
    }
}