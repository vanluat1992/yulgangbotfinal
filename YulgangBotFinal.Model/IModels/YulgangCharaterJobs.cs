﻿namespace YulgangBotFinal.Model.IModels
{
    public enum YulgangCharaterJobs
    {
        None,
        Blade,
        Sword,
        Spear,
        Bow,
        Healer,
        Ninja,
        Busker,
        SuperBlade,
        SuperSword,
        Boxing
    }
}