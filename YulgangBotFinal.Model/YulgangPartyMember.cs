﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model
{
    public class YulgangPartyMember
    {
        public long Id { set; get; }
        public string Name { set; get; }
        public int CurrentHp { set; get; }
        public int CurrentMp { set; get; }
        public int MaxHp { set; get; }
        public int MaxMp { set; get; }
        public YulgangCharaterJobs Job { set; get; }
    }
}