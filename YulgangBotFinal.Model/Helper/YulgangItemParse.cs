﻿using System.Diagnostics;
using System.IO;
using YulgangBotFinal.Model.DefaultModels;
using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.Helper
{
    public static class YulgangItemParse
    {
        public static IYulgangItemDrop GetItemDrop(this byte[] data)
        {
            var stream = new MemoryStream(data);
            var bread = new BinaryReader(stream);
            var session = bread.ReadInt64();
            var id = bread.ReadInt64();
            var count = bread.ReadInt32();
            var x = bread.ReadSingle();
            var z = bread.ReadSingle();
            var y = bread.ReadSingle();

            var magic0 = bread.ReadInt32();
            var magic1 = bread.ReadInt32();
            var magic2 = bread.ReadInt32();
            var magic3 = bread.ReadInt32();
            var magic4 = bread.ReadInt32();
            var result = new YulgangItemDrop()
            {
                Id =  session,
                Count = count,
                Location = new DefaultModels.YulgangLocation {X = x, Z = z, Y = y},
                BasicInfo = new YulgangYbiGameItem() {Id = (int) id}
            };
            return result;
        }

        public static IYulgangGameItem GetGameItem(this byte[] data)
        {
            if (data == null)
                Debug.WriteLine("GetItem Null");
            var stream = new MemoryStream(data);
            var bread = new BinaryReader(stream);
            var id = bread.ReadInt64();
            var itemId = bread.ReadInt64();
            var count = bread.ReadInt32();

            var result = new YulgangGameItem()
            {
                Id = id,
                Count = count,
                BasicInfo = new YulgangYbiGameItem()
                {
                    Id = itemId
                }
            };
            return result;
        }
    }
}