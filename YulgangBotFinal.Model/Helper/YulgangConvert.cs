﻿using YulgangBotFinal.Model.IModels;

namespace YulgangBotFinal.Model.Helper
{
    public static class YulgangConvert
    {
        public static YulgangCharaterJobs GetJobYbi(this int val)
        {
            return YulgangCharaterJobs.Blade;
        }

        public static YulgangCharaterJobs GetJobGame(this int val)
        {
            return (YulgangCharaterJobs) val;
        }
    }
}