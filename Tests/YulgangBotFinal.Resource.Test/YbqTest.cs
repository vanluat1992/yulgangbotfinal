﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YulgangBotFinal.Resource.Ybq;

namespace YulgangBotFinal.Resource.Test
{
    [TestClass]
    public class YbqTest
    {
        [TestMethod]
        public void ReadQuest()
        {
           var tmp= YbqUtils.ReadQuest("Resource\\Ybq.cfg");
            foreach (var quest in tmp)
            {
                Debug.WriteLine($"{quest.Key}: {quest.Value.NameQuest.Data}");
            }
        }

        [TestMethod]
        public void ReadWrap()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Wrap));
            using (StringReader stream = new StringReader(File.OpenText("Resource\\map.xl").ReadToEnd()))
            {
                var t = xs.Deserialize(stream);
            }
        }
    }
}
